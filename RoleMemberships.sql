﻿/*
This script would be use to create database users and add the user in to corresponding roles as per metdadata define in this script.
Add service account or group without domain and environment prifix to script below which is being use to add record in table #RoleMemebrship.
Role membership of all service accounts should be same for all environment and for all tenants.
For role membership of the group which being use to provision access to employees, if group need to be added in specific role for the tenant or environment, add condition logic during th insert for those.

*/
SET NOCOUNT ON;

/***********************************************
CREATE TABLE
***********************************************/
IF OBJECT_ID('tempdb.dbo.#RoleMemebrship') IS NOT NULL
       DROP TABLE #RoleMemebrship;

CREATE TABLE #RoleMemebrship
(
    Id INT IDENTITY(1, 1)
   ,UserName sysname NOT NULL
   ,type CHAR(1) NOT NULL
   ,RoleName sysname NOT NULL
   ,CHECK  (type IN ('U','S','G'))
   ,PRIMARY KEY CLUSTERED (type,UserName,RoleName)
);

INSERT INTO #RoleMemebrship ( type, RoleName, UserName)
VALUES ('G','Access Development','SQL-Development-SLG');;

/**************************************************
-- Call centralized stored procedure which is being use to apply rolememberships.
***************************************************/

EXEC DBA.API.Security_ApplyRoleMemberships @DbName=[$(DatabaseName)],@CreateLoginNotInTarget=0,@DropUserNotInSource=1,@DropRoleMembershipNotInSource=1;

SET NOCOUNT OFF;
GO

