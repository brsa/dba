﻿CREATE TABLE [ObjectInventory].[Temp_Dependency] (
    [ID]                       INT           IDENTITY (1, 1) NOT NULL,
    [LogDate]                  DATE          NOT NULL,
    [DatabaseName]             VARCHAR (100) NOT NULL,
    [SchemaName]               VARCHAR (30)  NOT NULL,
    [ObjectName]               VARCHAR (100) NOT NULL,
    [ReferencingObjectType]    VARCHAR (60)  NULL,
    [Referenced_server_name]   VARCHAR (50)  NULL,
    [Referenced_database_name] VARCHAR (100) NULL,
    [referenced_schema_name]   VARCHAR (30)  NULL,
    [referenced_entity_name]   VARCHAR (100) NULL,
    [ReferencedObjectType]     NVARCHAR (60) NULL,
    [base_object_name]         VARCHAR (200) NULL,
    [ReferencedEntity]         AS            (((((([Referenced_server_name]+case when [Referenced_server_name]<>'' then '.' else '' end)+[Referenced_database_name])+'.')+[referenced_schema_name])+'.')+[referenced_entity_name]),
    [ReferencingEntity]        AS            (((([DatabaseName]+'.')+[SchemaName])+'.')+[ObjectName]),
    CONSTRAINT [PK_ObjectInventory_Temp_Dependency] PRIMARY KEY CLUSTERED ([ID] ASC) ON [DBDATA]
);

