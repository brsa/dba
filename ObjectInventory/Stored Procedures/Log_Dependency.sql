﻿/*****************************************************************************
AUTHOR : AJAY H.
CREATE DATE : 03/21/2016
DESCRIPTION: Collect database stats
SAMPLE CALL:EXEC ObjectInventory.Log_Dependency;

**HISTORY**
-----------------------------------------------------------------------------------
Update Date    Updated By          Notes
-----------------------------------------------------------------------------------		 
										

******************************************************************************/
CREATE PROC [ObjectInventory].[Log_Dependency]
@Debug BIT=0
AS
BEGIN
SET NOCOUNT ON;

--Aggressively clean out temp table of everything except the most recent
--WHILE @@ROWCOUNT <> 0
--	DELETE TOP (1000) dbo.Monitor_Temp_Dependency;
TRUNCATE TABLE ObjectInventory.Temp_Dependency;

EXEC dbo.sp_foreachdb @command = N'USE ? ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	DECLARE @LogDateTime DATETIME2(0)=SYSDATETIME()
			,@DatabaseName VARCHAR(100)=DB_NAME();
	INSERT INTO DBA.ObjectInventory.Temp_Dependency 
	        ( LogDate ,DatabaseName ,SchemaName ,ObjectName ,ReferencingObjectType 
			  ,Referenced_server_name ,Referenced_database_name ,referenced_schema_name ,referenced_entity_name ,ReferencedObjectType, base_object_name 	          
	        )
	SELECT	DISTINCT
			@LogDateTime AS LogDate ,
			@DatabaseName AS DatabaseName,
			s.name AS SchemaName ,
			OBJECT_NAME(referencing_id) AS ObjectName ,
			o.type_desc AS ReferencingObjectType,
			CASE WHEN referenced_server_name IS NULL OR referenced_server_name='''' THEN ''''  
				 ELSE referenced_server_name END  AS referenced_server_name ,
			CASE WHEN referenced_database_name IS NULL OR referenced_database_name='''' THEN @DatabaseName 
			     ELSE referenced_database_name END  AS referenced_database_name ,
			CASE WHEN referenced_schema_name IS NULL OR referenced_schema_name='''' THEN ''dbo'' 
			     ELSE referenced_schema_name END AS referenced_schema_name ,
			referenced_entity_name ,
			o2.type_desc AS ReferencedObjectType ,
			REPLACE(REPLACE(base_object_name,''['', ''''), '']'', '''') AS base_object_name			
	FROM    sys.sql_expression_dependencies sed 
			LEFT JOIN sys.objects o ON sed.referencing_id = o.object_id
												AND o.type_desc <> ''CHECK_CONSTRAINT''
			LEFT JOIN sys.schemas s ON s.schema_id = o.schema_id
			LEFT JOIN sys.objects o2 ON sed.referenced_id = o2.object_id
													AND o2.type_desc <> ''CHECK_CONSTRAINT''
			LEFT JOIN sys.synonyms syn ON sed.referenced_id = syn.object_id
	WHERE   s.name IS NOT NULL
			AND referenced_entity_name <> ''sysdiagrams''
			AND OBJECT_NAME(referencing_id) NOT LIKE ''%_DEVDB'';'                    -- nvarchar(max)
        , @print_dbname = @Debug               -- bit
        , @print_command_only = @Debug         -- bit
        , @user_only = 1                  -- bit
        , @exclude_list = N'master,msdb,model,tempdb,ReportServer,ReportServerTempDB';

END;