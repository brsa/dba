/**********************************************************************
SMcKenna
Date: 11/27/2019
Description: Logs SSRS report run performance metrics for the previous month.


EXEC usp_SSRS_PreviousMonthExecLogArchive
**********************************************************************/
CREATE PROC usp_SSRS_PreviousMonthExecLogArchive
AS
BEGIN
    DECLARE @nvchtablename NVARCHAR(255),
            @strsql NVARCHAR(MAX),
            @dtStartDate DATETIME,
            @dtEndDate DATETIME;

    SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0);
    SET @dtEndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()) - 1, -1);
    SET @nvchtablename
        = N'SSRSExecLog' + CONVERT(NVARCHAR(4), YEAR(@dtStartDate)) + CONVERT(NVARCHAR(4), MONTH(@dtStartDate));
    SELECT @nvchtablename,
           @dtEndDate,
           @dtStartDate;

    SET @strsql
        = N'IF NOT EXISTS (select * from sysobjects where name=''' + @nvchtablename
          + N''' AND xtype=''U''     ) BEGIN SELECT * INTO DBA.dbo.' + @nvchtablename
          + N' 
FROM [ReportServer].dbo.ExecutionLog3
WHERE [TimeStart] > ''' + CONVERT(NVARCHAR(100), @dtStartDate) + N'''
AND [TimeStart] <  ''' + CONVERT(NVARCHAR(100), @dtEndDate) + N''' END';

    EXEC (@strsql);


END;
