﻿CREATE TABLE [dbo].[Monitor_Temp_Dependencies] (
    [ID]                              INT           IDENTITY (1, 1) NOT NULL,
    [LogDate]                         DATE          NOT NULL,
    [ServerName]                      VARCHAR (128)  NOT NULL,
    [DatabaseName]                    VARCHAR (128) NOT NULL,
    [SchemaName]                      VARCHAR (128)  NOT NULL,
    [ObjectName]                      VARCHAR (128) NOT NULL,
    [ReferencingObjectType]           VARCHAR (128)  NULL,
    [Referenced_server_name]          VARCHAR (128)  NULL,
    [Referenced_database_name]        VARCHAR (128) NULL,
    [referenced_schema_name]          VARCHAR (128)  NULL,
    [referenced_entity_name]          VARCHAR (128) NULL,
    [ReferencedObjectType]            VARCHAR (128) NULL,
    [base_object_name]                VARCHAR (200) NULL,
    [ReferencingEntity]               AS            (((((([ServerName]+'.')+[DatabaseName])+'.')+[SchemaName])+'.')+[ObjectName]),
    [ReferencedEntity]                AS            (((((([Referenced_server_name]+'.')+[Referenced_database_name])+'.')+[referenced_schema_name])+'.')+[referenced_entity_name]),
    [external_referenced_object_name] AS            (case when (len([base_object_name])-len(replace([base_object_name],'.','')))=(2) then [ServerName]+'.' else '' end+[base_object_name]),
    CONSTRAINT [PK_Monitor_Temp_Dependencies] PRIMARY KEY CLUSTERED ([ID] ASC)
);

