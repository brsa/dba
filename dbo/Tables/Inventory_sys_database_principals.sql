﻿CREATE TABLE dbo.Inventory_sys_database_principals (
	ID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_sys_database_principals_ID PRIMARY KEY CLUSTERED (ID),
	ServerName NVARCHAR(128) NOT NULL,
	AGName NVARCHAR(128) NULL,
	DatabaseName NVARCHAR(128) NOT NULL,
	name SYSNAME NOT NULL,
	principal_id INT NOT NULL,
	type_desc NVARCHAR(60) NULL,
	default_schema_name SYSNAME NULL,
	owning_principal_id INT NULL,
	sid VARBINARY(85) NULL,
	is_fixed_role BIT NOT NULL,
	authentication_type_desc NVARCHAR(60) NULL,
	default_language_name SYSNAME NULL,
	default_language_lcid INT NULL,
	DateAdded DATETIME2(0) NOT NULL,
	DateRemoved DATETIME2(0) NULL,
	Active BIT NOT NULL
	)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_database_principals_DateAdded
	ON dbo.Inventory_sys_database_principals (DateAdded)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_database_principals_DateRemoved_Active
	ON dbo.Inventory_sys_database_principals (DateRemoved)
	WHERE Active = 0
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_database_principals_DatabaseName
	ON dbo.Inventory_sys_database_principals (DatabaseName)
	WITH (DATA_COMPRESSION = PAGE);
GO