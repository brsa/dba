﻿CREATE TABLE [dbo].[NotificationList] (
    [NotificationListName] VARCHAR (500)  NOT NULL,
    [EmailList]            VARCHAR (8000) NOT NULL,
    CONSTRAINT [PK_NotificationList] PRIMARY KEY CLUSTERED ([NotificationListName] ASC) ON [PRIMARY],
    CONSTRAINT [CHK_ListName] CHECK ([NotificationListName] like 'Notify_%')
) ON [PRIMARY];

