﻿
CREATE TABLE [dbo].[Monitor_AgStatus] (
    [LogDateTime]      SMALLDATETIME NOT NULL,
    [ServerName]       VARCHAR (50)  NOT NULL,
    [DBName]           VARCHAR (50)  NOT NULL,
    [AgRole]           NVARCHAR (60) NULL,
    [SynchState]       NVARCHAR (60) NULL,
    [SynchHardenedLSN] NUMERIC (25)  NULL,
    CONSTRAINT [PK_cfn_Monitor_AgStatus] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [ServerName] ASC, [DBName] ASC) WITH (DATA_COMPRESSION = PAGE)
);


