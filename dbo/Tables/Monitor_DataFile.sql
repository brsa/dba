﻿CREATE TABLE dbo.Monitor_DataFile(
	 LogDateTime      DATETIME2(0) NOT NULL,
     DbName           NVARCHAR(128), 
     LogicalFileName  NVARCHAR(128), 
     FileType         NVARCHAR(10),
     FileSizeMB       INT, 
     SpaceUsedMB      INT, 
     FreeSpaceMB      INT, 
     GrowthAmount     VARCHAR(20), 
     PhysicalFileName NVARCHAR(520)
 CONSTRAINT PK_Monitor_DataFile PRIMARY KEY CLUSTERED 
	(LogDateTime, DbName, LogicalFileName)WITH (DATA_COMPRESSION = PAGE) ON [DBDATA]
) ON [DBDATA]