﻿CREATE TABLE dbo.CFNRoles (
	ID INT NOT NULL IDENTITY,
	Environment NVARCHAR(4000) NOT NULL,
	DatabaseName NVARCHAR(4000) NULL,
	RoleName NVARCHAR(128) NOT NULL,
	PermissionState NVARCHAR(60) NOT NULL,
	PermissionType NVARCHAR(4000) NOT NULL,
	ObjectType NVARCHAR(60) NOT NULL,
	SchemaName NVARCHAR(128) NULL,
	ObjectName NVARCHAR(128) NULL,
	ColumnName NVARCHAR(128) NULL, 
    CONSTRAINT [PK_CFNRoles] PRIMARY KEY ([ID])
)
