﻿CREATE TABLE [dbo].[cfn_ServiceBrokerLog] (
    [LogId]           INT            IDENTITY (1, 1) NOT NULL,
    [MessageDateTime] DATETIME2 (0)  DEFAULT (getdate()) NULL,
    [ServiceName]     NVARCHAR (512) NULL,
    [QueueName]       NVARCHAR (256) NULL,
    [MessageType]     NVARCHAR (256) NULL,
    [ErrorNumber]     INT            NULL,
    [ErrorMessage]    NVARCHAR (MAX) NULL,
    [SbMessage]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_cfn_ServiceBrokerLog] PRIMARY KEY CLUSTERED ([LogId] ASC) WITH (DATA_COMPRESSION = PAGE)
);

