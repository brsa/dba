﻿CREATE TABLE [dbo].[Monitor_OpenTransaction] (
    [LogId]            INT             IDENTITY (1, 1) NOT NULL,
    [LogDateTime]      DATETIME2 (0)   DEFAULT (getdate()) NOT NULL,
    [SessionId]        SMALLINT        NULL,
    [DbName]           [sysname]       NOT NULL,
    [HostName]         NVARCHAR (128)  NULL,
    [ProgramName]      NVARCHAR (128)  NULL,
    [LoginName]        NVARCHAR (128)  NULL,
    [LoginTime]        DATETIME2 (3)   NULL,
    [LastRequestStart] DATETIME2 (3)   NULL,
    [LastRequestEnd]   DATETIME2 (3)   NULL,
    [TransactionCnt]   INT             NULL,
    [TransactionStart] DATETIME2 (3)   NULL,
    [TransactionState] TINYINT         NULL,
    [Command]          NVARCHAR (32)   NULL,
    [WaitTime]         INT             NULL,
    [WaitResource]     NVARCHAR (256)  NULL,
    [SqlText]          NVARCHAR (MAX)  NULL,
    [InputBuffer]      NVARCHAR (4000) NULL,
    [SqlStatement]     NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_cfn_Monitor_OpenTransaction] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [LogId] ASC) WITH (DATA_COMPRESSION = PAGE)
);






