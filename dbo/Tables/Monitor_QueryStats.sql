﻿CREATE TABLE [dbo].[Monitor_QueryStats] (
    [LogDateTime]        DATETIME2 (0) NOT NULL,
    [DbName]             [sysname]     NOT NULL,
    [SchemaName]         [sysname]     NOT NULL,
    [ObjectName]         VARCHAR (255) NOT NULL,
    [LineNumber]         INT           NOT NULL,
    [TextLine]           VARCHAR (500) NOT NULL,
    [IntervalMinutes]    INT           NULL,
    [NumCalls]           BIGINT        NULL,
    [TotalPhysicalReads] BIGINT        NULL,
    [TotalLogicalReads]  BIGINT        NULL,
    [TotalLogicalWrites] BIGINT        NULL,
    [TotalWorkerTime]    BIGINT        NULL,
    [TotalClrTime]       BIGINT        NULL,
    [TotalElapsedTime]   BIGINT        NULL,
    CONSTRAINT [PK_Monitor_QueryStats] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [DbName] ASC, [SchemaName] ASC, [ObjectName] ASC, [LineNumber] ASC) WITH (DATA_COMPRESSION = PAGE)
);

 


