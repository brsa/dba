﻿CREATE TABLE [dbo].[Monitor_Temp_TableDetails] (
    [LogDateTime]       DATETIME2 (0) CONSTRAINT [DF_Monitor_Temp_TableDetails_LogDateTime] DEFAULT (sysdatetime()) NOT NULL,
    [DatabaseName]      [sysname]     NOT NULL,
    [SchemaName]        [sysname]     NOT NULL,
    [Table_Name]        [sysname]     NOT NULL,
    [Column_Id]         INT           NULL,
    [Column_Name]       [sysname]     NOT NULL,
    [System_Data_Type]  [sysname]     NULL,
    [Max_Length]        INT           NULL,
    [Is_Nullable]       BIT           NULL,
    [Is_identity]       BIT           NULL,
    [Last_Value]        SQL_VARIANT   NULL,
    [Max_Value]         BIGINT        NULL,
    [Object_Id]         INT           NULL,
    [Extended_Property] SQL_VARIANT   NULL,
    CONSTRAINT [PK_Monitor_Temp_TableDetails] PRIMARY KEY CLUSTERED ([DatabaseName] ASC, [SchemaName] ASC, [Table_Name] ASC, [Column_Name] ASC)
);

