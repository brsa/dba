﻿CREATE TABLE [dbo].[ObjectExceptions] (
    [DatabaseName] VARCHAR (100) NOT NULL,
    [ObjectName]   VARCHAR (200) NOT NULL,
    [ObjectType]   VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_ObjectExceptions] PRIMARY KEY CLUSTERED ([DatabaseName] ASC, [ObjectName] ASC, [ObjectType] ASC) ON [PRIMARY]
);

