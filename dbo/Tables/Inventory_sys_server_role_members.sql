﻿CREATE TABLE dbo.Inventory_sys_server_role_members (
	ID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_sys_server_role_members_ID PRIMARY KEY CLUSTERED (ID),
	ServerName NVARCHAR(128) NOT NULL,
	role_principal_id INT NOT NULL,
	member_principal_id INT NOT NULL,
	DateAdded DATETIME2(0) NOT NULL,
	DateRemoved DATETIME2(0) NULL,
	Active BIT NOT NULL
	)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_server_role_members_DateAdded
	ON dbo.Inventory_sys_server_role_members (DateAdded)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_server_role_members_DateRemoved_Active
	ON dbo.Inventory_sys_server_role_members (DateRemoved)
	WHERE Active = 0
	WITH (DATA_COMPRESSION = PAGE);
GO