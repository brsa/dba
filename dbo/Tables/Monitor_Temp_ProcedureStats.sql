﻿CREATE TABLE dbo.Monitor_Temp_ProcedureStats (
    LogDateTime        datetime2(0)  NOT NULL,
    DbName             sysname       NOT NULL,
    SchemaName         sysname       NOT NULL,
    ObjectName         varchar (255) NOT NULL,
    PlanId             int           NOT NULL,
    SqlHandle          varbinary(64),
    PlanHandle         varbinary(64),
    CachedTime         datetime2(3),
    ExecutionCount     bigint,
    TotalWorkerTime    bigint,
    TotalLogicalWrites bigint,
    TotalLogicalReads  bigint,
    TotalElapsedTime   bigint
    CONSTRAINT PK_Monitor_Temp_ProcedureStats PRIMARY KEY CLUSTERED (LogDateTime, DbName, SchemaName, ObjectName, PlanId) WITH (DATA_COMPRESSION = PAGE)
);
GO

CREATE NONCLUSTERED INDEX IX_temp_ProcStats_Agg 
		ON dbo.Monitor_Temp_ProcedureStats (DbName, SchemaName, ObjectName, LogDateTime) WITH (DATA_COMPRESSION = PAGE);
