﻿CREATE TABLE dbo.Monitor_Temp_SpinlockStats (
    LogDateTime        datetime2 (0) NOT NULL,
    SpinlockName       nvarchar(60) NOT NULL,
    Collisions         bigint,
    Spins              bigint,
    SleepTime          bigint,
	Backoffs           BIGINT,
    CONSTRAINT PK_Monitor_temp_SpinlockStats PRIMARY KEY CLUSTERED (LogDateTime, SpinlockName) WITH (DATA_COMPRESSION = PAGE)
);


GO