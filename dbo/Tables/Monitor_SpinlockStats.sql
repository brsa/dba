﻿CREATE TABLE [dbo].[Monitor_SpinlockStats] (
    [LogDateTime]       DATETIME2 (0) NOT NULL,
    [SpinlockName]      NVARCHAR (60) NOT NULL,
    [IntervalMinutes]   INT           NULL,
    [Collisions]        BIGINT        NULL,
    [Spins]             BIGINT        NULL,
    [SpinsPerCollision] AS            (case when [Collisions]=(0) then (0) else CONVERT([real],([Spins]*(1.0))/[Collisions]) end),
    [SleepTime]         BIGINT        NULL,
    [Backoffs]          BIGINT        NULL,
    CONSTRAINT [PK_Monitor_SpinlockStats] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [SpinlockName] ASC) WITH (DATA_COMPRESSION = PAGE)
);


