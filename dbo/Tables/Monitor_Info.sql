﻿CREATE TABLE [dbo].[Monitor_Info] (
    [ServerName]   [sysname]     NOT NULL,
    [LogDateTime]  DATETIME2 (0) NOT NULL,
    [FacetName]    NVARCHAR (70) NOT NULL,
    [FacetType]    VARCHAR (32)  NULL,
    [Value]        SQL_VARIANT   NULL,
    [ValueVarchar] AS            (CONVERT([varchar](8000),[Value])),
    CONSTRAINT [PK_cfn_Monitor_Info] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [ServerName] ASC, [FacetName] ASC) WITH (DATA_COMPRESSION = PAGE)
);



GO

CREATE NONCLUSTERED INDEX [IX_Monitor_Info_Server_Facet]
    ON [dbo].[Monitor_Info]([ServerName] ASC, [FacetType] ASC, [FacetName] ASC)
    INCLUDE([Value]) WITH (DATA_COMPRESSION = PAGE);


GO

