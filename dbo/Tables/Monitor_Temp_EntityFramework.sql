﻿CREATE TABLE [dbo].[Monitor_Temp_EntityFramework] (
    [ID]                    INT                IDENTITY (1, 1) NOT NULL,
    [LogDateTime]           DATETIME2 (2)      NOT NULL,
    [EventName]             VARCHAR (500)      NULL,
    [EventTime]             DATETIMEOFFSET (7) NULL,
    [EventTimeUTC]          DATETIMEOFFSET (7) NOT NULL,
    [cpu_time]              DECIMAL (20)       NULL,
    [duration]              DECIMAL (20)       NULL,
    [physical_reads]        DECIMAL (20)       NULL,
    [logical_reads]         DECIMAL (20)       NULL,
    [writes]                DECIMAL (20)       NULL,
    [result]                VARCHAR (MAX)      NULL,
    [object_name]           VARCHAR (500)      NULL,
    [statement]             VARCHAR (MAX)      NULL,
    [output_parameters]     VARCHAR (MAX)      NULL,
    [username]              VARCHAR (200)      NULL,
    [sql_text]              VARCHAR (MAX)      NULL,
    [server_principal_name] VARCHAR (200)      NULL,
    [server_instance_name]  VARCHAR (100)      NULL,
    [nt_username]           VARCHAR (200)      NULL,
    [database_name]         VARCHAR (200)      NULL,
    [client_hostname]       VARCHAR (100)      NULL,
    [client_app_name]       VARCHAR (200)      NULL,
    [task_time]             DECIMAL (20)       NULL,
    [attach_activity_id]    VARCHAR (200)      NULL,
    [state]                 VARCHAR (MAX)      NULL,
    [row_count]             INT                NULL,
    [last_row_count]        INT                NULL,
    CONSTRAINT [PK_Monitor_temp_EntityFramework] PRIMARY KEY CLUSTERED ([EventTimeUTC] ASC, [ID] ASC) WITH (DATA_COMPRESSION = PAGE) ON [DBDATA]
) TEXTIMAGE_ON [DBDATA];



GO


