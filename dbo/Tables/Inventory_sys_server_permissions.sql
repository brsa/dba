﻿CREATE TABLE dbo.Inventory_sys_server_permissions (
	ID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_sys_server_permissions_ID PRIMARY KEY CLUSTERED (ID),
	ServerName NVARCHAR(128) NOT NULL,
	class_desc NVARCHAR(60) NULL,
	major_name NVARCHAR(128) NULL,
	minor_name NVARCHAR(128) NULL,
	grantee_principal_id INT NOT NULL,
	grantor_principal_id INT NOT NULL,
	permission_name NVARCHAR(128) NULL,
	state_desc NVARCHAR(60) NULL,
	DateAdded DATETIME2(0) NOT NULL,
	DateRemoved DATETIME2(0) NULL,
	Active BIT NOT NULL
	)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_server_permissions_DateAdded
	ON dbo.Inventory_sys_server_permissions (DateAdded)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_server_permissions_DateRemoved_Active
	ON dbo.Inventory_sys_server_permissions (DateRemoved)
	WHERE Active = 0
	WITH (DATA_COMPRESSION = PAGE);
GO