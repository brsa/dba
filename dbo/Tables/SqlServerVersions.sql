﻿    CREATE TABLE dbo.SqlServerVersions
    (
        MajorVersionNumber TINYINT NOT NULL,
        MinorVersionNumber SMALLINT NOT NULL,
        Branch VARCHAR(34) NOT NULL,
        [Url] VARCHAR(99) NOT NULL,
        ReleaseDate DATE NOT NULL,
        MainstreamSupportEndDate DATE NOT NULL,
        ExtendedSupportEndDate DATE NOT NULL,
        MajorVersionName VARCHAR(19) NOT NULL,
        MinorVersionName VARCHAR(67) NOT NULL,
        CONSTRAINT PK_SqlServerVersions PRIMARY KEY CLUSTERED ( MajorVersionNumber ASC,MinorVersionNumber ASC,  ReleaseDate ASC)
    );