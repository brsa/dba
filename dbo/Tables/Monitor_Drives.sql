﻿CREATE TABLE [dbo].[Monitor_Drives] (
    [LogDateTime]      DATETIME2 (0)   NOT NULL,
    [DriveLabel]       NVARCHAR (100)  NOT NULL,
    [FileSystemType]   NVARCHAR (512)  NULL,
    [TotalGB]          NUMERIC (10, 3) NULL,
    [AvailableGB]      NUMERIC (10, 3) NULL,
    [AvailablePercent] NUMERIC (5, 3)  NULL,
    CONSTRAINT [PK_Monitor_Drives] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [DriveLabel] ASC) WITH (DATA_COMPRESSION = PAGE)
);

