﻿CREATE TABLE [dbo].[Monitor_ObjectChanges] (
    [LogDateTime]  DATETIME2 (0)  NOT NULL,
    [Event_Time]   DATETIME2 (7)  NOT NULL,
    [ServerName]   VARCHAR (128)  NULL,
    [DatabaseName] VARCHAR (128)  NOT NULL,
    [UserName]     VARCHAR (128)  NULL,
    [ObjectType]   VARCHAR (35)   NULL,
    [ObjectName]   VARCHAR (128)  NOT NULL,
    [SchemaName]   VARCHAR (128)  NOT NULL,
    [Statement]    VARCHAR (4000) NULL,
    [SPID]         SMALLINT       NOT NULL,
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Monitor_ObjectChanges] PRIMARY KEY CLUSTERED ([Event_Time] ASC, [DatabaseName] ASC, [ObjectName] ASC, [ID] ASC)
);



