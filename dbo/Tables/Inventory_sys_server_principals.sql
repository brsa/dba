﻿CREATE TABLE dbo.Inventory_sys_server_principals (
	ID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_sys_server_principals_ID PRIMARY KEY CLUSTERED (ID),
	ServerName NVARCHAR(128) NOT NULL,
	name SYSNAME NOT NULL,
	principal_id INT NOT NULL,
	sid VARBINARY(85) NULL,
	type_desc NVARCHAR(60) NULL,
	is_disabled BIT NULL,
	default_database_name SYSNAME NULL,
	default_language_name SYSNAME NULL,
	credential_id INT NULL,
	owning_principal_id INT NULL,
	is_fixed_role BIT NULL,
	DateAdded DATETIME2(0) NOT NULL,
	DateRemoved DATETIME2(0) NULL,
	Active BIT NOT NULL
	)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_server_principals_DateAdded
	ON dbo.Inventory_sys_server_principals (DateAdded)
	WITH (DATA_COMPRESSION = PAGE);
GO

CREATE NONCLUSTERED INDEX IX_sys_server_principals_DateRemoved_Active
	ON dbo.Inventory_sys_server_principals (DateRemoved)
	WHERE Active = 0
	WITH (DATA_COMPRESSION = PAGE);
GO