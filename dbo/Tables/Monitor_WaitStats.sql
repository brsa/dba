﻿CREATE TABLE dbo.Monitor_WaitStats (
    LogDateTime        datetime2 (0) NOT NULL,
    WaitType           nvarchar(60) NOT NULL,
	IntervalMinutes    int,
    WaitingTaskCount   bigint,
    WaitTimeMs         bigint,
    MaxWaitTimeMs      bigint,
    SignalWaitTimeMs   bigint,
    CONSTRAINT PK_Monitor_WaitStats PRIMARY KEY CLUSTERED (LogDateTime, WaitType) WITH (DATA_COMPRESSION = PAGE)
);


GO
