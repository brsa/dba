﻿CREATE TABLE dbo.Monitor_Temp_VirtualFileStats (
    LogDateTime          datetime2(0)  NOT NULL,
    DbName               sysname       NOT NULL,
    LogicalFileName      sysname       NOT NULL,
	FileSizeBytes        bigint,
    NumReads             bigint,
    NumWrites            bigint,
	BytesRead            bigint,
	BytesWritten         bigint,
	IoStallReadMs        bigint,
	IoStallWriteMs       bigint,
	IoStallQueuedReadMs  bigint,
	IoStallQueuedWriteMs bigint,
	IoStall              bigint,
    CONSTRAINT PK_Monitor_Temp_VirtualFileStats PRIMARY KEY CLUSTERED (LogDateTime, DbName, LogicalFileName) WITH (DATA_COMPRESSION = PAGE)
);
GO


