﻿CREATE TABLE [dbo].[Monitor_temp_QueryStats] (
    [LogDateTime]        DATETIME2 (0) NOT NULL,
    [DbName]             [sysname]     NOT NULL,
    [SchemaName]         [sysname]     NOT NULL,
    [ObjectName]         VARCHAR (255) NOT NULL,
    [LineNumber]         INT           NOT NULL,
    [SqlText]            VARCHAR (MAX) NULL,
    [TextLine]           VARCHAR (500) NULL,
    [NumCalls]           BIGINT        NULL,
    [TotalPhysicalReads] BIGINT        NULL,
    [TotalLogicalReads]  BIGINT        NULL,
    [TotalLogicalWrites] BIGINT        NULL,
    [TotalWorkerTime]    BIGINT        NULL,
    [TotalClrTime]       BIGINT        NULL,
    [TotalElapsedTime]   BIGINT        NULL,
    CONSTRAINT [PK_Monitor_temp_QueryStats] PRIMARY KEY CLUSTERED ([LogDateTime] ASC, [DbName] ASC, [SchemaName] ASC, [ObjectName] ASC, [LineNumber] ASC) WITH (DATA_COMPRESSION = PAGE)
);


GO
CREATE NONCLUSTERED INDEX [IX_temp_QueryStats_Aggregation]
    ON [dbo].[Monitor_temp_QueryStats]([LogDateTime] ASC, [DbName] ASC, [SchemaName] ASC, [ObjectName] ASC, [TextLine] ASC) WITH (DATA_COMPRESSION = PAGE);



GO