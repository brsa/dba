﻿CREATE TABLE [dbo].[Monitor_Disk_SQLIOPerformance](
	[ID] [int] NOT NULL,
	[ServerName] [nvarchar](50) NOT NULL,
	[Duration] [nvarchar](10) NULL,
	[thread] [nvarchar](10) NULL,
	[IO] [nvarchar](10) NULL,
	[Drive] [nvarchar](10) NULL,
	[BlockSize] [nvarchar](10) NULL,
	[TestBlock] [nvarchar](10) NULL,
	[TestType] [nvarchar](25) NULL,
	[IOPS] [nvarchar](20) NULL,
	[MBPS] [nvarchar](20) NULL,
	[minLAT] [nvarchar](20) NULL,
	[avgLAT] [nvarchar](20) NULL,
	[maxLAT] [nvarchar](20) NULL,
	[Updated] [datetime] NOT NULL,
	[TestSize] [nvarchar](10) NULL,
 CONSTRAINT [PK_Monitor_Disk_SQLIOPerformance] PRIMARY KEY CLUSTERED 
(
	[ServerName],
	[Updated],
	[ID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DBDATA]
) ON [DBDATA]