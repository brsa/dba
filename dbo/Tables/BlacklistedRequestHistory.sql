﻿CREATE TABLE [dbo].[BlacklistedRequestHistory] (
    [ID]               INT             IDENTITY (1, 1) NOT NULL,
    [LogDateTime]      DATETIME2 (0)   DEFAULT (getdate()) NULL,
    [SessionID]        SMALLINT        NULL,
    [DbName]           [sysname]       NULL,
    [HostName]         NVARCHAR (128)  NULL,
    [ProgramName]      NVARCHAR (128)  NULL,
    [LoginName]        NVARCHAR (128)  NULL,
    [LoginTime]        DATETIME2 (3)   NULL,
    [LastRequestStart] DATETIME2 (3)   NULL,
    [LastRequestEnd]   DATETIME2 (3)   NULL,
    [TransactionCnt]   INT             NULL,
    [Command]          NVARCHAR (32)   NULL,
    [WaitTime]         INT             NULL,
    [WaitResource]     NVARCHAR (256)  NULL,
    [WaitDescription]  NVARCHAR (1000) NULL,
    [SqlText]          NVARCHAR (MAX)  NULL,
    [SqlStatement]     NVARCHAR (MAX)  NULL,
    [InputBuffer]      NVARCHAR (4000) NULL,
    [SessionInfo]      NVARCHAR (MAX)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (DATA_COMPRESSION = PAGE) ON [DBDATA]
) ON [DBDATA] TEXTIMAGE_ON [DBDATA];


GO

CREATE INDEX IX_BlacklistedRequest_LogDatetime ON BlacklistedRequestHistory(LogDateTime) WITH (DATA_COMPRESSION=PAGE);
