﻿CREATE TABLE [dbo].[Monitor_Temp_ObjectInventory] (
    [LogDateTime]               DATETIME2 (0)  NULL,
    [DatabaseName]              NVARCHAR (256) NOT NULL,
    [DatabaseID]                SMALLINT       NOT NULL,
    [SchemaName]                [sysname]      NOT NULL,
    [ObjectName]                [sysname]      NOT NULL,
    [ObjectType]                CHAR (2)       NOT NULL,
    [CreateDate]                DATETIME       NOT NULL,
    [LastUpdateDate]            DATETIME       NOT NULL,
    [LastExecuted]              DATETIME       NULL,
    [Rowscount]                 BIGINT         NULL,
    [IsHeap]                    BIT            NULL,
    [LastRead]                  DATETIME       NULL,
    [LastWrite]                 DATETIME       NULL,
    [TotalNolockCount]          INT            NULL,
    [TransactionIsolationLevel] VARCHAR (100)  NULL,
    CONSTRAINT [PK_Monitor_Temp_ObjectInventory] PRIMARY KEY CLUSTERED ([DatabaseName] ASC, [SchemaName] ASC, [ObjectName] ASC)
);

