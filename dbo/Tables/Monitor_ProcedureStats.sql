﻿CREATE TABLE dbo.Monitor_ProcedureStats (
    LogDateTime        datetime2(0)  NOT NULL,
    DbName             sysname       NOT NULL,
    SchemaName         sysname       NOT NULL,
    ObjectName         varchar (255) NOT NULL,
    IntervalMinutes    int,
    NewSqlHandle       bit DEFAULT 0,
    NewPlanHandle      bit DEFAULT 0,
    PlanCount          int,
    LastCachedTime     datetime2(3),
    ExecutionCount     bigint,
    TotalWorkerTime    bigint,
    TotalLogicalWrites bigint,
    TotalLogicalReads  bigint,
    TotalElapsedTime   bigint
    CONSTRAINT PK_Monitor_ProcedureStats PRIMARY KEY CLUSTERED (LogDateTime, DbName, SchemaName, ObjectName) WITH (DATA_COMPRESSION = PAGE)
);
GO