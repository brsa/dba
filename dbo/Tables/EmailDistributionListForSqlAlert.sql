﻿CREATE TABLE [dbo].[EmailDistributionListForSqlAlert] (
    [DistributionCode] VARCHAR (50)  NOT NULL,
    [EmailAddress]     VARCHAR (MAX) NOT NULL,
    [Note]             VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_DistributionCode] PRIMARY KEY CLUSTERED ([DistributionCode] ASC)
);

