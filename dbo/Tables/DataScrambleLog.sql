﻿CREATE TABLE [dbo].[DataScrambleLog] (
    [ScrambleLogId]  INT   IDENTITY (1, 1) NOT NULL,
    [Failed]         INT             NOT NULL,
    [DatabaseName]   VARCHAR (100)   NOT NULL,
    [SchemaName]     VARCHAR (100)   NOT NULL,
    [TableName]      VARCHAR (100)   NOT NULL,
    [ColumnName]     VARCHAR (100)   NOT NULL,
    [DynamicSqlText] VARCHAR (MAX)   NULL,
    [ErrorProcedure] NVARCHAR (128)  NULL,
    [Line]           INT             NULL,
    [Message]        NVARCHAR (4000) NULL,
    [HostName]       NVARCHAR (128)  NULL,
    [Updated]        DATETIME        NULL CONSTRAINT DF_DataScrambleLog_Updated DEFAULT (GETDATE()),
	CONSTRAINT [PK_DataScrambleLog] PRIMARY KEY CLUSTERED (DatabaseName,SchemaName,TableName,ColumnName) WITH (DATA_COMPRESSION = PAGE)
);

