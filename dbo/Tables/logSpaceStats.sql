﻿CREATE TABLE dbo.logSpaceStats 
( 
id INT IDENTITY (1,1), 
logDate datetime DEFAULT GETDATE(), 
databaseName sysname, 
logSize decimal(18,5), 
logUsed decimal(18,5) 
) 
GO 
 