﻿CREATE TABLE [dbo].[Monitor_Temp_LinkedServerUsage]
(
	LogDateTime DATETIME CONSTRAINT DF_LinkedServerUsage_DEVDB_LogDateTime DEFAULT(GETDATE()),
	linked_server_name VARCHAR(128) NOT NULL,
	server_principal_name VARCHAR(128), 
	database_name VARCHAR(128) NOT NULL,
	CONSTRAINT PK_Monitor_Temp_LinkedServerUsage PRIMARY KEY CLUSTERED (linked_server_name,server_principal_name,database_name)
);
