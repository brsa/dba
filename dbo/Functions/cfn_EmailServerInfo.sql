CREATE FUNCTION [dbo].[cfn_EmailServerInfo]()
RETURNS NVARCHAR(MAX)
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150107
    This function returns an HTML table containing standard info about a SQL instance to
	be included in email alerts/reports
	* Instance name
	* Physical Server
	* Instance start time

PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
BEGIN
    DECLARE @ServerInfo NVARCHAR(MAX);
    
    
	SELECT @ServerInfo = N'<table>
		<tr>
			<th> SQL Instance </th>
			<td> ' + @@SERVERNAME + N'</td>
		</tr><tr>
			<th> Physical Server </th>
			<td> ' + CAST(SERVERPROPERTY('ComputerNamePhysicalNetBIOS') AS NVARCHAR(128)) + N'</td>
		</tr><tr>
			<th> Instance Start Time </th>
			<td> ' + (SELECT CONVERT(NVARCHAR(20),create_date,120) FROM sys.databases WHERE name = 'tempdb') + N'</td>
		</tr><tr>
			<th> Domain </th>
			<td> ' + (SELECT CONVERT(NVARCHAR(20),DEFAULT_DOMAIN(),120)) + N'</td>
		</tr>
	</table>';

    RETURN(@ServerInfo);
END;