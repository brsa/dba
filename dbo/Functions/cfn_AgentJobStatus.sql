﻿CREATE FUNCTION dbo.cfn_AgentJobStatus (@JobName sysname)
RETURNS TABLE
AS
RETURN
SELECT TOP 1
    IsRunning = CASE WHEN ja.job_id IS NOT NULL AND ja.stop_execution_date IS NULL THEN 1 ELSE 0 END,
    LastRunTime = ja.start_execution_date,
    NextRunTime = ja.next_scheduled_run_date,
    LastJobStep = js.step_name,
    JobOutcome = CASE 
                    WHEN ja.job_id IS NOT NULL AND ja.stop_execution_date IS NULL THEN 'Running'
                    WHEN run_status = 0 THEN 'Failed'
                    WHEN run_status = 1 THEN 'Succeeded'
                    WHEN run_status = 2 THEN 'Retry'
                    WHEN run_status = 3 THEN 'Cancelled'
                END
FROM msdb.dbo.sysjobs j
LEFT JOIN msdb.dbo.sysjobactivity ja 
    ON ja.job_id = j.job_id
    AND ja.run_requested_date IS NOT NULL
    AND ja.start_execution_date IS NOT NULL
LEFT JOIN msdb.dbo.sysjobsteps js
    ON js.job_id = ja.job_id
    AND js.step_id = ja.last_executed_step_id
LEFT JOIN msdb.dbo.sysjobhistory jh
    ON jh.job_id = j.job_id
    AND jh.instance_id = ja.job_history_id
WHERE j.name = @JobName
ORDER BY ja.start_execution_date DESC;
GO


