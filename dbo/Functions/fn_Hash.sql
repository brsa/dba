﻿CREATE FUNCTION [dbo].[fn_Hash] (@Salt VARCHAR(20), @Value VARCHAR(50),@Classification VARCHAR(50), @Algorithm VARCHAR(10))
RETURNS TABLE
/*************************************************************************************************
AUTHOR: Ajay H.
CREATED: 2016-10-26
	This function can be use to hash value.
PARAMETERS:
	@Salt - Text string 
	@Value - Value to be hashed.
	@Classification - Type of hashing (Email, DOB, SSN)
	@Algorith - 128 bits (16 bytes) for MD2, MD4, and MD5; 160 bits (20 bytes) for SHA and SHA1; 256 bits (32 bytes) for SHA2_256, and 512 bits (64 bytes) for SHA2_512.

EXAMPLES:
* SELECT * FROM dbo.fn_Hash('adasdsdsd','123456789', '3', 'SHA2_512')
SELECT * FROM dbo.fn_Hash('adasdsdsd','1/1/2017', '11', 'SHA2_512')
SELECT * FROM dbo.fn_Hash('adasdsdsd','010117', '11', 'SHA2_512')
SELECT * FROM dbo.fn_Hash('adasdsdsd','20170101', '11', 'SHA2_512')
SELECT * FROM dbo.fn_Hash('adasdsdsd','20170132', '11', 'SHA2_512')
SELECT * FROM dbo.fn_Hash('adasdsdsd','test@cheesecake.com', '6', 'SHA2_512')

**************************************************************************************************
MODIFICATIONS:
**************************************************************************************************

*************************************************************************************************/
AS
RETURN

SELECT CASE 
	WHEN @Classification='3' -- SSN 
		THEN (CASE WHEN CHARINDEX('-', @Value) > 0 
			THEN LEFT(RIGHT('000000000000000' + ABS(CONVERT(BIGINT,HASHBYTES(@Algorithm, REPLACE(@Value,'-','') + @Salt))) ,LEN(REPLACE(@Value,'-',''))),3) + '-' +
			SUBSTRING(RIGHT('000000000000000' + ABS(CONVERT(BIGINT,HASHBYTES(@Algorithm, REPLACE(@Value,'-','') + @Salt))) ,LEN(REPLACE(@Value,'-',''))),4,2) + '-' +
			RIGHT(RIGHT('000000000000000' + ABS(CONVERT(BIGINT,HASHBYTES(@Algorithm, REPLACE(@Value,'-','')+ @Salt))) ,LEN(REPLACE(@Value,'-',''))),4)
			ELSE RIGHT('000000000000000' + ABS(CONVERT(BIGINT,HASHBYTES(@Algorithm,@Value + @Salt))) ,LEN(@Value)) END
		)
	WHEN @Classification='11' -- Date of Birth
		THEN CASE WHEN isNumeric(@value) = 1 AND len(@value) = 6 AND ISDATE(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2)) = 1 THEN 
		CONVERT(VARCHAR(6),
		RIGHT ('00' + CAST(DATEPART(MONTH,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2) AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2) AS DATETIME)))))),5)),5)),2)) AS VARCHAR(2)), 2)
		+ RIGHT('00' + CASE WHEN DATEPART(DAY,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2) AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2) AS DATETIME)))))),5)),5)),2)) IN (29,30,31) THEN '28'
			ELSE 
		CAST(DATEPART(DAY,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2) AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(LEFT(@Value,2) + '/' + SUBSTRING(@VALUE,3,2) + '/' + RIGHT(@VALUE, 2) AS DATETIME)))))),5)),5)),2)) AS VARCHAR(2)) END
		, 2)
		+ RIGHT(@Value, 2)
		)
		WHEN isNumeric(@value) = 1 AND len(@value) = 8 AND ISDATE(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4)) = 1 THEN
		CONVERT(VARCHAR(8),
		LEFT(@Value, 4)  + RIGHT ('00' + CAST(DATEPART(MONTH,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4)  AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4) AS DATETIME)))))),5)),5)),2)) AS VARCHAR(2)), 2)
		+ RIGHT('00' + CASE WHEN DATEPART(DAY,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4) AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4) AS DATETIME)))))),5)),5)),2)) IN (29,30,31) THEN '28'
			ELSE 
		CAST(DATEPART(DAY,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4) AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(SUBSTRING(@VALUE,5,2) + '/' + RIGHT(@VALUE, 2) + '/' + LEFT(@Value,4) AS DATETIME)))))),5)),5)),2)) AS VARCHAR(2)) END
		, 2)
		)
		WHEN ISDATE( @VALUE) = 1 THEN  
		CONVERT(VARCHAR(10),
		RIGHT ('00' + CAST(DATEPART(MONTH,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(@Value AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(@Value AS DATETIME)))))),5)),5)),2)) AS VARCHAR(2)), 2)
		+ '-'+  RIGHT('00' + CASE WHEN DATEPART(DAY,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(@Value AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(@Value AS DATETIME)))))),5)),5)),2)) IN (29,30,31) THEN '28'
			ELSE 
		CAST(DATEPART(DAY,DATEADD(DAY,CONVERT(INT,LEFT('00000' + ABS(LEFT('00000'+CONVERT(BIGINT,HASHBYTES(@Algorithm,CONVERT(VARCHAR(20),CONVERT(INT,CAST(@Value AS DATETIME)) + CONVERT(VARCHAR(20),CONVERT(INT,CAST(@Value AS DATETIME)))))),5)),5)),2)) AS VARCHAR(2)) END
		, 2)
		+ '-'+ CAST(DATEPART(YEAR,CAST(@Value AS DATETIME)) AS VARCHAR(4))
		)
		ELSE @VALUE
		END

	WHEN @Classification= '6' -- Email
		THEN (CASE WHEN CHARINDEX('@',@Value) = 0 THEN @Value
		WHEN CHARINDEX('@commonwealth.com',RTRIM(LTRIM(@Value))) > 0 THEN @Value
		ELSE
		RIGHT('0000000000000000000000000' + ABS(CONVERT(BIGINT,HASHBYTES(@Algorithm, LEFT(@Value,CHARINDEX('@',@Value)-1) + @Salt))),CHARINDEX('@',@Value) -1) + '@cfnfakedomain.com'
		--right('0000000000000000000000000' + ABS(CONVERT(BIGINT,HASHBYTES(@Algorithm, left(@value,charindex('@',@value)-1) + @Salt))),charindex('@',@value) -1) + right(@value,len(@value)-charindex('@',@value)+1)
		END
		)	
END AS HashValue;