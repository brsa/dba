﻿


-- Need to put Error Handling EVERYWHERE & log errors to a table
-- ServiceBroker is asychronous, so errors can't bubble back up
-- to a user/application session to be handled there.


CREATE PROCEDURE [dbo].[cfn_SBBackupLog]
AS
SET NOCOUNT ON 

DECLARE @Message table 
    (ConversationHandle uniqueidentifier,
     ServiceName nvarchar(512),
     ContractName nvarchar(256),
     MessageType nvarchar(256),
     MessageXML nvarchar(max));

DECLARE @SBQueueName sysname = 'BackupLogQueue',
    @ConversationHandle uniqueidentifier,
    @ServiceName nvarchar(512),
    @ContractName nvarchar(256),
    @MessageType nvarchar(256),
    @MessageXML xml,
    @NewConversation uniqueidentifier,
    @ServerName sysname,
    @DbName sysname,
    --@Environment varchar(32),
    @BackupPath varchar(512),
    @Compressed bit,
    @RetainDays tinyint,
    @SQL nvarchar(max),
    @ErrNumber int,
    @ErrSeverity int,
    @ErrState int,
    @ErrMsg varchar(2048);
  
--Just process one command. Leave the rest in queue, in case the server crashes (or something) mid-backup.
BEGIN TRY
	--SILLY WORKAROUND
		-- SQL 2008 doesn't like RECEIVE to be the first statement in a TRY-CATCH block
		-- To make this work, put the RECEIVE in an IF statement that is always true!
		-- We should remove this once we're fully on SQL 2014
    IF 1=1
		RECEIVE TOP(1) conversation_handle, service_name, service_contract_name, message_type_name, CONVERT(NVARCHAR(MAX), message_body)
		FROM dbo.BackupLogQueue
		INTO @Message;
END TRY
BEGIN CATCH
    SET @ErrNumber = ERROR_NUMBER();
    SET @ErrSeverity = ERROR_SEVERITY();
    SET @ErrState = ERROR_STATE();
    SET @ErrMsg = ERROR_MESSAGE();
    SET @ErrMsg = 'Unable to Receive from Queue - ' + @ErrMsg;
    INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, QueueName, ErrorNumber, ErrorMessage)
    VALUES (GETDATE(), @SBQueueName, @ErrNumber, @ErrMsg);
    RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
    RETURN (1);
END CATCH;

SELECT @ConversationHandle = ConversationHandle,
       @ServiceName = ServiceName,
       @ContractName = ContractName,
       @MessageType = MessageType,
       @MessageXML = MessageXML
FROM @Message;

IF @@ROWCOUNT = 0
 BEGIN
    --PRINT 'No messages to process';
    RETURN (0);
 END;

 IF @MessageType = '//Backup/Message/Log'
BEGIN
    --Parse the XML
    SELECT @ServerName = Tbl.Col.value('ServerName[1]', 'sysname'),  
           @DbName = Tbl.Col.value('DbName[1]', 'sysname'),  
           --@Environment = Tbl.Col.value('Environment[1]', 'varchar(32)'),
           @BackupPath = Tbl.Col.value('BackupPath[1]', 'varchar(512)'),
           @Compressed = Tbl.Col.value('Compressed[1]', 'bit'),
           @RetainDays = Tbl.Col.value('RetainDays[1]', 'tinyint')
    FROM   @MessageXML.nodes('//row') Tbl(Col);

    -- Validations on parameters
    IF @ServerName <> @@SERVERNAME
     BEGIN
        SET @ErrNumber = NULL;
        SET @ErrSeverity = 16;
        SET @ErrState = 1;
        SET @ErrMsg = 'ServerName specified in message does not match this server.';
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
     END
     --End Validations

    DECLARE db_cur CURSOR FOR
        SELECT name
        FROM sys.databases
        WHERE state = 0 --online
        AND recovery_model IN (1,2) -- Full, Bulk
        AND name LIKE @DbName;

    OPEN db_cur;
    FETCH NEXT FROM db_cur INTO @DbName;
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @SQL = 'EXEC cfn_DatabaseBackup @Databases = ''' + @DbName + ''', @Directory = ''' + @BackupPath + ''', '
         + ' @BackupType = ''LOG'', @Verify = ''Y'', '
         + CASE WHEN @RetainDays > 0 THEN '@CleanupTime = ' + CAST(@RetainDays*24 AS nvarchar(10)) + ', ' ELSE '' END 
         + ' @Checksum = ''Y'', @Compress = ''' + CASE WHEN @Compressed = 0 THEN 'N' ELSE 'Y' END + ''', '
         + ' @NumberOfFiles = 1, @LogToTable = ''Y'';';
 
        --Validate that we appear to have built the @SQL string properly
        --When building dynamic SQL with this many variables, it's too easy to let a NULL sneak in.
        IF LEN(COALESCE(@SQL,'')) <10
         BEGIN
            SET @ErrNumber = NULL;
            SET @ErrSeverity = 16;
            SET @ErrState = 1;
            SET @ErrMsg = 'Error constructing dyanamic SQL call for DatabaseBackup.';
            INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
            VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
            RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
            RETURN (1);
         END

        --Execute the SQL
        BEGIN TRY
            EXEC sp_executesql @stmt = @SQL;
        END TRY
        BEGIN CATCH
            SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
        END CATCH;
        FETCH NEXT FROM db_cur INTO @DbName;
    END;
    CLOSE db_cur;
    DEALLOCATE db_cur;
END
ELSE IF @MessageType = 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
 BEGIN
    --End the conversation
    BEGIN TRY
        END CONVERSATION @ConversationHandle;
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
 END
ELSE
 BEGIN -- No logic to handle this message type. Log/raise an error
    SET @ErrNumber = 50000;
    SET @ErrSeverity = 16;
    SET @ErrState = 1;
    SET @ErrMsg = 'MessageType "' + @MessageType + '" not supported';
    INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
    VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
    RAISERROR (@ErrMsg, @ErrSeverity, @ErrState);
    RETURN (1);
 END;