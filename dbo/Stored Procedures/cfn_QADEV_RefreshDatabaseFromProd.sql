﻿CREATE PROCEDURE dbo.cfn_QADEV_RefreshDatabaseFromProd
	 @DbName sysname,
	 @SourceEnvironment nvarchar(32)	 = 'Production',
	 @SourceAlias nvarchar(128)	 = 'TESLA',
	 @AgName sysname = NULL,
	 @DataDrive char(1) = 'D',
	 @LogDrive char(1) = 'L',
	 @Debug bit = 1
AS
SET NOCOUNT ON;

IF EXISTS (SELECT 1 FROM master.sys.extended_properties WHERE class = 0 AND name = 'SecurityEnvironment' AND value = 'Production')
BEGIN
	SELECT 'This is a production server. Refresh procedure can only be run on Dev or QA servers'
	RETURN 0;
END;	

DECLARE @Sql nvarchar(max);
DECLARE @RestoreSql nvarchar(max);
DECLARE @BackupHistoryServer sysname = 'DC01DB-P015SQL';
DECLARE @FileNum nvarchar(260) =1;
DECLARE @DateStamp nvarchar(15);
DECLARE @ReplicaName nvarchar(256);

CREATE TABLE #BackupFiles (
	BackupFinishDate datetime2(0),
	FamilySequenceNumber int,
	PhysicalDeviceName nvarchar(260)
	);

CREATE TABLE #DataFiles (
	LogicalName nvarchar(128),
	PhysicalName nvarchar(260),
	Type char(1),
	FileGroupName nvarchar(128),
	Size numeric(20,0),
	MaxSize numeric(20,0),
	FileId bigint,
	CreateLSN numeric(25,0),
	DropLSN numeric(25,0),
	UniqueId uniqueidentifier,
	ReadOnlyLSN numeric(25,0),
	ReadWriteLSN numeric(25,0),
	BackupSizeInBytes bigint,
	SourceBlockSize int,
	FileGroupId int,
	LogGroupGUID uniqueidentifier,
	DifferentialBaseLSN numeric(25,0),
	DifferentialBaseGUID uniqueidentifier,
	IsReadOnly bit,
	IsPresent bit,
	TDEThumbprint varbinary(32)
	);

--If @AgName is null, but the DB is already in an AG, we want to put the DB back into that AG.
SELECT @AgName = ag.name
FROM sys.availability_groups ag
LEFT JOIN sys.dm_hadr_database_replica_states rs
	ON rs.group_id = ag.group_id AND rs.is_local = 1
WHERE db_name(database_id) = @DbName;

DECLARE replica_cur CURSOR FOR
	SELECT ar.replica_server_name
	FROM sys.availability_groups ag
	JOIN sys.availability_replicas ar ON ar.group_id = ag.group_id
	WHERE ar.replica_server_name <> @@SERVERNAME
	AND ag.name = @AgName;

--Compute the current date/time into a varchar @DateStamp. We'll use this later in file names
SELECT @DateStamp = REPLACE(REPLACE(REPLACE(CONVERT(varchar(20),GETDATE(),120),'-',''),':',''),' ','_')


PRINT '     ----- Availability Group ' + @AgName;


--Get most recent backup info from central repository
SELECT @Sql = 'SELECT * FROM  OPENROWSET(''SQLNCLI'', ''Server=' + @BackupHistoryServer + ';Trusted_Connection=yes;'',' + CHAR(10) +
    '''SELECT b.BackupFinishDate, bf.FamilySequenceNumber, bf.PhysicalDeviceName
FROM  DatabaseManager.dbo.cfn_GetLastFullBackup (''''' + @SourceEnvironment + ''''', ''''' + @SourceAlias + ''''', ''''' + @DbName + ''''') b 
JOIN DatabaseManager.dbo.BackupHistory bh ON b.BackupFinishDate = bh.BackupFinishDate AND b.ServerName = bh.ServerName AND b.DatabaseName = bh.DatabaseName
JOIN DatabaseManager.dbo.BackupHistoryFile bf ON b.ServerName= bf.ServerName AND b.DatabaseName = bf.DatabaseName and bf.MediaSetID = bh.MediaSetID
WHERE 1=1;'');';

---IF @Debug =1
---BEGIN
---	PRINT @Sql;
---END
---ELSE
BEGIN TRY
	INSERT INTO #BackupFiles (BackupFinishDate, FamilySequenceNumber, PhysicalDeviceName)
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH;
	RAISERROR ('Unable to get backup history from central server',16,0);
	RETURN -1;
END CATCH;

--RESTORE FILELISTONLY to get the data file list.
SELECT @Sql = 'RESTORE FILELISTONLY FROM DISK = ''' + PhysicalDeviceName + ''''
FROM #BackupFiles
WHERE FamilySequenceNumber = 1;

---IF @Debug =1
---BEGIN
---	PRINT @Sql;
---END
---ELSE
BEGIN TRY
	INSERT INTO #DataFiles
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH
	RAISERROR ('Error while getting data file list using RESTORE FILELISTONLY',16,0);
	RETURN -1;
END CATCH;

--Build the RESTORE database statement.
	SELECT @RestoreSql = 'RESTORE DATABASE [' + @DbName + '] FROM ';

	--Add the backup file locations for multi-file backups
	SELECT @RestoreSql = @RestoreSql + CHAR(10) 
				+ CASE WHEN FamilySequenceNumber = 1 THEN '   ' ELSE '  ,' END
				+ ' DISK = ''' + PhysicalDeviceName + ''' '
	FROM #BackupFiles
	ORDER BY FamilySequenceNumber;

	--WITH options
	SELECT @RestoreSql = @RestoreSql + CHAR(10) + ' WITH '

	--MOVE data files.
	SELECT @RestoreSql = @RestoreSql + CHAR(10) 
		+ CASE WHEN FileId = 1 THEN '   ' ELSE '  ,' END
		+ ' MOVE ''' + LogicalName + ''' TO ''' 
		+ CASE WHEN Type = 'L' THEN @LogDrive + ':\Logs\' ELSE @DataDrive + ':\Data\' END 
		--+ SUBSTRING(STUFF(PhysicalName,LEN(PhysicalName)-3,0,'_' + @DateStamp),2,260)
		+ @DbName + '_' + LogicalName + '_' + @DateStamp 
		+ CASE WHEN FileId = 1 THEN '.mdf' WHEN Type = 'L' THEN '.ldf' ELSE @DataDrive + '.ndf' END 
		+ ''''
	FROM #DataFiles
	ORDER BY FileId

	--More WITH options. REPLACE and NORECOVERY. We'll recover when we're ready.
	SELECT @RestoreSql = @RestoreSql + CHAR(10) + '  , NORECOVERY, REPLACE'

--OK, the RESTORE statement is built
IF @Debug = 1
BEGIN
	PRINT '     ----- Generic Restore Statement';
	PRINT '/*';
	PRINT @RestoreSql ;
	PRINT '--*/'
END;

--Cleanup -- If the sproc fails in a specific spot, it can leave the TBD_ database just hanging out
--So, if it exists at the start, we should drop it.
--This normally happens at the end, but if the end never happened, it happens at the start, too.
PRINT '     ----- Dropping old DB'
SELECT @Sql = 'IF EXISTS (SELECT 1 FROM sys.databases WHERE name = ''TBD_' + @DbName + ''')
					DROP DATABASE [TBD_' + @DbName + '];';
IF @Debug = 1
BEGIN
	PRINT @Sql;
END;
ELSE
BEGIN TRY
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH
	RAISERROR ('Error dropping TBD_ database',16,0);
	RETURN -1;
END CATCH;



--If DB is in AG
--need to do "real" check here instead of @AgName. In case we use the param to add a DB to the AG when it isn't already there.
IF @AgName IS NOT NULL
BEGIN
	IF dbo.dm_hadr_db_role(@AgName) <> 'PRIMARY'
	BEGIN
		RAISERROR ('AG is not in primary role',16,1);
		RETURN -1;
	END;
	--Remove DB from AG if it's in the AG.
	IF dbo.dm_hadr_db_role(@DbName) = 'PRIMARY'
	BEGIN
		PRINT '     ----- Removing ' + @DbName + ' From AG ' + @AgName;
		SELECT @Sql = 'USE [master];
			ALTER AVAILABILITY GROUP [' + @AgName + '] REMOVE DATABASE [' + @DbName + '];
			--wait 10 seconds before going on, otherwise we might hit race condition 
			--where secondary is still in use when we restore there
			WAITFOR DELAY ''00:00:10'';';
		IF @Debug = 1
		BEGIN
			PRINT @Sql;
		END;
		ELSE
		BEGIN TRY
			EXEC sp_executesql @Sql;
		END TRY
		BEGIN CATCH
			RAISERROR ('Error removing database from Availability Group',16,0);
			RETURN -1;
		END CATCH;
	END;
	--Restore to all secondaries
	OPEN replica_cur;
	FETCH NEXT FROM replica_cur INTO @ReplicaName;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--use sp_executesql across OPENDATASOURCE() to restore to secondary
		PRINT '     ----- Restoring to ' + @ReplicaName;
		SELECT @Sql = 'EXEC OPENDATASOURCE(''SQLNCLI'',''Data Source=' + @ReplicaName 
					+ ';Integrated Security=SSPI'').master.dbo.sp_executesql N''' + REPLACE(@RestoreSql,'''','''''') + ''';';
		IF @Debug = 1
		BEGIN
			PRINT @Sql;
		END;
		ELSE
		BEGIN TRY
			EXEC sp_executesql @Sql;
		END TRY
		BEGIN CATCH
			RAISERROR ('Error restoring database to secondary',16,0);
			RETURN -1;
		END CATCH;
		FETCH NEXT FROM replica_cur INTO @ReplicaName;
	END;
	--Close the cursor, but don't deallocate. We'll need the cursor again later.
	CLOSE replica_cur;
END;
--Restore to primary. Append _new to database name as we restore.
--the original DB is still online & maybe someone is using it

PRINT '     ----- Restore to Primary with _new on DbName'
SELECT @Sql = STUFF(@RestoreSQL,CHARINDEX(@DbName,@RestoreSql),LEN(@DbName),@DbName+'_new')
IF @Debug = 1
BEGIN
	PRINT @Sql;
END;
ELSE
BEGIN TRY
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH
	RAISERROR ('Error restoring database to primary as _new',16,0);
	RETURN -1;
END CATCH;

--Recover the database on the Primary node (this one)
PRINT '     ----- Recover DB on primary'
SELECT @Sql = 'RESTORE DATABASE [' + @DbName + '_new] WITH RECOVERY;';
IF @Debug = 1
BEGIN
	PRINT @Sql;
END;
ELSE
BEGIN TRY
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH
	RAISERROR ('Error recovering _new database on primary',16,0);
	RETURN -1;
END CATCH;

--Swap database names on Primary
PRINT '     ----- Swap in new DB'
--If the DB already exists (it probably does, unless this is the first time...
IF EXISTS (SELECT 1 FROM sys.databases WHERE name = @DbName)
BEGIN
	--set offline, set online, then shuffle names
	SELECT @Sql = 'ALTER DATABASE [' + @DbName + '] SET OFFLINE WITH ROLLBACK IMMEDIATE;';
	IF @Debug = 1
	BEGIN
		PRINT @Sql;
	END;
	ELSE
	BEGIN TRY
		EXEC sp_executesql @Sql;
	END TRY
	BEGIN CATCH
		RAISERROR ('Error setting database offline before swapping names',16,0);
		RETURN -1;
	END CATCH;
	SELECT @Sql = 'ALTER DATABASE [' + @DbName + '] SET ONLINE;';
	IF @Debug = 1
	BEGIN
		PRINT @Sql;
	END;
	ELSE
	BEGIN TRY
		EXEC sp_executesql @Sql;
	END TRY
	BEGIN CATCH
		RAISERROR ('Error bringing database online before swapping names',16,0);
		RETURN -1;
	END CATCH;
	SELECT @Sql = 'ALTER DATABASE [' + @DbName + '] MODIFY NAME = [TBD_' + @DbName + '];';
	IF @Debug = 1
	BEGIN
		PRINT @Sql;
	END;
	ELSE
	BEGIN TRY
		EXEC sp_executesql @Sql;
	END TRY
	BEGIN CATCH
		RAISERROR ('Error renaming database to TBD_ when swapping names',16,0);
		RETURN -1;
	END CATCH;
END;
SELECT @Sql = 'ALTER DATABASE [' + @DbName + '_new] MODIFY NAME = [' + @DbName + '];';
IF @Debug = 1
BEGIN
	PRINT @Sql;
END;
ELSE
BEGIN TRY
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH
	RAISERROR ('Error swapping name of _new database',16,0);
	RETURN -1;
END CATCH;


--If DB was in AG (or one is specified), put it back in
IF @AgName IS NOT NULL
BEGIN
	PRINT '     ----- Adding database to AG'
	SELECT @Sql = 'USE [master];
			ALTER AVAILABILITY GROUP [' + @AgName + '] ADD DATABASE [' + @DbName + '];';
	IF @Debug = 1
	BEGIN
		PRINT @Sql;
	END;
	ELSE
	BEGIN TRY
		EXEC sp_executesql @Sql;
	END TRY
	BEGIN CATCH
		RAISERROR ('Error adding database to Availability Group',16,0);
		RETURN -1;
	END CATCH;
	--Loop through all secondaries to start data syncing
	OPEN replica_cur;
	FETCH NEXT FROM replica_cur INTO @ReplicaName;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--use sp_executesql across OPENDATASOURCE() to execute command on secondary
		SELECT @Sql = 'EXEC OPENDATASOURCE(''SQLNCLI'',''Data Source=' + @ReplicaName 
					+ ';Integrated Security=SSPI'').master.dbo.sp_executesql N''' 
					+ 'ALTER DATABASE [' + @DbName + '] SET HADR AVAILABILITY GROUP = [' + @AgName + '];'';';
		IF @Debug = 1
		BEGIN
			PRINT @Sql;
		END;
		ELSE
		BEGIN TRY
			EXEC sp_executesql @Sql;
		END TRY
		BEGIN CATCH
			RAISERROR ('Error joining secondary to Availability Group',16,0);
			RETURN -1;
		END CATCH;
		FETCH NEXT FROM replica_cur INTO @ReplicaName;
	END;
END;


--Drop the old db (if there was one)
PRINT '     ----- Dropping old DB'
SELECT @Sql = 'IF EXISTS (SELECT 1 FROM sys.databases WHERE name = ''TBD_' + @DbName + ''')
					DROP DATABASE [TBD_' + @DbName + '];';
IF @Debug = 1
BEGIN
	PRINT @Sql;
END;
ELSE
BEGIN TRY
	EXEC sp_executesql @Sql;
END TRY
BEGIN CATCH
	RAISERROR ('Error dropping TBD_ database',16,0);
	RETURN -1;
END CATCH;


DEALLOCATE replica_cur;

--Fix orphaned user/login pairs
EXEC dbo.cfn_Security_OrphanedUsersAutoFix @Debug=0;

/*
DROP TABLE #BackupFiles
DROP TABLE #DataFiles
--*/

GO
