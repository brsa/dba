﻿CREATE PROCEDURE [dbo].[cfn_Monitor_Log_LinkedServerUsage]
AS
SET NOCOUNT ON;

IF OBJECT_ID ('tempdb.dbo.#oledb_data_read') IS NOT NULL
    DROP TABLE #oledb_data_read;

SELECT event_data = CONVERT (XML, event_data)
INTO #oledb_data_read
FROM sys.fn_xe_file_target_read_file (N'OLEDB_DATA_READ*.xel', NULL, NULL, NULL);

;WITH LinkedServerUsage
AS
(
SELECT DISTINCT
	  linked_server_name = t.event_data.value (N'(event/data[@name="linked_server_name"]/value)[1]', N'nvarchar(max)')
      ,server_principal_name = t.event_data.value (N'(event/action[@name="server_principal_name"]/value)[1]', N'nvarchar(max)')
      ,database_name = t.event_data.value (N'(event/action[@name="database_name"]/value)[1]', N'nvarchar(max)')
FROM #oledb_data_read t
)
INSERT INTO dbo.Monitor_Temp_LinkedServerUsage (linked_server_name, server_principal_name, database_name) 
SELECT t.linked_server_name, t.server_principal_name, t.database_name
FROM LinkedServerUsage t
LEFT JOIN dbo.Monitor_Temp_LinkedServerUsage lsud ON lsud.linked_server_name=t.linked_server_name
	AND lsud.server_principal_name=t.server_principal_name
	AND lsud.database_name=t.database_name
WHERE lsud.server_principal_name IS NULL;