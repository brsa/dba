﻿CREATE PROCEDURE dbo.cfn_Monitor_Log_WaitStats
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150822
    This procedure logs Wait stats from the DMVs to Monitor_WaitStats.
	Calculates a delta since the last run, and saves that delta to the table.
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME(),
	@LastLogDateTime datetime2(0),
	@IntervalMinutes int;

--Lets truncate seconds & round to the nearest minute.
SELECT @LogDateTime = CONVERT(varchar(20),@LogDateTime,100)

--When did we last check stats?
SELECT @LastLogDateTime = MAX(LogDateTime) FROM Monitor_WaitStats;

--If the instance rebooted since then, we should consider that restart time as our last time.
    -- Stats are only cumulative since start time, so @IntervalMinutues should reflect a shorter time.
	-- Also if we just restarted, we oughtn't compare against last collection to compute a delta.
SELECT @LastLogDateTime = CASE WHEN create_date > @LastLogDateTime  OR @LastLogDateTime IS NULL THEN create_date 
								ELSE @LastLogDateTime END 
FROM sys.databases WHERE name = 'tempdb';

--This could easily be in-line below. But I'm going to do it here for clarity
SELECT @IntervalMinutes = COALESCE(DATEDIFF(mi,@LastLogDateTime,@LogDateTime),0);

--We use a physical table for our temp table, because we need to compare between this run & last run
INSERT INTO Monitor_temp_WaitStats (LogDateTime, WaitType, WaitingTaskCount, WaitTimeMs, MaxWaitTimeMs, SignalWaitTimeMs)
SELECT @LogDateTime AS LogDateTime,
    wait_type,
	waiting_tasks_count,
	wait_time_ms,
	max_wait_time_ms,
	signal_wait_time_ms
FROM sys.dm_os_wait_stats;


--Now compare this most recent collection with the previous collection, and compute the delta.
INSERT INTO Monitor_WaitStats (LogDateTime, WaitType, IntervalMinutes, WaitingTaskCount, WaitTimeMs, MaxWaitTimeMs, SignalWaitTimeMs)
SELECT LogDateTime = @LogDateTime,
	WaitType = new.WaitType,
	IntervalMinutes = @IntervalMinutes,
	WaitingTaskCount = new.WaitingTaskCount - COALESCE(old.WaitingTaskCount,0),
	WaitTimeMs = new.WaitTimeMs - COALESCE(old.WaitTimeMs,0),
	MaxWaitTimeMs = new.MaxWaitTimeMs,
	SignalWaitTimeMs = new.SignalWaitTimeMs - COALESCE(old.SignalWaitTimeMs,0)
FROM Monitor_temp_WaitStats new
LEFT JOIN Monitor_Temp_WaitStats old
	ON old.WaitType = new.WaitType AND old.LogDateTime = @LastLogDateTime
WHERE new.LogDateTime = @LogDateTime;


--Aggressively clean out temp table of everything except the most recent
WHILE @@ROWCOUNT <> 0
	DELETE TOP (1000) Monitor_temp_WaitStats 
	WHERE LogDateTime < DATEADD(hh,-72,@LogDateTime);
