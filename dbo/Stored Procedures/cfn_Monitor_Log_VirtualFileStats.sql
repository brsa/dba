﻿CREATE PROCEDURE dbo.cfn_Monitor_Log_VirtualFileStats
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150804
    This procedure logs file IO stats from the DMVs to cfn_Monitor_VirtualFileStats.
	Calculates a delta since the last run, and saves that delta to the table.
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME(),
	@LastLogDateTime datetime2(0),
	@IntervalMinutes int;

--When did we last check stats?
SELECT @LastLogDateTime = MAX(LogDateTime) FROM Monitor_VirtualFileStats;

--If the instance rebooted since then, we should consider that restart time as our last time.
    -- Stats are only cumulative since start time, so @IntervalMinutues should reflect a shorter time.
	-- Also if we just restarted, we oughtn't compare against last collection to compute a delta.
SELECT @LastLogDateTime = CASE WHEN create_date > @LastLogDateTime  OR @LastLogDateTime IS NULL THEN create_date 
								ELSE @LastLogDateTime END 
FROM sys.databases WHERE name = 'tempdb';

--This could easily be in-line below. But I'm going to do it here for clarity
SELECT @IntervalMinutes = COALESCE(DATEDIFF(mi,@LastLogDateTime,@LogDateTime),0);

--We use a physical table for our temp table, because we need to compare between this run & last run
INSERT INTO Monitor_Temp_VirtualFileStats (LogDateTime, DbName, LogicalFileName, FileSizeBytes, NumReads, 
								NumWrites, BytesRead, BytesWritten, IoStallReadMs, IoStallWriteMs, IoStall)
SELECT @LogDateTime AS LogDateTime, 
    db_name(mf.database_id) AS DBName,
    mf.name AS LogicalFileName,
    vfs.size_on_disk_bytes AS FileSizeBytes,
    vfs.num_of_reads AS NumReads,
    vfs.num_of_writes AS NumWrites,
    vfs.num_of_bytes_read AS BytesRead,
    vfs.num_of_bytes_written AS BytesWritten,
    vfs.io_stall_read_ms AS IoStallReadMs,
    vfs.io_stall_write_ms AS IoStallWriteMs,
    --2014 only. vfs.io_stall_queued_read_ms AS IoStallQueuedReadMs,
    --2014 only. vfs.io_stall_queued_write_ms AS IoStallQueuedWriteMs,
    vfs.io_stall AS IoStall
FROM sys.master_files mf
-- 2008R2 won't let me APPLY this DMV, which is silly.
-- But I'll just have to so a JOIN and pass NULL params for now.
-- Eventually, we can switch back, once we're off 2008. 
--CROSS APPLY sys.dm_io_virtual_file_stats(mf.database_id,mf.file_id) vfs;
JOIN sys.dm_io_virtual_file_stats(NULL,NULL) vfs ON vfs.database_id = mf.database_id and vfs.file_id = mf.file_id;


--Now compare this most recent collection with the previous collection, and compute the delta.
--make sure you do a left join here, so you can get new data files. duh.
INSERT INTO Monitor_VirtualFileStats(LogDateTime, DbName, LogicalFileName, IntervalMinutes, FileSizeGrowthMb, NumReads, NumWrites, 
                       BytesRead, BytesWritten, IoStallReadMs, IoStallWriteMs, IoStallQueuedReadMs, IoStallQueuedWriteMs, IoStall)
SELECT new.LogDateTime,
       new.DbName,
	   new.LogicalFileName,
	   @IntervalMinutes,
	   (new.FileSizeBytes - COALESCE(old.FileSizeBytes,0)) / 1024/1024 AS FileGrowthMb,
	   new.NumReads - COALESCE(old.NumReads,0) AS NumReads,
	   new.NumWrites - COALESCE(old.NumWrites,0) AS NumWrites,
	   new.BytesRead - COALESCE(old.BytesRead,0) AS BytesRead,
	   new.BytesWritten - COALESCE(old.BytesWritten,0) AS BytesWritten,
	   new.IoStallReadMs - COALESCE(old.IoStallReadMs,0) AS IoStallReadMs,
	   new.IoStallWriteMs - COALESCE(old.IoStallWriteMs,0) AS IoStallWriteMs,
	   new.IoStallQueuedReadMs - COALESCE(old.IoStallQueuedReadMs,0) AS IoStallQueuedReadMs,
	   new.IoStallQueuedWriteMs - COALESCE(old.IoStallQueuedWriteMs,0) AS IoStallQueuedWriteMs,
	   new.IoStall - COALESCE(old.IoStall,0) AS IoStall
FROM Monitor_Temp_VirtualFileStats new
LEFT JOIN Monitor_Temp_VirtualFileStats old
	ON old.DbName = new.DbName AND old.LogicalFileName = new.LogicalFileName
	AND old.LogDateTime = @LastLogDateTime
WHERE new.LogDateTime = @LogDateTime;


--Aggressively clean out temp table of everything except the most recent
WHILE @@ROWCOUNT <> 0
	DELETE TOP (1000) Monitor_temp_VirtualFileStats
	WHERE LogDateTime < DATEADD(hh,-72,@LogDateTime);