﻿/*****************************************************************************************
Name: cfn_Cleanup_PermissionsTables

Desc: Delete old and inactive permissions from the Inventory Permissions tables

Parameters: @RetainDays - number of days to retain data in the tables

Dependencies: 
	dbo.sys_Inventory_sys_database_permissions
	dbo.sys_Inventory_sys_database_principals
	dbo.sys_Inventory_sys_database_role_members
	dbo.sys_Inventory_sys_server_permissions
	dbo.sys_Inventory_sys_server_principals
	dbo.sys_Inventory_sys_server_role_members

Auth: MDAK

Date: 08/19/15

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
	
*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Cleanup_PermissionsTables @RetainDays = 10;
*/

CREATE PROCEDURE [dbo].[cfn_Cleanup_PermissionsTables]
	@RetainDays INT = 10
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TableName NVARCHAR(128),
		@CleanupDateTime DATETIME2(0),
		@SQL NVARCHAR(MAX);

	SELECT @CleanupDateTime = DATEADD(dd, - 1 * @RetainDays, SYSDATETIME());

	PRINT 'Cleaning up inactive permissions inventory data older than ' + CONVERT(VARCHAR(30),@CleanupDateTime,120);

	DECLARE tbl_cursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT name
	FROM sys.objects
	WHERE type = 'U'
		AND name LIKE 'Inventory_sys_%';

	--Use cursor to cleanup all cfn_Inventory_sys_xxxx tables
	OPEN tbl_cursor;

	FETCH NEXT FROM tbl_cursor
	INTO @TableName;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = N'WHILE EXISTS (SELECT 1 FROM ' + QUOTENAME(@TableName) + ' WHERE Active = 0 AND DateRemoved <= @CleanupDateTime)
			DELETE FROM ' + QUOTENAME(@TableName) + ' WHERE Active = 0 AND DateRemoved <= @CleanupDateTime;';
	
		EXEC sp_executesql @stmt=@SQL, @params=N'@CleanupDateTime datetime2(0)', @CleanupDateTime = @CleanupDateTime;
	
		FETCH NEXT FROM tbl_cursor
		INTO @TableName;
	END;

	CLOSE tbl_cursor;
	DEALLOCATE tbl_cursor;
END;