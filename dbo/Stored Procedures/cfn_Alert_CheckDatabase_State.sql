﻿/*****************************************************************************************
Name: cfn_Alert_CheckDatabase_State

Description: Sends out an alert for inaccessable databases.

Parameters: @Recipients - Email recipient for the alert.

Author: Ajay H.

Date: 12/22/14

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
09/04/15		MDAK					Excluded mirrored secondary databases from alert.
										Add "User Access" column in the output.
*****************************************************************************************/

/***	Sample Execution	***
EXEC cfn_Alert_CheckDatabase_State @EmailRecipients = 'AHirapara@commonwealth.com';
*/

CREATE PROCEDURE [dbo].[cfn_Alert_CheckDatabase_State]
@EmailRecipients VARCHAR(1000)=NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;
	DECLARE 
		@EmailFrom varchar(max),
		@EmailBody nvarchar(max),
		@EmailSubject varchar(100),
		@checkDate DATETIME = GETDATE();
	DECLARE @tmpName TABLE (servername VARCHAR(128),name VARCHAR(128),state_desc VARCHAR(60),user_access_desc VARCHAR(60));

	INSERT INTO @tmpName( servername ,name ,state_desc ,user_access_desc )
	SELECT @@SERVERNAME AS servername, d.name, d.state_desc, d.user_access_desc
	FROM sys.databases d
	JOIN sys.database_mirroring m ON d.database_id = m.database_id
	WHERE (d.state <> 0
		OR d.user_access <> 0)
		AND d.log_reuse_wait_desc NOT IN ('REPLICATION', 'LOG_BACKUP')
		AND COALESCE(m.mirroring_role,0) <> 2;

	IF EXISTS (SELECT name FROM @tmpName)
	BEGIN
		--print 'found'	
		IF (
		SELECT DATEDIFF(SECOND,ISNULL(MAX(sent_date),@checkDate),@checkDate) 
		FROM [msdb].[dbo].[sysmail_mailitems] WITH (NOLOCK)
		WHERE subject = 'ALERT: Database Not Accessible Alert'
		AND sent_status = 1 ) = 0 OR (
		SELECT DATEDIFF(SECOND,ISNULL(MAX(sent_date),@checkDate),@checkDate) 
		FROM [msdb].[dbo].[sysmail_mailitems] WITH (NOLOCK)
		WHERE subject = 'ALERT: Database Not Accessible Alert'
		AND sent_status = 1 ) > 54000 --15 minutes
		BEGIN
			SELECT @EmailBody=dbo.cfn_EmailCss();

			SET @EmailBody =@EmailBody+
			'<H1><font size="2">Database Not Accessible Alert: </font></H1>' +
			'<table border>' +
				'<tr>
				<TH>Server Name</TH>
				<TH>Database Name</TH>
				<TH>State Description</TH>
				<TH>User Access</TH>
				</tr>' +
			CAST ( ( SELECT DISTINCT
							 td = servername,''
							,td = name,''
							,td = state_desc,''
							,td = user_access_desc,''
			FROM @tmpName FOR XML PATH ('tr') , TYPE  ) AS NVARCHAR(MAX) ) + N'</table>' ; 

			SELECT @EmailBody = @EmailBody + '<hr>' + dbo.cfn_EmailServerInfo();
			SET @EmailSubject = 'ALERT: Database Not Accessible Alert';
			SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

			EXEC msdb.dbo.sp_send_dbmail
				@recipients = @EmailRecipients,
				@from_address = @EmailFrom,
				@subject = @EmailSubject,
				@body = @EmailBody,
				@body_format = 'HTML',
				@importance = 'High';		
		END;
	END;

END;
