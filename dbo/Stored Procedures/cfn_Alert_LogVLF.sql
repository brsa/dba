﻿CREATE PROC [dbo].[cfn_Alert_LogVLF]
	@VlfAlertThreshold tinyint = 75,
	@EmailRecipients varchar(100) = NULL,
    @Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141001
    This Alert checks all tran logs for the number of VLFs. When transaction logs contains
    "too many" VLFs, an email alert is sent to @EmailRecipients with a summary of all logs 
    that have VLF fragmentation.
    The @VlfAlertThreshold controls what DBs should be included in the alert
       
PARAMETERS
* @VlfAlertThreshold - Default 50 - Number of VLFs. If a DB has more than this number of VLFs,
		that DB's info will be included in the email alert.
* @EmailRecipients - semicolon-separated list of email addresses to send alert
EXAMPLES:

**************************************************************************************************
MODIFICATIONS:
    20150107 - In email, specify from address, and include standard server info in footer
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @DbName sysname,
        @SQL nvarchar(max),
        @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
	@EmailSubject varchar(255);

SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

CREATE TABLE #Results (dbname sysname, logfilename sysname, physicalname sysname,growth sysname);
EXEC sp_MSforeachdb 
  'INSERT #Results
     SELECT ''?'', name, physical_name,
            CASE WHEN growth  = 0 THEN ''fixed'' ELSE
              CASE WHEN is_percent_growth = 0 THEN CONVERT(varchar(10), (growth/128)) +'' MB''
              WHEN  is_percent_growth = 1 THEN CONVERT(varchar(10), growth) +'' PERCENT''  END
              END AS [growth]
       FROM [?].sys.database_files
       WHERE type_desc = ''LOG'';  ';
--Getting number of VLFs
ALTER TABLE #Results ADD vlf int;
CREATE TABLE #LogInfo
  (RecoveryUnitID tinyint,
   fileid tinyint,
   file_size bigint,
   start_offset bigint,
   FSeqNo int,
   [status] tinyint,
   parity tinyint,
   create_lsn numeric(25,0) );


DECLARE db_cur CURSOR FOR
  SELECT dbname FROM #Results ORDER BY dbname;
  
OPEN db_cur;
FETCH NEXT FROM db_cur INTO @DbName;
WHILE @@FETCH_STATUS=0
  BEGIN
    DELETE FROM #LogInfo;
    --RecoveryUnitID column is only used in 2012 and up.
    IF CAST(LEFT(CAST(SERVERPROPERTY('ProductVersion') as varchar(12)),2) as tinyint) <= 10
    	SET @sql='Insert #LogInfo(fileid, file_size, start_offset, FSeqNo, [status], parity, create_lsn) Exec(''DBCC loginfo ('+QUOTENAME(@dbname)+')'')';
    ELSE 
    	SET @sql='Insert #LogInfo(RecoveryUnitID, fileid, file_size, start_offset, FSeqNo, [status], parity, create_lsn) Exec(''DBCC loginfo ('+QUOTENAME(@dbname)+')'')';
    PRINT @sql;
    EXEC (@sql);
    UPDATE #Results SET vlf=(SELECT COUNT(*) FROM #LogInfo) WHERE dbname=@DbName;
    FETCH Next FROM db_cur INTO @DbName;
  END;
CLOSE db_cur;
DEALLOCATE db_cur;

IF @Debug = 1
BEGIN
    SELECT * FROM #Results ORDER BY vlf DESC;
END;

-- If nothing over threshold, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #Results WHERE dbo.dm_hadr_db_role(DbName) IN ('PRIMARY','ONLINE') AND vlf >= @VlfAlertThreshold)
	RETURN (0);


IF @Debug = 0
BEGIN
    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

    --Build the body of the email based on the #Results
    set @EmailBody  = @EmailBody +
             N'<H2>Databases with high number of VLFs:</H2>' +
             N'<table> <tr>' +
             N'<th> Database Name </th>' +
             N'<th> Logical File Name </th>' + 
             N'<th> File Path </th>' +
             N'<th> AutoGrowth </th>' +
             N'<th> Number VLFs </th></tr>'+
	     CAST(( SELECT
		     td = dbname, '',
		     td = logfilename, '',
		     td = physicalname, '',
		     td = growth, '',
		     td = CAST(vlf AS nvarchar(10)), ''
		     FROM #Results
             WHERE dbo.dm_hadr_db_role(DbName) IN ('PRIMARY','ONLINE')
			 AND vlf >= @VlfAlertThreshold
             ORDER BY dbname
		     FOR XML PATH ('tr'), ELEMENTS
		     ) AS nvarchar(max)) +
            N'</table>';


	SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

    SET @EmailSubject = 'ALERT: Too Many VLFs'
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;