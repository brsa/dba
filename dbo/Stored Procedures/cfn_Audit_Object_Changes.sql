﻿/*****************************************************************************
AUTHOR : AJAY H.
CREATE DATE : 05/10/2016
DESCRIPTION: Collect database stats
SAMPLE CALL:
EXEC dbo.cfn_Audit_Object_Changes;

**HISTORY**
-----------------------------------------------------------------------------------
Update Date    Updated By          Notes
-----------------------------------------------------------------------------------         
7/28/2021           SMcKenna                   INC0020035  - Multiple values returned due to several audits -                                                              
       Added is_state_enabled = 1 to filter out disabled audits
******************************************************************************/
CREATE PROC [dbo].[cfn_Audit_Object_Changes]
AS
SET NOCOUNT ON;
DECLARE @AuditFilePath VARCHAR(500)=(SELECT REPLACE(log_file_path+log_file_name,'.sqlaudit','*.sqlaudit') FROM sys.server_file_audits WHERE name LIKE '%object_changes' AND is_state_enabled = 1);
DECLARE @LogDateTime DATETIME2=SYSDATETIME();
DECLARE @LastEventTime DATETIME2=(SELECT MAX(Event_Time) FROM dbo.Monitor_ObjectChanges);

IF @LastEventTime IS NULL
       SET @LastEventTime= DATEADD(DD,-100,@LogDateTime);

--  DROP TABLE #audit_data;
CREATE TABLE #audit_data
       (event_time DATETIME2(7) NOT NULL ,server_instance_name NVARCHAR(128) ,class_type VARCHAR(2) ,database_name NVARCHAR(128) ,server_principal_name NVARCHAR(128) 
       ,object_name NVARCHAR(128) ,schema_name NVARCHAR(128) , statement NVARCHAR(4000),session_id SMALLINT NOT NULL);

CREATE CLUSTERED INDEX CIX_audit_data ON #audit_data(event_time);

--PRINT @AuditFilePath;
IF @AuditFilePath<>''
BEGIN

       INSERT INTO #audit_data
             ( event_time ,server_instance_name ,class_type ,database_name ,server_principal_name ,object_name ,schema_name ,statement ,session_id)
       SELECT  A.event_time ,A.server_instance_name ,A.class_type ,A.database_name ,A.server_principal_name ,A.object_name ,A.schema_name ,A.statement ,A.session_id
       FROM    sys.fn_get_audit_file(@AuditFilePath, DEFAULT, DEFAULT) A;

       INSERT INTO dbo.Monitor_ObjectChanges
                    ( LogDateTime , Event_Time ,ServerName ,DatabaseName ,UserName ,ObjectType ,ObjectName ,SchemaName ,Statement ,SPID )
       SELECT  @LogDateTime ,A.event_time ,A.server_instance_name ,A.database_name ,A.server_principal_name ,C.class_type_desc ,A.object_name ,A.schema_name ,A.statement ,A.session_id
       FROM    #audit_data A
                    LEFT JOIN sys.dm_audit_class_type_map C ON C.class_type = A.class_type
       WHERE   RTRIM(LTRIM(CONVERT(VARCHAR(8000), A.statement))) <> ''
                    AND A.statement NOT LIKE 'truncate table %'
                    AND A.statement NOT LIKE 'UPDATE STATISTICS %'
                    AND A.statement NOT LIKE 'ALTER INDEX % REBUILD %'
                    AND A.statement NOT LIKE 'EXEC %'
                    AND A.database_name NOT IN ( 'distribution', 'tempdb', 'SSISDB' )
                    AND A.statement NOT LIKE 'set identity_insert %'
                    AND A.statement NOT LIKE 'OPEN %'
                    AND A.object_name <> 'dbo'
                    AND A.class_type <> 'CK' --Exclude "COLUMN ENCRYPTION KEY". We started seeing those after SQL 2017 upgrade
                    AND A.event_time > @LastEventTime;

END;
GO
