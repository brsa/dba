﻿


-- Need to put Error Handling EVERYWHERE & log errors to a table
-- ServiceBroker is asychronous, so errors can't bubble back up
-- to a user/application session to be handled there.

CREATE PROCEDURE [dbo].[cfn_SBBackupFull]
AS
SET NOCOUNT ON 

DECLARE @Message table 
    (ConversationHandle uniqueidentifier,
     ServiceName nvarchar(512),
     ContractName nvarchar(256),
     MessageType nvarchar(256),
     MessageXML nvarchar(max));

DECLARE @SBQueueName sysname = 'BackupFullQueue',
    @ConversationHandle uniqueidentifier,
    @ServiceName nvarchar(512),
    @ContractName nvarchar(256),
    @MessageType nvarchar(256),
    @MessageXML xml,
    @NewConversation uniqueidentifier,
    @ServerName sysname,
    @DbName sysname,
    @BackupPathFull nvarchar(512),
    @BackupPathLog nvarchar(512),
    @Compressed bit,
    @NumStreams tinyint,
    @RetainDaysFull tinyint,
    @RetainDaysLog tinyint,
    @IsInAG bit,
    @SQL nvarchar(max),
    @ErrNumber int,
    @ErrSeverity int,
    @ErrState int,
    @ErrMsg nvarchar(2048);

  
--Just process one command. Leave the rest in queue, in case the server crashes (or something) mid-backup.
BEGIN TRY
    	--SILLY WORKAROUND
		-- SQL 2008 doesn't like RECEIVE to be the first statement in a TRY-CATCH block
		-- To make this work, put the RECEIVE in an IF statement that is always true!
		-- We should remove this once we're fully on SQL 2014
    IF 1=1
		RECEIVE TOP(1) conversation_handle, service_name, service_contract_name, message_type_name, CONVERT(NVARCHAR(MAX), message_body)
		FROM dbo.BackupFullQueue
		INTO @Message;
END TRY
BEGIN CATCH
    SET @ErrNumber = ERROR_NUMBER();
    SET @ErrSeverity = ERROR_SEVERITY();
    SET @ErrState = ERROR_STATE();
    SET @ErrMsg = ERROR_MESSAGE();
    SET @ErrMsg = 'Unable to Receive from Queue - ' + @ErrMsg;
    INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, QueueName, ErrorNumber, ErrorMessage)
    VALUES (GETDATE(), @SBQueueName, @ErrNumber, @ErrMsg);
    RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
    RETURN (1);
END CATCH;

SELECT @ConversationHandle = ConversationHandle,
       @ServiceName = ServiceName,
       @ContractName = ContractName,
       @MessageType = MessageType,
       @MessageXML = MessageXML
FROM @Message;
--
IF @@ROWCOUNT = 0
 BEGIN
    PRINT 'No messages to process';
    RETURN (0);
 END;

 IF @MessageType = '//Backup/Message/Full'
BEGIN
    --Parse the XML
    SELECT @ServerName = Tbl.Col.value('ServerName[1]', 'sysname'),  
           @DbName     = Tbl.Col.value('DbName[1]', 'sysname'),  
           @BackupPathFull = Tbl.Col.value('BackupPathFull[1]', 'nvarchar(512)'),
           @BackupPathLog = Tbl.Col.value('BackupPathLog[1]', 'nvarchar(512)'),
           @Compressed = Tbl.Col.value('Compressed[1]', 'bit'),
           @NumStreams = Tbl.Col.value('NumStreams[1]', 'tinyint'),
           @RetainDaysFull = Tbl.Col.value('RetainDaysFull[1]', 'tinyint'),
           @RetainDaysLog = Tbl.Col.value('RetainDaysLog[1]', 'tinyint')
    FROM   @MessageXML.nodes('//row') Tbl(Col);
    
    -- Validations on parameters
    IF @ServerName <> @@SERVERNAME
     BEGIN
     	SET @ErrNumber = NULL;
        SET @ErrSeverity = 16;
        SET @ErrState = 1;
        SET @ErrMsg = 'ServerName specified in message does not match this server.';
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
     END
    --End Validations
    
    -- Build the Backup statement
    -- just want to execute Ola's Backup code
    	--Convert @RetainDays into hours when passing to DatabaseBackup
    	--Convert @Compressed from bit into Y/N
        --Use CopyOnly=Y & OverrideBackupPreference=Y for AG databases
    
	--WORKAROUND
		--Turning this into dynamic SQL to make it compatible with 2008, where the column doesn't exist.
		--Revert this to simple SQL once we're off 2008.
    SET @SQL = 'SELECT @IsInAG = CASE WHEN group_database_id IS NOT NULL THEN 1 ELSE 0 END
				FROM sys.databases 
				WHERE name = @DbName'
	EXEC sp_executesql @SQL, '@IsInAG bit out', @IsInAG = @IsInAG OUT
    
    --Full Backup command
    SET @SQL = 'EXEC DatabaseBackup @Databases = ''' + @DbName + ''', @Directory = ''' + @BackupPathFull + ''', '
             + ' @BackupType = ''FULL'', @Verify = ''Y'', ' 
             + CASE WHEN @RetainDaysFull > 0 THEN '@CleanupTime = ' + CAST(@RetainDaysFull*24 AS nvarchar(10)) + ', ' ELSE '' END 
             + ' @Checksum = ''Y'', @Compress = ''' + CASE WHEN @Compressed = 0 THEN 'N' ELSE 'Y' END + ''', '
             + CASE WHEN @NumStreams > 0 THEN ' @NumberOfFiles = ' + CAST(@NumStreams AS nvarchar(10)) + ', ' ELSE '' END 
             + CASE WHEN @IsInAG = 1 THEN ' @CopyOnly = ''Y'', @OverrideBackupPreference = ''Y'', ' ELSE '' END
             + '@LogToTable = ''Y'';' + CHAR(10);
    --Log backup command too
      --This is a workaround to ensure proper cleanup of log files on an AG when using Ola's backup code
      --@OverrideBackupPreference = Y to force a log backup on this server that has a recent full
    SET @SQL = @SQL + 'EXEC DatabaseBackup @Databases = ''' + @DbName + ''', @Directory = ''' + @BackupPathLog + ''', '
         + ' @BackupType = ''LOG'', @Verify = ''Y'', '
         + CASE WHEN @RetainDaysLog > 0 THEN '@CleanupTime = ' + CAST(@RetainDaysLog*24 AS nvarchar(10)) + ', ' ELSE '' END 
         + ' @Checksum = ''Y'', @Compress = ''' + CASE WHEN @Compressed = 0 THEN 'N' ELSE 'Y' END + ''', '
         + ' @NumberOfFiles = 1, @LogToTable = ''Y'', @OverrideBackupPreference = ''Y'';';

    --Validate that we appear to have built the @SQL string properly
        --When building dynamic SQL with this many variables, it's too easy to let a NULL sneak in.
    IF LEN(COALESCE(@SQL,'')) <10
     BEGIN
        SET @ErrNumber = NULL;
        SET @ErrSeverity = 16;
        SET @ErrState = 1;
        SET @ErrMsg = 'Error constructing dyanamic SQL call for DatabaseBackup.';
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
     END
       
    -- Execute the SQL
    BEGIN TRY
        --PRINT @SQL
        EXEC sp_executesql @stmt = @SQL;
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;

END
ELSE IF @MessageType = 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
 BEGIN
    --End the conversation
    BEGIN TRY
        END CONVERSATION @ConversationHandle;
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
 END
ELSE
 BEGIN -- No logic to handle this message type. Log/raise an error
    SET @ErrNumber = 50000;
    SET @ErrSeverity = 16;
    SET @ErrState = 1;
    SET @ErrMsg = 'MessageType "' + @MessageType + '" not supported';
    INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
    VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, CAST(@MessageXML AS varchar(max)));
    RAISERROR (@ErrMsg, @ErrSeverity, @ErrState);
    RETURN (1);
 END;