﻿CREATE PROCEDURE [dbo].[cfn_Alert_MSX_errors]
    @EmailRecipients varchar(max) = NULL,
    @Repair bit = 0,
    @Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150213
    This procedure checks msdb for errors when target servers (TSXs) are trying to download
    SQL Agent jobs from this server (the MSX)

	Currently only handles fixing JOBS (object_type = 1). For other object types, @Repair will do nothing.

PARAMETERS
* @EmailRecipients - delimited list of email addresses. Used for sending email alert.
* @Repair - if 1, Figure out SQL to re-initialize the stuck job, Maybe we can un-stuck it.
* @Debug - if 1, don't send an email, instead print resultset.
EXAMPLES:
* EXEC dbo.cfn_Alert_MSX_errors @Repair = 1, @Debug = 1
	* Generates output of stuck commands & SQL to fix it. DOES NOT perform repair
* EXEC dbo.cfn_Alert_MSX_errors @Repair = 0, @Debug = 1
	* Generates output of stuck commands. DOES NOT generate "fix sql" nor performs repair
* EXEC dbo.cfn_Alert_MSX_errors @Repair = 1, @Debug = 0
	* Generates no output. Performs repair & emails alert.
* EXEC dbo.cfn_Alert_MSX_errors @Repair = 0, @Debug = 0
	* Generates no output. Emails alert, but does not perform repair

**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;

SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

DECLARE @Sql nvarchar(max),
        @ObjectId uniqueidentifier,
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

CREATE TABLE #ServerStatus(
	ServerId int,
	ServerName nvarchar(30),
	Location nvarchar(200),
	TimeZoneAdj int,
	EnlistDate datetime,
	LastPollDate datetime,
	ServerStatus int,
	UnreadInstructions int,
	LocalTime datetime,
	EnlistedByNtUser nvarchar(100),
	PollInterval int
	);

CREATE TABLE #Results (
    InstanceID int,
    TargetServer sysname,
	JobName sysname,
    ErrorMessage nvarchar(2048),
    DatePosted datetime2(0),
	LastPollDate datetime2(0),
	UnreadInstructions int
    );

CREATE TABLE #Sql (
    ID int identity(1,1),
    ObjectID uniqueidentifier,
    SqlStatement nvarchar(max)
    );

INSERT INTO #ServerStatus(ServerId, ServerName, Location, TimeZoneAdj,EnlistDate, LastPollDate, ServerStatus, UnreadInstructions, LocalTime, EnlistedByNtUser, PollInterval)
EXEC msdb.dbo.sp_help_targetserver;

INSERT INTO #Results (InstanceID, TargetServer, JobName, ErrorMessage, DatePosted, LastPollDate, UnreadInstructions)
SELECT dl.instance_id, dl.target_server, ISNULL(j.name,'') AS JobName, dl.error_message, dl.date_posted, s.LastPollDate, s.UnreadInstructions
FROM msdb.dbo.sysdownloadlist dl
JOIN #ServerStatus s ON s.ServerName = dl.target_server 
		AND (s.ServerStatus <> 1 OR s.UnreadInstructions > 10)
LEFT JOIN msdb.dbo.sysjobs j ON dl.object_id = j.job_id AND dl.object_type = 1 --jobs...I should handle schedules too, eh?
WHERE (dl.error_message IS NOT NULL
		OR dl.status = 0);


--Need to test this repair code to be sure it's reliable before turning it on.
--If running a repair, build SQL to re-initialize this job in 3 steps:
-- 1 - Delete existing instructions for the job.
-- 2 - post a command to delete the job from the TSX.
-- 3 - post a command to re-insert job at the TSX.

IF @Repair = 1
BEGIN
   INSERT INTO #Sql (ObjectID, SqlStatement)
   SELECT DISTINCT object_id,
		'DELETE dl FROM msdb.dbo.sysdownloadlist dl WHERE object_id = ''' + CAST(object_id AS varchar(50)) + '''' 
		   + 'AND target_server = ''' + target_server + ''';' + CHAR(13)
           + 'EXEC msdb.dbo.sp_post_msx_operation @operation = ''DELETE'', @object_type = ''JOB'', @job_id = ''' 
               + CAST(object_id AS varchar(50)) + ''', @specific_target_server = ''' + target_server + ''';' + CHAR(13)
           + 'EXEC msdb.dbo.sp_post_msx_operation @operation = ''INSERT'', @object_type = ''JOB'', @job_id = ''' 
               + CAST(object_id AS varchar(50)) + ''', @specific_target_server = ''' + target_server + ''';'
   FROM msdb.dbo.sysdownloadlist 
   WHERE instance_id IN (SELECT InstanceID FROM #Results)
   AND object_type = 1; --Jobs; --need to handle other types, too, probably.
END;

-- in Debug Mode, output #Results & #Sql tables
IF @Debug > 0
BEGIN
    SELECT * FROM #Results ORDER BY InstanceID;
    SELECT * FROM #Sql ORDER BY ID;
END;

-- If no rows returned, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #Results)
    RETURN (0);

--OK, there's stuff to repair, and we're not in Debug mode. 
IF (@Debug = 0 AND @Repair = 1)
BEGIN
    DECLARE sql_cur CURSOR FOR
        SELECT SqlStatement, ObjectId FROM #Sql ORDER BY ID;
    
    OPEN sql_cur;
    FETCH NEXT FROM sql_cur INTO @Sql, @ObjectId;
    WHILE @@FETCH_STATUS = 0
    BEGIN
		-- re-initalize it using previously generated SQL
        EXEC sp_executesql @Sql;

        FETCH NEXT FROM sql_cur INTO @Sql, @ObjectId;
    END;
    CLOSE sql_cur;
    DEALLOCATE sql_cur;
END;

--Send an email only if @Debug = 0
IF @Debug = 0
BEGIN
    
    --We could try to "repair" the issue by using sp_post_msx_operation 
    -- to post delete/insert instructions. But if that doesn't work, we'd still need to alert

    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();
    
    --Build the body of the email based on the #Results
    SET @EmailBody = @EmailBody + N'<h2>Errors downloading instructions from MSX</h2>' + CHAR(10); 

	IF @Repair = 1
		SET @EmailBody = @EmailBody + N'<br><h3>Failed instructions were automatically re-initialized.</h3>' + CHAR(10); 

    
	SET @EmailBody = @EmailBody + '<br><div>If you are unable to resolve the below errors directly, you can re-initialize the job by '
					+ 'removing the job from the server (on the MSX), waiting for at least a minute, then re-adding it. </div><br>';
    
	SET @EmailBody = @EmailBody +
            N'<table><tr>' +
            N'<th>Server Name</th>' +
            N'<th>Job Name</th>' +
            N'<th>Date Posted</th>' +
            N'<th>Error Message</th>' +
            N'<th>Unread Instructions</th> </tr>' +
            CAST(( SELECT
                    td = TargetServer, '',
                    td = JobName, '',
                    td = CONVERT(varchar(20),DatePosted,120), '',
                    td = COALESCE(ErrorMessage,''), '',
                    td = CAST(UnreadInstructions AS varchar(10)), ''
                    FROM #Results
                    ORDER BY TargetServer, InstanceID
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';

	SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();
	
    SET @EmailSubject = 'ALERT: Errors downloading SQL Agent instructions from MSX:';

    EXEC msdb.dbo.sp_send_dbmail
        @Recipients = @EmailRecipients,
        @Subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';


END;

DROP TABLE #Results;
GO