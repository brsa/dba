﻿


/*****************************************************************************************
Name: cfn_hexadecimal

Desc: Converts a binary value to hexadecimal.
	This is a rename of Microsoft's sp_hexadecimal script. Used with cfn_Server_Logins.
	http://blogs.msdn.com/b/saponsqlserver/archive/2012/03/29/sql-server-2012-alwayson-part-5-preparing-to-build-an-alwayson-availability-group.aspx

Parameters:
	@binvalue
	@hexvalue - OUTPUT

Dependencies:

Auth: MDAK

Date: 10/24/14

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
	
*****************************************************************************************/

/***	Sample Execution	***
DECLARE	@hexvalue varchar(514)
EXEC dbo.cfn_hexadecimal @binvalue = 1234, @hexvalue = @hexvalue OUTPUT
SELECT @hexvalue as N'@hexvalue'
*/

CREATE PROCEDURE [dbo].[cfn_hexadecimal]
    @binvalue varbinary(256),
    @hexvalue varchar (514) OUTPUT
AS
DECLARE @charvalue varchar (514)
DECLARE @i int
DECLARE @length int
DECLARE @hexstring char(16)
SELECT @charvalue = '0x'
SELECT @i = 1
SELECT @length = DATALENGTH (@binvalue)
SELECT @hexstring = '0123456789ABCDEF'
WHILE (@i <= @length)
BEGIN
  DECLARE @tempint int
  DECLARE @firstint int
  DECLARE @secondint int
  SELECT @tempint = CONVERT(int, SUBSTRING(@binvalue,@i,1))
  SELECT @firstint = FLOOR(@tempint/16)
  SELECT @secondint = @tempint - (@firstint*16)
  SELECT @charvalue = @charvalue +
    SUBSTRING(@hexstring, @firstint+1, 1) +
    SUBSTRING(@hexstring, @secondint+1, 1)
  SELECT @i = @i + 1
END

SELECT @hexvalue = @charvalue