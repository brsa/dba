﻿CREATE PROCEDURE [dbo].[cfn_Monitor_Log_DataFile]
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150605
    This procedure logs information on all data files & their sizes for all databases.

PARAMETERS
* NONE
**************************************************************************************************
MODIFICATIONS:
    20150101 - 
*************************************************************************************************/
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME();


CREATE TABLE #FileSizeInfo 
  ( 
     DbName           NVARCHAR(128), 
     LogicalFileName  NVARCHAR(128), 
     FileType         NVARCHAR(10),
     FileSizeMB       INT, 
     SpaceUsedMB      INT, 
     FreeSpaceMB      INT, 
     FreeSpacePct     VARCHAR(7),
	 GrowthAmount     VARCHAR(20), 
     PhysicalFileName NVARCHAR(520)
  ) 


-- Because of log-shipped databases, we want to use sys.master_files for the file location NOT sys.sysfiles
	-- sys.master_files will show the location on *this* server. 
	-- sys.sysfiles in the DB will show the location of the files on the *primary* server.
	-- Using sys.master_files has the right location in all cases.
-- Because of TempDB, we want to use sys.sysfiles for the file size, not sys.master_files
	-- sys.master_files will show the *starting* file size, not the actual file size.
	-- sys.sysfiles will show the *current* file size
	-- Using sys.sysfiles has the right current file size in all cases.
INSERT #FileSizeInfo (DbName, FileSizeMB, SpaceUsedMB, GrowthAmount, LogicalFileName, PhysicalFileName, FileType, FreeSpaceMB, FreeSpacePct) 
EXEC master.dbo.sp_MSforeachdb 
  'USE [?] 
    SELECT   ''?'' AS DatabaseName,   
	CAST(f.size/128.0 AS decimal(20,2)) AS FileSize, 
	CASE
		WHEN mf.type_desc = ''FILESTREAM'' THEN CAST(f.size/128.0 AS decimal(20,2))
		ELSE CAST(FILEPROPERTY(mf.name, ''SpaceUsed'')/128.0 as decimal (20,2)) 
	END AS ''SpaceUsed'', 
	CASE 
		WHEN mf.type_desc = ''FILESTREAM'' THEN NULL
		WHEN mf.is_percent_growth = 0 
			THEN convert(varchar,ceiling((mf.growth * 8192.0)/(1024.0*1024.0)))  + '' MB'' 
		ELSE convert (varchar, mf.growth) + '' Percent'' 
	END AS FileGrowth, mf.name AS LogicalFileName, 
	mf.physical_name AS PhysicalFileName, mf.type_desc AS FileType,
	CAST(f.size/128.0 - CAST(FILEPROPERTY(mf.name, ''SpaceUsed'' ) AS int)/128.0 AS int) AS FreeSpaceMB,   
	CAST(100 * (CAST (((f.size/128.0 -CAST(FILEPROPERTY(mf.name,   
		''SpaceUsed'' ) AS int)/128.0)/(f.size/128.0))   AS decimal(4,2))) AS varchar(8)) + ''%'' AS FreeSpacePct 
	FROM sys.master_files mf 
	JOIN [?].sys.database_files f ON f.file_id = mf.file_id AND mf.database_id = db_id(''?'')
	' 
 
 

INSERT INTO Monitor_DataFile (LogDateTime, DbName, LogicalFileName, FileType, FileSizeMB, 
						SpaceUsedMB, FreeSpaceMB, GrowthAmount, PhysicalFileName)
SELECT @LogDateTime, DbName, LogicalFileName, FileType, FileSizeMB, SpaceUsedMB, 
		FreeSpaceMB, GrowthAmount, PhysicalFileName
FROM #FileSizeInfo

