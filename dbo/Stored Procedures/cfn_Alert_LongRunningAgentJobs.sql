﻿/*****************************************************
Author:	AJAY HIRAPARA
Create Date: 11/17/2017
Purpose: Find/stop long running job.

SAMPLE CALL: EXEC DBA.dbo.cfn_Alert_LongRunningAgentJobs @Debug = 1;

**Change History**
-----------------------------------------------------------------------------------
UpdateDate    UpdatedBy          Notes
-----------------------------------------------------------------------------------	

*********************************************************/
CREATE PROC [dbo].[cfn_Alert_LongRunningAgentJobs]
@EmailRecipients varchar(100) = NULL,
@StopLongRunningJobs BIT = 0,
@AlertThresholdInMinutes INT=180 ,
@Debug BIT = 1
AS
BEGIN
SET NOCOUNT ON;

	SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

	DECLARE @dtChecked DATETIME=GETDATE();

	DECLARE @LongRunningJobs TABLE 
		(
			job_id UNIQUEIDENTIFIER
			,Name sysname
			,Description NVARCHAR(512)
			,LastRunTime DATETIME
			,Status NCHAR(30)
			,iID INT IDENTITY(1,1)
		);

	INSERT INTO @LongRunningJobs  (job_id,Name,Description ,LastRunTime ,Status)
	SELECT 
		j.job_id
		,CONVERT(VARCHAR(50), j.name) AS name
		,CONVERT(VARCHAR(100), j.description) AS description
		,S.LastRunTime
        ,S.JobOutcome
	FROM msdb.dbo.sysjobs j
	JOIN msdb.dbo.syscategories S2 ON S2.category_id = j.category_id 
	CROSS APPLY dbo.cfn_AgentJobStatus(j.name) S
	WHERE j.enabled=1
		AND s.JobOutcome='Running'
		AND DATEDIFF(MINUTE, s.LastRunTime, @dtChecked) > @AlertThresholdInMinutes /*180 minutes threshold*/
		AND s2.name NOT LIKE 'REPL%' /*Exclude replication jobs*/
		AND J.name NOT LIKE '%DB-Engine%' /*Exclude PPS engine jobs*/
		AND J.name NOT LIKE 'MAINTENANCE: %' 
		AND J.name NOT LIKE 'BACKUP: %' 
		AND J.name NOT LIKE '%LONG_RUNNING%'
		AND J.name NOT LIKE 'SSIS: Trading - TaxLot - DataLoad%';
/*---------------------------
Stop long running jobs
----------------------------*/
	--/*
	IF @StopLongRunningJobs=1
	BEGIN
		DECLARE @iID INT=1
			,@JobID NVARCHAR(500);

		WHILE @iID <= (SELECT MAX(IID) FROM @LongRunningJobs LRJ)
		BEGIN

			SELECT @JobID=job_id
			FROM @LongRunningJobs l
			WHERE iID=@iID;

			EXEC msdb.dbo.sp_stop_job @job_id =@JobID;

			SET @iID=@iID+1;

		END;
	END;
	--*/

IF @Debug=1
BEGIN
	SELECT @@SERVERNAME AS ServerName,job_id ,Name ,Description ,LastRunTime ,Status , iID
		FROM @LongRunningJobs;
	RETURN
END;

/*--------------------------------------------------
Send out email for summary of long running jobs.
----------------------------------------------------*/
IF NOT EXISTS(SELECT * FROM @LongRunningJobs)
RETURN;

DECLARE @EmailBody VARCHAR(MAX)
,@Style VARCHAR(MAX)
,@StopOrRepoted VARCHAR(MAX)
,@subject VARCHAR(200);

SET @StopOrRepoted='<H1><font size="1">'+'This process is design to stop long running jobs so it can be handle before it start impacting other processes on the server. '
			+CASE WHEN @StopLongRunningJobs=1 THEN '<br> -> Below listed SQL Agent jobs were running for over 3 hours.<br><font color="red"> -> Process has stopped these jobs. It is required to review jobs, rerun it if needed and find the root cause of longer execution upon receipt of the email.:'
	ELSE '<br> ->Below listed SQL Agent jobs are running for over 3 hours.<br><font color="red"> -> It is required to review jobs, rerun it if needed and find the root cause of longer execution upon receipt of the email.' END  + '</font></H1>';
SET @subject='ALERT: Long-running SQL Agent Jobs';

SELECT @Style=dbo.cfn_EmailCss();

SET @EmailBody =@Style + @StopOrRepoted +
'<table border>' +
	'<tr>
	<TH>Job Name</TH>
	<TH>Description</TH>
	<TH>Run Time (Minutes)</TH>
	<TH>Status</TH>
	</tr>'; 

SELECT @EmailBody=COALESCE(@EmailBody+' ','')+ '<TR><TD>' + LTRIM(RTRIM(Name)) + '</TD><TD>' + LTRIM(RTRIM(Description))  + '</TD><TD>' + RTRIM(LTRIM(STR(DATEDIFF(MINUTE, LastRunTime, GETDATE())))) + '</TD><TD>' + CONVERT(VARCHAR(20), Status)+ '</TD></TR>'
FROM @LongRunningJobs lrj;

SET @EmailBody=@EmailBody+'</table>'; 

SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

EXEC msdb.dbo.sp_send_dbmail	
@recipients = @EmailRecipients
,@subject = @subject
,@body = @EmailBody
,@body_format = 'HTML'
,@importance = 'high';

END;