﻿/*****************************************************************************************
Name: cfn_Security_ServerLogins

Desc: Generates a script of all server level logins, server level permissions, and server role assignments.
	This will NOT assign missing permissions for a server role, but it will create missing server roles just so the script doesn't break.
	This is a modification of Microsoft's sap_help_revlogin script.
	http://blogs.msdn.com/b/saponsqlserver/archive/2012/03/29/sql-server-2012-alwayson-part-5-preparing-to-build-an-alwayson-availability-group.aspx

Parameters:
	@login_name - The login name to sync. Default value is NULL, which will sync all logins.

Dependencies: 
	dbo.cfn_hexadecimal

Auth: MDAK

Date: 10/24/14

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
05/05/15		MDAK					Renamed sproc
05/14/15		MDAK					Added logic to create missing server roles
11/24/15		MDAK					Removed logic to check database assignments
*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Security_ServerLogins;
EXEC dbo.cfn_Security_ServerLogins @login_name = 'C360USER';
*/

CREATE PROCEDURE [dbo].[cfn_Security_ServerLogins] @login_name NVARCHAR(128) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @name NVARCHAR(128) = NULL,
		@type VARCHAR(1) = NULL,
		@hasaccess INT = NULL,
		@denylogin INT = NULL,
		@is_disabled INT = NULL,
		@PWD_varbinary VARBINARY(256) = NULL,
		@PWD_string VARCHAR(514) = NULL,
		@SID_varbinary VARBINARY(85) = NULL,
		@SID_string VARCHAR(514) = NULL,
		@tmpstr VARCHAR(1024) = NULL,
		@tmpstr2 VARCHAR(1024) = NULL,
		@is_policy_checked VARCHAR(3) = NULL,
		@is_expiration_checked VARCHAR(3) = NULL,
		@defaultdb NVARCHAR(128) = NULL,
		@role_name NVARCHAR(128) = NULL,
		@permission NVARCHAR(128) = NULL,
		@crlf CHAR(2) = CHAR(13) + CHAR(10),
		@vchScript VARCHAR(MAX) = NULL;

	IF COALESCE(@login_name, '') = '' -- If no login specified, sync all logins
	BEGIN
		DECLARE login_curs CURSOR
		FOR
		SELECT p.sid,
			p.name,
			p.type,
			p.is_disabled,
			p.default_database_name,
			l.hasaccess,
			l.denylogin
		FROM master.sys.server_principals p
		LEFT JOIN master.sys.syslogins l ON l.name = p.name
		WHERE p.type IN (
				'S',
				'U',
				'G'
				)
			AND p.name <> 'sa'
			AND p.name NOT LIKE '##%'
			AND p.name NOT LIKE 'NT AUTHORITY\%'
			AND p.name NOT LIKE 'NT SERVICE\%'
			AND p.is_disabled = 0;
	END
	ELSE -- If a login name was specified, sync only that login
	BEGIN
		DECLARE login_curs CURSOR
		FOR
		SELECT p.sid,
			p.name,
			p.type,
			p.is_disabled,
			p.default_database_name,
			l.hasaccess,
			l.denylogin
		FROM master.sys.server_principals p
		LEFT JOIN master.sys.syslogins l ON l.name = p.name
		WHERE p.type IN (
				'S',
				'U',
				'G'
				)
			AND p.name = @login_name
			AND p.name NOT LIKE '##%'
			AND p.name NOT LIKE 'NT AUTHORITY\%'
			AND p.name NOT LIKE 'NT SERVICE\%';
	END;

	OPEN login_curs;

	FETCH NEXT
	FROM login_curs
	INTO @SID_varbinary,
		@name,
		@type,
		@is_disabled,
		@defaultdb,
		@hasaccess,
		@denylogin;

	IF (@@FETCH_STATUS = -1)
	BEGIN
		SET @vchScript = 'No login(s) found.';

		SELECT @vchScript;

		RETURN -1;
	END;

	SET @tmpstr = 'USE [master];';
	SET @vchScript = @tmpstr;
	SET @tmpstr = 'DECLARE @str varchar(300) = NULL;';
	SET @vchScript = @vchScript + @crlf + @tmpstr;
	SET @tmpstr = 'DECLARE @str2 varchar(1000) = NULL;';
	SET @vchScript = @vchScript + @crlf + @tmpstr;

	WHILE (@@FETCH_STATUS <> -1)
	BEGIN
		IF (@@FETCH_STATUS <> -2)
		BEGIN
			SET @vchScript = @vchScript + @crlf;
			SET @tmpstr = '/* Login: ' + @name + ' */';
			SET @vchScript = @vchScript + @crlf + @tmpstr;

			-- Obtain SID
			EXEC dbo.cfn_hexadecimal @SID_varbinary,
					@SID_string OUTPUT;

			IF (
				@type IN (
					'U',
					'G'
					)
				)
			BEGIN -- NT authenticated account/group
				SET @tmpstr = '    CREATE LOGIN ' + QUOTENAME(@name) + ' FROM WINDOWS WITH DEFAULT_DATABASE = ' + QUOTENAME(@defaultdb) + '';
			END;
			ELSE
			BEGIN -- SQL Server authentication
				-- Obtain password
				SET @PWD_varbinary = CAST(LOGINPROPERTY(@name, 'PasswordHash') AS VARBINARY(256));

				EXEC dbo.cfn_hexadecimal @PWD_varbinary,
					@PWD_string OUTPUT;

				-- Obtain password policy state
				SELECT @is_policy_checked = CASE is_policy_checked
						WHEN 1
							THEN 'ON'
						WHEN 0
							THEN 'OFF'
						ELSE NULL
						END
				FROM sys.sql_logins
				WHERE name = @name;

				SELECT @is_expiration_checked = CASE is_expiration_checked
						WHEN 1
							THEN 'ON'
						WHEN 0
							THEN 'OFF'
						ELSE NULL
						END
				FROM sys.sql_logins
				WHERE name = @name;

				SET @tmpstr = '    CREATE LOGIN ' + QUOTENAME(@name) + ' WITH PASSWORD = ' + @PWD_string + ' HASHED, SID = ' + @SID_string + ', DEFAULT_DATABASE = ' + QUOTENAME(@defaultdb) + '';

				IF (@is_policy_checked IS NOT NULL)
				BEGIN
					SET @tmpstr = @tmpstr + ', CHECK_POLICY = ' + @is_policy_checked;
				END;

				IF (@is_expiration_checked IS NOT NULL)
				BEGIN
					SET @tmpstr = @tmpstr + ', CHECK_EXPIRATION = ' + @is_expiration_checked;
				END;
			END;

			IF (@denylogin = 1)
			BEGIN -- Login is denied access
				SET @tmpstr = @tmpstr + '; DENY CONNECT SQL TO ' + QUOTENAME(@name);
			END;
			ELSE IF (@hasaccess = 0)
			BEGIN -- Login exists but does not have access
				SET @tmpstr = @tmpstr + '; REVOKE CONNECT SQL TO ' + QUOTENAME(@name);
			END;

			IF (@is_disabled = 1)
			BEGIN -- Login is disabled
				SET @tmpstr = @tmpstr + '; ALTER LOGIN ' + QUOTENAME(@name) + ' DISABLE';
			END;

			-- If a login does not match on sid and name...
				-- If the login matches on name (sid mismatch), drop the existing login and recreate with new sid
				-- If it matches on neither, create the login
				-- Note... if it matches on sid but the name is different, job will blow up when attempting to create the login
					-- This unlikely case will have to be handled manually (one of the logins will need to be dropped)
			SET @tmpstr2 = 'IF NOT EXISTS (SELECT sid FROM sys.server_principals WHERE sid = ' + @SID_string + ' AND name = ''' + @name + ''')';
			SET @vchScript = @vchScript + @crlf + @tmpstr2;
			SET @vchScript = @vchScript + @crlf + 'BEGIN';
			SET @tmpstr2 = '    IF EXISTS (SELECT name FROM sys.server_principals WHERE name = ''' + @name + ''')';
			SET @vchScript = @vchScript + @crlf + @tmpstr2;
			SET @vchScript = @vchScript + @crlf + '    BEGIN';
			SET @tmpstr2 = '        DROP LOGIN ' + QUOTENAME(@name) + ';';
			SET @vchScript = @vchScript + @crlf + @tmpstr2;
			SET @vchScript = @vchScript + @crlf + '    END;';
			SET @vchScript = @vchScript + @crlf + @tmpstr + ';'; -- Print create login command
			SET @vchScript = @vchScript + @crlf + 'END;';

			-- Check for server permissions for account
			DECLARE server_perm CURSOR
			FOR
			SELECT dp.permission_name
			FROM sys.server_permissions dp
			INNER JOIN sys.server_principals sp ON dp.grantee_principal_id = sp.principal_id
				AND name = @name
			--IMPERSONATE perms break the permission sync.
			--Short term: don't sync IMPERSONATE permission
			--Long  term: Fix the sync so it works with these object-level permissions
			WHERE dp.permission_name <> 'IMPERSONATE';

			OPEN server_perm;

			FETCH server_perm
			INTO @permission;

			WHILE (@@FETCH_STATUS <> -1)
			BEGIN
				IF (@@FETCH_STATUS <> -2)
				BEGIN
					SET @vchScript = @vchScript + @crlf + '    GRANT ' + @permission + ' TO ' + QUOTENAME(@name) + ';';

					FETCH server_perm
					INTO @permission;
				END;
			END;

			DEALLOCATE server_perm;

			-- Check for server roles of those logins
			DECLARE role_cursor CURSOR
			FOR
			SELECT sp.name
			FROM sys.server_principals sp,
				sys.server_role_members srm
			WHERE sp.principal_id = srm.role_principal_id
				AND srm.member_principal_id = (
					SELECT principal_id
					FROM sys.server_principals
					WHERE name = @name
					);

			OPEN role_cursor;

			FETCH role_cursor
			INTO @role_name;

			WHILE (@@FETCH_STATUS <> -1)
			BEGIN
				IF (@@FETCH_STATUS <> -2)
				BEGIN
					SET @vchScript = @vchScript + @crlf + '    IF NOT EXISTS (SELECT name FROM sys.server_principals WHERE name = ''' + @role_name + ''')';
					SET @vchScript = @vchScript + @crlf + '    BEGIN';
					SET @vchScript = @vchScript + @crlf + '        USE [master] CREATE SERVER ROLE ' + QUOTENAME(@role_name) + ';';
					SET @vchScript = @vchScript + @crlf + '    END;';
					SET @vchScript = @vchScript + @crlf + '    EXEC sp_addsrvrolemember ' + QUOTENAME(@name) + ', ' + QUOTENAME(@role_name) + ';';
				END;

				FETCH role_cursor
				INTO @role_name;
			END;

			CLOSE role_cursor;

			DEALLOCATE role_cursor;
		END;

		FETCH NEXT
		FROM login_curs
		INTO @SID_varbinary,
			@name,
			@type,
			@is_disabled,
			@defaultdb,
			@hasaccess,
			@denylogin;
	END;

	CLOSE login_curs;

	DEALLOCATE login_curs;

	SELECT @vchScript;
END;