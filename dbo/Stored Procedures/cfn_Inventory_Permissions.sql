﻿/*****************************************************************************************
Name: cfn_Inventory_Permissions

Desc: An inventory of all database and server permissions and roles on the current server.
	The output is stored in these tables on the DBA database:
	dbo.Inventory_sys_database_permissions
	dbo.Inventory_sys_database_principals
	dbo.Inventory_sys_database_role_members
	dbo.Inventory_sys_server_permissions
	dbo.Inventory_sys_server_principals
	dbo.Inventory_sys_server_role_members

Parameters: 

Dependencies: 

Auth: MDAK

Date: 07/23/15

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------

*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Inventory_Permissions;
*/
CREATE PROCEDURE [dbo].[cfn_Inventory_Permissions]
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @sqlVersion INT = NULL,
		@Date DATETIME2(0) = SYSDATETIME(),
		@ServerName NVARCHAR(128) = @@SERVERNAME,
		@AGName NVARCHAR(128) = NULL,
		@DatabaseName NVARCHAR(128) = NULL,
		@SQL NVARCHAR(4000) = NULL, 
		@Params NVARCHAR(4000) = NULL;

	IF OBJECT_ID('tempdb.dbo.#ServerPermissions') IS NOT NULL
		DROP TABLE #ServerPermissions;
	CREATE TABLE #ServerPermissions (
		class_desc NVARCHAR(60) NULL,
		major_name NVARCHAR(128) NULL,
		minor_name NVARCHAR(128) NULL,
		grantee_principal_id INT NOT NULL,
		grantor_principal_id INT NOT NULL,
		permission_name NVARCHAR(128) NULL,
		state_desc NVARCHAR(60) NULL
		);

	IF OBJECT_ID('tempdb.dbo.#ServerPrincipals') IS NOT NULL
		DROP TABLE #ServerPrincipals;
	CREATE TABLE #ServerPrincipals (
		name SYSNAME NOT NULL,
		principal_id INT NOT NULL,
		sid VARBINARY(85) NULL,
		type_desc NVARCHAR(60) NULL,
		is_disabled BIT NULL,
		default_database_name SYSNAME NULL,
		default_language_name SYSNAME NULL,
		credential_id INT NULL,
		owning_principal_id INT NULL,
		is_fixed_role BIT NULL
		);

	IF OBJECT_ID('tempdb.dbo.#ServerRoleMembers') IS NOT NULL
		DROP TABLE #ServerRoleMembers;
	CREATE TABLE #ServerRoleMembers (
		role_principal_id INT NOT NULL,
		member_principal_id INT NOT NULL
		);

	IF OBJECT_ID('tempdb.dbo.#Databases') IS NOT NULL
		DROP TABLE #Databases;
	CREATE TABLE #Databases (
		AGName NVARCHAR(128) NULL,
		DatabaseName NVARCHAR(128) NOT NULL
		);

	IF OBJECT_ID('tempdb.dbo.#DatabasePermissions') IS NOT NULL
		DROP TABLE #DatabasePermissions;
	CREATE TABLE #DatabasePermissions (
		AGName NVARCHAR(128) NULL,
		DatabaseName NVARCHAR(128) NOT NULL,
		class_desc NVARCHAR(60) NULL,
		schema_name NVARCHAR(128) NULL,
		major_name NVARCHAR(128) NULL,
		minor_name NVARCHAR(128) NULL,
		grantee_principal_id INT NOT NULL,
		grantor_principal_id INT NOT NULL,
		permission_name NVARCHAR(128) NULL,
		state_desc NVARCHAR(60) NULL
		);

	IF OBJECT_ID('tempdb.dbo.#DatabasePrincipals') IS NOT NULL
		DROP TABLE #DatabasePrincipals;
	CREATE TABLE #DatabasePrincipals (
		AGName NVARCHAR(128) NULL,
		DatabaseName NVARCHAR(128) NOT NULL,
		name SYSNAME NOT NULL,
		principal_id INT NOT NULL,
		type_desc NVARCHAR(60) NULL,
		default_schema_name SYSNAME NULL,
		owning_principal_id INT NULL,
		sid VARBINARY(85) NULL,
		is_fixed_role BIT NOT NULL,
		authentication_type_desc NVARCHAR(60) NULL,
		default_language_name SYSNAME NULL,
		default_language_lcid INT NULL
		);

	IF OBJECT_ID('tempdb.dbo.#DatabaseRoleMembers') IS NOT NULL
		DROP TABLE #DatabaseRoleMembers;
	CREATE TABLE #DatabaseRoleMembers (
		AGName NVARCHAR(128) NULL,
		DatabaseName NVARCHAR(128) NOT NULL,
		role_principal_id INT NOT NULL,
		member_principal_id INT NOT NULL
		);

	-- Get the SQL Server major version number	
	SELECT @sqlVersion = CONVERT(INT, LEFT(CONVERT(NVARCHAR(128),SERVERPROPERTY('ProductVersion')),CHARINDEX('.', CONVERT(NVARCHAR(128),SERVERPROPERTY('ProductVersion')))-1));


	/****************************************
		GATHER ALL SERVER PERMISSIONS
	****************************************/
	-- Server permissions
	SET @SQL = 
	'SELECT perm.class_desc,
		CASE perm.class_desc
			WHEN ''SERVER'' THEN NULL
			WHEN ''SERVER_PRINCIPAL'' THEN princ.name
			WHEN ''ENDPOINT'' THEN endp.name '
		+ CASE WHEN @sqlVersion >= 11
			THEN 'WHEN ''AVAILABILITY GROUP'' THEN ag.name '
			ELSE ''
			END
		+ 'END AS [major_name],
			NULL AS [minor_name],
			perm.grantee_principal_id,
			perm.grantor_principal_id,
			perm.permission_name,
			perm.state_desc
		FROM sys.server_permissions perm
			LEFT JOIN sys.server_principals princ ON perm.major_id = princ.principal_id
			LEFT JOIN sys.endpoints endp ON perm.major_id = endp.endpoint_id'
		+ CASE WHEN @sqlVersion >= 11
			THEN ' LEFT JOIN (sys.availability_replicas r 
				JOIN sys.availability_groups ag ON r.group_id = ag.group_id)
					ON perm.major_id = r.replica_metadata_id'
			ELSE '' END;

	INSERT INTO #ServerPermissions (
		class_desc,
		major_name,
		minor_name,
		grantee_principal_id,
		grantor_principal_id,
		permission_name,
		state_desc
		)
	EXEC sp_executesql @SQL;

	-- Server principals
	SET @SQL = 
	'SELECT name,
		principal_id,
		sid,
		type_desc,
		is_disabled,
		default_database_name,
		default_language_name,
		credential_id, '
		+ CASE WHEN @sqlVersion >= 11
			THEN 'owning_principal_id,
				is_fixed_role '
			ELSE 'NULL AS [owning_principal_id],
				NULL AS [is_fixed_role] '
			END
		+ 'FROM sys.server_principals';

	INSERT INTO #ServerPrincipals (
		name,
		principal_id,
		sid,
		type_desc,
		is_disabled,
		default_database_name,
		default_language_name,
		credential_id,
		owning_principal_id,
		is_fixed_role
		)
	EXEC sp_executesql @SQL;

	-- Server role members
	INSERT INTO #ServerRoleMembers (
		role_principal_id,
		member_principal_id
		)
	SELECT role_principal_id,
		member_principal_id
	FROM sys.server_role_members;


	/****************************************
		GATHER ALL DATABASE PERMISSIONS
	****************************************/
	-- Determine list of databases to query
	-- Only grab AG databases if the current replica is primary
	IF @sqlVersion >= 11
	BEGIN
		SET @SQL =
		'SELECT ag.name AS [AGName],
			d.name AS [DatabaseName]
		FROM sys.databases d
			LEFT JOIN sys.dm_hadr_availability_replica_states rs ON d.replica_id = rs.replica_id
				AND rs.is_local = 1
			LEFT JOIN sys.availability_groups ag ON rs.group_id = ag.group_id
		WHERE d.name NOT IN (''model'',''tempdb'',''distribution'')
			AND COALESCE(rs.role_desc COLLATE database_default, d.state_desc) IN (''PRIMARY'',''ONLINE'')';
	END
	ELSE
	BEGIN
		SET @SQL =
		'SELECT NULL AS [AGName],
			name AS [DatabaseName]
		FROM sys.databases
		WHERE name NOT IN (''model'',''tempdb'',''distribution'')
			AND state_desc = ''ONLINE''';
	END;

	INSERT INTO #Databases (
		AGName,
		DatabaseName
		)
	EXEC sp_executesql @SQL;

	DECLARE db_cursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT AGName,
			DatabaseName
		FROM #Databases;

	OPEN db_cursor;

	FETCH NEXT FROM db_cursor
	INTO @AGName,
		@DatabaseName;

	SET @Params = '@AGName NVARCHAR(128), @DatabaseName NVARCHAR(128)';

	-- Loop through all databases
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		-- One more check to see if the database is ONLINE and accepting connections
		IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name = @DatabaseName AND state_desc = 'ONLINE' AND collation_name IS NOT NULL)
		BEGIN
			FETCH NEXT FROM db_cursor
			INTO @AGName,
				@DatabaseName;

			CONTINUE;
		END;

		-- Database permissions
		SET @SQL = 
		'SELECT @AGName AS [AGName],
			@DatabaseName AS [DatabaseName],
			p.class_desc,
			CASE p.class_desc
				WHEN ''OBJECT_OR_COLUMN'' THEN os.name
				WHEN ''TYPE'' THEN ts.name
				WHEN ''XML_SCHEMA_COLLECTION'' THEN xss.name
			END AS [schema_name],
			CASE p.class_desc
				WHEN ''DATABASE'' THEN @DatabaseName
				WHEN ''OBJECT_OR_COLUMN'' THEN o.name
				WHEN ''SCHEMA'' THEN s.name
				WHEN ''DATABASE_PRINCIPAL'' THEN pr.name
				WHEN ''ASSEMBLY'' THEN a.name
				WHEN ''TYPE'' THEN t.name
				WHEN ''XML_SCHEMA_COLLECTION'' THEN xsc.name
				WHEN ''MESSAGE_TYPE'' THEN smt.name
				WHEN ''SERVICE_CONTRACT'' THEN sc.name
				WHEN ''SERVICE'' THEN sbs.name
				WHEN ''REMOTE_SERVICE_BINDING'' THEN rsb.name
				WHEN ''ROUTE'' THEN r.name
				WHEN ''FULLTEXT_CATALOG'' THEN fc.name
				WHEN ''SYMMETRIC_KEY'' THEN sk.name
				WHEN ''CERTIFICATE'' THEN cer.name
				WHEN ''ASYMMETRIC_KEY'' THEN ak.name
				WHEN ''FULLTEXT_STOPLIST'' THEN fs.name '
			+ CASE WHEN @sqlVersion >= 11
				THEN 'WHEN ''SEARCH_PROPERTY_LIST'' THEN rspl.name '
				ELSE ''
				END
			+ 'END COLLATE database_default AS [major_name],
				c.name AS [minor_name],
				p.grantee_principal_id,
				p.grantor_principal_id,
				p.permission_name,
				p.state_desc
			FROM [' + @DatabaseName + '].sys.database_permissions p
				LEFT JOIN ([' + @DatabaseName + '].sys.all_objects o
					JOIN [' + @DatabaseName + '].sys.schemas os ON o.schema_id = os.schema_id)
						ON p.major_id = o.object_id
				LEFT JOIN [' + @DatabaseName + '].sys.columns c ON o.object_id = c.object_id
					AND p.minor_id = c.column_id
				LEFT JOIN [' + @DatabaseName + '].sys.schemas s ON p.major_id = s.schema_id
				LEFT JOIN [' + @DatabaseName + '].sys.database_principals pr ON p.major_id = pr.principal_id
				LEFT JOIN [' + @DatabaseName + '].sys.assemblies a ON p.major_id = a.assembly_id
				LEFT JOIN ([' + @DatabaseName + '].sys.types t
					JOIN [' + @DatabaseName + '].sys.schemas ts ON t.schema_id = ts.schema_id)
						ON p.major_id = t.user_type_id
				LEFT JOIN ([' + @DatabaseName + '].sys.xml_schema_collections xsc
					JOIN [' + @DatabaseName + '].sys.schemas xss ON xsc.schema_id = xss.schema_id)
						ON p.major_id = xsc.xml_collection_id
				LEFT JOIN [' + @DatabaseName + '].sys.service_message_types smt ON p.major_id = smt.message_type_id
				LEFT JOIN [' + @DatabaseName + '].sys.service_contracts sc ON p.major_id = sc.service_contract_id
				LEFT JOIN [' + @DatabaseName + '].sys.services sbs ON p.major_id = sbs.service_id
				LEFT JOIN [' + @DatabaseName + '].sys.remote_service_bindings rsb ON p.major_id = rsb.remote_service_binding_id
				LEFT JOIN [' + @DatabaseName + '].sys.routes r ON p.major_id = r.route_id
				LEFT JOIN [' + @DatabaseName + '].sys.fulltext_catalogs fc ON P.major_id = fc.fulltext_catalog_id
				LEFT JOIN [' + @DatabaseName + '].sys.symmetric_keys sk ON p.major_id = sk.symmetric_key_id
				LEFT JOIN [' + @DatabaseName + '].sys.certificates cer ON p.major_id = cer.certificate_id
				LEFT JOIN [' + @DatabaseName + '].sys.asymmetric_keys ak ON p.major_id = ak.asymmetric_key_id
				LEFT JOIN [' + @DatabaseName + '].sys.fulltext_stoplists fs ON P.major_id = fs.stoplist_id '
			+ CASE WHEN @sqlVersion >= 11
				THEN 'LEFT JOIN [' + @DatabaseName + '].sys.registered_search_property_lists rspl ON p.major_id = rspl.property_list_id '
				ELSE ''
				END
			+ 'JOIN [' + @DatabaseName + '].sys.database_principals dp ON p.grantee_principal_id = dp.principal_id
				JOIN [' + @DatabaseName + '].sys.database_principals g ON p.grantor_principal_id = g.principal_id';

		INSERT INTO #DatabasePermissions (
			AGName,
			DatabaseName,
			class_desc,
			schema_name,
			major_name,
			minor_name,
			grantee_principal_id,
			grantor_principal_id,
			permission_name,
			state_desc
			)
		EXEC sp_executesql @SQL, @Params, @AGName = @AGName, @DatabaseName = @DatabaseName;

		-- Database principals
		SET @SQL = 
		'SELECT @AGName,
			@DatabaseName,
			name,
			principal_id,
			type_desc,
			default_schema_name,
			owning_principal_id,
			sid,
			is_fixed_role, '
			+ CASE WHEN @sqlVersion >= 11
				THEN 'authentication_type_desc,
						default_language_name,
						default_language_lcid '
				ELSE 'NULL AS [authentication_type_desc],
						NULL AS [default_language_name],
						NULL AS [default_language_lcid] '
				END
			+ 'FROM [' + @DatabaseName + '].sys.database_principals';
		
		INSERT INTO #DatabasePrincipals (
			AGName,
			DatabaseName,
			name,
			principal_id,
			type_desc,
			default_schema_name,
			owning_principal_id,
			sid,
			is_fixed_role,
			authentication_type_desc,
			default_language_name,
			default_language_lcid
			)
		EXEC sp_executesql @SQL, @Params, @AGName = @AGName, @DatabaseName = @DatabaseName;
		
		-- Database role members
		SET @SQL =
		'SELECT @AGName,
			@DatabaseName,
			role_principal_id,
			member_principal_id
		FROM [' + @DatabaseName + '].sys.database_role_members';

		INSERT INTO #DatabaseRoleMembers (
			AGName,
			DatabaseName,
			role_principal_id,
			member_principal_id
			)
		EXEC sp_executesql @SQL, @Params, @AGName = @AGName, @DatabaseName = @DatabaseName;

		FETCH NEXT FROM db_cursor
		INTO @AGName,
		@DatabaseName;
	END;

	CLOSE db_cursor;
	DEALLOCATE db_cursor;

	-- ====================================
	--		UPDATE server permissions
	-- ====================================

	UPDATE dbo.Inventory_sys_server_permissions
	SET DateRemoved = @Date,
		Active = 0
	FROM dbo.Inventory_sys_server_permissions i
		LEFT JOIN #ServerPermissions s ON COALESCE(i.class_desc,'') = COALESCE(s.class_desc,'')
			AND COALESCE(i.major_name,'') = COALESCE(s.major_name,'')
			AND COALESCE(i.minor_name,'') = COALESCE(s.minor_name,'')
			AND i.grantee_principal_id = s.grantee_principal_id
			AND i.grantor_principal_id = s.grantor_principal_id
			AND COALESCE(i.permission_name,'') = COALESCE(s.permission_name,'')
			AND COALESCE(i.state_desc,'') = COALESCE(s.state_desc,'')
	WHERE i.DateRemoved IS NULL
		AND s.grantee_principal_id IS NULL;

	-- ====================================
	--		INSERT new server permissions
	-- ====================================

	INSERT INTO dbo.Inventory_sys_server_permissions (
		ServerName,
		class_desc,
		major_name,
		minor_name,
		grantee_principal_id,
		grantor_principal_id,
		permission_name,
		state_desc,
		DateAdded,
		DateRemoved,
		Active
		)
	SELECT @ServerName,
		s.class_desc,
		s.major_name,
		s.minor_name,
		s.grantee_principal_id,
		s.grantor_principal_id,
		s.permission_name,
		s.state_desc,
		@Date,
		NULL,
		1
	FROM #ServerPermissions s
		LEFT JOIN dbo.Inventory_sys_server_permissions i ON COALESCE(s.class_desc,'') = COALESCE(i.class_desc,'')
			AND COALESCE(s.major_name,'') = COALESCE(i.major_name,'')
			AND COALESCE(s.minor_name,'') = COALESCE(i.minor_name,'')
			AND s.grantee_principal_id = i.grantee_principal_id
			AND s.grantor_principal_id = i.grantor_principal_id
			AND COALESCE(s.permission_name,'') = COALESCE(i.permission_name,'')
			AND COALESCE(s.state_desc,'') = COALESCE(i.state_desc,'')
			AND i.DateRemoved IS NULL
	WHERE i.grantee_principal_id IS NULL;
	
	-- ====================================
	--		UPDATE server principals
	-- ====================================

	UPDATE dbo.Inventory_sys_server_principals
	SET DateRemoved = @Date,
		Active = 0
	FROM dbo.Inventory_sys_server_principals i
		LEFT JOIN #ServerPrincipals s ON i.name = s.name
			AND i.principal_id = s.principal_id
			AND COALESCE(i.sid,'') = COALESCE(s.sid,'')
			AND COALESCE(i.type_desc,'') = COALESCE(s.type_desc,'')
			AND COALESCE(i.is_disabled,'') = COALESCE(s.is_disabled,'')
			AND COALESCE(i.default_database_name,'') = COALESCE(s.default_database_name,'')
			AND COALESCE(i.default_language_name,'') = COALESCE(s.default_language_name,'')
			AND COALESCE(i.credential_id,'') = COALESCE(s.credential_id,'')
			AND COALESCE(i.owning_principal_id,'') = COALESCE(s.owning_principal_id,'')
			AND COALESCE(i.is_fixed_role,'') = COALESCE(s.is_fixed_role,'')
	WHERE i.DateRemoved IS NULL
		AND s.principal_id IS NULL;

	-- ====================================
	--		INSERT new server principals
	-- ====================================

	INSERT INTO dbo.Inventory_sys_server_principals (
		ServerName,
		name,
		principal_id,
		sid,
		type_desc,
		is_disabled,
		default_database_name,
		default_language_name,
		credential_id,
		owning_principal_id,
		is_fixed_role,
		DateAdded,
		DateRemoved,
		Active
		)
	SELECT @ServerName,
		s.name,
		s.principal_id,
		s.sid,
		s.type_desc,
		s.is_disabled,
		s.default_database_name,
		s.default_language_name,
		s.credential_id,
		s.owning_principal_id,
		s.is_fixed_role,
		@Date,
		NULL,
		1
	FROM #ServerPrincipals s
		LEFT JOIN dbo.Inventory_sys_server_principals i ON s.name = i.name
			AND s.principal_id = i.principal_id
			AND COALESCE(s.sid,'') = COALESCE(i.sid,'')
			AND COALESCE(s.type_desc,'') = COALESCE(i.type_desc,'')
			AND COALESCE(s.is_disabled,'') = COALESCE(i.is_disabled,'')
			AND COALESCE(s.default_database_name,'') = COALESCE(i.default_database_name,'')
			AND COALESCE(s.default_language_name,'') = COALESCE(i.default_language_name,'')
			AND COALESCE(s.credential_id,'') = COALESCE(i.credential_id,'')
			AND COALESCE(s.owning_principal_id,'') = COALESCE(i.owning_principal_id,'')
			AND COALESCE(s.is_fixed_role,'') = COALESCE(i.is_fixed_role,'')
			AND i.DateRemoved IS NULL
	WHERE i.principal_id IS NULL;

	-- ====================================
	--		UPDATE server role members
	-- ====================================

	UPDATE dbo.Inventory_sys_server_role_members
	SET DateRemoved = @Date,
		Active = 0
	FROM dbo.Inventory_sys_server_role_members i
		LEFT JOIN #ServerRoleMembers s ON i.role_principal_id = s.role_principal_id
			AND i.member_principal_id = s.member_principal_id
	WHERE i.DateRemoved IS NULL
		AND s.role_principal_id IS NULL;

	-- ====================================
	--		INSERT new server role members
	-- ====================================

	INSERT INTO dbo.Inventory_sys_server_role_members (
		ServerName,
		role_principal_id,
		member_principal_id,
		DateAdded,
		DateRemoved,
		Active
		)
	SELECT @ServerName,
		s.role_principal_id,
		s.member_principal_id,
		@Date,
		NULL,
		1
	FROM #ServerRoleMembers s
		LEFT JOIN dbo.Inventory_sys_server_role_members i ON s.role_principal_id = i.role_principal_id
			AND s.member_principal_id = i.member_principal_id
			AND i.DateRemoved IS NULL
	WHERE i.role_principal_id IS NULL;

	-- ====================================
	--		UPDATE database permissions
	-- ====================================

	UPDATE dbo.Inventory_sys_database_permissions
	SET DateRemoved = @Date,
		Active = 0
	FROM dbo.Inventory_sys_database_permissions i
		LEFT JOIN #DatabasePermissions d ON COALESCE(i.AGName,'') = COALESCE(d.AGName,'')
			AND i.DatabaseName = d.DatabaseName
			AND COALESCE(i.class_desc,'') = COALESCE(d.class_desc,'')
			AND COALESCE(i.schema_name,'') = COALESCE(d.schema_name,'')
			AND COALESCE(i.major_name,'') = COALESCE(d.major_name,'')
			AND COALESCE(i.minor_name,'') = COALESCE(d.minor_name,'')
			AND i.grantee_principal_id = d.grantee_principal_id
			AND i.grantor_principal_id = d.grantor_principal_id
			AND COALESCE(i.permission_name,'') = COALESCE(d.permission_name,'')
			AND COALESCE(i.state_desc,'') = COALESCE(d.state_desc,'')
	WHERE i.DateRemoved IS NULL
		AND d.grantee_principal_id IS NULL;

	-- ====================================
	--		INSERT new database permissions
	-- ====================================

	INSERT INTO dbo.Inventory_sys_database_permissions (
		ServerName,
		AGName,
		DatabaseName,
		class_desc,
		schema_name,
		major_name,
		minor_name,
		grantee_principal_id,
		grantor_principal_id,
		permission_name,
		state_desc,
		DateAdded,
		DateRemoved,
		Active
		)
	SELECT @ServerName,
		d.AGName,
		d.DatabaseName,
		d.class_desc,
		d.schema_name,
		d.major_name,
		d.minor_name,
		d.grantee_principal_id,
		d.grantor_principal_id,
		d.permission_name,
		d.state_desc,
		@Date,
		NULL,
		1
	FROM #DatabasePermissions d
		LEFT JOIN dbo.Inventory_sys_database_permissions i ON COALESCE(d.AGName,'') = COALESCE(i.AGName,'')
			AND d.DatabaseName = i.DatabaseName
			AND COALESCE(d.class_desc,'') = COALESCE(i.class_desc,'')
			AND COALESCE(d.schema_name,'') = COALESCE(i.schema_name,'')
			AND COALESCE(d.major_name,'') = COALESCE(i.major_name,'')
			AND COALESCE(d.minor_name,'') = COALESCE(i.minor_name,'')
			AND d.grantee_principal_id = i.grantee_principal_id
			AND d.grantor_principal_id = i.grantor_principal_id
			AND COALESCE(d.permission_name,'') = COALESCE(i.permission_name,'')
			AND COALESCE(d.state_desc,'') = COALESCE(i.state_desc,'')
			AND i.DateRemoved IS NULL
	WHERE i.grantee_principal_id IS NULL;

	-- ====================================
	--		UPDATE database principals
	-- ====================================

	UPDATE dbo.Inventory_sys_database_principals
	SET DateRemoved = @Date,
		Active = 0
	FROM dbo.Inventory_sys_database_principals i
		LEFT JOIN #DatabasePrincipals d ON COALESCE(i.AGName,'') = COALESCE(d.AGName,'')
			AND i.DatabaseName = d.DatabaseName
			AND i.name = d.name
			AND i.principal_id = d.principal_id
			AND COALESCE(i.type_desc,'') = COALESCE(d.type_desc,'')
			AND COALESCE(i.default_schema_name,'') = COALESCE(d.default_schema_name,'')
			AND COALESCE(i.owning_principal_id,'') = COALESCE(d.owning_principal_id,'')
			AND COALESCE(i.sid,'') = COALESCE(d.sid,'')
			AND i.is_fixed_role = d.is_fixed_role
			AND COALESCE(i.authentication_type_desc,'') = COALESCE(d.authentication_type_desc,'')
			AND COALESCE(i.default_language_name,'') = COALESCE(d.default_language_name,'')
			AND COALESCE(i.default_language_lcid,'') = COALESCE(d.default_language_lcid,'')
	WHERE i.DateRemoved IS NULL
		AND d.principal_id IS NULL;

	-- ====================================
	--		INSERT new database principals
	-- ====================================

	INSERT INTO dbo.Inventory_sys_database_principals (
		ServerName,
		AGName,
		DatabaseName,
		name,
		principal_id,
		type_desc,
		default_schema_name,
		owning_principal_id,
		sid,
		is_fixed_role,
		authentication_type_desc,
		default_language_name,
		default_language_lcid,
		DateAdded,
		DateRemoved,
		Active
		)
	SELECT @ServerName,
		d.AGName,
		d.DatabaseName,
		d.name,
		d.principal_id,
		d.type_desc,
		d.default_schema_name,
		d.owning_principal_id,
		d.sid,
		d.is_fixed_role,
		d.authentication_type_desc,
		d.default_language_name,
		d.default_language_lcid,
		@Date,
		NULL,
		1
	FROM #DatabasePrincipals d
		LEFT JOIN dbo.Inventory_sys_database_principals i ON COALESCE(d.AGName,'') = COALESCE(i.AGName,'')
			AND d.DatabaseName = i.DatabaseName
			AND d.name = i.name
			AND d.principal_id = i.principal_id
			AND COALESCE(d.type_desc,'') = COALESCE(i.type_desc,'')
			AND COALESCE(d.default_schema_name,'') = COALESCE(i.default_schema_name,'')
			AND COALESCE(d.owning_principal_id,'') = COALESCE(i.owning_principal_id,'')
			AND COALESCE(d.sid,'') = COALESCE(i.sid,'')
			AND d.is_fixed_role = i.is_fixed_role
			AND COALESCE(d.authentication_type_desc,'') = COALESCE(i.authentication_type_desc,'')
			AND COALESCE(d.default_language_name,'') = COALESCE(i.default_language_name,'')
			AND COALESCE(d.default_language_lcid,'') = COALESCE(i.default_language_lcid,'')
			AND i.DateRemoved IS NULL
	WHERE i.principal_id IS NULL;

	-- ====================================
	--		UPDATE database role members
	-- ====================================

	UPDATE dbo.Inventory_sys_database_role_members
	SET DateRemoved = @Date,
		Active = 0
	FROM dbo.Inventory_sys_database_role_members i
		LEFT JOIN #DatabaseRoleMembers d ON COALESCE(i.AGName,'') = COALESCE(d.AGName,'')
			AND i.DatabaseName = d.DatabaseName
			AND i.role_principal_id = d.role_principal_id
			AND i.member_principal_id = d.member_principal_id
	WHERE i.DateRemoved IS NULL
		AND d.role_principal_id IS NULL;

	-- ====================================
	--		INSERT new database role members
	-- ====================================

	INSERT INTO dbo.Inventory_sys_database_role_members (
		ServerName,
		AGName,
		DatabaseName,
		role_principal_id,
		member_principal_id,
		DateAdded,
		DateRemoved,
		Active
		)
	SELECT @ServerName,
		d.AGName,
		d.DatabaseName,
		d.role_principal_id,
		d.member_principal_id,
		@Date,
		NULL,
		1
	FROM #DatabaseRoleMembers d
		LEFT JOIN dbo.Inventory_sys_database_role_members i ON COALESCE(d.AGName,'') = COALESCE(i.AGName,'')
			AND d.DatabaseName = i.DatabaseName
			AND d.role_principal_id = i.role_principal_id
			AND d.member_principal_id = i.member_principal_id
			AND i.DateRemoved IS NULL
	WHERE i.role_principal_id IS NULL;
END
