﻿

CREATE PROCEDURE [dbo].[cfn_AlertServiceBrokerLog] (
    @EmailRecipients varchar(max) = NULL,
    @StartTime datetime2(0) = NULL,
    @ServiceName sysname = NULL,
    @QueueName sysname = NULL,
    @MessageType sysname = NULL,
    @Debug bit = 0
    )
AS
SET NOCOUNT ON ;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;
-- Parameter Validation
--If NULL, then use 8 hours ago
SET @StartTime = COALESCE(@StartTime,DATEADD(hh,-8,GETDATE()));


IF @StartTime < GETDATE()-7
BEGIN
    RAISERROR('@StartDate parameter must be within the last 7 days',16,1);
    RETURN(1);
END
--

--Variable declarations
DECLARE @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);
--

--Yes, SELECT *.  =D
SELECT *
INTO #Results
FROM cfn_ServiceBrokerLog
WHERE MessageDateTime >= GETDATE()-7 --7 day hard limit, regardless of parameter
AND MessageDateTime >= @StartTime
AND ServiceName = COALESCE(ServiceName,@ServiceName)
AND QueueName = COALESCE(QueueName,@QueueName)
AND MessageType = COALESCE(MessageType,@MessageType);

IF @Debug = 1
BEGIN
    SELECT * FROM #Results;
END;


-- If no rows returned, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #Results)
    RETURN (0);

IF @Debug = 0
BEGIN
    -- Send Email WITH STYLE
    SET @EmailBody = dbo.cfn_EmailCss();

    --Build the body of the email based on the #Results
    SET @EmailBody = @EmailBody + N'<H2>Service Broker log messages for ' + @@ServerName + N'</H2>' +
            N'<table border="0" cellpadding="2" cellspacing="0" style ="font-family:">' +
            N'<tr><th>Start Time: </th><td>' + CONVERT(nvarchar(20), @StartTime, 120) + N'</td></tr>' +
            N'<tr><th>End Time: </th><td>' + CONVERT(nvarchar(20), GETDATE(), 120) + N'</td></tr>' +
            N'<tr><th>Service Name: </th><td>' + COALESCE(@ServiceName,N'ALL') + N'</td></tr>' +
            N'<tr><th>Queue Name: </th><td>' + COALESCE(@QueueName,N'ALL') + N'</td></tr>' +
            N'<tr><th>MessageType: </th><td>' + COALESCE(@MessageType,N'ALL') + N'</td></tr>' +
            N'</table><BR><BR>' +
            N'<table border="1" cellpadding="1" cellspacing="0" style ="font-family:''Segoe UI'',''Arial''">' +
            N'<tr><th>Message Date/Time</th>' +
            N'<th>Service Name</th>' +
            N'<th>Queue Name</th>' +
            N'<th>Message Type</th>' +
            N'<th>Error Number</th>' +
            N'<th>Error Message</th></tr>' +
            CAST(( SELECT 
                    td = CONVERT(varchar(20), MessageDateTime, 120), '',
                    td = ServiceName, '',
                    td = QueueName, '',
                    td = MessageType, '',
                    td = ErrorNumber, '',
                    td = ErrorMessage
                FROM #Results
                FOR XML PATH('tr'),ELEMENTS
                ) AS NVARCHAR(MAX)) +
             N'</table>';

    SET @EmailSubject = 'ALERT: Service Broker Errors';

    EXEC msdb.dbo.sp_send_dbmail
        @Recipients = @EmailRecipients,
        @Subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;