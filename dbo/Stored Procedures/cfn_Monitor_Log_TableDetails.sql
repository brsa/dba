﻿CREATE PROC dbo.cfn_Monitor_Log_TableDetails
AS
SET NOCOUNT ON;
BEGIN
	TRUNCATE TABLE dbo.Monitor_Temp_TableDetails;

	EXEC dbo.sp_foreachdb @command = N'USE ?; INSERT INTO DBA.dbo.Monitor_Temp_TableDetails (DatabaseName, SchemaName, Table_Name, Column_Id, Column_Name
										, System_Data_Type, Max_Length, Is_Nullable, Is_identity,Last_Value, Max_Value, Object_Id, Extended_Property)
				SELECT DB_NAME() AS DatabaseName
				, ISNULL(SCHEMA_NAME(t.schema_id),''dbo'') AS SchemaName
				, T.name AS table_name,  AC.column_id As Column_id, AC.name AS column_name
				, TY.name AS system_data_type, AC.max_length, AC.is_nullable, AC.is_identity, ID.last_value
				, Case 
			When TY.name = ''tinyint'' AND AC.is_identity = ''1'' Then 255 
			When TY.name = ''smallint'' AND AC.is_identity = ''1'' Then 32767 
			When TY.name = ''int''   AND AC.is_identity = ''1'' Then 2147483647 
			When TY.name = ''bigint''  AND AC.is_identity = ''1'' Then 9223372036854775807
			End As [max_value]
				, T.object_id , ep.value AS Extended_Property
		FROM  sys.tables AS T 
		JOIN  sys.all_columns AS AC ON T.object_id = AC.object_id 
		JOIN  sys.types AS TY ON AC.system_type_id = TY.system_type_id 
			  								AND AC.user_type_id = TY.user_type_id
		LEFT JOIN sys.identity_columns ID on AC.object_id = ID.object_id 
			  								AND AC.is_identity = 1
		LEFT JOIN sys.extended_properties AS ep ON ep.major_id = t.object_id AND ep.minor_id = ac.column_id
		WHERE (T.is_ms_shipped = 0) 
		ORDER BY table_name, AC.column_id' ,                   -- nvarchar(max)
	@print_dbname = 0 ,             -- bit
	@print_command_only=0,
	@suppress_quotename = 0 ,       -- bit
	@system_only = 0 ,              -- bit
	@user_only = 1 ;
END;