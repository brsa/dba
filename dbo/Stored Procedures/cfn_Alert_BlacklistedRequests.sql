CREATE PROCEDURE [dbo].[cfn_Alert_BlacklistedRequests]
    @DurationThreshold smallint = 60,
    @EmailRecipients varchar(max) = NULL,
    @EmailThreshold smallint = 10,
    @Debug tinyint = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150420
    This procedure checks for sessions meeting specific criteria that we don't want to see running
    in production. The initial version is based on Program Name, but future additions
    may be based on LoginName or more complicated criteria. 
    Requests running longer @DurationThreshold that are on the blacklist will trigger an alert.
    Log the session to a table (only when not in @Debug mode).
    @Debug parameter controls whether to email @EmailRecipients or to output resultset.
    @EmailThreshold throttles email so that the server only sends email every N minutes.

    A future version might also auto-kill certain sessions in order to prevent unwanted load in Prod.

PARAMETERS
* @DurationThreshold - seconds - Alters when requesrs have been running longer than this many seconds.
* @EmailRecipients   - delimited list of email addresses. Used for sending email alert.
* @EmailThreshold    - minutes - Only send an email every this many minutes.
* @Debug             - Supress sending email & output a resultset instead
**************************************************************************************************
MODIFICATIONS:
    20141222 - AM2 - 

*************************************************************************************************/
SET NOCOUNT ON;
--READ UNCOMMITTED, since we're dealing with blocking, we don't want to make things worse.
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

IF @DurationThreshold >0
    SET @DurationThreshold = -1*@DurationThreshold;

DECLARE @Id int = 1,
        @Spid int = 0,
        @JobIdHex nvarchar(34),
        @JobName nvarchar(256),
        @WaitResource nvarchar(256),
        @DbName nvarchar(256),
        @ObjectName nvarchar(256),
        @IndexName nvarchar(256),
        @Sql nvarchar(max),
            @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

CREATE TABLE #ProgramBlacklist (
  ProgramName nvarchar(128)
  );

INSERT INTO #ProgramBlacklist (ProgramName)
VALUES ('Microsoft SQL Server Management Studio%'),
       ('Microsoft SQL Server Data Tools, T-SQL Editor');

CREATE TABLE #Sessions (
    ID int identity(1,1) PRIMARY KEY,
    SessionID smallint,
    DbName sysname,
    HostName nvarchar(128),
    ProgramName nvarchar(128),
    LoginName nvarchar(128),
    LoginTime datetime2(3),
    LastRequestStart datetime2(3),
    LastRequestEnd datetime2(3),
    TransactionCnt int,
    Command nvarchar(32),
    WaitTime int,
    WaitResource nvarchar(256),
    WaitDescription nvarchar(1000),
    SqlText nvarchar(max),
    SqlStatement nvarchar(max),
    InputBuffer nvarchar(4000),
    SessionInfo nvarchar(max),
    );

CREATE TABLE #InputBuffer (
    EventType nvarchar(30),
    Params smallint,
    EventInfo nvarchar(4000)
    );


--Grab all sessions for blacklisted requests

INSERT INTO #Sessions (SessionID, DbName, HostName, ProgramName, LoginName, LoginTime, LastRequestStart, 
                    LastRequestEnd, TransactionCnt, Command, WaitTime, WaitResource, SqlText, SqlStatement)
SELECT s.session_id AS SessionID, 
       db_name(r.database_id) AS DbName,
       s.host_name AS HostName,
       s.program_name AS ProgramName,
       s.login_name AS LoginName,
       s.login_time AS LoginTime,
       s.last_request_start_time AS LastRequestStart,
       s.last_request_end_time AS LastRequestEnd,
       -- Need to use sysprocesses for now until we're fully on 2012/2014
       (SELECT TOP 1 sp.open_tran FROM master.sys.sysprocesses sp WHERE sp.spid = s.session_id) AS TransactionCnt,
       --s.open_transaction_count AS TransactionCnt,
       r.command AS Command,
       r.wait_time AS WaitTime,
       r.wait_resource AS WaitResource,
       COALESCE(t.text,'') AS SqlText,
       COALESCE(SUBSTRING(t.text, (r.statement_start_offset/2)+1, (
                (CASE r.statement_end_offset
                   WHEN -1 THEN DATALENGTH(t.text)
                   ELSE r.statement_end_offset
                 END - r.statement_start_offset)
              /2) + 1),'') AS SqlStatement
FROM sys.dm_exec_sessions s
INNER JOIN sys.dm_exec_requests r ON r.session_id = s.session_id
OUTER APPLY sys.dm_exec_sql_text (r.sql_handle) t
JOIN #ProgramBlacklist bl ON s.program_name LIKE bl.ProgramName
AND start_time < DATEADD(ss,@DurationThreshold,GETDATE());



-- Grab the input buffer for all sessions, too.
WHILE EXISTS (SELECT 1 FROM #Sessions WHERE InputBuffer IS NULL)
BEGIN
    TRUNCATE TABLE #InputBuffer;
    
    SELECT TOP 1 @Spid = SessionID, @ID = ID
    FROM #Sessions
    WHERE InputBuffer IS NULL;

    SET @Sql = 'DBCC INPUTBUFFER (' + CAST(@Spid AS varchar(10)) + ');';

    INSERT INTO #InputBuffer
    EXEC sp_executesql @sql;

    --SELECT @id, @Spid, COALESCE((SELECT TOP 1 EventInfo FROM #InputBuffer),'')
    --EXEC sp_executesql @sql;

    UPDATE b
    SET InputBuffer = COALESCE((SELECT TOP 1 EventInfo FROM #InputBuffer),'')
    FROM #Sessions b
    WHERE ID = @Id;
END;

--Convert Hex job_ids for SQL Agent jobs to names.
WHILE EXISTS(SELECT 1 FROM #Sessions WHERE ProgramName LIKE 'SQLAgent - TSQL JobStep (Job 0x%')
BEGIN
    SELECT @JobIdHex = '', @JobName = '';

    SELECT TOP 1 @ID = ID, 
            @JobIdHex =  SUBSTRING(ProgramName,30,34)
    FROM #Sessions
    WHERE ProgramName LIKE 'SQLAgent - TSQL JobStep (Job 0x%';

    SELECT @Sql = N'SELECT @JobName = name FROM msdb.dbo.sysjobs WHERE job_id = ' + @JobIdHex;
    EXEC sp_executesql @Sql, N'@JobName nvarchar(256) OUT', @JobName = @JobName OUT;

    UPDATE b
    SET ProgramName = LEFT(REPLACE(ProgramName,@JobIdHex,@JobName),128)
    FROM #Sessions b
    WHERE ID = @Id;
END;

--Decypher wait resources.
DECLARE wait_cur CURSOR FOR
    SELECT SessionID, WaitResource FROM #Sessions WHERE WaitResource <> '';

OPEN wait_cur;
FETCH NEXT FROM wait_cur INTO @Spid, @WaitResource;
WHILE @@FETCH_STATUS = 0
BEGIN
    IF @WaitResource LIKE 'KEY%'
    BEGIN
        --Decypher DB portion of wait resource
        SET @WaitResource = LTRIM(REPLACE(@WaitResource,'KEY:',''));
        SET @DbName = db_name(SUBSTRING(@WaitResource,0,CHARINDEX(':',@WaitResource)));
        --now get the object name
        SET @WaitResource = SUBSTRING(@WaitResource,CHARINDEX(':',@WaitResource)+1,256);
        SELECT @Sql = 'SELECT @ObjectName = SCHEMA_NAME(o.schema_id) + ''.'' + o.name, @IndexName = i.name ' +
            'FROM [' + @DbName + '].sys.partitions p ' +
            'JOIN [' + @DbName + '].sys.objects o ON p.OBJECT_ID = o.OBJECT_ID ' +
            'JOIN [' + @DbName + '].sys.indexes i ON p.OBJECT_ID = i.OBJECT_ID  AND p.index_id = i.index_id ' +
            'WHERE p.hobt_id = SUBSTRING(@WaitResource,0,CHARINDEX('' '',@WaitResource))'
        EXEC sp_executesql @sql,N'@WaitResource nvarchar(256),@ObjectName nvarchar(256) OUT,@IndexName nvarchar(256) OUT',
                @WaitResource = @WaitResource, @ObjectName = @ObjectName OUT, @IndexName = @IndexName OUT
        --now populate the WaitDescription column
        UPDATE b
        SET WaitDescription = 'KEY WAIT: ' + @DbName + '.' + @ObjectName + ' (' + COALESCE(@IndexName,'') + ')'
        FROM #Sessions b
        WHERE SessionID = @Spid;
    END;
    ELSE IF @WaitResource LIKE 'OBJECT%'
    BEGIN
        --Decypher DB portion of wait resource
        SET @WaitResource = LTRIM(REPLACE(@WaitResource,'OBJECT:',''));
        SET @DbName = db_name(SUBSTRING(@WaitResource,0,CHARINDEX(':',@WaitResource)));
        --now get the object name
        SET @WaitResource = SUBSTRING(@WaitResource,CHARINDEX(':',@WaitResource)+1,256);
        SET @Sql = 'SELECT @ObjectName = schema_name(schema_id) + ''.'' + name FROM [' + @DbName + '].sys.objects WHERE object_id = SUBSTRING(@WaitResource,0,CHARINDEX('':'',@WaitResource))';
        EXEC sp_executesql @sql,N'@WaitResource nvarchar(256),@ObjectName nvarchar(256) OUT',@WaitResource = @WaitResource, @ObjectName = @ObjectName OUT;
        --Now populate the WaitDescription column
        UPDATE b
        SET WaitDescription = 'OBJECT WAIT: ' + @DbName + '.' + @ObjectName
        FROM #Sessions b
        WHERE SessionID = @Spid;
    END;
    ELSE IF (@WaitResource LIKE 'PAGE%' OR @WaitResource LIKE 'RID%')
    BEGIN
        --Decypher DB portion of wait resource
        SELECT @WaitResource = LTRIM(REPLACE(@WaitResource,'PAGE:',''));
        SELECT @WaitResource = LTRIM(REPLACE(@WaitResource,'RID:',''));
        SET @DbName = db_name(SUBSTRING(@WaitResource,0,CHARINDEX(':',@WaitResource)));
        --now get the file name
        SET @WaitResource = SUBSTRING(@WaitResource,CHARINDEX(':',@WaitResource)+1,256)
        SELECT @ObjectName = name 
        FROM sys.master_files
        WHERE database_id = db_id(@DbName)
        AND file_id = SUBSTRING(@WaitResource,0,CHARINDEX(':',@WaitResource));
        --Now populate the WaitDescription column
        SET @WaitResource = SUBSTRING(@WaitResource,CHARINDEX(':',@WaitResource)+1,256)
        IF @WaitResource LIKE '%:%'
            UPDATE b
            SET WaitDescription = 'ROW WAIT: ' + @DbName + ' File: ' + @ObjectName + ' Page_id/Slot: ' + @WaitResource
            FROM #Sessions b
            WHERE SessionID = @Spid;
        ELSE
            UPDATE b
            SET WaitDescription = 'PAGE WAIT: ' + @DbName + ' File: ' + @ObjectName + ' Page_id: ' + @WaitResource
            FROM #Sessions b
            WHERE SessionID = @Spid;
    END;
    FETCH NEXT FROM wait_cur INTO @Spid, @WaitResource;
END;
CLOSE wait_cur;
DEALLOCATE wait_cur;



-- Populate SessionInfo column with HTML details for sending email
-- Since there's a bunch of logic here, code is more readable doing this separate than mashing it in with the rest of HTML email creation
UPDATE b
SET SessionInfo = '<span style="font-weight:bold">Login = </span>' + LoginName + '<br>' +
                  '<span style="font-weight:bold">Host Name = </span>' + HostName + '<br>' +
                  CASE WHEN TransactionCnt <> 0 
                    THEN '<span style="font-weight:bold">Transaction Count = </span>' + CAST(TransactionCnt AS nvarchar(10)) + '<br>' 
                    ELSE ''
                  END +
                  CASE WHEN WaitResource <> ''
                    THEN '<span style="font-weight:bold">Wait Resource = </span>' + COALESCE(WaitDescription,WaitResource) + '<br>' 
                    ELSE ''
                  END +
                  '<span style="font-weight:bold">DbName = </span>' + DbName + '<br>' +
                  '<span style="font-weight:bold">Last Request = </span>' + CONVERT(varchar(20),LastRequestStart,20) + '<br>' +
                  '<span style="font-weight:bold">Program Name = </span>' + ProgramName + '<br>'
FROM #Sessions b;

--output results in debug mode:
IF @Debug = 1
BEGIN
    IF NOT EXISTS (SELECT 1 FROM #Sessions)
        SELECT 'No Blacklisted Sessions Detected' AS BlacklistedSessions;
    ELSE
    BEGIN
        SELECT * FROM #Sessions;
    END;

END;

-- If no rows returned, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #Sessions)
    RETURN (0);

--Check EmailThreshold before continuing. RETURN within @EmailThreshold
-- should this check be at the top of the alert?
IF EXISTS (SELECT 1 FROM BlacklistedRequestHistory WHERE LogDateTime >= DATEADD(mi,-1*@EmailThreshold,GETDATE()))
    RETURN(0);

-- Logging & Email Only happens when @Debug = 0
IF @Debug = 0
BEGIN
    --Log leading blockers to a real table, too.
    INSERT INTO BlacklistedRequestHistory (SessionID, DbName, HostName, ProgramName, LoginName, LoginTime, LastRequestStart, 
                        LastRequestEnd, TransactionCnt, Command, WaitTime, WaitResource, SqlText, SqlStatement)
    SELECT SessionID, DbName, HostName, ProgramName, LoginName, LoginTime, LastRequestStart, 
                        LastRequestEnd, TransactionCnt, Command, WaitTime, WaitResource, SqlText, SqlStatement
    FROM #Sessions;

    --Email about blocking
    -- TBD: Possibly email users if they are doing bad stuff as well (based on loginname)?

    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

    --Build the body of the email

    --Blacklisted sessions
    SET @EmailBody = @EmailBody + N'<h2>Blacklisted request(s):</h2>' + CHAR(10) +
            N'<table><tr>' +
            N'<th>SPID</th>' +
            N'<th>Wait Time (sec)</th>' +
            N'<th>Session Info</th>' +
            N'<th>SQL Statement</th>' +
            N'<th>Input Buffer</th>' + 
            N'</tr>' +
            CAST(( SELECT
                    td = CAST(SessionID AS nvarchar(10)), '',
                    td = CAST(WaitTime/1000 AS nvarchar(10)), '',
                    td = SessionInfo ,'',
                    td = LEFT(SqlStatement,300), '',    --Truncate string, too long for email alert
                    td = LEFT(InputBuffer,300), ''      --Truncate string, too long for email alert
                    FROM #Sessions s
                    ORDER BY WaitTime desc
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';
    
    SELECT @EmailBody = @EmailBody + '<hr>' + dbo.cfn_EmailServerInfo();

    SELECT @EmailBody = REPLACE(@EmailBody,'&lt;','<');
    SELECT @EmailBody = REPLACE(@EmailBody,'&gt;','>');


    SET @EmailSubject = 'ALERT: Blacklisted Sessions Detected';
    SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
        @from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;

DROP TABLE #Sessions;
DROP TABLE #InputBuffer;

