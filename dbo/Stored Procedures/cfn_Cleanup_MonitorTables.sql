CREATE PROCEDURE cfn_Cleanup_MonitorTables
	@RetainDays int = 30
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150128
    This procedure cleans up data in the cfn_Monitor_xxx tables in the DBA database.
	Data is pulled centrally to the DatabaseManager DB for long-term use.
	The tables in DBA are effectively just staging tables to prepare the data to be pulled.

	Also clean up CommandLog table used by Ola Hallengren's backup code.

	For each table, delete in batches to minimize blocking.


PARAMETERS
* @RetainDays - number of days to retain data on cfn_Monitor_xxx tables.
**************************************************************************************************
MODIFICATIONS:
    20150107 - 
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @TableName varchar(256),
	@BatchSize int = 1000,
	@RowCnt int,
	@CleanupDateTime datetime2(0),
	@sql nvarchar(max);

SELECT @CleanupDateTime = DATEADD(dd, - 1 * @RetainDays, SYSDATETIME());

PRINT 'Cleaning up monitor data older than ' + CONVERT(varchar(30),@CleanupDateTime,120);

DECLARE tab_cur CURSOR FOR
	SELECT DISTINCT o.name
	FROM sys.objects o 
	JOIN sys.columns c ON c.object_id = o.object_id
	WHERE o.type = 'U'
	AND c.name = 'LogDateTime'
	AND o.name LIKE 'Monitor%';


--Use cursor to cleanup all cfn_Monitor_xxxx tables
OPEN tab_cur;
FETCH NEXT FROM tab_cur INTO @TableName;

WHILE @@FETCH_STATUS = 0
BEGIN
	--Special handling for cfn_Monitor_Info
		--We can clean up the history, but keep the most recent.
		--This is because we only log a change...we need the most recent to detect a change.
	IF @TableName = 'Monitor_Info'
	BEGIN
		SELECT @RowCnt = 1; --initialize so we go into the loop the first time.
		WHILE (@RowCnt > 0)
		BEGIN
			DELETE TOP (@BatchSize) i
			FROM Monitor_Info i
			LEFT JOIN dbo.Monitor_CurrentInfo ci ON ci.ServerName = i.ServerName
				AND ci.LogDateTime = i.LogDateTime
				AND ci.FacetName = i.FacetName
			WHERE i.LogDateTime <= @CleanupDateTime
				AND ci.LogDateTime IS NULL;
			SET @RowCnt = @@ROWCOUNT;
		END;
	END
	--Generic cleanup, use TSQL to delete all rows older than N days
	ELSE
	BEGIN
		SET @sql = N'WHILE EXISTS (SELECT 1 FROM [' + @TableName + '] WHERE LogDateTime <= @CleanupDateTime)
			DELETE t1 FROM [' + @TableName + '] AS t1 WHERE LogDateTime <= @CleanupDateTime;';
		--PRINT @sql;
		EXEC sp_executesql @stmt=@sql, @params=N'@CleanupDateTime datetime2(0)', @CleanupDateTime = @CleanupDateTime;
	END;
	--
	FETCH NEXT FROM tab_cur INTO @TableName;
END;

CLOSE tab_cur;
DEALLOCATE tab_cur;

    

GO

