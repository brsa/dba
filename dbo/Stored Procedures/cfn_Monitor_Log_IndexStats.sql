﻿CREATE PROCEDURE [dbo].[cfn_Monitor_Log_IndexStats]
	@Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150817
    This procedure logs index useage stats as well as the basic info on the index definition.
	Unlike procedure stats, I'm not computing the delta--just capturing stats directly.
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
DECLARE @Sql nvarchar(max),
	    @DbName nvarchar(128),
		@LogDateTime datetime2(0) = SYSDATETIME();

DECLARE db_cur CURSOR FOR
	SELECT name
	FROM sys.databases
	WHERE state_desc = 'ONLINE'
	AND name NOT IN ('tempdb');

OPEN db_cur;
FETCH NEXT FROM db_cur INTO @DbName;

WHILE @@FETCH_STATUS = 0
BEGIN

	SELECT @Sql = 'SELECT 
		SYSDATETIME(),
		DbName = ''' + @DbName + ''', 
		SchemaName = s.name, 
		ObjectName = o.name,
		IndexName =  COALESCE(i.name,''''),
		IndexType = i.type_desc, 
		KeyColumns = Cols.KeyColumns,
		IncludedColumns = Cols.InclColumns,
		FilterDefinition = i.filter_definition,
		DataCompression = size.DataCompression,
		IsUnique = i.is_unique, 
		IsPrimaryKey = i.is_primary_key, 
		IsFiltered = i.has_filter,
		IsView = CASE WHEN o.type = ''V'' THEN 1 ELSE 0 END,
		UserSeeks = ius.user_seeks,
		UserScans = ius.user_scans,
		UserLookups = ius.user_lookups,
		UserUpdates = ius.user_updates,
		NumRows = size.NumRows,
		InRowPages = size.InRowPages,
		LobPages = size.LobPages,
		OverflowPages = size.OverflowPages,
		UsedPages = size.UsedPages,
		ReservedPages = size.ReservedPages
	FROM [' + @DbName + '].sys.objects o
	JOIN [' + @DbName + '].sys.indexes i ON i.object_id = o.object_id
	JOIN [' + @DbName + '].sys.schemas s ON s.schema_id = o.schema_id
	LEFT JOIN [' + @DbName + '].sys.dm_db_index_usage_stats ius ON i.object_id = ius.object_id AND ius.index_id = i.index_id AND ius.database_id = db_id(''' + @DbName + ''')
	CROSS APPLY( SELECT 
					SUBSTRING( ( SELECT '', '' + co.name
								FROM [' + @DbName + '].sys.index_columns ic
								JOIN [' + @DbName + '].sys.columns co ON co.object_id = i.object_id AND co.column_id = ic.column_id
								WHERE ic.object_id = i.object_id AND ic.index_id = i.index_id AND ic.is_included_column = 0
								ORDER BY ic.key_ordinal
								FOR XML PATH('''')
								), 3, 10000 )    AS KeyColumns   
				--Include the clustering columns in the included column list, even if they are not explicitly listed 
				--Order doesnt matter in included columns, so dont worry about matching definition order
				, SUBSTRING( ( SELECT '', '' + col.name
								FROM (SELECT co.name
									FROM [' + @DbName + '].sys.index_columns ic
									JOIN [' + @DbName + '].sys.columns co ON co.object_id = i.object_id AND co.column_id = ic.column_id
									WHERE ic.object_id = i.object_id AND ic.index_id = i.index_id AND ic.is_included_column = 1
									UNION
									SELECT co.name
									FROM [' + @DbName + '].sys.indexes pk 
									JOIN [' + @DbName + '].sys.index_columns ic ON ic.object_id = pk.object_id AND ic.index_id = pk.index_id AND ic.is_included_column = 0
									JOIN [' + @DbName + '].sys.columns co ON co.object_id = i.object_id AND co.column_id = ic.column_id
									WHERE i.object_id = pk.object_id AND pk.type = 1
									AND pk.index_id <> i.index_id
									) col
								FOR XML PATH('''')
								), 3, 10000	)    AS InclColumns    

	) Cols
	--This might be the wrong move, but lets just roll this stuff up into a single line, just in case.
	CROSS APPLY (SELECT InRowPages = SUM(ps.in_row_reserved_page_count),
						LobPages = SUM(ps.lob_reserved_page_count),
						OverflowPages = SUM(ps.row_overflow_reserved_page_count),
						UsedPages = SUM(ps.used_page_count),
						ReservedPages = SUM(ps.reserved_page_count),
						NumRows = SUM(ps.row_count),
						DataCompression = MAX(p.data_compression_desc)
				 FROM [' + @DbName + '].sys.dm_db_partition_stats ps
				 JOIN [' + @DbName + '].sys.partitions p ON p.partition_id = ps.partition_id AND p.object_id = ps.object_id 
							AND p.index_id = ps.index_id and p.partition_number = ps.partition_number
				 WHERE ps.object_id = i.object_id AND ps.index_id = i.index_id
				 ) AS Size
	WHERE o.is_ms_shipped = 0
	AND o.type IN (''U'',''V'');';

	PRINT @DbName
	PRINT @sql

	IF @Debug = 1
		EXEC sp_executesql @Sql;
	ELSE
		INSERT INTO dbo.Monitor_IndexStats (LogDateTime, DbName, SchemaName, ObjectName, IndexName, IndexType, 
			  KeyColumns, IncludedColumns, FilterDefinition, DataCompression, 
			  IsUnique, IsPrimaryKey, IsFiltered, IsView,
			  UserSeeks, UserScans, UserLookups, UserUpdates, NumRows, 
			  InRowPages, LobPages, OverflowPages, UsedPages, ReservedPages)
		EXEC sp_executesql @Sql;


	FETCH NEXT FROM db_cur INTO @DbName;
END;

CLOSE db_cur;
DEALLOCATE db_cur;

GO






