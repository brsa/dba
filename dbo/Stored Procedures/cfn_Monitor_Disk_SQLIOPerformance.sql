﻿/*****************************************************************************************
Name: cfn_Monitor_Disk_SQLIOPerformance

Desc: Check disk performance using SQLIO. This is meant to be run on-demand when a new server is built
	and SQL Server is installed.

Auth: Don Tam

Date: 12/20/11

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
10/08/14		MDAK					Added a step to initialize test files before running performance test
										Added 5 second delay in between tests to allow disk subsystem to settle
10/13/14		MDAK					Fixed DEL command to actually delete test file
05/07/15		MDAK					Renamed sproc
										Insert individual statements into a temp table, then insert into centralized table
*****************************************************************************************/


-- EXEC cfn_Monitor_Disk_SQLIOPerformance @drive='C:', @thread=8, @testFileSize=1024


CREATE PROCEDURE [dbo].[cfn_Monitor_Disk_SQLIOPerformance]
	@drive VARCHAR(10) = NULL,	-- Test all drives by default
	@thread VARCHAR(10) = 8,
	@testFileSize VARCHAR(10) = 1024

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @path VARCHAR(100),
		@block VARCHAR(10),
		@IOPS VARCHAR(20),
		@MBPS VARCHAR(20),
		@minLAT VARCHAR(20),
		@avgLAT VARCHAR(20),
		@maxLAT VARCHAR(20),
		@testfile VARCHAR(100),
		@cmdTxt VARCHAR(1000),
		@line VARCHAR(100),
		@i INT,
		@dateRun DATETIME = GETDATE();
	DECLARE @tmpTableList TABLE (
		line VARCHAR(100)
		);
	DECLARE @tmpNFS TABLE (
		line VARCHAR(100)
		);
	DECLARE @tmpIOTest TABLE (
		line VARCHAR(100)
		);

	CREATE TABLE #diskPerformance (
		ServerName NVARCHAR(50) NULL,
		Duration NVARCHAR(10) NULL,
		thread NVARCHAR(10) NULL,
		IO NVARCHAR(10) NULL,
		Drive NVARCHAR(10) NULL,
		BlockSize NVARCHAR(10) NULL,
		TestBlock NVARCHAR(10) NULL,
		TestType NVARCHAR(25) NULL,
		IOPS NVARCHAR(20) NULL,
		MBPS NVARCHAR(20) NULL,
		minLAT NVARCHAR(20) NULL,
		avgLAT NVARCHAR(20) NULL,
		maxLAT NVARCHAR(20) NULL,
		Updated DATETIME NULL,
		TestSize NVARCHAR(10) NULL
	);

	EXEC master.dbo.xp_fileexist 'C:\sqlio\sqlio.exe', @i out;

	-- Copy SQLIO tool to the local machine if it doesn't exist
	IF @i <> 1
	BEGIN
		EXEC master.dbo.xp_cmdshell 'xcopy /Iy \\commonwealth.com\public\Installs\Microsoft\SQL\Tools\SQLIO\. C:\sqlio', no_output;
	END;

	EXEC master.dbo.xp_fileexist 'C:\sqlio\sqlio.exe', @i out;

	IF @i = 1
	BEGIN
		IF ISNULL(@drive, '') = ''	-- If @drive input parameter is not specified
		BEGIN
			SET @cmdTxt = 'fsutil fsinfo drives';
		
			INSERT INTO @tmpTableList
			EXEC master.dbo.xp_cmdshell @cmdTxt;
		
			SELECT @line = REPLACE(line, 'Drives: ', '')
			FROM @tmpTableList
			WHERE line IS NOT NULL;
		END

		ELSE
		BEGIN
			IF PATINDEX('%\%', @drive) = 0	-- If @drive parameter is missing '\'
			BEGIN
				SET @drive = @drive + '\';
			END;

			SET @line = @drive;
		END;
	
		WHILE (CHARINDEX('\', @line) > 0)
		BEGIN
			SET @drive = LTRIM(RTRIM(SUBSTRING(@line, 1, CHARINDEX('\', @line) - 1)));
	
			SET @cmdTxt = 'fsutil fsinfo ntfsinfo ' + @drive;

			INSERT INTO @tmpNFS
			EXEC master.dbo.xp_cmdshell @cmdTxt;

			SELECT @block = RTRIM(LTRIM(REPLACE(line, 'Bytes Per Cluster :', '')))
			FROM @tmpNFS 
			WHERE PATINDEX('%Per Cluster%', line) > 0;


			IF ISNULL(@block, '') <> ''
			BEGIN
				SET @testfile = @drive + '\' + 'testfile.dat ';
				SET @path = @testfile + @thread + ' 0x0 ' + @testFileSize;
				SET @cmdTxt = 'echo ' + @path + ' > C:\sqlio\param.txt';
				EXEC master.dbo.xp_cmdshell @cmdTxt, no_output;

				-- Initialize Files
				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s10 -o8 -fsequential -b8 -LS -FC:\sqlio\param.txt';
				EXEC master.dbo.xp_cmdshell @cmdTxt, no_output;

					WAITFOR DELAY '00:00:05';

				-- Test Write --
				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o1 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,1,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o2 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
				(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,2,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o4 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,4,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o8 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,8,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o16 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,16,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o32 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,32,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kW -s60 -frandom -o64 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,64,@drive,@block,64,'write',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);
			
					WAITFOR DELAY '00:00:05';


				-- Test Read --
				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o1 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,1,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o2 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,2,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o4 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,4,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o8 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,8,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o16 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,16,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o32 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,32,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);

					WAITFOR DELAY '00:00:05';

				SET @cmdTxt = 'C:\sqlio\sqlio -kR -s60 -frandom -o64 -b64 -LS -FC:\sqlio\param.txt';
				DELETE @tmpIOTest;
				INSERT INTO @tmpIOTest
				EXEC master.dbo.xp_cmdshell @cmdTxt;
				SELECT @IOPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'IOs/sec:%';
				SELECT @MBPS=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'MBs/sec:%';
				SELECT @minLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Min_Latency%';
				SELECT @avgLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Avg_Latency%';
				SELECT @maxLAT=RTRIM(LTRIM(SUBSTRING(line,PATINDEX('%:%',line)+1,LEN(line)-PATINDEX('%:%',line)+1))) FROM @tmpIOTest WHERE line LIKE 'Max_Latency%';
				INSERT INTO #diskPerformance
					(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
					 VALUES(@@SERVERNAME,60,@thread,64,@drive,@block,64,'read',@IOPS,@MBPS,@minLAT,@avgLAT,@maxLAT,GETDATE(),@testFileSize);


				EXEC master.dbo.xp_fileexist @testfile, @i out;
				IF @i = 1
				BEGIN
					SET @cmdTxt = 'DEL /Q ' + @testfile;
					EXEC master.dbo.xp_cmdshell @cmdTxt, no_output;
				END;  
	    
				SET @block=NULL;
				SET @IOPS=NULL;
				SET @MBPS=NULL;
				SET @minLAT=NULL;
				SET @avgLAT=NULL;
				SET @maxLAT=NULL;
	     
			END;

			DELETE @tmpNFS;
			SET @line = SUBSTRING(@line, CHARINDEX('\', @line) + LEN('\'), len(@line));
		END;
	END;

	-- Insert data into table on central management server
	INSERT INTO dbo.Monitor_Disk_SQLIOPerformance
		(ServerName,Duration,thread,IO,Drive,BlockSize,TestBlock,testType,IOPS,MBPS,minLAT,avgLAT,maxLAT,Updated,TestSize)
	SELECT ServerName, Duration, thread, IO, Drive, BlockSize, TestBlock, TestType, IOPs, MBPS, minLAT, avgLAT, maxLAT, Updated, TestSize
	FROM #diskPerformance;

	RETURN 0;
END;