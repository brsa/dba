﻿CREATE PROC [dbo].[cfn_DROP_DEVDB_Objects]
@DBExclusionList VARCHAR(1000)=''
,@Debug BIT=1
AS
SET NOCOUNT ON;
IF OBJECT_ID('tempdb.dbo.#DEVDBObjectsList') IS NOT NULL
	DROP TABLE #DEVDBObjectsList;

CREATE TABLE #DEVDBObjectsList (iID INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,DatabaseName VARCHAR(100),ObjectName VARCHAR(200),SchemaName VARCHAR(100),ObjectType VARCHAR(100),CreateDate DATETIME, ModifyDate DATETIME);

INSERT INTO #DEVDBObjectsList
        ( DatabaseName ,
          ObjectName ,
		  SchemaName ,
          ObjectType ,
          CreateDate ,
          ModifyDate
        )
EXEC sys.sp_MSforeachdb  @command1=N'USE [?] 
DECLARE @CurrentDate DATETIME=GETDATE()
SELECT DB_NAME(),O.name,SCHEMA_NAME(O.schema_id) AS SchemaName,O.type_desc,O.create_date,O.modify_date
FROM sys.objects O 
WHERE DATEDIFF(DAY,O.modify_date, @CurrentDate) > 30 
AND O.name LIKE ''%_DEVDB''';

DELETE DOL
FROM #DEVDBObjectsList DOL
WHERE DOL.DatabaseName IN (SELECT value FROM dbo.fn_split(@DBExclusionList,',') )
OR dbo.dm_hadr_db_role( DOL.DatabaseName) NOT IN( 'PRIMARY', 'ONLINE');

IF @debug=1
BEGIN
	SELECT *
	FROM #DEVDBObjectsList DOL;
	RETURN;
END;

/*DROP Objects*/
DECLARE @TotalObjectsToDelete INT=(SELECT MAX(iID) FROM #DEVDBObjectsList)
	,@iID INT =1
	,@DropScript NVARCHAR(500);

WHILE @iID <= @TotalObjectsToDelete
BEGIN

SET @DropScript=(SELECT 'USE ['+DatabaseName+'];IF DBA.dbo.dm_hadr_db_role( '''+DatabaseName+''') IN( ''PRIMARY'', ''ONLINE'')
					BEGIN
					 DROP '+CASE WHEN ObjectType='USER_TABLE' THEN ' TABLE '
					  WHEN ObjectType='SQL_STORED_PROCEDURE' THEN ' PROCEDURE '
					  WHEN ObjectType='SYNONYM' THEN ' SYNONYM '
					  WHEN ObjectType='VIEW' THEN ' VIEW '
					  WHEN ObjectType='SQL_TRIGGER' THEN ' TRIGGER '
					  WHEN ObjectType LIKE '%FUNCTION%' THEN ' FUNCTION ' END+ '['+SchemaName +'].['+ObjectName+'];
					END' 
					FROM #DEVDBObjectsList 
					WHERE iID=@iID
				);

PRINT @DropScript;
EXEC sys.sp_executesql @DropScript;

SET @iID=@iID+1;

END;
