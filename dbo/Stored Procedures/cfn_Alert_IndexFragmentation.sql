CREATE PROCEDURE [dbo].[cfn_Alert_IndexFragmentation]
	@StartTime datetime = NULL,
	@EndTime datetime = NULL,
	@EmailRecipients varchar(max) = NULL,
    @Debug tinyint = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150306
    This procedure expects Ola Hallengren's IndexOptimize has already been run in a log-only mode,
	with parameters indicating REBUILD operations prior to running this alert.
	For Example:
		EXECUTE DBA.dbo.IndexOptimize
			@Databases = 'ALL_DATABASES',
			@LOBCompaction = 'Y',
			@FragmentationLow = NULL,
			@FragmentationMedium = 'INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE',
			@FragmentationHigh = 'INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE',
			@FragmentationLevel1 = 15,
			@LogToTable = 'Y',
			@Execute = 'N';

    If this procedure is run within the same SQL Agent job as the IndexOptimize procedure, it will
	automatically detect the appropriate time frame to query for.

	If it is run in a different manner, the @StartDate & @EndDate parameters are used to indicate
	what portion of the IndexOptimize log (DBA.dbo.CommandLog) should be queried.

PARAMETERS
* @StartTime - Indicates starting Date/Time to use when querying CommandLog for REBUILD commands.
				If being run within the same SQL Agent job as IndexOptimize, this can be NULL
* @EndTime - Indicates ending Date/Time to use when querying CommandLog for REBUILD commands.
				If being run within the same SQL Agent job as IndexOptimize, this can be NULL
* @EmailRecipients - delimited list of email addresses. Used for sending email alert.
**************************************************************************************************
MODIFICATIONS:
    20150324 - AM2 - Add filter to only alert on tables > 50MB in size
	
*************************************************************************************************/
SET NOCOUNT ON;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;
/* DEBUG:
SELECT @StartTime  = '3/5/2015',
		 @EndTime  = '3/6/2015',
		 @EmailRecipients = 'amallon@commonwealth.com'
--*/

DECLARE @JobName nvarchar(128),
        @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

IF @EndTime IS NULL
	SELECT @EndTime = GETDATE();
IF @StartTime IS NULL
BEGIN
	SELECT @JobName = program_name
	FROM sys.dm_exec_sessions
	WHERE session_id = @@SPID
	
	BEGIN TRY
		SELECT @JobName = name
		FROM msdb.dbo.sysjobs
		WHERE job_id = CONVERT(varbinary(34),SUBSTRING(@JobName,PATINDEX('%Job 0x%',@JobName)+4,34),1);
	END TRY
	BEGIN CATCH
		SELECT @JobName = '~';
	END CATCH;

	SELECT @StartTime = MAX(start_execution_date)
	FROM msdb.dbo.sysjobs j  
	INNER JOIN msdb.dbo.sysjobactivity ja ON j.job_id = ja.job_id 
	WHERE j.name = @JobName 
END;

IF @StartTime IS NULL
BEGIN
	RAISERROR ('@StartTime must be specified, unless being run from within the Monthly SQL Agent job',16,1);
	RETURN;
END;

SELECT ID, DatabaseName, SchemaName, ObjectName, ObjectType, IndexName, 
		ExtendedInfo.value('(/ExtendedInfo/PageCount)[1]','decimal(15,3)')*8/1024 As IndexSizeMB, 
		ExtendedInfo.value('(/ExtendedInfo/Fragmentation)[1]','decimal(7,4)') As FragmentationPercent,
		Command
INTO #Results
FROM dbo.CommandLog
WHERE StartTime BETWEEN @StartTime AND @EndTime
AND CommandType = 'ALTER_INDEX'
AND Command LIKE '%REBUILD%'




IF @Debug = 1
BEGIN
    SELECT * FROM #Results ORDER BY DatabaseName, SchemaName, ObjectName, IndexName;
END;


IF NOT EXISTS (SELECT 1 FROM #Results)
    RETURN (0);

IF @Debug = 0
BEGIN
    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

	    --Build the body of the email based on the #Results
    SET @EmailBody = @EmailBody + N'<h2>Index Rebuilds needed on ' + @@SERVERNAME + N':</h2>' + CHAR(10) +
			N'<div>' +
			N'For additional details, use the following SQL commands:' + 
			N'<ul><li>EXEC DBA.dbo.cfn_Alert_IndexFragmentation @StartTime = ''' + CONVERT(nvarchar(30),@StartTime,120) + 
			N''', @EndTime = ''' + CONVERT(nvarchar(30),@EndTime,120) + N''', @Debug = 1; </li>' +
			N'<li>EXEC sp_helpindex3 ''[TableName]''</li></ul>' +
			N'</div>' +
			N'<table><tr>' +
            N'<th>Database Name</th>' +
            N'<th>Object</th>' +
            N'<th>Index</th>' +
            N'<th>Index Size (Mb)</th>' +
            N'<th>Fragmentation Percent</th></tr>' +
            CAST(( SELECT
                    td = DatabaseName, '',
                    td = SchemaName + '.' + ObjectName, '',
                    td = IndexName, '',
                    td = CAST(IndexSizeMb AS nvarchar(20)), '',
                    td = CAST(FragmentationPercent AS nvarchar(10)), ''
                    FROM #Results
					WHERE IndexSizeMB >= 50
                    ORDER BY DatabaseName, SchemaName, ObjectName, IndexName
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';

    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();
		
	SET @EmailSubject = 'ALERT: Index Fragmentation';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;


DROP TABLE #Results

GO

