﻿CREATE PROCEDURE dbo.cfn_Monitor_Log_QueryStats

AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141001
    This procedure logs query stats from the DMVs to cfn_Monitor_QueryStats.
	Calculates a delta since the last run, and saves that delta to the table.
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME(),
	@LastLogDateTime datetime2(0),
	@IntervalMinutes int;

--When did we last check stats?
SELECT @LastLogDateTime = MAX(LogDateTime) FROM Monitor_QueryStats;

--If the instance rebooted since then, we should consider that restart time as our last time.
    -- Stats are only cumulative since start time, so @IntervalMinutues should reflect a shorter time.
	-- Also if we just restarted, we oughtn't compare against last collection to compute a delta.
SELECT @LastLogDateTime = CASE WHEN create_date > @LastLogDateTime THEN create_date 
								ELSE @LastLogDateTime END 
FROM sys.databases WHERE name = 'tempdb';

--This could easily be in-line below. But I'm going to do it here for clarity
SELECT @IntervalMinutes = COALESCE(DATEDIFF(mi,@LastLogDateTime,@LogDateTime),0);

--We use a physical table for our temp table, because we need to compare between this run & last run
INSERT INTO Monitor_temp_QueryStats (LogDateTime, DbName, SchemaName, ObjectName, LineNumber, /*SqlText,*/ TextLine, 
	NumCalls, TotalPhysicalReads, TotalLogicalReads, TotalLogicalWrites, TotalWorkerTime, TotalClrTime, TotalElapsedTime)
SELECT @LogDateTime AS LogDateTime,
	COALESCE(CASE WHEN t.dbid = 32767 THEN 'Resource' ELSE DB_NAME(t.dbid) END,'Ad-Hoc Query') AS DbName,
	COALESCE(OBJECT_SCHEMA_NAME(t.objectid, t.dbid),'') AS SchemaName,
	COALESCE(OBJECT_NAME(t.objectid, t.dbid),CONVERT(varchar(128),qs.sql_handle,1),'') AS ObjectName,
	ROW_NUMBER() OVER(PARTITION BY qs.sql_handle ORDER BY qs.statement_start_offset) AS LineNumber,
	/*COALESCE(t.text,'') AS SqlText,*/
	LEFT(COALESCE(SUBSTRING(t.text, (qs.statement_start_offset/2)+1, (
                (CASE qs.statement_end_offset
                   WHEN -1 THEN DATALENGTH(t.text)
                   ELSE qs.statement_end_offset
                 END - qs.statement_start_offset)
              /2) + 1),''),500) AS TextLine,
	qs.execution_count,
	qs.total_physical_reads,
	qs.total_logical_reads,
	qs.total_logical_writes,
	qs.total_worker_time,
	qs.total_clr_time,
	qs.total_elapsed_time
FROM sys.dm_exec_query_stats qs
OUTER APPLY sys.dm_exec_sql_text(qs.sql_handle) t;

--Now compare this most recent collection with the previous collection, and compute the delta.
WITH QueryStats_Aggregated
AS (
	SELECT LogDateTime, DBName, SchemaName, ObjectName, TextLine,
		 MIN(LineNumber) AS LineNumber,
         SUM(NumCalls) AS NumCalls,
         SUM(TotalPhysicalReads) AS TotalPhysicalReads,
		 SUM(TotalLogicalReads) AS TotalLogicalReads,
         SUM(TotalLogicalWrites) AS TotalLogicalWrites,
         SUM(TotalWorkerTime) AS TotalWorkerTime,
		 SUM(TotalClrTime) AS TotalClrTime,
         SUM(TotalElapsedTime) AS TotalElapsedTime
    FROM dbo.Monitor_temp_QueryStats
    GROUP BY LogDateTime, DBName, SchemaName, ObjectName, TextLine
)
INSERT INTO Monitor_QueryStats (LogDateTime, DbName, SchemaName, ObjectName, LineNumber, TextLine, IntervalMinutes,
			NumCalls, TotalPhysicalReads, TotalLogicalReads, TotalLogicalWrites, TotalWorkerTime, TotalClrTime, TotalElapsedTime)
SELECT new.LogDateTime, new.DbName, new.SchemaName, new.ObjectName, 
	new.LineNumber AS LineNumber,
	new.TextLine, 
	@IntervalMinutes AS IntervalMinutes,
	new.NumCalls - COALESCE(old.NumCalls,0) AS NumCalls,
	new.TotalPhysicalReads - COALESCE(old.TotalPhysicalReads,0) AS TotalPhysicalReads,
	new.TotalLogicalReads - COALESCE(old.TotalLogicalReads,0) AS TotalLogicalReads,
	new.TotalLogicalWrites - COALESCE(old.TotalLogicalWrites,0) AS TotalLogicalWrites,
	new.TotalWorkerTime - COALESCE(old.TotalWorkerTime,0) AS TotalWorkerTime,
	new.TotalClrTime - COALESCE(old.TotalClrTime,0) AS TotalClrTime,
	new.TotalElapsedTime - COALESCE(old.TotalElapsedTime,0) AS TotalElapsedTime
FROM QueryStats_Aggregated new
LEFT JOIN QueryStats_Aggregated old
	ON old.DbName = new.DbName AND old.SchemaName = new.SchemaName AND old.ObjectName = new.ObjectName 
		AND old.TextLine = new.TextLine AND old.LogDateTime = @LastLogDateTime
WHERE new.LogDateTime = @LogDateTime
AND new.NumCalls - COALESCE(old.NumCalls,0) > 0;


--Aggressively clean out temp table of everything except the last 72 hours
WHILE @@ROWCOUNT <> 0
	DELETE TOP (1000) Monitor_temp_QueryStats 
	WHERE LogDateTime < DATEADD(hh,-24,@LogDateTime);