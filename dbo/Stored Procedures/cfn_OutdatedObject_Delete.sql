﻿CREATE PROCEDURE [dbo].[cfn_OutdatedObject_Delete]
 @databasename varchar(100)=null
-- =============================================
-- Author:		Don Tam
-- Create date: 9/28/2012
-- Description:	check and validate database objects
-- cfn_deleteOutdatedObject 'cfn_security'
-- =============================================
-- AM2  Make some hacky changes so I can move this into DBA for TESLA.
--   Wrapping some stuff in dyamic SQL just to make the TFS build happy with outside references.
--   Revisit this in the near future.
-- =============================================
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	Declare @query varchar(8000)
	Declare @vDBName varchar(100), @cmd varchar(255), @msg varchar(255), @objName varchar(200), @objType varchar(100), @checkOn datetime=getdate()

	Declare @outdatedObj table (
	databasename varchar(100),
	name varchar(200),
	typeDesc varchar(100),
	num int identity(1,1)
	)

	Declare @irec int, @itotal int
	set @vDBName = @databasename

	insert into @outdatedObj
	(
	databasename,
	name,
	typeDesc
	)
	exec ('[TFS-SOURCE].[DatabaseManager].dbo.cfn_listOfOutdatedObjectsToDelete ''' + @@servername + ''', ''' + @vDBName + ''';')
	/*
	select @vDBName as databasename, name, typeDesc
	from [TFS-SOURCE].[SchemaManager].dbo.outdatedObj
	where sqlserver = @@servername
	and databasename = @vDBName
	and (datediff(day,insertedOn, getdate()) >=30
	or ok2Delete = 1)
	*/
	--and isNULL(ok2Delete,0) = 0

	if exists (select top 1 name from @outdatedObj)
	BEGIN
	set @irec = 1
	select @itotal = max(num) from @outdatedObj

	while (@irec <= @itotal)
	BEGIN
	select
	@vDBName = databasename,
	@objName = name,
	@objType=typeDesc from @outdatedObj
	where num = @irec

		select @query = '
		if exists (select top 1 name from sys.databases where name = '''+ @vDBName + ''')
		BEGIN
					use [' + @vDBName + '];

		if exists (select top 1 name from dbo.sysobjects where name = ''' + @objName + ''')
		BEGIN
			BEGIN TRY
			drop ' + @objType + ' dbo.[' + @objName + '];
			END TRY
			BEGIN CATCH
				print ''[' + @vDBName + ']->drop ' + @objType + ' [' + @objName + ']''
			END CATCH
		END
		END
			update [TFS-SOURCE].[DatabaseManager].dbo.outdatedObj
			set [deletedOn] = getdate()
			where name = ''' + @objName + '''
			and databasename = ''' + @vDBName + '''
			and typeDesc = ''' + @objType + '''
			and [sqlserver] = @@servername
		'
		print @query
		exec(@query)
		set @irec = @irec + 1
	END


	END


END


GO