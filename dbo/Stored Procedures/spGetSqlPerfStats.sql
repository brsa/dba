﻿CREATE PROC dbo.spGetSQLPerfStats 
AS 
SET NOCOUNT ON 

CREATE TABLE #tFileList 
( 
databaseName sysname, 
logSize decimal(18,5), 
logUsed decimal(18,5), 
status INT 
) 

INSERT INTO #tFileList 
       EXEC spSQLPerf 

INSERT INTO logSpaceStats (databaseName, logSize, logUsed) 
SELECT databasename, logSize, logUsed 
FROM #tFileList 

DROP TABLE #tFileList 
GO