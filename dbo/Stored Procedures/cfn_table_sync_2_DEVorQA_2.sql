﻿-- =============================================
-- Author:		Don Tam
-- Create date: 3/20/2018
-- Description:	CFN Custom Table Sync process for QA and DEV
-- =============================================
CREATE PROCEDURE [dbo].[cfn_table_sync_2_DEVorQA_2]
@debug bit = 0

AS

BEGIN
SET NOCOUNT ON;

DECLARE @servername VARCHAR(100) ,
    @sqlAlias VARCHAR(25) ,
    @num INT = 1 ,
    @total INT ,
    @srcDatabase VARCHAR(100) ,
    @srcTable VARCHAR(200) ,
    @queryTxt VARCHAR(8000);

DECLARE @tbList TABLE (sourceserver VARCHAR(100) ,databasename VARCHAR(100) , tablename VARCHAR(200) ,num INT IDENTITY(1, 1) );

exec dbo.CFN_BCPIn;

IF OBJECT_ID('tempdb.dbo.##tmpCFNRepltableSynError') IS NOT NULL
    DROP TABLE ##tmpCFNRepltableSynError;

CREATE TABLE ##tmpCFNRepltableSynError (servername VARCHAR(100) ,databasename VARCHAR(100) ,tablename VARCHAR(200) ,msg VARCHAR(1000));

SET @servername = @@SERVERNAME;

INSERT INTO @tbList ( sourceserver ,databasename ,tablename )
SELECT base.SourceServer,base.SourceDatabase,base.SourceTable 
from openrowset('sqlncli', 'server=DC01DB-P015SQL;Trusted_Connection=yes;', 'SELECT * FROM [DatabaseManager].[dbo].[TableRefreshInTestEnvironment]; ') base
	WHERE base.Environment = CASE WHEN @servername LIKE '%DEV%' THEN 'development' 
									 WHEN @servername LIKE 'DEV%' THEN 'development' 
									 WHEN @servername LIKE 'QA%' THEN 'QA' WHEN @servername LIKE '%QA%' THEN 'QA' END
									 and base.sync = 1
ORDER BY SourceServer,sourcedatabase, priority, sourcetable;
--	SELECT base.vchSourceServer,base.vchSourceDatabase,base.vchSourceTable
--	FROM DBMANAGEMENT.DatabaseManager.dbo.repl_table_data_2_QAorDEV base
--	WHERE base.vchEnvironment = CASE WHEN @servername LIKE '%DEV%' THEN 'development' 
--									 WHEN @servername LIKE 'DEV%' THEN 'development' 
--									 WHEN @servername LIKE 'QA%' THEN 'QA' WHEN @servername LIKE '%QA%' THEN 'QA' END
--ORDER BY vchSourceServer,vchsourcedatabase, ipriority, vchsourcetable;


SELECT @total = COUNT(tablename) FROM @tbList;

WHILE @num <= @total
BEGIN
 
 SET @queryTxt = NULL;
 SELECT @sqlAlias = sourceserver ,
        @srcDatabase = databasename ,
        @srcTable = tablename
 FROM   @tbList
 WHERE  num = @num;

 PRINT @srcDatabase + '.dbo.' + @srctable;

 IF ISNULL(@srcDatabase, '') <> '' AND ISNULL(@srcTable, '') <> ''
 BEGIN
	SET @queryTxt = '
	declare @queryTxt varchar(8000), @ibit bit = 0, @outtablepath varchar(100)

	if  '''+ @srctable + ''' = ''AccountAccessEmps'' or '''+ @srctable + '''  = ''associations'' or '''+ @srctable + '''  = ''permission'' 
	or '''+ @srctable + '''  = ''user_roles'' or '''+ @srctable + '''  = ''UserEmailQuestionAnswered'' or '''+ @srctable + ''' = ''UserLogin'' or '''+ @srctable + ''' = ''UserLoginHH'' or '''+ @srctable + '''  = ''users''
	or '''+ @srctable + '''  = ''UsersPassword''
	BEGIN
		print ''delete temp iuid records''
		select @queryTxt = ''delete new
		from '+@srcDatabase+'.dbo.' + @srctable + '_Sync_DEVDB new
		join master.[dbo].[cfn_security_sync_exclusion] ex
		on new.iuid = ex.iuid
		''
		exec(@queryTxt)
	END
	';
	if @debug = 1
	BEGIN
		print @queryTxt;
	END

		EXEC(@queryTxt);

	SET @queryTxt = '
	declare @queryTxt varchar(8000), @ibit bit = 0
	use ['+@srcDatabase+'];
	if not exists (select top 1 * from tempdb.INFORMATION_SCHEMA.columns where table_name like ''' + @srctable + '_Sync_DEVDB%'' and column_name = ''error'')
	BEGIN
	if object_id (''dbo.' + @srctable + '_Sync_DEVDB'') is not NULL and (select max(ordinal_position) from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + ''') > 1
	BEGIN
		select @queryTxt = ''
		CREATE CLUSTERED INDEX [idx_' + @srctable + '_Sync_DEVDB_ClusteredIndex] ON [dbo].[' + @srctable + '_Sync_DEVDB](''
		if exists (select top 1 * from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where table_name = '''+ @srctable + ''') BEGIN
			select @queryTxt = coalesce(@queryTxt,char(10))  +char(13) + case when row_number() over (order by column_name) > 1 then '','' else '''' end + ''['' + column_name + ''] ASC '' 
			from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
			where table_name = '''+ @srctable + ''' and constraint_name like ''PK%'' END
		ELSE BEGIN
			select @queryTxt = coalesce(@queryTxt,char(10))  +char(13) + case when row_number() over (order by col.name) > 1 then '','' else '''' end + ''['' + col.name + ''] ASC ''
			from sys.indexes id join sys.index_columns idx on id.object_id = idx.object_id and id.index_id = idx.index_id
			join sys.columns col on idx.object_id = col.object_id and idx.column_id = col.column_id
			where object_name(id.object_id) = '''+ @srctable + '''
			and id.type_desc = ''clustered'' END

		select @queryTxt = coalesce(@queryTxt,char(10))+char(13)+'') WITH (data_compression=page, SORT_IN_TEMPDB = ON);''
		exec(@queryTxt)
		set @queryTxt = NULL
		if exists (select top 1 name from sys.triggers) BEGIN
			select @queryTxt = coalesce(@queryTxt,'' '') + ''disable trigger dbo.'' +name+ '' on '+ @srctable + ';'' from sys.triggers
			where object_name(parent_id) = '''+ @srctable + '''; END
		exec(@queryTxt)
	END
	END
	';
	if @debug = 1
	BEGIN
		print @queryTxt;
	END
	ELSE
	BEGIN
		EXEC(@queryTxt);
	END

	/*Rename user if username is exist in DEV/QA but IUID is different from Production. We would need to 
	rename user in DEV and QA. For clean up, We can run stored procedure cfn_Security.dbo.sp_Users_Remove*/
	IF (@srcDatabase = 'CFN_Security' AND @srcTable = 'users' )
	BEGIN
		SET @queryTxt = '
			UPDATE x
			SET x.vchUserID=x.vchUserID + ''_DUPLICATE_''+CAST(RIGHT(x.iuid,1) AS CHAR(1))
			FROM cfn_security.dbo.users x
			JOIN '+@srcDatabase+'.dbo.' + @srctable + '_Sync_DEVDB x2 ON x.IUID<>x2.IUID
					AND x.vchUserID=x2.vchUserID;';

			if @debug = 1
	BEGIN
		print @queryTxt;
	END
	ELSE
	BEGIN
		EXEC(@queryTxt);
	END

	END;

	SET @queryTxt = '
	declare @queryTxt varchar(8000), @ibit bit = 0
	use ['+@srcDatabase+'];
	if not exists (select top 1 * from tempdb.INFORMATION_SCHEMA.columns where table_name like ''' + @srctable + '_Sync_DEVDB%'' and column_name = ''error'')
	BEGIN
	if object_id (''dbo.' + @srctable + '_Sync_DEVDB'') is not NULL and (select max(ordinal_position) from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + ''') > 1
	BEGIN
	if exists (select top 1 * from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + '''
	and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, ''IsIdentity'') = 1)
	BEGIN
		select @queryTxt = ''set identity_insert dbo.'+ @srctable + ' on;''
		set @ibit = 1;
	END
	print ''==>>>> update existing data''
	select @queryTxt = coalesce(@queryTxt,char(10)) + ''
	merge into dbo.'+ @srctable + ' as target
	using dbo.'+ @srctable + '_Sync_DEVDB as source
	''
	if exists (select top 1 * from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where table_name = '''+ @srctable + ''') BEGIN
	select @queryTxt = coalesce(@queryTxt,char(10)) + case when ordinal_position = 1 then ''on '' else char(10) + ''and '' end + ''target.['' + column_name + ''] = source.['' + column_name+ '']''
	from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	where table_name = '''+ @srctable + ''' and left(constraint_name,3) = ''PK_'' order by ORDINAL_POSITION
	-- update when matched
	select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '' when matched then update set '' 
	select @queryTxt = coalesce(@queryTxt,char(10))  +char(13) + case when row_number() over (order by column_name) > 1 then '','' else '''' end + ''['' + column_name + ''] = source.['' +column_name + '']''
	from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + ''' and column_name not in (select column_name from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where table_name = '''+ @srctable + ''' and left(constraint_name,3) = ''PK_'' )
	END
	ELSE BEGIN
	select @queryTxt = coalesce(@queryTxt,char(10)) + case (col.column_id) when 1 then ''on '' else char(10) + ''and '' end + ''target.['' + col.name + ''] = source.['' + col.name+ '']''
	from sys.indexes id 
	join sys.index_columns idx on id.object_id = idx.object_id and id.index_id = idx.index_id
	join sys.columns col on idx.object_id = col.object_id and idx.column_id = col.column_id
	where object_name(id.object_id) = '''+ @srctable + '''
	and id.type_desc = ''clustered'' 
		-- update when matched
	select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '' when matched then update set '' 
	select @queryTxt = coalesce(@queryTxt,char(10))  +char(13) + case when row_number() over (order by column_name) > 1 then '','' else '''' end + ''['' + column_name + ''] = source.['' +column_name + '']''
	from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + ''' and column_name not in (select col.name from sys.indexes id join sys.index_columns idx on id.object_id = idx.object_id and id.index_id = idx.index_id
	left join sys.columns col on idx.object_id = col.object_id and idx.column_id = col.column_id where object_name(id.object_id) = '''+ @srctable + ''' and id.type_desc = ''clustered'')
	END
	select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '';''

	BEGIN TRY
		--print @queryTxt
		exec(@queryTxt)
	END TRY
	BEGIN CATCH
		print @queryTxt;
		print ERROR_MESSAGE();
		insert into ##tmpCFNRepltableSynError (servername,databasename,tablename,msg) values ('''+@@servername+''','''+@srcDatabase+''',''' + @srctable + ''', ERROR_MESSAGE());
	END CATCH END END
	';
	if @debug = 1
	BEGIN
		print @queryTxt;
	END
	ELSE
	BEGIN
		EXEC(@queryTxt);
	END
	--return
		set @queryTxt = '
	declare @queryTxt varchar(8000), @ibit bit = 0
	use ['+@srcDatabase+'];
	
	if object_id (''dbo.' + @srctable + '_Sync_DEVDB'') is not NULL
	BEGIN
	print ''==>>>> insert new data''

-- insert when not matched
	if exists (select top 1 * from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + '''
	and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, ''IsIdentity'') = 1)
	BEGIN
		select @queryTxt = ''set identity_insert dbo.'+ @srctable + ' on;''
	END
	select @queryTxt = coalesce(@queryTxt,char(10)) + ''
	merge into dbo.'+ @srctable + ' as target
	using dbo.'+ @srctable + '_Sync_DEVDB as source
	''

	if exists (select top 1 * from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where table_name = '''+ @srctable + ''') BEGIN
	select @queryTxt = coalesce(@queryTxt,char(10)) + case when ordinal_position = 1 then ''on '' else char(10) + ''and '' end + ''target.['' + column_name + ''] = source.['' + column_name+ '']''
	from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	where table_name = '''+ @srctable + ''' and left(constraint_name,3) = ''PK_'' order by ORDINAL_POSITION END
	ELSE BEGIN
	select @queryTxt = coalesce(@queryTxt,char(10)) + case (col.column_id) when 1 then ''on '' else char(10) + ''and '' end + ''target.['' + col.name + ''] = source.['' + col.name+ '']''
	from sys.indexes id join sys.index_columns idx on id.object_id = idx.object_id and id.index_id = idx.index_id
	join sys.columns col on idx.object_id = col.object_id and idx.column_id = col.column_id
	where object_name(id.object_id) = '''+ @srctable + '''
	and id.type_desc = ''clustered'' END

	select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '' when not matched then insert ('' 
	select @queryTxt = coalesce(@queryTxt,char(10))  +char(13) + case when row_number() over (order by column_name) > 1 then '','' else '''' end + ''['' + column_name + '']''
	from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + '''
	select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '') values ('' 
	select @queryTxt = coalesce(@queryTxt,char(10))  +char(13) + case when row_number() over (order by column_name) > 1 then '','' else '''' end + ''source.['' +column_name + '']''
	from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + '''
	select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '');'' END
	ELSE BEGIN select @queryTxt = coalesce(@queryTxt,char(10))+char(13) + '';'' END
	if exists (select top 1 * from INFORMATION_SCHEMA.COLUMNS where table_name = '''+ @srctable + '''
	and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, ''IsIdentity'') = 1)
	BEGIN
		set @queryTxt = coalesce(@queryTxt,char(10))+char(13) +'' set identity_insert dbo.'+ @srctable + ' off;''
	END
	BEGIN TRY
		exec(@queryTxt)
	END TRY
	BEGIN CATCH
		print @queryTxt;
		print ERROR_MESSAGE();
		insert into ##tmpCFNRepltableSynError (servername,databasename,tablename,msg) values ('''+@@servername+''','''+@srcDatabase+''',''' + @srctable + ''', ERROR_MESSAGE());
	END CATCH
	set @queryTxt = NULL
	if exists (select top 1 name from sys.triggers)
	BEGIN
		select @queryTxt = coalesce(@queryTxt,'' '') + ''enable trigger dbo.'' +name+ '' on '+ @srctable + ';'' from sys.triggers
		where object_name(parent_id) = '''+ @srctable + '''
	END
	exec(@queryTxt)
	if object_id (''dbo.' + @srctable + '_Sync_DEVDB'') is not NULL
	BEGIN
		drop table dbo.' + @srctable + '_Sync_DEVDB;
	END
	';
	--print @queryTxt
	--exec [DBMANAGEMENT].databasemanager.dbo.cfn_retrieveTableData @clientserver = 'Newton', @sqlAlias = @sqlAlias, @dbname = 'CFN_Security', @tbname = 'AccountAccessEmps'
	if @debug = 1
	BEGIN
		print @queryTxt;
	END
	ELSE
	BEGIN
		EXEC(@queryTxt);
	END
 END;
 SET @num = @num + 1;
END;

/*-----------------------------------------------------------------
--Send out an alert email if there is any issue during table sync
------------------------------------------------------------------*/
IF EXISTS (select top 1 * from dbo.##tmpCFNRepltableSynError ) and @debug = 0
BEGIN

	DECLARE @tableHTML VARCHAR(MAX);

	SELECT @tableHTML=dbo.cfn_EmailCss();

	SET @tableHTML =@tableHTML+
	'<H1><font size="2">Below is a list of tables which the automatic table sync process could not update. Please check and correct error. </font></H1>' +
	'<table border="1">' +
		'<tr>
		<TH>Server Name</TH>
		<TH>Database Name</TH>
		<TH>Table Name</TH>
		<TH>Description</TH>
		</tr>' +
	CAST ( ( SELECT 
					 td = servername,''
					,td = ltrim(rtrim(databasename)) ,''
					,td = ltrim(rtrim(tablename)) ,''
					,td = ltrim(rtrim(msg)),''
	FROM dbo.##tmpCFNRepltableSynError FOR XML PATH ('tr') , TYPE  ) AS NVARCHAR(MAX) ) + N'</table>' ; 

	/*Add Server info in email*/
	SELECT @tableHTML = @tableHTML + dbo.cfn_EmailServerInfo();

	EXEC msdb.dbo.sp_send_dbmail	
	@recipients = 'DTam@COMMONWEALTH.COM; AHirapara@COMMONWEALTH.COM;richardl@commonwealth.com'
	,@subject = 'ALERT: Table Sync Errors'
	,@body = @tableHTML
	,@body_format = 'HTML';

END;


END;