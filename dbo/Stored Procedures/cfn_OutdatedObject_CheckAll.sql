﻿CREATE PROCEDURE [dbo].[cfn_OutdatedObject_CheckAll]
-- =============================================
-- Author:		Don Tam
-- Create date: 9/28/2012
-- Description:	check outdated objects
-- =============================================
-- AM2  Moving this to DBA for TESLA, and adjusting object names
-- =============================================
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--IF PATINDEX('%DEV%',@@servername) > 0 or PATINDEX('%QA%',@@servername) > 0
--BEGIN

	Declare @query varchar(8000)
	Declare @vDBName varchar(100), @cmd varchar(255), @msg varchar(255), @objName varchar(100), @checkOn datetime=getdate()
	Declare @dbList table (
	dbName varchar(100),
	num int identity(1,1)
	)
		Declare @errorLog table (
	servername varchar(100),
	dbname varchar(100),
	objname varchar(100),
	msg varchar(255),
	checkOn datetime
	)

	Declare @irec int, @itotal int

	insert into @dbList
	select name as dbName from master.sys.databases
	where database_id > 4
	and state = 0
	and name not like '%_DEV'
	and name not like '%_SCHEMA'
	and name not like 'tmp_%_CHECK'

	select @itotal = count(*) from @dbList
	select @irec = 1

	while @irec <= @itotal
	BEGIN
		select @vDBName = dbName from @dbList
		where num = @irec
			
			exec cfn_OutdatedObject_Check @vDBName;
			exec cfn_OutdatedObject_Delete @vDBName;
		select @irec = @irec + 1

	END
--END
--ELSE
--BEGIN
--	print 'Can only be run on development or QA server.'
--END

END


GO