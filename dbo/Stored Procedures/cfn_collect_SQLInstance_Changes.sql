
-- =============================================
-- Author:		Don Tam
-- Create date: 9/13/2016
-- Description:	collect SQL instance change info
-- =============================================
CREATE PROCEDURE cfn_collect_SQLInstance_Changes

AS
BEGIN

	SET NOCOUNT ON;
	declare @filename varchar(100), @num int =1 , @total int

--- Server Level Changes
	declare @filelist table(
	filename varchar(100),
	num int identity(1,1)
	)
	declare @filetable table (
		[SQLServer] [nvarchar](128) NULL,
		[eventdesc] [nvarchar](500) NULL,
		[DatabaseName] [nvarchar](256) NULL,
		[ApplicationName] [nvarchar](256) NULL,
		[LoginName] [nvarchar](256) NULL,
		[hostname] [nvarchar](256) NULL,
		[SPID] [int] NULL,
		[StartTime] [datetime] NULL
	)
	SELECT   @filename='dir /s /b "' + substring(cast(value as nvarchar(1000)), 0,charindex('log_',cast(value as nvarchar(1000)))) + '"log_*.trc'
	FROM   ::fn_trace_getinfo(default)
	WHERE   traceid = 1 and   property = 2;

	--print @filename
	insert into @filelist (filename)
	exec xp_cmdshell @filename;

	select @total=max(num) from @filelist
	where filename is not NULL;

	while @num <= @total
	BEGIN
		select @filename = filename
		from @filelist
		where num = @num;

		insert into @filetable ([SQLServer] ,[eventdesc] ,[DatabaseName] ,[ApplicationName] ,[LoginName] ,[hostname],[SPID],[StartTime])
		SELECT distinct
		@@servername as SQLServer, 
		case when isNULL(cast(textdata as varchar(max)),'') = '' and EventClass <> 111 and EventClass <> 109 then te.Name + ' (' + v.subclass_name + ')' 
		when isNULL(cast(textdata as varchar(500)),'') = '' and EventClass = 109 then left(te.Name, charindex('Event',te.Name)-1) + t.TargetUserName
		when charindex('Configuration', cast(textdata as varchar(1000))) > 0 then Replace(substring(cast(textdata as varchar(1000)), charindex('Configuration', cast(textdata as varchar(1000))), len(cast(textdata as varchar(1000))) - charindex('Configuration', cast(textdata as varchar(1000)))), 'Run the RECONFIGURE statement to install.','') 
		when isNULL(cast(textdata as varchar(max)),'') = '' and EventClass = 111 then replace(te.name, 'Role Event', roleName + ' Role')
		else cast(textdata as varchar(max)) end as eventdesc,
			t.DatabaseName ,
			t.ApplicationName,
			t.LoginName ,
			t.hostname,
			t.SPID ,
			t.StartTime 
		FROM   ::fn_trace_gettable(@filename, default) AS t 
		left JOIN sys.trace_events AS te  ON t.EventClass = te.trace_event_id 
		left JOIN sys.trace_subclass_values v ON v.trace_event_id = te.trace_event_id
		where  (textdata like '%spid' + convert(varchar(5),spid) + '%Configuration option%')
		or EventClass in (102,103,104,106,108,109,110,111)
		or subclass_name in ('started','shutdown');


		set @num = @num + 1;
	END
	
	--- Database Properties Changes

	DECLARE @TotalLogs int, @logpath varchar(100)
	declare @listlog table (
	line varchar(100),
	num int identity(1,1)
	)
	declare @errorlog table (
	logDate datetime,
	ProcessInfo varchar(25),
	Text varchar(500),
	num int identity(1,1)
	)
	SELECT @logpath='dir /B /S "' + rtrim(ltrim(path)) + '"ErrorLog*'
	FROM Sys.dm_os_server_diagnostics_log_configurations;
	
	insert into @listlog
	exec master.dbo.xp_cmdshell @logpath;

	--select * from @listlog;

	select @totalLogs = max(num)
	from @listlog
	where line is not NULL;

	set @num = 0;
	
	while @num < @TotalLogs
	BEGIN
		BEGIN TRY
		--print @num
			insert into @errorlog (logdate, ProcessInfo, Text)
			exec master.dbo.xp_readErrorlog @num, 1, N'Setting database';
			insert into @errorlog (logdate, ProcessInfo, Text)
			exec master.dbo.xp_readErrorlog @num, 1, N'Server process ID is ';
			insert into @errorlog (logdate, ProcessInfo, Text)
			exec master.dbo.xp_readErrorlog @num, 1, N'shutdown';
			insert into @errorlog (logdate, ProcessInfo, Text)
			exec master.dbo.xp_readErrorlog @num, 1, N'shut down';
			insert into @errorlog (logdate, ProcessInfo, Text)
			exec master.dbo.xp_readErrorlog @num, 1, N'state of the local availability ';
			insert into @errorlog (logdate, ProcessInfo, Text)
			exec master.dbo.xp_readErrorlog @num, 1, N' transition to the primary role';
		END TRY
		BEGIN CATCH
			print @num;
		END CATCH
		set @num = @num + 1;
	END

	insert into @filetable ([SQLServer] ,[eventdesc] ,[DatabaseName] ,[ApplicationName] ,[LoginName] ,[hostname],[SPID],[StartTime])
	select @@servername as SQLServer, Text as eventDesc, case when charindex(' ON for database ', Text) > 0 then 
	replace(replace(substring(Text, charindex('''',Text), len(Text)-charindex('''',Text)+2),'''',''),'.','') else 'Master' end as DatabaseName, ProcessInfo as ApplicationName, NULL as loginName, @@servername as hostname, case when isNumeric(replace(processInfo,'spid','')) = 1 then replace(processInfo,'spid','') else null end as spid, logdate as Starttime
	from  @errorlog;

	select distinct * from @filetable
	order by starttime desc;
END
GO
