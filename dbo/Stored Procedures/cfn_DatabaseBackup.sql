﻿CREATE PROCEDURE [dbo].[cfn_DatabaseBackup]
@Databases nvarchar(max),
@Directory nvarchar(max),
@BackupType nvarchar(max),
@Verify nvarchar(max) = NULL,
@CleanupTime int = NULL,
@Compress nvarchar(max) = NULL,
@CopyOnly nvarchar(max) = NULL,
@ChangeBackupType nvarchar(max) = NULL,
@BackupSoftware nvarchar(max) = NULL,
@CheckSum nvarchar(max) = NULL,
@BlockSize int = NULL,
@BufferCount int = NULL,
@MaxTransferSize int = NULL,
@NumberOfFiles int = NULL,
@CompressionLevel int = NULL,
@Description nvarchar(max) = NULL,
@Threads int = NULL,
@Throttle int = NULL,
@Encrypt nvarchar(max) = NULL,
@EncryptionAlgorithm nvarchar(max) = NULL,
@ServerCertificate nvarchar(max) = NULL,
@ServerAsymmetricKey nvarchar(max) = NULL,
@EncryptionKey nvarchar(max) = NULL,
@ReadWriteFileGroups nvarchar(max) = NULL,
@OverrideBackupPreference nvarchar(max) = NULL,
@NoRecovery nvarchar(max) = NULL,
@URL nvarchar(max) = NULL,
@Credential nvarchar(max) = NULL,
@MirrorDirectory nvarchar(max) = NULL,
@MirrorCleanupTime int = NULL,
@LogToTable nvarchar(max) = NULL,
@Execute nvarchar(max) = NULL

AS

BEGIN

  /***************************************************************************************************
  This is simply a wrapper for Ola Hallengren's backup code.
  This wrapper is designed to drop all raiserror output from Ola's DatabaseBackup. 
  Discarding error messages is needed to prevent informational (Sev 10) errors from being written
  to the SQL Error Log when using Service Broker to execute backups

  Handle backups to NUL device here, since Ola doesn't support that

  Only pass parameters explicitly specified. Anything else will pick up defaults from 
  the real DatabaseBackup procedured

  ***************************************************************************************************/
  SET NOCOUNT ON

  DECLARE @cmd varchar(8000);
  DECLARE @sql varchar(8000);

  --NUL directory path not supported by Ola. So this is a hack
  IF @Directory LIKE 'NUL%'
  BEGIN
     SELECT @sql = 'BACKUP LOG [' + name + '] TO DISK = ''NUL'';' FROM sys.databases 
     WHERE recovery_model_desc = 'FULL' AND state_desc = 'ONLINE'
     AND name = @Databases;
     
     SET @cmd = 'sqlcmd -S"' + @@SERVERNAME + '" -E -d"' + DB_NAME() + '" -Q"' + @sql + '" -b'
     EXEC xp_cmdshell @cmd, no_output;

    RETURN (0);
  END

  -- For Real log backups:
  SET @sql = 'DatabaseBackup @Databases = ''' + @Databases + ''', ' +
        '@Directory = ''' + @Directory + ''', ' +
        '@BackupType = ''' + @BackupType  + ''', ' +
        CASE WHEN @Verify IS NOT NULL THEN '@Verify = ''' + @Verify + ''', ' ELSE '' END +
        CASE WHEN @CleanupTime IS NOT NULL THEN '@CleanupTime = ' + CAST(@CleanupTime as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @Compress IS NOT NULL THEN '@Compress = ''' + @Compress + ''', ' ELSE '' END +
        CASE WHEN @CopyOnly IS NOT NULL THEN '@CopyOnly = ''' + @CopyOnly + ''', ' ELSE '' END +
        CASE WHEN @ChangeBackupType IS NOT NULL THEN '@ChangeBackupType = ''' + @ChangeBackupType + ''', ' ELSE '' END +
        CASE WHEN @BackupSoftware IS NOT NULL THEN '@BackupSoftware = ''' + @BackupSoftware + ''', ' ELSE '' END +
        CASE WHEN @Checksum IS NOT NULL THEN '@Checksum = ''' + @Checksum + ''', ' ELSE '' END +
        CASE WHEN @BlockSize IS NOT NULL THEN '@BlockSize = ' + CAST(@BlockSize as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @BufferCount IS NOT NULL THEN '@BufferCount = ' + CAST(@BufferCount as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @MaxTransferSize IS NOT NULL THEN '@MaxTransferSize = ' + CAST(@MaxTransferSize as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @NumberOfFiles IS NOT NULL THEN '@NumberOfFiles = ' + CAST(@NumberOfFiles as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @CompressionLevel IS NOT NULL THEN '@CompressionLevel = ' + CAST(@CompressionLevel as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @Description IS NOT NULL THEN '@Description = ''' + @Description + ''', ' ELSE '' END +
        CASE WHEN @Threads IS NOT NULL THEN '@Threads = ' + CAST(@Threads as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @Throttle IS NOT NULL THEN '@Throttle = ' + CAST(@Throttle as varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @Encrypt IS NOT NULL THEN '@Encrypt = ''' + @Encrypt + ''', ' ELSE '' END +
        CASE WHEN @EncryptionAlgorithm IS NOT NULL THEN '@EncryptionAlgorithm = ''' + @EncryptionAlgorithm + ''', ' ELSE '' END +
        CASE WHEN @ServerCertificate IS NOT NULL THEN '@ServerCertificate = ''' + @ServerCertificate + ''', ' ELSE '' END +
        CASE WHEN @ServerAsymmetricKey IS NOT NULL THEN '@ServerAsymmetricKey = ''' + @ServerAsymmetricKey + ''', ' ELSE '' END +
        CASE WHEN @EncryptionKey IS NOT NULL THEN '@EncryptionKey = ''' + @EncryptionKey + ''', ' ELSE '' END +
        CASE WHEN @ReadWriteFileGroups IS NOT NULL THEN '@ReadWriteFileGroups = ''' + @ReadWriteFileGroups + ''', ' ELSE '' END +
        CASE WHEN @OverrideBackupPreference IS NOT NULL THEN '@OverrideBackupPreference = ''' + @OverrideBackupPreference + ''', ' ELSE '' END +
        CASE WHEN @NoRecovery IS NOT NULL THEN '@NoRecovery = ''' + @NoRecovery + ''', ' ELSE '' END +
        CASE WHEN @URL IS NOT NULL THEN '@URL = ''' + @URL + ''', ' ELSE '' END +
        CASE WHEN @Credential IS NOT NULL THEN '@Credential = ''' + @Credential + ''', ' ELSE '' END +
        CASE WHEN @MirrorDirectory IS NOT NULL THEN '@MirrorDirectory = ''' + @MirrorDirectory + ''', ' ELSE '' END +
        CASE WHEN @MirrorCleanupTime IS NOT NULL THEN '@MirrorCleanupTime = ' + CAST(@MirrorCleanupTime AS varchar(10)) + ', ' ELSE '' END +
        CASE WHEN @LogToTable IS NOT NULL THEN '@LogToTable = ''' + @LogToTable + ''', ' ELSE '' END +
        CASE WHEN @Execute IS NOT NULL THEN '@Execute = ''' + @Execute + ''', ' ELSE '@Execute = ''Y'';' END;
       
    
    
    SET @cmd = 'sqlcmd -S"' + @@SERVERNAME + '" -E -d"' + DB_NAME() + '" -Q"' + @sql + '" -b'

    EXEC xp_cmdshell @cmd, no_output


    

END