﻿CREATE PROCEDURE [dbo].[cfn_OutdatedObject_Check]
 @databasename varchar(100)=null
-- =============================================
-- Author:		Don Tam
-- Create date: 9/28/2012
-- Description:	check and validate database objects
-- cfn_checkOutdatedObject @databasename = 'cfnAccountDB'
-- =============================================
-- AM2  Make some hacky changes so I can move this into DBA for TESLA.
--   Wrapping some stuff in dyamic SQL just to make the TFS build happy with outside references.
--   Revisit this in the near future.
-- =============================================
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--IF PATINDEX('%DEV%',@@servername) > 0 or PATINDEX('%QA%',@@servername) > 0
--BEGIN

	Declare @query varchar(8000)
	Declare @vDBName varchar(100), @cmd varchar(255), @msg varchar(255), @objName varchar(100), @checkOn datetime=getdate()

	CREATE TABLE #outdatedObj (
	sqlserver varchar(100),
	databasename varchar(100),
	name varchar(100),
	typeDesc varchar(100),
	updatedOn datetime
	)

	Declare @irec int, @itotal int
	set @vDBName = @databasename

	select @query = '
	USE ['+@vDBName+'];
	select @@servername as sqlserver, 
	'''+@vDBName+''' as databasename, 
	name as objectname,
	--replace(type,''_'','' '') as typeDesc,
	case type
	when ''SN'' then ''synonym''
	when ''P'' then ''procedure''
	when ''FN'' then ''function''
	when ''T'' then ''function''
	when ''IF'' then ''function''
	when ''TF'' then ''function''
	when ''V'' then ''view''
	when ''U'' then ''table''
	else ''unknown''
	end as typeDesc,
	convert(varchar(20),modify_date, 100) as updatedOn 
	from sys.objects
	where type in (''SN'',''P'',''FN'',''T'',''V'',''U'',''TF'',''IF'')
	--and datediff(day,modify_date,getdate())>= 30
	and name like ''%_DEVDB%''
	order by type
	'
	--print @query
	insert into #outdatedObj
	exec(@query)

	/*
	delete [TFS-SOURCE].DatabaseManager.dbo.outdatedObj
	where sqlserver = @@servername
	and databasename = @vDBName
	--and isNULL(ok2Delete,0) = 0
	*/

	if exists (select top 1 * from #outdatedObj)
	BEGIN

		EXEC( 'update [TFS-SOURCE].DatabaseManager.dbo.outdatedObj
		set deletedOn = NULL
		from #outdatedObj new
		join [TFS-SOURCE].DatabaseManager.dbo.outdatedObj old with (nolock)
		on new.sqlserver = old.sqlserver
		and new.databasename = old.databasename
		and new.name = old.name
		and new.typeDesc = old.typedesc

		insert into [TFS-SOURCE].DatabaseManager.dbo.outdatedObj
		(
		sqlserver,
		databasename,
		name,
		typeDesc,
		insertedOn
		)
		select 
		new.sqlserver,
		new.databasename,
		new.name,
		new.typeDesc,
		new.updatedOn as insertedOn
		from #outdatedObj new
		left join [TFS-SOURCE].DatabaseManager.dbo.outdatedObj old with (nolock)
		on new.sqlserver = old.sqlserver
		and new.databasename = old.databasename
		and new.name = old.name
		and new.typeDesc = old.typedesc
		where old.insertedOn is NULL')

		select * from #outdatedObj
		where sqlserver = @@servername
		and databasename = @vDBName
	END

--END
--ELSE
--BEGIN
	--print 'Can only be run on development or QA server.'
--END
END



GO