﻿CREATE PROCEDURE [dbo].CFN_GRANALLUser2DBA

AS
DECLARE @tbl table (
		createuser varchar(1000),
		num int identity(1,1) 
);

DECLARE @num int = 1, @total int, @queryTxt varchar(2000);
insert into @tbl (createuser)
select 'USE DBA;
CREATE USER ['+l.name+'] WITH DEFAULT_SCHEMA=[API];' 
from master.dbo.sysusers l
left join dba.dbo.sysusers dba
on l.name = dba.name
where l.uid > 6
and l.islogin=1
and l.isntgroup = 1
and l.name not in ( 'WALTHAM\clustersqlservice', 'WALTHAM\commvault.svc', 'WALTHAM\Development', 'WALTHAM\ssrs2016.svc')
and dba.name is null;

insert into @tbl (createuser)
select 'USE DBA;
CREATE USER ['+l.name+'] WITH DEFAULT_SCHEMA=[API];' 
from master.dbo.sysusers l
left join dba.dbo.sysusers dba
on l.name = dba.name
where l.uid > 6
and l.islogin=1
and l.isntgroup = 0
and l.name not like '##%'
and dba.name is null;

  select @total = max (num)
  from @tbl;


  while (@num <= @total)
  BEGIN
	 select @queryTxt = createuser
	 from @tbl
	 where num = @num;
	 BEGIN TRY
	
	 exec(@queryTxt);

	 END TRY
	 BEGIN CATCH
			  print @queryTxt;
	 END CATCH
	 set @num = @num + 1;


  END
