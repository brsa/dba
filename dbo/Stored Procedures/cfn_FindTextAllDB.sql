﻿CREATE PROCEDURE [dbo].[cfn_FindTextAllDB] 
	@SearchValue varchar(5000)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY;

	IF len(RTRIM(LTRIM(@SearchValue))) = 0
	BEGIN
		PRINT 'You must enter search value';
		RETURN;
	END;

	SELECT @SearchValue = REPLACE(@SearchValue,'_','[_]');

	DECLARE @id INT, @cnt INT, @sql NVARCHAR(max), @currentDb SYSNAME, @AccessDB INT;

	CREATE TABLE #results(id INT IDENTITY PRIMARY KEY,DatabaseName VARCHAR(150),servername VARCHAR(500),ObjectName VARCHAR(1000),Objecttype VARCHAR(50), Directions VARCHAR(MAX), SearchMatch VARCHAR(MAX));
	CREATE TABLE #database(id INT IDENTITY PRIMARY KEY,DatabaseName SYSNAME,Searched BIT);
	CREATE TABLE #searchterms(string VARCHAR(4000) COLLATE SQL_Latin1_General_CP1_CI_AS);
		
	IF CHARINDEX('|', @SearchValue) > 0
	BEGIN
		INSERT	#searchterms(string)
		SELECT	'%' + Value + '%' 
		FROM	dbo.fn_split(@SearchValue,'|'); 
	END;
	ELSE
	BEGIN
		INSERT	#searchterms(string)
		SELECT	'%' + @SearchValue + '%';
	END;
		

	INSERT	INTO #database (databaseName)
	SELECT	d.NAME
	FROM	sys.databases d WITH (NOLOCK)
	WHERE	d.source_database_id IS NULL
	ORDER BY d.name;

	SELECT	@id = 1, 
			@cnt = MAX(id) 
	FROM #database;

	WHILE @id <= @cnt
	BEGIN

		SELECT	@currentDb = DatabaseName 
		FROM	#database
		WHERE	id = @id;
		
		SET @AccessDB = 1;

		BEGIN TRY	
			SELECT	@sql = 'USE [' + @currentDB + '];';
			EXEC sys.sp_ExecuteSQL @stmt = @sql;	
		END TRY
		BEGIN CATCH
			SET @AccessDB = 0;

			UPDATE	#database
			SET		Searched = 0
			WHERE	DatabaseName = @currentDB;
		END CATCH;

		IF @AccessDB = 1
		BEGIN
			SELECT	@sql = @sql + '	INSERT INTO #results
									SELECT DISTINCT 
										@_CurrentDB AS ''DatabaseName'',''' +	 
										@@SERVERNAME + ''' AS ''servername'',	 
										SCHEMA_NAME(so.schema_id) + ''.'' + OBJECT_NAME(c.id) AS ''Object name'',
										CASE WHEN OBJECTPROPERTY(c.id, ''IsReplProc'') = 1 
												THEN ''Replication stored procedure''
 											WHEN OBJECTPROPERTY(c.id, ''IsExtendedProc'') = 1 
												THEN ''Extended stored procedure''				
											WHEN OBJECTPROPERTY(c.id, ''IsProcedure'') = 1 
												THEN ''Stored Procedure'' 
											WHEN OBJECTPROPERTY(c.id, ''IsTrigger'') = 1 
												THEN ''Trigger'' 
											WHEN OBJECTPROPERTY(c.id, ''IsTableFunction'') = 1 
												THEN ''Table-valued function'' 
											WHEN OBJECTPROPERTY(c.id, ''IsScalarFunction'') = 1 
												THEN ''Scalar-valued function''
 											WHEN OBJECTPROPERTY(c.id, ''IsInlineFunction'') = 1 
												THEN ''Inline function''	
											WHEN OBJECTPROPERTY(c.id, ''IsTrigger'') = 1 
												THEN ''Trigger''
											WHEN OBJECTPROPERTY(c.id, ''IsView'') = 1 
												THEN ''View''
											END AS ''Object type'',
											''EXEC ['' +  @_CurrentDB COLLATE SQL_Latin1_General_CP1_CI_AS + ''].sys.sp_helptext '''''' + SCHEMA_NAME(so.schema_id) + ''.'' + OBJECT_NAME(c.id) + '''''''' AS ''Directions'',
											SearchMatch=replace(s.string, ''%'', '''')
									FROM	sys.syscomments c
									JOIN [' + @currentDb + '].sys.sysobjects o ON c.id = o.id 
									JOIN [' + @currentDb + '].sys.objects so ON c.id = so.object_id 
									JOIN #searchterms s on c.text COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string 
									WHERE	encrypted = 0 AND
											(
											OBJECTPROPERTY(c.id, ''IsReplProc'') = 1		OR
											OBJECTPROPERTY(c.id, ''IsExtendedProc'') = 1	OR
											OBJECTPROPERTY(c.id, ''IsProcedure'') = 1		OR
											OBJECTPROPERTY(c.id, ''IsTrigger'') = 1		OR
											OBJECTPROPERTY(c.id, ''IsTableFunction'') = 1	OR
											OBJECTPROPERTY(c.id, ''IsScalarFunction'') = 1	OR
											OBJECTPROPERTY(c.id, ''IsTrigger'') = 1	OR
											OBJECTPROPERTY(c.id, ''IsView'') = 1	OR
											OBJECTPROPERTY(c.id, ''IsInlineFunction'') = 1	
											)';

			EXEC sys.sp_ExecuteSQL @stmt = @sql, 
				@params = N'@_CurrentDB VARCHAR(100)'
				, @_CurrentDB = @CurrentDB;	

			-- search synonyms
			SELECT @sql = '	INSERT INTO #results
							SELECT	''' + @CurrentDB + ''' AS DatabaseName,''' +
									@@SERVERNAME+ ''' AS servername,
									name AS ObjectName,
									''Synonym'' ObjectType,
									''NA'',
									SearchMatch=REPLACE(s.string, ''%'', '''')
							FROM	[' + @CurrentDB + '].sys.synonyms 
							JOIN	#searchterms s ON base_object_name COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string';

			EXEC sys.sp_ExecuteSQL @stmt = @sql;

			INSERT INTO #results
			SELECT database_name  AS [database],
				@@SERVERNAME as servername,
				j.name + ' STEP: ' + convert(varchar(10),js.step_id) as [Object Name],
				'SQL Agent Job ' as [Object Type], 
				'use msdb; select js.command FROM	[msdb].dbo.sysjobs j with(nolock)
					JOIN	[msdb].dbo.sysjobsteps js with(nolock)
						ON	js.job_id = j.job_id 
					join #searchterms s on js.command COLLATE SQL_Latin1_General_CP1_CI_AS like s.string'
					AS [Run this command to see the object text],
				SearchMatch=replace(s.string, '%', '')
			FROM	[msdb].dbo.sysjobs j with (nolock)
			JOIN	[msdb].dbo.sysjobsteps js with (nolock) ON	js.job_id = j.job_id
					join #searchterms s on js.command COLLATE SQL_Latin1_General_CP1_CI_AS like s.string;

			INSERT INTO #results 		
			SELECT 'master' AS [database],
				@@SERVERNAME AS servername,
				srvname AS [object name],
				'Linked Server' AS [object type],
				'select * FROM master.sys.sysservers with (nolock)
					join #searchterms s on srvname COLLATE SQL_Latin1_General_CP1_CI_AS like s.string or dataSource COLLATE SQL_Latin1_General_CP1_CI_AS like s.string
						AS [Run this command to see the object text]',
				SearchMatch=replace(s.string, '%', '')
			FROM master.sys.sysservers WITH (NOLOCK)
			JOIN #searchterms s ON srvname COLLATE SQL_Latin1_General_CP1_CI_AS like s.string or dataSource COLLATE SQL_Latin1_General_CP1_CI_AS like s.string;

			UPDATE	#database
			SET		Searched = 1
			WHERE	DatabaseName = @currentDB;

		END;

		SET @id = @id + 1;
	END;
		
	INSERT	INTO #results
	SELECT	'[' + Database_Name + ']'  AS 'DatabaseName',
			@@SERVERNAME AS servername,
			j.name + ' STEP: ' + CONVERT(VARCHAR(10),js.step_id) ObjectName,
			'SQL Agent Job ' AS ObjectType, 
			'USE msdb; 
			SELECT	js.command 
			FROM	[msdb].dbo.sysjobs j WITH (NOLOCK)
			JOIN	[msdb].dbo.sysjobsteps js WITH (NOLOCK) ON	js.job_id = j.job_id 
			JOIN	#searchterms s ON js.command COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string' AS [Run this command to see the object text],
			SearchMatch=REPLACE(s.string, '%', '')
	FROM	[msdb].dbo.sysjobs j WITH (NOLOCK)
	JOIN	[msdb].dbo.sysjobsteps js WITH (NOLOCK) ON	js.job_id = j.job_id
	JOIN	#searchterms s ON js.command COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string;

	INSERT	INTO #results 	
	SELECT	'[master]' AS DatabaseName,
			@@SERVERNAME AS servername,
			srvname AS objectname,
			'Linked Server' AS objecttype,
			'SELECT * 
			FROM master.sys.sysservers WITH (NOLOCK)
			JOIN #searchterms s ON srvname COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string OR dataSource COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string AS [Run this command to see the object text]',
			SearchMatch=REPLACE(s.string, '%', '')
	FROM	master.sys.sysservers WITH (NOLOCK)
	JOIN	#searchterms s ON srvname COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string OR dataSource COLLATE SQL_Latin1_General_CP1_CI_AS LIKE s.string;
		
	SELECT	ID, DatabaseName as 'Database', ServerName, ObjectName AS [Object Name], ObjectType AS [Object Type],
			Directions AS [Run this command to see the objecttext], SearchMatch
	FROM #results
	ORDER BY [database], [Object Name], [Object type];

	SELECT	DatabaseName AS [Database Name],
			'Database Not Searched' AS [Not Searched]
	FROM	#database
	WHERE	Searched = 0
	ORDER BY [Not Searched], [Database Name];

	END TRY
	BEGIN CATCH
		THROW;
	END CATCH;
END;
