﻿/*
https://stackoverflow.com/questions/7048839/sql-server-query-to-find-all-permissions-access-for-all-users-in-a-database
Security Audit Report
1) List all access provisioned to a sql user or windows user/group directly 
2) List all access provisioned to a sql user or windows user/group through a database or application role
3) List all access provisioned to the public role

Columns Returned:
UserName        : SQL or Windows/Active Directory user cccount.  This could also be an Active Directory group.
UserType        : Value will be either ''SQL User'' or ''Windows User''.  This reflects the type of user defined for the 
                  SQL Server user account.
DatabaseUserName: Name of the associated user as defined in the database user account.  The database user may not be the
                  same as the server user.
Role            : The role name.  This will be null if the associated permissions to the object are defined at directly
                  on the user account, otherwise this will be the name of the role that the user is a member of.
PermissionType  : Type of permissions the user/role has on an object. Examples could include CONNECT, EXECUTE, SELECT
                  DELETE, INSERT, ALTER, CONTROL, TAKE OWNERSHIP, VIEW DEFINITION, etc.
                  This value may not be populated for all roles.  Some built in roles have implicit permission
                  definitions.
PermissionState : Reflects the state of the permission type, examples could include GRANT, DENY, etc.
                  This value may not be populated for all roles.  Some built in roles have implicit permission
                  definitions.
ObjectType      : Type of object the user/role is assigned permissions on.  Examples could include USER_TABLE, 
                  SQL_SCALAR_FUNCTION, SQL_INLINE_TABLE_VALUED_FUNCTION, SQL_STORED_PROCEDURE, VIEW, etc.   
                  This value may not be populated for all roles.  Some built in roles have implicit permission
                  definitions.          
ObjectName      : Name of the object that the user/role is assigned permissions on.  
                  This value may not be populated for all roles.  Some built in roles have implicit permission
                  definitions.
ColumnName      : Name of the column of the object that the user/role is assigned permissions on. This value
                  is only populated if the object is a table, view or a table value function.                 
*/

/*
--SAMPLE CALL:
IF OBJECT_ID('tempdb.dbo.#DatabaseSecurityReport') IS NOT NULL
	DROP TABLE #DatabaseSecurityReport;

CREATE TABLE #DatabaseSecurityReport (UserName VARCHAR(200),UserType VARCHAR(100),DatabaseUserName VARCHAR(500)
	,Role VARCHAR(200),PermissionType VARCHAR(200),PermissionState VARCHAR(200),ObjectType VARCHAR(200)
	,ObjectName VARCHAR(200),ColumnName VARCHAR(200));

INSERT INTO #DatabaseSecurityReport (UserName ,UserType ,DatabaseUserName ,Role ,PermissionType ,PermissionState ,ObjectType ,ObjectName ,ColumnName)
EXEC DBA.dbo.cfn_Audit_DatabaseSecurity @DBName = N'DBA' ,@Debug = 0;

*/
CREATE PROC [dbo].[cfn_Audit_DatabaseSecurity]
@DBName NVARCHAR(100)
,@Debug BIT =0
AS
SET NOCOUNT ON;

DECLARE @Sql NVARCHAR(MAX);

SET @Sql='USE ['+@DBName+'];
--List all access provisioned to a sql user or windows user/group directly 
SELECT  
    [UserName] = CASE princ.[type] WHEN ''S'' THEN princ.[name] WHEN ''U'' THEN ulogin.[name] COLLATE Latin1_General_CI_AI END,
    [UserType] = CASE princ.[type] WHEN ''S'' THEN ''SQL User'' WHEN ''U'' THEN ''Windows User'' END,  
    [DatabaseUserName] = princ.[name],       
    [Role] = null,      
    [PermissionType] = perm.[permission_name],       
    [PermissionState] = perm.[state_desc],       
    [ObjectType] = obj.type_desc,[SchemaName] =SCHEMA_NAME(obj.schema_id) , [ObjectName] = OBJECT_NAME(perm.major_id),[ColumnName] = col.[name]
FROM sys.database_principals princ --database user 
LEFT JOIN sys.login_token ulogin on princ.[sid] = ulogin.[sid] --Login accounts
LEFT JOIN sys.database_permissions perm ON perm.[grantee_principal_id] = princ.[principal_id] --Permissions
LEFT JOIN sys.columns col ON col.[object_id] = perm.major_id 
                    AND col.[column_id] = perm.[minor_id] --Table columns
LEFT JOIN sys.objects obj ON perm.[major_id] = obj.[object_id]
WHERE princ.[type] in (''S'',''U'')
UNION
--List all access provisioned to a sql user or windows user/group through a database or application role
SELECT  
    [UserName] = CASE memberprinc.[type]  WHEN ''S'' THEN memberprinc.[name] WHEN ''U'' THEN ulogin.[name] COLLATE Latin1_General_CI_AI END,
    [UserType] = CASE memberprinc.[type] WHEN ''S'' THEN ''SQL User'' WHEN ''U'' THEN ''Windows User'' END, 
    [DatabaseUserName] = memberprinc.[name],   
    [Role] = roleprinc.[name],      
    [PermissionType] = perm.[permission_name],       
    [PermissionState] = perm.[state_desc],       
    [ObjectType] =CASE WHEN perm.class_desc <> ''SCHEMA'' THEN obj.type_desc ELSE ''SCHEMA'' END,[SchemaName]=SCHEMA_NAME(obj.schema_id)  ,[ObjectName] =CASE WHEN perm.class_desc <> ''SCHEMA'' THEN OBJECT_NAME(perm.major_id) ELSE schem.name END,[ColumnName] = col.[name]
FROM  sys.database_role_members members --Role/member associations
JOIN sys.database_principals roleprinc ON roleprinc.[principal_id] = members.[role_principal_id]--Roles
JOIN sys.database_principals memberprinc ON memberprinc.[principal_id] = members.[member_principal_id] --Role members (database users)
LEFT JOIN sys.login_token ulogin on memberprinc.[sid] = ulogin.[sid] --Login accounts
LEFT JOIN sys.database_permissions perm ON perm.[grantee_principal_id] = roleprinc.[principal_id]--Permissions
LEFT JOIN sys.columns col on col.[object_id] = perm.major_id 
      AND col.[column_id] = perm.[minor_id] --Table columns
LEFT JOIN sys.objects obj ON perm.[major_id] = obj.[object_id]
LEFT JOIN sys.schemas schem ON perm.major_id = schem.schema_id
UNION
--List all access provisioned to the public role, which everyone gets by default
SELECT  
    [UserName] = ''Public'',
    [UserType] = ''Public'', 
    [DatabaseUserName] = ''Public'',       
    [Role] = roleprinc.[name],      
    [PermissionType] = perm.[permission_name],       
    [PermissionState] = perm.[state_desc],       
    [ObjectType] = obj.type_desc, [SchemaName]=SCHEMA_NAME(obj.schema_id),[ObjectName] = OBJECT_NAME(perm.major_id),[ColumnName] = col.[name]
FROM sys.database_principals roleprinc --Roles
LEFT JOIN sys.database_permissions perm ON perm.[grantee_principal_id] = roleprinc.[principal_id] --Role permissions
LEFT JOIN sys.columns col on col.[object_id] = perm.major_id --Table columns
       AND col.[column_id] = perm.[minor_id]                   
JOIN sys.objects obj ON obj.[object_id] = perm.[major_id] --All objects 
WHERE roleprinc.[type] = ''R'' --Only roles
	AND roleprinc.[name] = ''public'' --Only public role 
	AND obj.is_ms_shipped = 0 --Only objects of ours, not the MS objects
ORDER BY princ.[Name],OBJECT_NAME(perm.major_id),col.[name],perm.[permission_name],perm.[state_desc],obj.type_desc;--perm.[class_desc] ';

IF @Debug=1
	PRINT @Sql;
ELSE
	EXEC sys.sp_executesql @stmt=@Sql;


GO


