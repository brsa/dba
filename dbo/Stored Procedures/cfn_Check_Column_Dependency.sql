﻿/*****************************************************************************
AUTHOR : AJAY H.
CREATE DATE : 05/02/2016
DESCRIPTION: Check Column Dependency.
SAMPLE CALL:
EXEC DBA.dbo.cfn_Check_Column_Dependency 'PPSPosition','dQuantity';

**HISTORY**
-----------------------------------------------------------------------------------
Update Date    Updated By          Notes
-----------------------------------------------------------------------------------		 
										

******************************************************************************/

CREATE PROCEDURE [dbo].[cfn_Check_Column_Dependency]
@TableName VARCHAR(100)
,@ColumnName VARCHAR(100)
AS
SET NOCOUNT ON;
--DECLARE @TableName VARCHAR(100)= 'AccountDST';
--DECLARE @ColumnName VARCHAR(100)= 'vchRepName';
DECLARE @ColumnDependencies TABLE(ReferencingObject VARCHAR(100),referenced_database_name VARCHAR(100),referenced_schema_name VARCHAR(30), referenced_entity_name VARCHAR(100));        
DECLARE @Database TABLE (iID INT IDENTITY(1,1),DatabaseName VARCHAR(200));

	INSERT INTO @Database (  DatabaseName )
	SELECT D.name AS DatabaseName
	FROM sys.databases D
	WHERE D.state_desc='ONLINE' 
	AND name NOT IN ('master','msdb','model','tempdb','DBA');

	DECLARE @iID INT=1
		,@DatabaseName VARCHAR(100)
		,@SQL NVARCHAR(MAX);

	WHILE @iID <= (SELECT COUNT(*) FROM @Database)
	BEGIN

		SELECT @DatabaseName=DatabaseName
		FROM @Database D 
		WHERE D.iID=@iID;

		SET @SQL='USE ['+@DatabaseName+'] 

		SELECT OBJECT_NAME (referencing_id),ISNULL(referenced_database_name,DB_NAME()), 
			   referenced_schema_name, referenced_entity_name
		FROM sys.sql_expression_dependencies d
		WHERE OBJECT_NAME(d.referenced_id) = '''+@TableName+'''
			  AND OBJECT_DEFINITION (referencing_id)  LIKE ''%'+@ColumnName+'%'' ; ';

		--PRINT @SQL;
		INSERT INTO @ColumnDependencies
				( ReferencingObject ,
				  referenced_database_name ,
				  referenced_schema_name ,
				  referenced_entity_name 
				)	
		EXEC sys.sp_executesql @stmt=@SQL;

		SET @iID=@iID+1;

	END;

SELECT CD.ReferencingObject ,
       CD.referenced_database_name ,
       CD.referenced_schema_name ,
       CD.referenced_entity_name ,
	   @ColumnName AS Referenced_Column_Name
FROM @ColumnDependencies CD;
GO


