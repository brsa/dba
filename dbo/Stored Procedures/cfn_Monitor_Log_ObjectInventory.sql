﻿/*****************************************************************************
AUTHOR : AJAY H.
CREATE DATE : 03/21/2016
DESCRIPTION: Collect database stats
SAMPLE CALL:EXEC dbo.cfn_Monitor_Log_ObjectInventory;

**HISTORY**
-----------------------------------------------------------------------------------
Update Date    Updated By          Notes
-----------------------------------------------------------------------------------		 
										

******************************************************************************/
CREATE PROC [dbo].[cfn_Monitor_Log_ObjectInventory]
@Debug BIT=0
AS
BEGIN
SET NOCOUNT ON;

DECLARE @DatabasesList TABLE (iID INT IDENTITY(1,1),DatabaseName VARCHAR(200));

TRUNCATE TABLE dbo.Monitor_Temp_ObjectInventory
INSERT INTO @DatabasesList (  DatabaseName )
SELECT D.name AS DatabaseName
FROM sys.databases D
WHERE D.state_desc='ONLINE' 
AND name NOT IN ('master','msdb','model','tempdb','SSISDB','ReportServer','ReportServerTempDB');

DECLARE @Sql NVARCHAR(MAX),
	    @DbName NVARCHAR(128)='',
		@iID INT=1;

WHILE @iID <= (SELECT MAX(DL.iID) FROM @DatabasesList DL)
BEGIN

	SELECT @DbName=DatabaseName
	FROM @DatabasesList D 
	WHERE D.iID=@iID;

	SET @Sql=N'USE ['+@DbName+'] ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	DECLARE @LogDateTime DATETIME2(0)=SYSDATETIME();
	SELECT  @LogDateTime AS LogDateTime
			,DB_NAME() AS DatabaseName
			,DB_ID() AS DatabaseID
			,SCHEMA_NAME(O.schema_id) AS SchemaName
			,O.name AS ObjectName
			,O.type AS ObjectType
			,O.create_date AS CreateDate
			,O.modify_date AS LastUpdateDate
			,spStats.LastExecuted 
			,T.Rowscount
			,T.IsHeap
			,CASE WHEN  rw.LastRead IS NULL and rw.LastScan IS NOT NULL then rw.LastScan
								when rw.LastRead < rw.LastScan then rw.LastScan
								else rw.LastRead END AS LastRead
			,rw.LastWrite
			,n.TotalNolockCount
			,n.TransactionIsolationLevel
	FROM sys.objects O
	LEFT JOIN (
		SELECT object_id
			,MAX(last_execution_time) AS LastExecuted
		FROM sys.dm_exec_procedure_stats
		WHERE database_id=DB_ID(DB_NAME())
		GROUP BY object_id
			UNION ALL
		SELECT object_id
			,MAX(last_execution_time) AS LastExecuted
		FROM sys.dm_exec_trigger_stats
		WHERE database_id=DB_ID(DB_NAME())
		GROUP BY object_id
		) spStats ON o.object_id=spStats.object_id
	LEFT JOIN (
			SELECT object_id, SUM(row_count) AS Rowscount,CASE WHEN SUM(CASE WHEN index_id=0 THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS IsHeap
			FROM sys.dm_db_partition_stats
			WHERE index_id < 2
			GROUP BY object_id
			) T ON T.object_id = O.object_id
	LEFT JOIN (
			SELECT  ius.object_id ,
					MAX(ius.last_user_scan) AS LastScan ,
					MAX(ius.last_user_seek) AS LastRead ,
					MAX(ius.last_user_update) as LastWrite
			FROM    sys.dm_db_index_usage_stats AS ius
			WHERE   ius.database_id = DB_ID()
			GROUP BY ius.database_id ,
					ius.object_id
			) RW ON RW.object_id=O.object_id
	LEFT JOIN (
			SELECT sc.object_id
				,(LEN(sc.definition) - LEN(REPLACE(sc.definition, ''NOLOCK'', ''''))) / LEN(''NOLOCK'') AS TotalNolockCount
				,CASE WHEN PATINDEX(''%SET%TRANSACTION%ISOLATION%LEVEL%READ%UNCOMMITTED%'',sc.definition) > 0 THEN ''READ UNCOMMITTED'' 
					  WHEN PATINDEX(''%SET%TRANSACTION%ISOLATION%LEVEL%READ%COMMITTED%'',sc.definition) > 0 THEN ''READ COMMITTED''
					  WHEN PATINDEX(''%SET%TRANSACTION%ISOLATION%LEVEL%SNAPSHOT%'',sc.definition) > 0 THEN ''SNAPSHOT''
					  WHEN PATINDEX(''%SET%TRANSACTION%ISOLATION%LEVEL%SERIALIZABLE%'',sc.definition) > 0 THEN ''SERIALIZABLE''
				END AS TransactionIsolationLevel
			FROM sys.sql_modules  sc
			) n ON n.object_id=o.object_id
	WHERE O.is_ms_shipped=0 AND O.name NOT LIKE ''sp_MS%'' AND O.type IN (''FN'', ''FS'', ''FT'', ''IF'',''P'', ''TF'', ''V'',''U'',''SN'',''TR'')';
 
	IF @Debug=1
	BEGIN
		PRINT @Sql;
	END; 
	ELSE
	BEGIN
		INSERT INTO Monitor_Temp_ObjectInventory
				( LogDateTime ,
				  DatabaseName ,
				  DatabaseID ,
				  SchemaName ,
				  ObjectName ,
				  ObjectType ,
				  CreateDate ,
				  LastUpdateDate ,
				  LastExecuted ,
				  Rowscount ,
				  IsHeap ,
				  LastRead ,
				  LastWrite ,
				  TotalNolockCount ,
				  TransactionIsolationLevel
				)
		EXEC sys.sp_executesql @Query=@Sql;
	END;

	SET @iID=@iID+1;
END;

END;

