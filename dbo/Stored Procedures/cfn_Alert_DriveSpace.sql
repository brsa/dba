﻿CREATE PROCEDURE [dbo].[cfn_Alert_DriveSpace]
	@EmailRecipients varchar(max) = NULL,
    @Debug tinyint = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141001
    This procedure checks all drives that contain data files for available space.
    If a drive is "low" on space, it will email @EmailRecipients an alert with a summary
    of all drives that are low.
    The threshold for "low" is stepped thresholds depending on the total drive size.
    Thresholds are hard-coded below.

PARAMETERS
* @EmailRecipients - delimited list of email addresses. Used for sending email alert.
**************************************************************************************************
MODIFICATIONS:
    20150107 - In email, specify from address, and include standard server info in footer
*************************************************************************************************/
SET NOCOUNT ON;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

DECLARE @database_id int,
        @file_id int,
        @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

CREATE TABLE #FileStats (
	database_id int,
	file_id int,
	volume_mount_point nvarchar(512),
	file_system_type nvarchar(512),
	total_bytes bigint,
	available_bytes bigint
	);

CREATE TABLE #Drives (
	volume_mount_point nvarchar(512),
	file_system_type nvarchar(512),
	total_GB numeric(10,3),
	available_GB numeric(10,3),
    available_percent numeric(5,3)
	);

--Cursors
DECLARE filestats_cur CURSOR FOR
    SELECT database_id, file_id
    FROM sys.master_files;


--Get filestats drive info for every datafile to get every drive
--just in case there's a mountpoint that isn't a drive
	
OPEN filestats_cur;
FETCH NEXT FROM filestats_cur INTO @database_id, @file_id;

WHILE @@FETCH_STATUS = 0
BEGIN
    INSERT INTO #FileStats (database_id, file_id, volume_mount_point, file_system_type, total_bytes, available_bytes)
    SELECT database_id, file_id, volume_mount_point, file_system_type, total_bytes, available_bytes
    FROM sys.dm_os_volume_stats (@database_id,@file_id);

    FETCH NEXT FROM filestats_cur INTO @database_id, @file_id;
END;
CLOSE filestats_cur;
DEALLOCATE filestats_cur;

--dedupe info to get drive info
INSERT INTO #Drives (volume_mount_point, file_system_type, total_GB, available_GB)
SELECT volume_mount_point, file_system_type, min(total_bytes)/1024/1024/1024., min(available_bytes)/1024/1024/1024.
FROM #FileStats
GROUP BY volume_mount_point, file_system_type;

UPDATE #Drives 
SET available_percent = available_GB/total_GB*100;
 

-- Build #Results table based on criteria. Easier to have criteria here than bundled into building XML
SELECT * 
INTO #Results
FROM #Drives
  -- We'll need to figure out what our alert thresholds are. 
  -- Here are some possible thresholds based on drive size
WHERE (total_GB <=  30 AND available_percent < 10)
   OR (total_GB BETWEEN  30 AND  100 AND available_percent < 5)
   OR (total_GB BETWEEN 100 AND  500 AND available_GB < 5)
   OR (total_GB BETWEEN 500 AND 1000 AND available_GB < 10)
   OR (total_GB > 1000 AND available_GB < 20);

-- in Debug Mode, output #Drives table
IF @Debug > 0
BEGIN
    SELECT * FROM #Drives ORDER BY volume_mount_point;
END;

-- in "detailed debug" mode, output #FileStats table
IF @Debug = 2
BEGIN
    SELECT * FROM #FileStats ORDER BY database_id, file_id;
END;

-- If no rows returned, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #Results)
    RETURN (0);

--Send an email only if @Debug = 0
IF @Debug = 0
BEGIN
    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS()

    --Build the body of the email based on the #Results
    SET @EmailBody = @EmailBody + N'<h2>Low Disk space for ' + @@SERVERNAME + N':</h2>' + CHAR(10) +
            N'<table><tr>' +
            N'<th>Drive</th>' +
            N'<th>File System</th>' +
            N'<th>Total Size (GB)</th>' +
            N'<th>Available Space (GB)</th>' +
            N'<th>Available Percent</th></tr>' +
            CAST(( SELECT
                    td = volume_mount_point, '',
                    td = file_system_type, '',
                    td = CAST(total_GB AS nvarchar(10)), '',
                    td = CAST(available_GB AS nvarchar(10)), '',
                    td = CAST(available_percent AS nvarchar(10)), ''
                    FROM #Results
                    ORDER BY volume_mount_point
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';

    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();
		
	SET @EmailSubject = 'ALERT: Low Disk Space';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;

DROP TABLE #Drives;
DROP TABLE #FileStats;
DROP TABLE #Results;