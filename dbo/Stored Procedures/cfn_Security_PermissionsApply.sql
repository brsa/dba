﻿/*****************************************************************************************
Name: cfn_Security_PermissionsApply

Desc: Generates a script of all high level permissions for various users and applys permissions on the server.
	Assigns different permissions depending on an extended server property to determine environment.
	This script will abort if it attempts to assign permissions to missing SQL logins or missing objects.
	
Parameters:
	@database - Specify a single database to apply permissions. Default value is NULL, which applies permissions 
				to all databases. All server level permissions are applied regardless.
	@debug - Debug flag. Set to 1 to output all execution scripts. Default value is 0, which executes all scripts.
	
Dependencies:
	dbo.CFNRoles - Metadata table of permission definitions.
	dbo.DatabaseSelect - Function parses a string and returns a table of databases to apply permissions to.
						Part of Ola Hallengren's DatabaseBackup solution. https://ola.hallengren.com/sql-server-backup.html
	dbo.fn_split - Function parses a string and returns a table of values. Default delimiter is ','.

Auth: MDAK

Date: 04/27/15

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
05/05/15		MDAK					Renamed sproc
05/19/15		MDAK					Add logic for 'ALL' @environment
05/22/15		MDAK					Abort procedure if run on Management environment
08/18/15		MDAK					Only collect from and apply permissions to databases that are
										primary in the AG or online
*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Security_PermissionsApply;
EXEC dbo.cfn_Security_PermissionsApply @database = 'CRM';
EXEC dbo.cfn_Security_PermissionsApply @debug = 1;
*/

CREATE PROCEDURE [dbo].[cfn_Security_PermissionsApply] @database NVARCHAR(128) = NULL,
	@debug BIT = 0,
	@DbExclusionList VARCHAR(MAX)=NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @sqlVersion INT = NULL,
		@environment NVARCHAR(128) = NULL,
		@databaseName NVARCHAR(128) = NULL,
		@loginName NVARCHAR(128) = NULL,
		@roleName NVARCHAR(128) = NULL,
		@permissionState NVARCHAR(60) = NULL,
		@permissionType NVARCHAR(128) = NULL,
		@objectType NVARCHAR(60) = NULL,
		@schemaName NVARCHAR(128) = NULL,
		@objectName NVARCHAR(128) = NULL,
		@columnName NVARCHAR(128) = NULL,
		@crlf CHAR(2) = CHAR(13) + CHAR(10),
		@SQL NVARCHAR(MAX) = NULL;
	
	CREATE TABLE #roles (
		RoleName NVARCHAR(128) NULL,
		PermissionState NVARCHAR(60) NULL,
		PermissionType NVARCHAR(128) NULL,
		ObjectType NVARCHAR(60) NULL
		);
	
	CREATE TABLE #permissions (
		DatabaseName NVARCHAR(128) NULL,
		RoleName NVARCHAR(128) NULL,
		PermissionState NVARCHAR(60) NULL,
		PermissionType NVARCHAR(128) NULL,
		SchemaName NVARCHAR(128) NULL,
		ObjectType NVARCHAR(60) NULL,
		ObjectName NVARCHAR(128) NULL,
		ColumnName NVARCHAR(128) NULL,
		);

	CREATE CLUSTERED INDEX IX_DatabaseName ON #permissions (DatabaseName);

	CREATE TABLE #DebugPermissionsScript (DebugScript VARCHAR(MAX));

	-- Get the SQL Server major version number	
	SELECT @sqlVersion = CONVERT(INT, LEFT(CONVERT(NVARCHAR(128),SERVERPROPERTY('ProductVersion')),CHARINDEX('.', CONVERT(NVARCHAR(128),SERVERPROPERTY('ProductVersion')))-1));

	-- What environment are we executing on?
	-- Production, QA, Development, Processing, Management
	SELECT @environment = CONVERT(NVARCHAR(128),value)
	FROM master.sys.extended_properties
	WHERE name = 'SecurityEnvironment';

	IF COALESCE(@environment, 'Management') = 'Management'
	BEGIN
		SET @SQL = 'Aborting procedure. ' + CASE 
			WHEN @environment = 'Management'
				THEN 'Script may not run on Management environment.'
			WHEN @environment IS NULL
				THEN 'SecurityEnvironment extended property not set on master database.'
			END;

		SELECT @SQL;
	
		RETURN -1;
	END;


/****************************************************************************
						CREATE PERMISSIONS MATRIX
****************************************************************************/

	-- Database permissions
	INSERT INTO #permissions (DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	SELECT d.DatabaseName, r.RoleName, r.PermissionState, p.Value AS PermissionType, r.ObjectType, r.SchemaName, r.ObjectName, r.ColumnName
	FROM dbo.CFNRoles r
		CROSS APPLY dbo.fn_split(r.Environment, ',') e
		CROSS APPLY dbo.DatabaseSelect(r.DatabaseName) d
		CROSS APPLY dbo.fn_split(r.PermissionType, ',') p
	WHERE e.Value IN (@environment, 'ALL')
		AND dbo.dm_hadr_db_role(d.DatabaseName) IN ('PRIMARY','ONLINE');

	IF COALESCE(@database,'') <> ''
	BEGIN
		DELETE #permissions
		WHERE DatabaseName <> @database;
	END;

	-- High-level roles and permissions
	INSERT INTO #roles (RoleName, PermissionState, PermissionType, ObjectType)
	SELECT r.RoleName, r.PermissionState, p.Value AS PermissionType, r.ObjectType
	FROM dbo.CFNRoles r
		CROSS APPLY dbo.fn_split(r.Environment, ',') e
		CROSS APPLY dbo.fn_split(r.PermissionType, ',') p
	WHERE r.ObjectType = 'SERVER'
		AND e.Value IN (@environment, 'ALL')
	UNION
	SELECT RoleName, PermissionState, PermissionType, ObjectType
	FROM #permissions;
/*********************************************************
Exclude database from this process.
************************************************************/
	--SELECT *
	DELETE p
	FROM #permissions p
	JOIN dbo.fn_split(@DbExclusionList,',') fs ON fs.Value=p.DatabaseName;
	RETURN;

/****************************************************************************
						CREATE MISSING LOGINS
****************************************************************************/
	
	/*
	Check for existence of logins
	ABORT if SQL logins are missing
	CREATE missing Windows logins
	*/
	SET @SQL = '/***** CREATE MISSING LOGINS *****/';
	
	DECLARE login_cursor CURSOR LOCAL FAST_FORWARD
	FOR
	SELECT DISTINCT rm.PermissionType
	FROM #roles rm
		LEFT JOIN #roles r ON rm.PermissionType = r.RoleName
	WHERE rm.PermissionState = 'ADDROLEMEMBER'
		AND r.PermissionType IS NULL;

	OPEN login_cursor;

	FETCH NEXT
	FROM login_cursor
	INTO @loginName;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF NOT EXISTS (SELECT name FROM sys.server_principals WHERE name = @loginName)
		BEGIN
			IF @loginName NOT LIKE 'WALTHAM\%'
			BEGIN
				SET @SQL = 'Aborting procedure. SQL login ' + QUOTENAME(@loginName) + ' missing from server. Create login and retry.';

				SELECT @SQL;

				RETURN -1;
			END;
			ELSE
			BEGIN
				SET @SQL = @SQL + @crlf + 'USE [master] CREATE LOGIN ' + QUOTENAME(@loginName) + ' FROM WINDOWS WITH DEFAULT_DATABASE = [master];';
			END;
		END;
		
		FETCH NEXT
		FROM login_cursor
		INTO @loginName;
	END;

	IF @debug = 0
	BEGIN
		EXEC sp_executesql @SQL;
	END;
	ELSE
	BEGIN
		INSERT INTO #DebugPermissionsScript
		        ( DebugScript )
		SELECT @SQL;
	END;

	CLOSE login_cursor;

	DEALLOCATE login_cursor;


/****************************************************************************
						CREATE SERVER ROLES
****************************************************************************/
	
	-- <SQL2012+> Create server roles
	IF @sqlVersion >= 11
	BEGIN
		SET @SQL = '/***** CREATE MISSING SERVER ROLES *****/';

		DECLARE role_cursor CURSOR LOCAL FAST_FORWARD
		FOR
		SELECT DISTINCT RoleName
		FROM #roles
		WHERE RoleName LIKE 'CFN%'
			AND ObjectType = 'Server';

		OPEN role_cursor;

		FETCH NEXT
		FROM role_cursor
		INTO @roleName;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF NOT EXISTS (SELECT name FROM sys.server_principals WHERE name = @roleName)
			BEGIN
				SET @SQL = @SQL + @crlf + 'USE [master] CREATE SERVER ROLE ' + QUOTENAME(@roleName) + ';';
			END;

			FETCH NEXT
			FROM role_cursor
			INTO @roleName;
		END;

		IF @debug = 0
		BEGIN
			EXEC sp_executesql @SQL;
		END;
		ELSE
		BEGIN
			INSERT INTO #DebugPermissionsScript
					( DebugScript )
			SELECT @SQL;
		END;

		CLOSE role_cursor;

		DEALLOCATE role_cursor;
	END;


/****************************************************************************
						APPLY SERVER ROLE PERMISSIONS
****************************************************************************/
	
	/*
	<SQL2012+>	1. Give permissions to role
				2. Add role members
	<SQL2008R2->	1. Add role members
	*/

	SET @SQL = '/***** APPLY SERVER ROLE PERMISSIONS *****/';

	DECLARE permission_cursor CURSOR LOCAL FAST_FORWARD
	FOR
	SELECT RoleName, PermissionState, PermissionType
	FROM #roles
	WHERE ObjectType = 'Server';

	OPEN permission_cursor;

	FETCH NEXT
	FROM permission_cursor
	INTO @roleName, @permissionState, @permissionType;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @sqlVersion >= 11
		BEGIN
			IF @permissionState = 'ADDROLEMEMBER'
			BEGIN
				SET @SQL = @SQL + @crlf + 'ALTER SERVER ROLE ' + QUOTENAME(@roleName) + ' ADD MEMBER ' + QUOTENAME(@permissionType) + ';';
			END;
			ELSE
			BEGIN
				IF @roleName LIKE 'CFN%'
				BEGIN
					SET @SQL = @SQL + @crlf + 'USE [master] ' + @permissionState + ' ' + @permissionType + ' TO ' + QUOTENAME(@roleName) + ';';
				END;
			END;
		END;
		ELSE -- <SQL2008R2->
		BEGIN
			IF @roleName NOT LIKE 'CFN%' AND @permissionState = 'ADDROLEMEMBER'
			BEGIN
				SET @SQL = @SQL + @crlf + 'EXEC sp_addsrvrolemember @loginame = N''' + @permissionType + ''', @rolename = N''' + @roleName + ''';';
			END;
		END;

		FETCH NEXT
		FROM permission_cursor
		INTO @roleName, @permissionState, @permissionType;
	END;

	IF @debug = 0
	BEGIN
		EXEC sp_executesql @SQL;
	END;
	ELSE
	BEGIN
		INSERT INTO #DebugPermissionsScript
		        ( DebugScript )
		SELECT @SQL;
	END;

	CLOSE permission_cursor;

	DEALLOCATE permission_cursor;


/****************************************************************************
					CREATE DATABASE ROLES AND PERMISSIONS
****************************************************************************/

	--For each database
	DECLARE db_cursor CURSOR LOCAL FAST_FORWARD
	FOR
	SELECT DISTINCT DatabaseName
	FROM #permissions;

	OPEN db_cursor;

	FETCH NEXT
	FROM db_cursor
	INTO @databaseName;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF dbo.dm_hadr_db_role(@databaseName) IN ('PRIMARY','ONLINE')
			AND DATABASEPROPERTYEX(@databaseName, 'updateability') = 'READ_WRITE'
		BEGIN
			--Cursor through and create all distinct missing database users
			SET @SQL = '/***** ' + QUOTENAME(@databaseName) + ' CREATE MISSING DATABASE USERS *****/';
			
			DECLARE login_cursor CURSOR LOCAL FAST_FORWARD
			FOR
			SELECT DISTINCT rm.PermissionType
			FROM #permissions rm
				LEFT JOIN #permissions r ON rm.PermissionType = r.RoleName
					AND r.DatabaseName = @databaseName
			WHERE rm.DatabaseName = @databaseName
				AND rm.PermissionState = 'ADDROLEMEMBER'
				AND r.PermissionType IS NULL;

			OPEN login_cursor;

			FETCH NEXT
			FROM login_cursor
			INTO @loginName;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @SQL = @SQL + @crlf + 'IF NOT EXISTS (SELECT name FROM ' + @databaseName + '.sys.database_principals WHERE name = ''' + @loginName + ''')';
				SET @SQL = @SQL + @crlf + 'BEGIN';
				SET @SQL = @SQL + @crlf + '    USE ' + QUOTENAME(@databaseName) + ' CREATE USER ' + QUOTENAME(@loginName) + ' FROM LOGIN ' + QUOTENAME(@loginName) + ';';
				SET @SQL = @SQL + @crlf + 'END;';

				FETCH NEXT
				FROM login_cursor
				INTO @loginName;
			END;

			CLOSE login_cursor;

			DEALLOCATE login_cursor;

			--Cursor through and create all distinct missing database roles
			SET @SQL = @SQL + @crlf + '/***** ' + QUOTENAME(@databaseName) + ' CREATE MISSING DATABASE ROLES *****/';
			
			DECLARE role_cursor CURSOR LOCAL FAST_FORWARD
			FOR
			SELECT DISTINCT RoleName
			FROM #permissions
			WHERE DatabaseName = @databaseName
				AND RoleName LIKE 'CFN%';

			OPEN role_cursor;

			FETCH NEXT
			FROM role_cursor
			INTO @roleName;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @SQL = @SQL + @crlf + 'IF NOT EXISTS (SELECT name FROM ' + @databaseName + '.sys.database_principals WHERE name = ''' + @roleName + ''')';
				SET @SQL = @SQL + @crlf + 'BEGIN';
				SET @SQL = @SQL + @crlf + '    USE ' + QUOTENAME(@databaseName) + ' CREATE ROLE ' + QUOTENAME(@roleName) + ';';
				SET @SQL = @SQL + @crlf + 'END;';

				FETCH NEXT
				FROM role_cursor
				INTO @roleName;
			END;

			CLOSE role_cursor;

			DEALLOCATE role_cursor;

			--Apply database role permissions
			SET @SQL = @SQL + @crlf + '/***** ' + QUOTENAME(@databaseName) + ' APPLY DATABASE ROLE PERMISSIONS *****/';

			DECLARE permission_cursor CURSOR LOCAL FAST_FORWARD
			FOR
			SELECT RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName
			FROM #permissions
			WHERE DatabaseName = @databaseName;

			OPEN permission_cursor;

			FETCH NEXT
			FROM permission_cursor
			INTO @roleName, @permissionState, @permissionType, @objectType, @schemaName, @objectName, @columnName;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @SQL = @SQL + @crlf + 'USE ' + QUOTENAME(@databaseName) + ' ' +
					CASE
						WHEN @permissionState = 'ADDROLEMEMBER' THEN
							CASE
								WHEN @sqlVersion >= 11 THEN 'ALTER ROLE ' + QUOTENAME(@roleName) + ' ADD MEMBER ' + QUOTENAME(@permissionType) + ';'
								ELSE 'EXEC sp_addrolemember N''' + @roleName + ''', N''' + @permissionType + ''';'
							END
						ELSE @permissionState + ' ' + @permissionType +
							CASE
								WHEN @objectType = 'DATABASE' THEN ''
								WHEN @objectType = 'OBJECT' THEN ' ON OBJECT::' + QUOTENAME(COALESCE(@schemaName, 'dbo')) + '.' + QUOTENAME(@objectName) +
									CASE 
										WHEN @columnName IS NOT NULL THEN ' (' + QUOTENAME(@columnName) + ')'
										ELSE ''
									END
								WHEN @objectType = 'SCHEMA' THEN ' ON SCHEMA::' + QUOTENAME(@schemaName)
							END
							+ ' TO ' + QUOTENAME(@roleName) + ';'
					END;

				FETCH NEXT
				FROM permission_cursor
				INTO @roleName, @permissionState, @permissionType, @objectType, @schemaName, @objectName, @columnName;
			END;

			IF @debug = 0
			BEGIN
				EXEC sp_executesql @SQL;
			END;
			ELSE
			BEGIN
				INSERT INTO #DebugPermissionsScript
						( DebugScript )
				SELECT @SQL;
			END;

			CLOSE permission_cursor;

			DEALLOCATE permission_cursor;
		END;

		FETCH NEXT
		FROM db_cursor
		INTO @databaseName;
	END;

	IF @debug=1
	BEGIN
		SELECT dps.DebugScript FROM #DebugPermissionsScript dps
	END

	CLOSE db_cursor;

	DEALLOCATE db_cursor;
END;
