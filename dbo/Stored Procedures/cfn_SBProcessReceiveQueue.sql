﻿


-- Need to put Error Handling EVERYWHERE & log errors to a table
-- ServiceBroker is asychronous, so errors can't bubble back up
-- to a user/application session to be handled there.

CREATE PROCEDURE [dbo].[cfn_SBProcessReceiveQueue]
AS
SET NOCOUNT ON 

DECLARE @Message table 
    (ConversationHandle uniqueidentifier,
     ServiceName nvarchar(512),
     ContractName nvarchar(256),
     MessageType nvarchar(256),
     MessageXML nvarchar(max));

DECLARE @SBQueueName sysname = 'ReceiveQueue',
    @ConversationHandle uniqueidentifier,
    @ServiceName nvarchar(512),
    @ContractName nvarchar(256),
    @MessageType nvarchar(256),
    @MessageXML nvarchar(max),
    @NewConversation uniqueidentifier,
    @ErrNumber int,
    @ErrSeverity int,
    @ErrState int,
    @ErrMsg varchar(2048);

--Just process one command at a time. Leave the rest in queue, in case the server crashes or something
BEGIN TRY
	--SILLY WORKAROUND
		-- SQL 2008 doesn't like RECEIVE to be the first statement in a TRY-CATCH block
		-- To make this work, put the RECEIVE in an IF statement that is always true!
		-- We should remove this once we're fully on SQL 2014
    IF 1=1
		RECEIVE TOP(1) conversation_handle, service_name, service_contract_name, message_type_name, CONVERT(NVARCHAR(MAX), message_body)
		FROM dbo.ReceiveQueue
		INTO @Message;
END TRY
BEGIN CATCH
    SET @ErrNumber = ERROR_NUMBER();
    SET @ErrSeverity = ERROR_SEVERITY();
    SET @ErrState = ERROR_STATE();
    SET @ErrMsg = ERROR_MESSAGE();
    SET @ErrMsg = 'Unable to Receive from Queue - ' + @ErrMsg;
    INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, QueueName, ErrorNumber, ErrorMessage)
    VALUES (GETDATE(), @SBQueueName, @ErrNumber, @ErrMsg);
    RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
    RETURN (1);
END CATCH;

SELECT TOP 1
       @ConversationHandle = ConversationHandle,
       @ServiceName = ServiceName,
       @ContractName = ContractName,
       @MessageType = MessageType,
       @MessageXML = MessageXML
FROM @Message;
--
IF @@ROWCOUNT = 0
 BEGIN
    PRINT 'No messages to process';
    RETURN (0);
 END;


IF @MessageType = '//Backup/Message/Full'
 BEGIN
    --Get a handle if we have an open conversation already
    SELECT TOP (1) @NewConversation = e.conversation_handle
    FROM sys.conversation_endpoints e
    JOIN sys.services s ON s.service_id = e.service_id
    WHERE e.far_service = '//Backup/Full'
    AND e.state IN ('SO','CO')
    AND s.name = '//Transmission/Receive';
    
    --Begin a conversation if we don't have a handle already
    IF @NewConversation IS NULL
    BEGIN TRY
        BEGIN DIALOG CONVERSATION @NewConversation
        FROM SERVICE [//Transmission/Receive]
        TO SERVICE '//Backup/Full'
        ON CONTRACT [//Backup/Contract]
        WITH ENCRYPTION = OFF;
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, @MessageXML);
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
    
    -- Send the message
    BEGIN TRY
		--SILLY WORKAROUND
		-- SQL 2008 doesn't like SEND to be the first statement in a TRY-CATCH block
		-- To make this work, put the SEND in an IF statement that is always true!
		-- We should remove this once we're fully on SQL 2014
		IF 1=1
			SEND ON CONVERSATION @NewConversation
				MESSAGE TYPE [//Backup/Message/Full] (@MessageXML);
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, @MessageXML);
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
 END
ELSE IF @MessageType = '//Backup/Message/Log'
 BEGIN
    --Get a handle if we have an open conversation already
    SELECT TOP (1) @NewConversation = e.conversation_handle
    FROM sys.conversation_endpoints e
    JOIN sys.services s ON s.service_id = e.service_id
    WHERE e.far_service = '//Backup/Log'
    AND e.state IN ('SO','CO') --started, conversing
    AND s.name = '//Transmission/Receive';
    
    --Begin a conversation if we don't have a handle already
    IF @NewConversation IS NULL
    BEGIN TRY
        BEGIN DIALOG CONVERSATION @NewConversation
        FROM SERVICE [//Transmission/Receive]
        TO SERVICE '//Backup/Log'
        ON CONTRACT [//Backup/Contract]
        WITH ENCRYPTION = OFF;
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, @MessageXML);
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
    
    -- Send the message
    BEGIN TRY
		--SILLY WORKAROUND
		-- SQL 2008 doesn't like SEND to be the first statement in a TRY-CATCH block
		-- To make this work, put the SEND in an IF statement that is always true!
		-- We should remove this once we're fully on SQL 2014
		IF 1=1
			SEND ON CONVERSATION @NewConversation
				MESSAGE TYPE [//Backup/Message/Log] (@MessageXML);
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, @MessageXML);
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
 END
ELSE IF @MessageType = 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
 BEGIN
    -- End the conversation
    BEGIN TRY
        END CONVERSATION @ConversationHandle;
    END TRY
    BEGIN CATCH
        SET @ErrNumber = ERROR_NUMBER();
        SET @ErrSeverity = ERROR_SEVERITY();
        SET @ErrState = ERROR_STATE();
        SET @ErrMsg = ERROR_MESSAGE();
        INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
        VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, @MessageXML);
        RAISERROR(@ErrMsg, @ErrSeverity, @ErrState);
        RETURN (1);
    END CATCH;
 END
ELSE -- No logic to handle this message type. Log/raise an error
 BEGIN
    SET @ErrNumber = 50000;
    SET @ErrSeverity = 16;
    SET @ErrState = 1;
    SET @ErrMsg = 'MessageType "' + @MessageType + '" not supported';
    INSERT INTO dbo.cfn_ServiceBrokerLog (MessageDateTime, ServiceName, QueueName, MessageType, ErrorNumber, ErrorMessage, SbMessage)
    VALUES (GETDATE(), @ServiceName, @SBQueueName, @MessageType, @ErrNumber, @ErrMsg, @MessageXML);
    RAISERROR (@ErrMsg, @ErrSeverity, @ErrState);
    RETURN (1);
 END; 

-- Conversation Cleanup
DECLARE conv_cur CURSOR FOR
    SELECT conversation_handle
    FROM sys.conversation_endpoints
    WHERE state IN ('DI','DO','CD');

OPEN conv_cur;
FETCH NEXT FROM conv_cur INTO @ConversationHandle;
WHILE @@FETCH_STATUS = 0
BEGIN
    END CONVERSATION @ConversationHandle WITH CLEANUP;
    FETCH NEXT FROM conv_cur INTO @ConversationHandle;
END;

CLOSE conv_cur;
DEALLOCATE conv_cur;