﻿CREATE PROCEDURE cfn_Security_OrphanedUsersAutoFix
	 @Debug bit = 1
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150218
       This procedure loops through all DBs that are online or primary in an AG.
	   For all DB Users of type "SQL Login", check if there is a Login that has the same name,
	   but a mis-matched SID. 

	   If there is a SID mismatch, then use sp_change_users_login with the 'Auto_Fix' parameter
	   to repair the login/user link.

	   If a User & login are matched on SID, but not name, leave it alone.

PARAMETERS
* @Debug - bit - If 1/True, then output a resultset of all statements that need to be run to fix.
                 If 0/False, run the statements to fix, but do not output anything.
**************************************************************************************************
MODIFICATIONS:
    20150505 - Renamed sproc
*************************************************************************************************/
SET NOCOUNT ON;


DECLARE @DbName sysname,
		@Sql nvarchar(max);

CREATE TABLE #SqlText (
	Id int identity(1,1), 
	SqlText nvarchar(max));

DECLARE db_cur CURSOR FOR
	SELECT name
	FROM master.sys.databases
	WHERE dbo.dm_hadr_db_role(name) IN ('PRIMARY','ONLINE');

DECLARE sql_cur CURSOR FOR
	SELECT SqlText
	FROM #SqlText;


OPEN db_cur;
FETCH NEXT FROM db_cur INTO @DbName;

WHILE @@FETCH_STATUS = 0
BEGIN
	
	--Need to COLLATE on name comparison in case a DB has a unique/nonstandard collation
	-- Check authentication type to filter out users created WITHOUT LOGIN
	SET @Sql = 'SELECT ''USE [' + @DbName + ']; EXEC sp_change_users_login ''''Auto_Fix'''', '''''' + dp.name + '''''';''
				FROM [' + @DbName + '].sys.database_principals dp
				LEFT JOIN sys.server_principals sp ON sp.sid = dp.sid
				WHERE dp.type = ''S'' 
				AND dp.authentication_type = 1
				AND sp.type IS NULL
				AND EXISTS (SELECT 1 FROM sys.server_principals sp1 
						WHERE dp.name COLLATE database_default = sp1.name COLLATE database_default
						);'
	
	INSERT #SqlText
	EXEC sp_executesql @stmt = @Sql;

	FETCH NEXT FROM db_cur INTO @DbName;
END;

CLOSE db_cur;
DEALLOCATE db_cur;


-- Output resultset in Debug Mode
IF @Debug = 1
BEGIN
	SELECT * FROM #SqlText
END;

-- Actually execute the SQL if not in Debug Mode
IF @Debug = 0
BEGIN
	OPEN sql_cur;
	FETCH NEXT FROM sql_cur INTO @Sql;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC sp_executesql @stmt = @Sql;
		FETCH NEXT FROM sql_cur INTO @Sql;
	END;

	CLOSE sql_cur;
	DEALLOCATE sql_cur;
END;

DROP TABLE #SqlText;

GO

