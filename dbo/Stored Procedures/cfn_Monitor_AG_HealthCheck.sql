﻿CREATE PROC dbo.cfn_Monitor_AG_HealthCheck
AS
SET NOCOUNT ON;

SELECT
	db.name AS databaseName
	,suspended = CASE is_suspended WHEN 1 THEN 'Suspended' WHEN 0 THEN 'Resumed' ELSE 'unknown' END
	,health = CASE st.synchronization_health WHEN 0 THEN 'Not Synchronizing' WHEN 1 THEN 'Partially Healthy' WHEN 2 THEN 'Healthy' END
	,is_ag_replica_local = CASE WHEN arstat.is_local = 1 THEN N'LOCAL' ELSE 'REMOTE' END
	,ag_replica_role = CASE WHEN arstat.role_desc IS NULL THEN N'DISCONNECTED' ELSE arstat.role_desc END
	,ag.name AS ag_name
	,ar.replica_server_name AS ag_replica_server
	,last_sent_time -- Time when the last log block was sent.
	,log_send_queue_size AS log_send_queue_sizeKB -- Amount of log records of the primary database that has not been sent to the secondary databases, in kilobytes (KB).
	,log_send_rate AS log_send_rateKBSec -- Rate at which log records are being sent to the secondary databases, in kilobytes (KB)/second.
	,redo_queue_size AS redo_queue_sizeKB --Amount of log records in the log files of the secondary replica that has not yet been redone, in kilobytes (KB).
	,redo_rate AS redo_rateKBSec -- Rate at which the log records are being redone on a given secondary database, in kilobytes (KB)/second.
	,filestream_send_rate AS filestream_send_rateKBSec -- The rate at which the FILESTREAM files are shipped to the secondary replica, in kilobytes (KB)/second.
	,CASE WHEN redo_rate=0 THEN 0 ELSE redo_queue_size / redo_rate END AS EstimatedRedo --Estimated 
	,CASE WHEN log_send_rate=0 THEN 0 ELSE log_send_queue_size / log_send_rate END AS EstimatedRPO --Estimated Potential Data Loss
FROM sys.dm_hadr_database_replica_states st
LEFT JOIN sys.availability_groups ag ON st.group_id = ag.group_id

JOIN sys.dm_hadr_availability_replica_states arstat ON st.replica_id = arstat.replica_id
LEFT JOIN sys.databases db ON db.database_id = st.database_id
LEFT JOIN sys.availability_replicas ar ON ar.group_id = st.group_id
		AND ar.replica_id = st.replica_id
--on st.group_id = ar.group_id 
WHERE arstat.is_local <> 1;
GO

