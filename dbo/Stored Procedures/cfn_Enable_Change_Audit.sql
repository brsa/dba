﻿/* =============================================

-- Author:		Don Tam

-- Create date: <Create Date,,>

-- Description:	enable database chage audit
-- store procedure required 'OLE Automation Proecures'
sp_configure 'Ole Automation Procedures', 1
RECONFIGURE

-- =============================================
*/
create procedure [dbo].[cfn_enable_change_audit]
as 
begin
    set nocount on;
    
    declare @queryTxt varchar(8000),@servername varchar(100)=replace(@@servername,'\','_')+'_object_changes'
    declare @objFso int
    declare @folderName varchar(200)
    declare @folderExists varchar(20)
    
    if exists(select top 1
               name
              from master.sys.server_audits as a
              where a.name=@servername)
    begin
        select @queryTxt='
USE [MASTER];
if exists (select top 1 name from master.sys.server_audit_specifications
where name = '''+@servername+'''
) BEGIN
	ALTER SERVER AUDIT SPECIFICATION ['+@servername+'] WITH (STATE = OFF);
	DROP SERVER AUDIT SPECIFICATION ['+@servername+'] END;
if exists (select top 1 name from master.sys.dm_server_audit_status
where name = '''+@servername+'''
) BEGIN
	ALTER SERVER AUDIT ['+@servername+'] WITH (STATE = OFF);
	DROP SERVER AUDIT ['+@servername+'] END;
if exists (select top 1 name from msdb.sys.database_audit_specifications
where name = '''+@servername+'_MSDB''
) BEGIN 
	USE [msdb];
	ALTER DATABASE AUDIT SPECIFICATION ['+@servername+'_MSDB] WITH (STATE = OFF);
	DROP DATABASE AUDIT SPECIFICATION ['+@servername+'_MSDB] END;
'
        
        exec(@queryTxt)
    end
    
    select @folderName=replace(convert(varchar(100),serverproperty('ErrorLogFileName')),'ERRORLOG','');
    
    exec master.dbo.sp_OACreate 'Scripting.FileSystemObject',@objFSO out
    
    exec master.dbo.sp_OAMethod @objFSO,'FolderExists',@folderExists out,@folderName
    
    if @folderExists<>'True'
    begin
        exec master.dbo.sp_OAMethod @objFSO,'CreateFolder',@folderExists out,@folderName
    end
    
    select @queryTxt='
	USE [MASTER];
if not exists (select top 1 name from master.sys.dm_server_audit_status
where name <> '''+@servername+'''
) BEGIN
	CREATE SERVER AUDIT ['+@servername+']
	TO FILE (FILEPATH = '''+@folderName+''', MAXSIZE = 100MB, MAX_ROLLOVER_FILES = 10)
	WITH (QUEUE_DELAY = 0, ON_FAILURE = CONTINUE);
	ALTER SERVER AUDIT ['+@servername+'] WITH (STATE = ON); END 
if not exists (select top 1 name from master.sys.server_audit_specifications
where name <> '''+@servername+'''
) BEGIN
	CREATE SERVER AUDIT SPECIFICATION ['+@servername+']
	FOR SERVER audit ['+@servername+']
		ADD (Schema_object_change_GROUP),
		ADD (DATABASE_OBJECT_CHANGE_GROUP);
	ALTER SERVER AUDIT SPECIFICATION ['+@servername+'] WITH (STATE = ON); END
if not exists (select top 1 name from msdb.sys.database_audit_specifications
where name = '''+@servername+'_MSDB''
) BEGIN 
USE [msdb];
CREATE DATABASE AUDIT SPECIFICATION ['+@servername+'_MSDB]
FOR SERVER AUDIT ['+@servername+']
ADD (EXECUTE ON OBJECT::[dbo].[sp_delete_job] BY [dbo]),
ADD (EXECUTE ON OBJECT::[dbo].[sp_delete_job] BY [SQLAgentUserRole]),
ADD (EXECUTE ON OBJECT::[dbo].[sp_add_job] BY [dbo]),
ADD (EXECUTE ON OBJECT::[dbo].[sp_add_job] BY [SQLAgentUserRole]),
ADD (EXECUTE ON OBJECT::[dbo].[sp_update_job] BY [dbo]),
ADD (EXECUTE ON OBJECT::[dbo].[sp_update_job] BY [SQLAgentUserRole]),
ADD (UPDATE ON OBJECT::[dbo].[sysjobs] BY [dbo]),
ADD (UPDATE ON OBJECT::[dbo].[sysjobs] BY [SQLAgentUserRole]),
ADD (DELETE ON OBJECT::[dbo].[sysjobs] BY [dbo]),
ADD (DELETE ON OBJECT::[dbo].[sysjobs] BY [SQLAgentUserRole]),
ADD (INSERT ON OBJECT::[dbo].[sysjobs] BY [dbo]),
ADD (INSERT ON OBJECT::[dbo].[sysjobs] BY [SQLAgentUserRole])
WITH (STATE = ON); END
'
    
    if not exists(select top 1
                   name
                  from master.sys.server_audits as a
                  where a.name=@servername and is_state_enabled=1)
    begin
        --print @queryTxt
        exec(@queryTxt)
    end
end