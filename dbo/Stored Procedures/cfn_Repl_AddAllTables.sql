﻿CREATE PROCEDURE dbo.cfn_Repl_AddAllTables
	@PubDbName nvarchar(256),
	@PublicationName nvarchar(256),
	@ExcludeTables nvarchar(256) = NULL,
	@Debug bit = 0
AS
SET NOCOUNT ON;
---------------------
-- The @ExcludeTables parameter requires the value be a comma-separated, single-quoted list.
-- That's kind of icky, but we can make this more robust in v 2.0.  I don't think we'll use this exclude list very often
---------------------
DECLARE @sql nvarchar(max);
DECLARE @ArticleName nvarchar(256);

CREATE TABLE #article (
	ID int identity(1,1) PRIMARY KEY,
	ArticleName nvarchar(256));

--Get all unpublished tables that have a PK
SET @sql = N'SELECT t.name
		FROM [' + @PubDbName + '].sys.objects t
		JOIN [' + @PubDbName + '].sys.objects pk ON pk.parent_object_id = t.object_id
		WHERE t.is_ms_shipped = 0
		AND t.is_published = 0
		AND t.name NOT IN (' + COALESCE(@ExcludeTables,'''''') + ')
		AND t.type = ''U'';';

INSERT INTO #article
EXEC sp_executesql @sql;

--Call cfn_Repl_AddArticle in a loop for every table in #article
--Debug mode works by passing parameter through to cfn_Repl_AddArticle to print statement

DECLARE article_cur CURSOR FOR
	SELECT DISTINCT ArticleName FROM #article;

OPEN article_cur;
FETCH NEXT FROM article_cur INTO @ArticleName;

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC dbo.cfn_Repl_AddArticle 
		@PubDbName = @PubDbName,
		@PublicationName = @PublicationName,
		@ArticleName = @ArticleName,
		@Debug = @Debug
	FETCH NEXT FROM article_cur INTO @ArticleName;
END;

CLOSE article_cur;
DEALLOCATE article_cur;

DROP TABLE #article

GO