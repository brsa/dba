﻿/*****************************************************************************
AUTHOR : AJAY H.
CREATE DATE : 03/21/2016
DESCRIPTION: Collect database stats
SAMPLE CALL:EXEC dbo.cfn_Monitor_Log_Dependencies;

**HISTORY**
-----------------------------------------------------------------------------------
Update Date    Updated By          Notes
-----------------------------------------------------------------------------------		 
										

******************************************************************************/
CREATE PROC [dbo].[cfn_Monitor_Log_Dependencies]
AS
BEGIN
SET NOCOUNT ON;

--Aggressively clean out temp table of everything except the most recent
--WHILE @@ROWCOUNT <> 0
--	DELETE TOP (1000) dbo.Monitor_Temp_Dependencies;
TRUNCATE TABLE dbo.Monitor_Temp_Dependencies;

DECLARE @DatabasesList TABLE (iID INT IDENTITY(1,1),DatabaseName VARCHAR(200));

INSERT INTO @DatabasesList (  DatabaseName )
SELECT D.name AS DatabaseName
FROM sys.databases D
WHERE D.state_desc='ONLINE' 
AND name NOT IN ('master','msdb','model','tempdb');

DECLARE @Sql NVARCHAR(MAX),
	    @DbName NVARCHAR(128)='',
		@iID INT=1;

WHILE @iID <= (SELECT MAX(DL.iID) FROM @DatabasesList DL)
BEGIN

	SELECT @DbName=DatabaseName
	FROM @DatabasesList D 
	WHERE D.iID=@iID;

	SET @Sql=N'USE ['+@DbName+'] ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT	DISTINCT
			SYSDATETIME() AS LogDate ,
			ServerName = CAST( SL.Value AS VARCHAR(30)) ,
			DatabaseName = DB_NAME() ,
			s.name AS SchemaName ,
			OBJECT_NAME(referencing_id) AS ObjectName ,
			ReferencingObjectType = o.type_desc ,
			CASE WHEN referenced_server_name IS NULL OR referenced_server_name='''' THEN CAST( SL.Value AS VARCHAR(30))  
				 ELSE referenced_server_name END  AS referenced_server_name ,
			CASE WHEN referenced_database_name IS NULL OR referenced_database_name='''' THEN DB_NAME() 
			     ELSE referenced_database_name END  AS referenced_database_name ,
			CASE WHEN referenced_schema_name IS NULL OR referenced_schema_name='''' THEN ''dbo'' 
			     ELSE referenced_schema_name END AS referenced_schema_name ,
			referenced_entity_name ,
			ReferencedObjectType = o2.type_desc ,
			REPLACE(REPLACE(base_object_name,''['', ''''), '']'', '''') AS base_object_name			
	FROM    sys.sql_expression_dependencies sed 
			LEFT JOIN sys.objects o ON sed.referencing_id = o.object_id
												AND o.type_desc != ''CHECK_CONSTRAINT''
			LEFT JOIN sys.schemas s ON s.schema_id = o.schema_id
			LEFT JOIN sys.objects o2 ON sed.referenced_id = o2.object_id
													AND o2.type_desc != ''CHECK_CONSTRAINT''
			LEFT JOIN sys.synonyms syn ON sed.referenced_id = syn.object_id
			LEFT JOIN master.sys.extended_properties SL  ON SL.name=''ServerAlias''
	WHERE   s.name IS NOT NULL
			AND referenced_entity_name <> ''sysdiagrams''
			AND OBJECT_NAME(referencing_id) NOT LIKE ''%_DEVDB'';';

	INSERT INTO dbo.Monitor_Temp_Dependencies 
	        ( LogDate ,ServerName ,DatabaseName ,SchemaName ,ObjectName ,ReferencingObjectType 
			  ,Referenced_server_name ,Referenced_database_name ,referenced_schema_name ,referenced_entity_name ,ReferencedObjectType, base_object_name 	          
	        )
	EXEC sys.sp_executesql @Query=@Sql;

	SET @iID=@iID+1;
END;

END;