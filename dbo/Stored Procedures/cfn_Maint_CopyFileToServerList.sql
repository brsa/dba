﻿CREATE PROCEDURE [dbo].[cfn_Maint_CopyFileToServerList]
	@ServerListCSV  nvarchar(max),
    @SourceFilePath nvarchar(512),
    @SourceFile     nvarchar(128)
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150625
       This Alert performs a robocopy to keep files in sync between a list of servers.
       The list of servers is a parameter @ServerListCSV.
	   If the server running the procedure is listed in @ServerListCSV, we'll try to exclude it. 
	      (ie, do not try to copy the file to yourself).
	   Requires access to administrative shares on remote server.

PARAMETERS
* @ServerListCSV - Comma-separated list of servers to copy file to
* @SourceFilePath - LOCAL path to the server running the sproc to copy file from
* @SourceFile     - Name of file to copy
EXAMPLES:
* EXEC cfn_os_CopyFileToServerList @ServerListCSV = 'SQLPROC01W,SQLPROC01M', @SourceFilePath = 'C:\Program Files\Microsoft SQL Server\110\DTS\Binn', @SourceFile = 'DTExec.exe.config'
**************************************************************************************************
MODIFICATIONS:
    20140804 - AM2 - 
*************************************************************************************************/
AS
SET NOCOUNT ON;

DECLARE @cmd nvarchar(4000);
DECLARE @rc int;

IF substring(@SourceFilePath,2,1) <> ':'
BEGIN
	RAISERROR (N'@SourceFilePath must be a local path.',16,1);
	RETURN -1;
END;

DECLARE server_cur CURSOR FOR  
	WITH ServerList AS (
		SELECT ID, Value AS ServerName FROM dbo.fn_split(@ServerListCSV,',')
	)
	SELECT 'robocopy "' + @SourceFilePath + '" "\\' + ServerName + '\' + STUFF(@SourceFilePath,2,1,'$') + '" "' + @SourceFile + '" /COPYALL /R:5 /W:5'
	FROM ServerList
	WHERE ServerName <> cast(SERVERPROPERTY('ComputerNamePhysicalNetBIOS') as sysname)
	  AND ServerName <> cast(SERVERPROPERTY('MachineName') as sysname)
	  AND ServerName <> cast(SERVERPROPERTY('ServerName') as sysname); 

OPEN server_cur   
FETCH NEXT FROM server_cur INTO @cmd   

WHILE @@FETCH_STATUS = 0   
BEGIN   
       EXEC @rc = xp_cmdshell @cmd, no_output;

	   IF @rc > 1
	   BEGIN
	       RAISERROR (N'Robocopy returned an exit code of %d, indicating an error.',16,1,@rc);
	       RETURN (@rc); --return if there's an error Robocopy return codes: http://goo.gl/QVWwaw
	   END;

       FETCH NEXT FROM server_cur INTO @cmd   
END   

CLOSE server_cur   
DEALLOCATE server_cur

GO