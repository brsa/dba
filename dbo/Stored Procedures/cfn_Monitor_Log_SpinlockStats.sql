﻿CREATE PROCEDURE dbo.cfn_Monitor_Log_SpinlockStats
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150822
    This procedure logs Spinlock stats from the DMVs to Monitor_SpinlockStats.
	Calculates a delta since the last run, and saves that delta to the table.
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME(),
	@LastLogDateTime datetime2(0),
	@IntervalMinutes int;

--Lets truncate seconds & round to the nearest minute.
SELECT @LogDateTime = CONVERT(varchar(20),@LogDateTime,100)

--When did we last check stats?
SELECT @LastLogDateTime = MAX(LogDateTime) FROM Monitor_SpinlockStats;

--If the instance rebooted since then, we should consider that restart time as our last time.
    -- Stats are only cumulative since start time, so @IntervalMinutues should reflect a shorter time.
	-- Also if we just restarted, we oughtn't compare against last collection to compute a delta.
SELECT @LastLogDateTime = CASE WHEN create_date > @LastLogDateTime  OR @LastLogDateTime IS NULL THEN create_date 
								ELSE @LastLogDateTime END 
FROM sys.databases WHERE name = 'tempdb';

--This could easily be in-line below. But I'm going to do it here for clarity
SELECT @IntervalMinutes = COALESCE(DATEDIFF(mi,@LastLogDateTime,@LogDateTime),0);

--We use a physical table for our temp table, because we need to compare between this run & last run
INSERT INTO Monitor_temp_SpinlockStats (LogDateTime, SpinlockName, Collisions, Spins, SleepTime, Backoffs)
SELECT @LogDateTime AS LogDateTime,
    name,
	collisions,
	spins,
	sleep_time,
	backoffs
FROM sys.dm_os_spinlock_stats;


--Now compare this most recent collection with the previous collection, and compute the delta.
INSERT INTO Monitor_SpinlockStats (LogDateTime, SpinlockName, IntervalMinutes, Collisions, Spins, SleepTime, Backoffs)
SELECT LogDateTime = @LogDateTime,
	SpinlockName = new.SpinlockName,
	IntervalMinutes = @IntervalMinutes,
	Collisions = new.Collisions - COALESCE(old.Collisions,0),
	Spins = new.Spins - COALESCE(old.Spins,0),
	SleepTime = new.SleepTime - COALESCE(old.SleepTime,0),
	Backoffs = new.Backoffs - COALESCE(old.Backoffs,0)
FROM Monitor_Temp_SpinlockStats new
LEFT JOIN Monitor_Temp_SpinlockStats old
	ON old.SpinlockName = new.SpinlockName AND old.LogDateTime = @LastLogDateTime
WHERE new.LogDateTime = @LogDateTime;


--Aggressively clean out temp table of everything except the most recent
WHILE @@ROWCOUNT <> 0
	DELETE TOP (1000) Monitor_temp_SpinlockStats 
	WHERE LogDateTime < DATEADD(hh,-72,@LogDateTime);
