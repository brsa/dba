CREATE PROCEDURE dbo.cfn_Monitor_Log_ServerInfo
	@Debug bit = 1
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141001
    This procedure outputs server configs from various aspects in a consolidated list.
	* sys.configurations
	* comma-separated list of everyone with sa
	* Lock Pages in Memory
	* Enabled global trace flags
	* certain SERVERPROPERTY() values
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @SQL nvarchar(max),
	@SQLMajVersion decimal(4,2);

CREATE TABLE #Info(
	ServerName sysname NOT NULL,
	FacetType varchar(32),
	FacetName nvarchar(70),
	Value sql_variant,
	PRIMARY KEY CLUSTERED (ServerName, FacetType, FacetName)
) ;

CREATE TABLE #cmdout (
	cmdout nvarchar(255)
);

CREATE TABLE #TraceStatus (
	TraceFlag varchar(10),
	TraceStatus bit,
	GlobalStatus bit,
	SessionStatus bit
);

SELECT @SQLMajVersion = SUBSTRING(CAST(SERVERPROPERTY('ProductVersion') AS varchar(100)),0,5);

--sys.configurations
INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME AS ServerName, 
       'sys.configurations' AS FacetType,
	   name AS FacetName,
	   value_in_use AS Value
FROM master.sys.configurations;

--extended properties
INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME, 'master extended_properties', name, value FROM master.sys.extended_properties WHERE class = 0

--server properties/info
INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME, 'Server Properties', 'Instance Start Time', create_date FROM sys.databases WHERE name = 'tempdb'
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Cpu Count',cpu_count FROM sys.dm_os_sys_info
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Cpu Hyperthread Ratio',hyperthread_ratio FROM sys.dm_os_sys_info
UNION ALL 
SELECT @@SERVERNAME, 'Server Properties', 'OS', Substring(@@version,PatIndex('%Windows%',@@version),8000)
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Edition', SERVERPROPERTY('Edition')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Product Level', SERVERPROPERTY('ProductLevel')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Product Version', SERVERPROPERTY('ProductVersion')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Resource DB Version', SERVERPROPERTY('ResourceVersion')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Collation', SERVERPROPERTY('Collation')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Physical Computer Name', SERVERPROPERTY('ComputerNamePhysicalNetBIOS')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Character Set', SERVERPROPERTY('SqlCharSetName')
UNION ALL
SELECT @@SERVERNAME, 'Server Properties', 'Sort Order', SERVERPROPERTY('SqlSortOrderName');

-- Until SQL 2008, Microsoft measures memory in bytes.
-- In SQL 2012, Microsoft changes to measure memory in KB instead of bytes. New column name, new math
-- Use dynamic SQL to avoid compilation errors
IF @SQLMajVersion < 11
BEGIN
	SET @sql = 'SELECT @@SERVERNAME, ''Server Properties'', ''Physical Memory (GB)'', CAST(physical_memory_in_bytes/1024/1024/1024.0 AS decimal(10,4)) FROM sys.dm_os_sys_info
	UNION ALL 
	SELECT @@SERVERNAME, ''Server Properties'', ''Virtual Memory (GB)'', CAST(virtual_memory_in_bytes/1024/1024/1024.0 AS decimal(10,4)) FROM sys.dm_os_sys_info;';
END
ELSE
BEGIN
	SET @sql = 'SELECT @@SERVERNAME, ''Server Properties'', ''Physical Memory (GB)'', CAST(physical_memory_kb/1024/1024.0 AS decimal(10,4)) FROM sys.dm_os_sys_info
	UNION ALL
	SELECT @@SERVERNAME, ''Server Properties'', ''Virtual Memory (GB)'', CAST(virtual_memory_kb/1024/1024.0 AS decimal(10,4)) FROM sys.dm_os_sys_info;';
END;

INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
EXEC sp_executesql @stmt = @sql;


--list of sysadmins
INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME, 'Permissions', 'SQL Sysadmin List', CAST(SUBSTRING((SELECT ',' + quotename(l.NAME)
										FROM master.sys.syslogins l
										INNER JOIN sys.server_principals p ON p.NAME = l.NAME
										WHERE l.sysadmin = 1
										AND p.is_disabled = 0
										ORDER BY type_desc
									FOR XML PATH('')),2,8000) AS varchar(8000));


--Service Broker GUID & endpoint info (name & Port number)
INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME, 'Server Config', 'ServiceBroker Endpoint', CAST(name AS sql_variant)
FROM sys.tcp_endpoints
WHERE type_desc = 'SERVICE_BROKER'
UNION
SELECT @@SERVERNAME, 'Server Config', 'ServiceBroker Endpoint Port', CAST(port AS sql_variant)
FROM sys.tcp_endpoints
WHERE type_desc = 'SERVICE_BROKER'
UNION
SELECT @@SERVERNAME, 'Server Config', 'ServiceBroker GUID', CAST(service_broker_guid AS sql_variant)
FROM sys.databases
WHERE name = 'DBA';


--Check if Lock Pages in Memory is enabled
--Check if Perform Volume Maintenance Tasks is enabled
INSERT INTO #cmdout
EXEC xp_cmdshell 'whoami /priv';
--LPIM
IF EXISTS (SELECT 1 FROM #cmdout WHERE cmdout LIKE 'SeLockMemoryPrivilege%Enabled%')
	INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
	SELECT @@SERVERNAME, 'Server Config', 'Lock Pages in Memory', 1;
ELSE
	INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
	SELECT @@SERVERNAME, 'Server Config', 'Lock Pages in Memory', 0;
--PVMT
IF EXISTS (SELECT 1 FROM #cmdout WHERE cmdout LIKE 'SeManageVolumePrivilege%Enabled%')
	INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
	SELECT @@SERVERNAME, 'Server Config', 'Perform Volume Maintenance Tasks', 1;
ELSE
	INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
	SELECT @@SERVERNAME, 'Server Config', 'Perform Volume Maintenance Tasks', 0;

TRUNCATE TABLE #cmdout;


--Global trace flags
INSERT #TraceStatus
EXEC ('dbcc tracestatus WITH NO_INFOMSGS');

INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME, 'Server Config', 'Global Trace Flags', COALESCE(CAST(SUBSTRING((SELECT ',' + quotename(TraceFlag)
										FROM #TraceStatus
										WHERE GlobalStatus = 1
										ORDER BY TraceFlag
									FOR XML PATH('')),2,8000) AS varchar(8000)),'');

--Service Account
DECLARE @SqlServiceAccount varchar(50),
		@RegistryKeyPath varchar(50)

--Determine path based on Default or named instance
SELECT @RegistryKeyPath = 'SYSTEM\CurrentControlSet\Services\'
		+ CASE 
			WHEN @@SERVICENAME = 'MSSQLSERVER' THEN 'MSSQLSERVER'
			ELSE 'MSSQL$' + @@SERVICENAME
		END

EXEC master.sys.xp_regread @rootkey='HKEY_LOCAL_MACHINE',
            @key         = @RegistryKeyPath,
            @value_name  = 'ObjectName',
            @value       = @SqlServiceAccount OUTPUT

INSERT INTO #Info (ServerName, FacetType, FacetName, Value)
SELECT @@SERVERNAME, 'Server Config', 'SQL Service Account', @SqlServiceAccount


IF @Debug = 1
	SELECT *
	FROM #Info;

IF @Debug = 0
BEGIN
	INSERT INTO Monitor_Info (ServerName, LogDateTime, FacetType, FacetName, Value)
	SELECT ServerName, GETDATE(), FacetType, FacetName, Value
	FROM #Info i
	WHERE NOT EXISTS (SELECT 1 FROM Monitor_CurrentInfo ci 
					  WHERE ci.ServerName = i.ServerName AND ci.FacetName = i.FacetName 
							AND ci.Value = i.Value)
END;

DROP TABLE #Info;
DROP TABLE #cmdout;
DROP TABLE #TraceStatus;