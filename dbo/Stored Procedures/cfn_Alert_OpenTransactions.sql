﻿CREATE PROCEDURE [dbo].[cfn_Alert_OpenTransactions]
    @LockingThreshold smallint = 60,
    @EmailRecipients varchar(max) = NULL,
    @EmailThreshold smallint = 10,
    @Debug tinyint = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141218
    This procedure checks for locking exceeding a duration of @LockingThreshold.
    @Debug parameter controls whether to email blocking to @EmailRecipients or to output resultset.

PARAMETERS
* @LockingThreshold - minutes - Alters when database locks have been holding log space
                       for this many minutes.
* @EmailRecipients   - delimited list of email addresses. Used for sending email alert.
* @Debug             - Supress sending email & output a resultset instead
**************************************************************************************************
MODIFICATIONS:
    20141222 - AM2 - Parse out the Hex jobid in ProgramName & turn into the Job Name.
    20141229 - AM2 - Parse out current SqlStatement from the complete SqlText.
                   - Start including SqlStatement in the email instead of SqlText
             - I now have 3 different answers to "What is the current SQL?"
               1) SqlText - This is the full output from sys.dm_exec_sql_text(). 
                          - If a procedure is running, this will be the CREATE PROCEDURE statement.
               2) SqlStatement - Uses Statement offset values to determine specific line from SqlText
                          - If a procedure is running, this is the specific statement within that proc
               3) InputBuffer - This is the output from DBCC INPUTBUFFER
                          - If a procedure is running, this is the EXEC statement
    20150107 - In email, specify from address, and include standard server info in footer

*************************************************************************************************/
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

--If we're in Debug mode, ignore @LockingThreshold parameter, Always use 1 minute.
IF @Debug = 1
	SET @LockingThreshold = 1

DECLARE @Id int = 1,
        @Spid int = 0,
        @JobIdHex nvarchar(34),
        @JobName nvarchar(256),
        @Sql nvarchar(max),
        @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

CREATE TABLE #OpenTrans (
    Id int identity(1,1) PRIMARY KEY,
    WaitingSpid smallint,
    BlockingSpid smallint,
    TransactionLengthMinutes AS DATEDIFF(mi,TransactionStart,GETDATE()),
    DbName sysname,
    HostName nvarchar(128),
    ProgramName nvarchar(128),
    LoginName nvarchar(128),
    LoginTime datetime2(3),
    LastRequestStart datetime2(3),
    LastRequestEnd datetime2(3),
    TransactionCnt int,
    TransactionStart datetime2(3),
    TransactionState tinyint,
	Command nvarchar(32),
    WaitTime int,
    WaitResource nvarchar(256),
    SqlText nvarchar(max),
    SqlStatement nvarchar(max),
    InputBuffer nvarchar(4000),
    SessionInfo nvarchar(max),
    );

CREATE TABLE #InputBuffer (
    EventType nvarchar(30),
    Params smallint,
    EventInfo nvarchar(4000)
    );


--Grab all sessions with open transactions

INSERT INTO #OpenTrans (WaitingSpid, BlockingSpid, DbName, HostName, ProgramName, LoginName, LoginTime, LastRequestStart, 
                    LastRequestEnd, TransactionCnt, TransactionStart, TransactionState, Command, WaitTime, WaitResource, SqlText, SqlStatement)
SELECT s.session_id AS WaitingSpid, 
       r.blocking_session_id AS BlockingSpid,
       COALESCE(db_name(dt.database_id),CAST(dt.database_id as nvarchar(10))) AS DbName,
       s.host_name AS HostName,
       s.program_name AS ProgramName,
       s.login_name AS LoginName,
       s.login_time AS LoginTime,
       s.last_request_start_time AS LastRequestStart,
       s.last_request_end_time AS LastRequestEnd,
       -- Need to use sysprocesses for now until we're fully on 2012/2014
	   (SELECT TOP 1 sp.open_tran FROM master.sys.sysprocesses sp WHERE sp.spid = s.session_id) AS TransactionCnt,
	   --s.open_transaction_count AS TransactionCnt,
       COALESCE(dt.database_transaction_begin_time,s.last_request_start_time) AS TransactionStart,
       dt.database_transaction_state AS TransactionState,
       r.command AS Command,
       r.wait_time AS WaitTime,
       r.wait_resource AS WaitResource,
       COALESCE(t.text,'') AS SqlText,
       COALESCE(SUBSTRING(t.text, (r.statement_start_offset/2)+1, (
                (CASE r.statement_end_offset
                   WHEN -1 THEN DATALENGTH(t.text)
                   ELSE r.statement_end_offset
                 END - r.statement_start_offset)
              /2) + 1),'') AS SqlStatement
FROM sys.dm_exec_sessions s
JOIN sys.dm_tran_session_transactions st ON st.session_id = s.session_id
JOIN sys.dm_tran_database_transactions dt ON dt.transaction_id = st.transaction_id
LEFT JOIN sys.dm_exec_requests r ON r.session_id = s.session_id
OUTER APPLY sys.dm_exec_sql_text (r.sql_handle) t
WHERE dt.database_transaction_state NOT IN (3) -- 3 means transaction has been initialized but has not generated any log records. Ignore it
AND COALESCE(dt.database_transaction_begin_time,s.last_request_start_time) < DATEADD(mi,-1*@LockingThreshold ,GETDATE());

-- Grab the input buffer for all sessions, too.
WHILE EXISTS (SELECT 1 FROM #OpenTrans WHERE InputBuffer IS NULL)
BEGIN
    TRUNCATE TABLE #InputBuffer;
    
    SELECT TOP 1 @Spid = WaitingSpid, @Id = Id
    FROM #OpenTrans
    WHERE InputBuffer IS NULL;

    SET @Sql = 'DBCC INPUTBUFFER (' + CAST(@Spid AS varchar(10)) + ');';

    INSERT INTO #InputBuffer
    EXEC sp_executesql @sql;

    UPDATE b
    SET InputBuffer = COALESCE((SELECT TOP 1 EventInfo FROM #InputBuffer),'')
    FROM #OpenTrans b
    WHERE ID = @Id;
END;

--Convert Hex job_ids for SQL Agent jobs to names.
WHILE EXISTS(SELECT 1 FROM #OpenTrans WHERE ProgramName LIKE 'SQLAgent - TSQL JobStep (Job 0x%')
BEGIN
    SELECT @JobIdHex = '', @JobName = '';

    SELECT TOP 1 @ID = ID, 
            @JobIdHex =  SUBSTRING(ProgramName,30,34)
    FROM #OpenTrans
    WHERE ProgramName LIKE 'SQLAgent - TSQL JobStep (Job 0x%';

    SELECT @Sql = N'SELECT @JobName = name FROM msdb.dbo.sysjobs WHERE job_id = ' + @JobIdHex;
    EXEC sp_executesql @Sql, N'@JobName nvarchar(256) OUT', @JobName = @JobName OUT;

    UPDATE b
    SET ProgramName = LEFT(REPLACE(ProgramName,@JobIdHex,@JobName),128)
    FROM #OpenTrans b
    WHERE ID = @Id;
END;

-- Populate SessionInfo column with HTML details for sending email
-- Since there's a bunch of logic here, code is more readable doing this separate than mashing it in with the rest of HTML email creation
UPDATE t
SET SessionInfo = '<span class="alert" style="font-weight:bold">' +
                  CASE TransactionState
                                WHEN 1 THEN 'The transaction has not been initialized.'
                                WHEN 3 THEN 'The transaction has been initialized but has not generated any log records.' -- We don’t alert on this status
                                WHEN 4 THEN 'The transaction has generated log records.'
                                WHEN 5 THEN 'The transaction has been prepared.'
                                WHEN 10 THEN 'The transaction has been committed.'
                                WHEN 11 THEN 'The transaction has been rolled back.'
                                WHEN 12 THEN 'The transaction is being committed. In this state the log record is being generated, but it has not been materialized or persisted.'
                                ELSE CAST(TransactionState as varchar)
                          END + '</span><br>' +
                  '<span style="font-weight:bold">Login = </span>' + LoginName + '<br>' +
                  '<span style="font-weight:bold">Host Name = </span>' + HostName + '<br>' +
                  CASE WHEN DbName <> ''
                    THEN '<span style="font-weight:bold">DbName = </span>' + DbName + '<br>' 
                    ELSE ''
                  END +
                  CASE WHEN WaitResource <> ''
                    THEN '<span style="font-weight:bold">Wait Resource = </span>' + WaitResource + '<br>' 
                    ELSE ''
                  END +
                  '<span style="font-weight:bold">Last Request = </span>' + CONVERT(varchar(20),LastRequestStart,20) + '<br>' +
                  '<span style="font-weight:bold">Program Name = </span>' + ProgramName + '<br>'
FROM #OpenTrans t;


--output results in debug mode:
IF @Debug = 1
BEGIN
    IF NOT EXISTS (SELECT 1 FROM #OpenTrans)
        SELECT 'No Open Transactions longer than ' + CAST(@LockingThreshold AS varchar(10)) + ' minutes exist' AS OpenTransactions;
    ELSE
    BEGIN
        SELECT * FROM #OpenTrans;
    END;

END;



-- If no rows returned, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #OpenTrans)
    RETURN (0);

--Check EmailThreshold before continuing. RETURN within @EmailThreshold
-- should this check be at the top of the alert?
IF EXISTS (SELECT 1 FROM Monitor_OpenTransaction WHERE LogDateTime >= DATEADD(mi,-1*@EmailThreshold,GETDATE()))
    RETURN(0);

-- Logging & Email Only happens when @Debug = 0
IF @Debug = 0
BEGIN
    --Log sessions to a real table, too.
    INSERT INTO Monitor_OpenTransaction (SessionId, DbName, HostName, ProgramName, LoginName, LoginTime, LastRequestStart, 
                        LastRequestEnd, TransactionCnt, TransactionStart, TransactionState, 
                        Command, WaitTime, WaitResource, SqlText, SqlStatement)
    SELECT WaitingSpid, DbName, HostName, ProgramName, LoginName, LoginTime, LastRequestStart, 
                        LastRequestEnd, TransactionCnt, TransactionStart, TransactionState, 
                        Command, WaitTime, WaitResource, SqlText, SqlStatement
    FROM #OpenTrans;

    --Email about long-running transactions
    -- TBD: Possibly email users if they are the leading blocker as well (based on loginname)?

    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

    --Build the body of the email

    SET @EmailBody = @EmailBody + N'<h2>Transactions open longer than ' + CAST(@LockingThreshold AS nvarchar(10))+ N' minutes:</h2>' + CHAR(10) +
            N'<table><tr>' +
            N'<th>SPID</th>' +
            N'<th>Transaction Start</th>' +
            N'<th>Session Info</th>' +
            N'<th>SQL Statement</th>' +
            N'<th>Input Buffer</th>' + 
            N'</tr>' +
            CAST(( SELECT
                    td = CAST(WaitingSpid AS nvarchar(10)), '',
                    td = CONVERT(varchar(20),TransactionStart,20), '',   
                    td = SessionInfo ,'',
                    td = LEFT(SqlStatement,300), '',    --Truncate string, too long for email alert
                    td = LEFT(InputBuffer,300), ''      --Truncate string, too long for email alert
                    FROM #OpenTrans b
                    ORDER BY TransactionStart
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';

    SELECT @EmailBody = REPLACE(@EmailBody,'&lt;','<');
    SELECT @EmailBody = REPLACE(@EmailBody,'&gt;','>');
    
    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

    SET @EmailSubject = 'ALERT: Long-running transactions';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;

DROP TABLE #OpenTrans;
DROP TABLE #InputBuffer;

Go