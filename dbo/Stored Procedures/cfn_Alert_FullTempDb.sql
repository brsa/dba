﻿CREATE PROCEDURE [dbo].[cfn_Alert_FullTempDb] 
    @EmailRecipients varchar(max) = NULL,
    @AlertThreshold tinyint = 80,
    @Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141001
    This stored procedure reports which queries take too much space in tempDB  
    The intent is to have it in a job and then create an alert for full tempdb that will execute this sproc

PARAMETERS
* @EmailRecipients - delimited list of email addresses. Used for sending email alert.
* @AlertThreshold  - Threshold (in percent) to trigger email alert when TempDB gets this full
* @Debug           - Outputs result set, rather than sending email.
**************************************************************************************************
MODIFICATIONS:
    20150107 - In email, specify from address, and include standard server info in footer
*************************************************************************************************/
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

DECLARE @SQL nvarchar(max),
        @DbUsagePercent int,
        @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

--Check TempDB file usage
CREATE TABLE #TempDbFileSizeInfo 
  ( 
     LogicalFileName  sysname,
     FileType         NVARCHAR(10),
     FileSizeMB       INT, 
     SpaceUsedMB      INT, 
     FreeSpaceMB      INT
  ) ;

SET @SQL = N'USE [tempdb]
    SELECT mf.name,
           mf.type_desc AS FileType,
           CAST(f.size/128.0 AS decimal(20,2)) AS FileSize, 
	       CAST(FILEPROPERTY(mf.name, ''SpaceUsed'')/128.0 as decimal (20,2)) as ''SpaceUsed'', 
	       CAST(f.size/128.0 - CAST(FILEPROPERTY(mf.name, ''SpaceUsed'' ) AS int)/128.0 AS int) AS FreeSpaceMB
    FROM sys.master_files mf 
    JOIN sys.sysfiles f ON f.fileid = mf.file_id AND mf.database_id = db_id()';

INSERT INTO  #TempDbFileSizeInfo (LogicalFileName, FileType, FileSizeMB, SpaceUsedMB, FreeSpaceMB)
EXEC sp_executesql @sql;

--Get percent full
SELECT @DbUsagePercent = SUM(CAST(SpaceUsedMB AS decimal(10,2)))/SUM(FileSizeMB)*100
FROM #TempDbFileSizeInfo
WHERE FileType = 'ROWS';

--Get some detail to include in the email
CREATE TABLE #SessionsInTempDb (              
    ID int identity (1,1) PRIMARY KEY,
    SessionId smallint,
    DbName nvarchar(256),
    HostName varchar(100),
    LoginName varchar(100),
    AllocPageCountUser bigint,
    DeallocPageCountUser bigint,
    AllocPageCountInternal bigint,
    DeallocPageCountInternal bigint,
    --Net usage = all allocated pages minus all deallocated pages
    NetPageCount AS (AllocPageCountUser + AllocPageCountInternal) - (DeallocPageCountUser + DeallocPageCountInternal) );

--Get all TempDB usage
INSERT INTO #SessionsInTempDb (SessionID, DbName, HostName, LoginName, AllocPageCountUser, DeallocPageCountUser, AllocPageCountInternal, DeallocPageCountInternal)              
SELECT sess.session_id, 
       -- Need to use sysprocesses for now until we're fully on 2012/2014
	   COALESCE((SELECT TOP 1 db_name(dbid) FROM master.sys.sysprocesses sp WHERE sp.spid = sess.session_id),'') AS DbName,
	   --db_name(sess.database_id) AS DbName, 
       sess.host_name,  
       sess.login_name, 
       SUM(su.user_objects_alloc_page_count),
       SUM(su.user_objects_dealloc_page_count), 
       SUM(su.internal_objects_alloc_page_count),
       SUM(su.internal_objects_dealloc_page_count)
FROM sys.dm_db_session_space_usage su   
INNER JOIN sys.dm_exec_sessions sess ON su.session_id = sess.session_id
WHERE sess.session_id > 50  
-- Need to use sysprocesses for now until we're fully on 2012/2014
GROUP BY sess.session_id,sess.host_name  , sess.login_name
--GROUP BY db_name(sess.database_id),sess.session_id,sess.host_name  , sess.login_name
ORDER BY SUM(su.internal_objects_alloc_page_count) DESC;

--Filter to just the rows using space
--Maybe we just want to do TOP N?
SELECT SessionId, HostName, LoginName, NetPageCount, CAST(NetPageCount*8/1024.0 AS decimal(15,3)) AS TempDBUsageMB
INTO #Results
FROM #SessionsInTempDb
WHERE NetPageCount > 0;

IF @Debug = 1
BEGIN
    SELECT 'Percent Full', @DbUsagePercent;
    SELECT * FROM #TempDbFileSizeInfo;
    SELECT * FROM #Results;
END;

--If we're using less than the alert threshold, return, nothing to do.
-- If no rows returned, nothing to do... just return
IF ( @DbUsagePercent < @AlertThreshold )
    OR (NOT EXISTS (SELECT 1 FROM #Results) )
    RETURN (0);


IF @Debug = 0
BEGIN
    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

    --Build the body of the email based on the #Results
    SET @EmailBody = @EmailBody + N'<h2>TempDb on ' + @@SERVERNAME + N' is <span class="alert">' + CAST(@DbUsagePercent as varchar(6)) + '%</span> full.</h2>'
    SET @EmailBody = @EmailBody + N'<h2>Sessions using TempDb:</h2>' + CHAR(10) +
            N'<table><tr>' +
            N'<th>Session ID</th>' +
            N'<th>Host Name</th>' +
            N'<th>Login Name</th>' +
            N'<th>TempDB Pages Allocated</th>' +
            N'<th>TempDB Space Usage (MB)</th></tr>' +
            CAST(( SELECT
                    td = CAST(SessionID AS nvarchar(10)), '',
                    td = HostName, '',
                    td = LoginName, '',
                    td = CAST(NetPageCount AS nvarchar(15)), '',
                    td = CAST(TempDBUsageMB AS nvarchar(15)), ''
                    FROM #Results
                    ORDER BY NetPageCount DESC
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';

    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

    SET @EmailSubject = 'ALERT: TempDB is ' + CAST(@DbUsagePercent as varchar(6)) + '% full';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;


DROP TABLE #SessionsInTempDb;
DROP TABLE #Results;