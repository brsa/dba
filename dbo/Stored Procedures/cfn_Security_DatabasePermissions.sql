﻿/*****************************************************************************************
Name: cfn_Security_DatabasePermissions

Desc: Generates a script of all database permissions.
	This script works on the understanding that server logins are in sync.
	This will not sync APPLICATION ROLES between servers, I could not figure out how to
		extract the password to recreate the role on the secondary replicas.
	The generated script will fail when run on a replica that is missing objects or application roles
		from the source server. Those need to be manually created before re-running this script.

Parameters:
	@database_name - The database to query. Default value is 'master', which will query the master database.

Dependencies:

Auth: MDAK

Date: 03/31/15

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
05/05/15		MDAK					Renamed sproc	
*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Security_DatabasePermissions;
EXEC dbo.cfn_Security_DatabasePermissions @database_name = 'msdb';
*/

CREATE PROCEDURE [dbo].[cfn_Security_DatabasePermissions] @database_name NVARCHAR(128) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @crlf CHAR(2) = CHAR(13) + CHAR(10),
		@vchScript NVARCHAR(MAX) = NULL,
		@SQL NVARCHAR(4000) = NULL;
	DECLARE @users TABLE (
		UserName NVARCHAR(128) NOT NULL,
		LoginName NVARCHAR(128) NOT NULL,
		DefaultSchema NVARCHAR(128) NULL,
		SID VARBINARY(85) NOT NULL,
		Type CHAR(1) NOT NULL,
		CreateScript NVARCHAR(4000) NULL
		);
	DECLARE @roles TABLE (
		UserName NVARCHAR(128) NULL,
		RoleName NVARCHAR(128) NOT NULL,
		RoleType CHAR(1) NOT NULL,
		DefaultSchema NVARCHAR(128) NULL,
		OwnerName NVARCHAR(128) NULL,
		IsFixedRole BIT NOT NULL,
		CreateScript NVARCHAR(4000) NULL,
		AddScript NVARCHAR(4000) NULL
		);
	DECLARE @permissions TABLE (
		UserName NVARCHAR(128) NOT NULL,
		PermissionState NVARCHAR(60) NOT NULL,
		PermissionName NVARCHAR(128) NOT NULL,
		ClassDesc NVARCHAR(60) NOT NULL,
		SchemaName NVARCHAR(128) NULL,
		MajorName NVARCHAR(128) NULL,
		MinorName NVARCHAR(128) NULL,
		PrincipalType CHAR(1) NULL,
		GrantScript NVARCHAR(4000) NULL
		);

	-- If no database specified, default to master
	IF COALESCE(@database_name, '') = ''
	BEGIN
		SET @database_name = 'master';
	END;

	-- Abort procedure if an invalid database is specified
	IF NOT EXISTS (
		SELECT name
		FROM sys.databases
		WHERE DATABASEPROPERTYEX(name, 'status') = 'ONLINE'
			AND DATABASEPROPERTYEX(name, 'updateability') = 'READ_WRITE'
			AND name NOT IN ('model','tempdb','distribution')
			AND name = @database_name
		)
	BEGIN	
		PRINT 'Aborting procedure: Database ' + QUOTENAME(@database_name) + ' is not a valid online, read/write database.';

		RETURN -1;	
	END;

	-- Gather all database users
	SET @SQL = 'SELECT users.name AS [UserName],
					logins.name AS [LoginName],
					users.default_schema_name AS [DefaultSchema],
					logins.sid AS [SID],
					users.type AS [Type]
				FROM ' + @database_name + '.sys.database_principals users WITH (NOLOCK)
					JOIN sys.server_principals logins WITH (NOLOCK) ON users.sid = logins.sid
				WHERE users.type IN (''S'', ''U'', ''G'')
					AND users.name <> ''dbo''
					AND users.name NOT LIKE ''##%''
					AND users.name NOT LIKE ''NT AUTHORITY\%''
					AND users.name NOT LIKE ''NT SERVICE\%''
				';
		
	INSERT INTO @users (UserName, LoginName, DefaultSchema, SID, Type)
	EXEC sp_executesql @SQL;

	-- Gather all database roles and members
	SET @SQL = 'SELECT users.name AS [UserName],
					roles.name AS [RoleName],
					roles.type AS [RoleType],
					roles.default_schema_name AS [DefaultSchema],
					owners.name AS [OwnerName],
					roles.is_fixed_role AS [IsFixedRole]
				FROM ' + @database_name + '.sys.database_principals roles WITH (NOLOCK)
					LEFT JOIN ' + @database_name + '.sys.database_role_members members WITH (NOLOCK) ON roles.principal_id = members.role_principal_id
					LEFT JOIN ' + @database_name + '.sys.database_principals users WITH (NOLOCK) ON members.member_principal_id = users.principal_id
					LEFT JOIN ' + @database_name + '.sys.database_principals owners WITH (NOLOCK) ON roles.owning_principal_id = owners.principal_id
				WHERE roles.type IN (''A'', ''R'')
					AND COALESCE(users.name, '''') <> ''dbo''
					AND COALESCE(users.name, '''') NOT LIKE ''##%''
					AND COALESCE(users.name, '''') NOT LIKE ''NT AUTHORITY\%''
					AND COALESCE(users.name, '''') NOT LIKE ''NT SERVICE\%''
				';

	INSERT INTO @roles (UserName, RoleName, RoleType, DefaultSchema, OwnerName, IsFixedRole)
	EXEC sp_executesql @SQL;

	-- Gather user and role permissions
	SET @SQL = 'SELECT users.name,
					perm.state_desc AS [PermissionState],
					perm.permission_name AS [PermissionName],
					perm.class_desc AS [ClassDesc],
					COALESCE(sch.name, schx.name) AS [SchemaName],
					COALESCE(obj.name, princ.name, xmls.name) AS [MajorName],
					col.name AS [MinorName],
					princ.type AS [PrincipalType]
				FROM ' + @database_name + '.sys.database_principals users WITH (NOLOCK)
					JOIN ' + @database_name + '.sys.database_permissions perm WITH (NOLOCK) ON users.principal_id = perm.grantee_principal_id
					LEFT JOIN ' + @database_name + '.sys.database_principals princ WITH (NOLOCK) ON perm.major_id = princ.principal_id
						AND perm.class_desc = ''DATABASE_PRINCIPAL''
					LEFT JOIN ' + @database_name + '.sys.all_objects obj WITH (NOLOCK) ON perm.major_id = obj.object_id
						AND perm.class_desc = ''OBJECT_OR_COLUMN''
					LEFT JOIN ' + @database_name + '.sys.schemas sch WITH (NOLOCK) ON obj.schema_id = sch.schema_id
					LEFT JOIN ' + @database_name + '.sys.all_columns col WITH (NOLOCK) ON perm.major_id = col.object_id
						AND col.column_id = perm.minor_id
					LEFT JOIN ' + @database_name + '.sys.xml_schema_collections xmls WITH (NOLOCK) ON perm.major_id = xmls.xml_collection_id
					LEFT JOIN ' + @database_name + '.sys.schemas schx WITH (NOLOCK) ON xmls.schema_id = schx.schema_id
				WHERE users.name <> ''dbo''
					AND users.is_fixed_role = 0
					AND users.name NOT LIKE ''##%''
					AND users.name NOT LIKE ''NT AUTHORITY\%''
					AND users.name NOT LIKE ''NT SERVICE\%''
				';
	
	INSERT INTO @permissions (UserName, PermissionState, PermissionName, ClassDesc, SchemaName, MajorName, MinorName, PrincipalType)
	EXEC sp_executesql @SQL;


	/********************************************
			GENERATE USER SCRIPTS
	********************************************/
	-- Create users
	UPDATE @users
	SET CreateScript =
		'IF NOT EXISTS (SELECT name FROM sys.database_principals WHERE name = ''' + u.UserName + ''')'
		+ @crlf + 'BEGIN'
		+ @crlf + '    CREATE USER ' + QUOTENAME(u.UserName) + ' FROM LOGIN ' + QUOTENAME(u.LoginName) + ';'
	FROM @users u;

	-- Create non-dbo default schemas
	UPDATE @users
	SET CreateScript = CreateScript
		+ @crlf + '    IF NOT EXISTS (SELECT name FROM sys.schemas WHERE name = ''' + u.DefaultSchema + ''')'
		+ @crlf + '    BEGIN'
		+ @crlf + '        EXEC (''CREATE SCHEMA ' + QUOTENAME(u.DefaultSchema) + ' AUTHORIZATION ' + QUOTENAME(u.UserName) + ''');'
		+ @crlf + '        ALTER USER ' + QUOTENAME(u.UserName) + ' WITH DEFAULT_SCHEMA = ' + QUOTENAME(u.DefaultSchema) + ';'
		+ @crlf + '    END;'
	FROM @users u
	WHERE u.Type IN ('S','U')
		AND u.DefaultSchema <> 'dbo';

	UPDATE @users
	SET CreateScript = CreateScript
		+ @crlf + 'END;'
	FROM @users u;


	/********************************************
			GENERATE ROLE SCRIPTS
	********************************************/
	-- Create database roles (this ignores application roles)
	UPDATE @roles
	SET CreateScript =
		'IF NOT EXISTS (SELECT name FROM sys.database_principals WHERE name = ''' + r.RoleName + ''')'
		+ @crlf + 'BEGIN'
		+ @crlf + '    CREATE ROLE ' + QUOTENAME(r.RoleName) + ' AUTHORIZATION ' + QUOTENAME(COALESCE(r.OwnerName, 'dbo')) + ';'
		+ @crlf + 'END;'
	FROM @roles r
	WHERE r.IsFixedRole = 0
		AND r.RoleType = 'R';

	-- Add members to database roles
	UPDATE @roles
	SET AddScript =		
		'ALTER ROLE ' + QUOTENAME(r.RoleName) + ' ADD MEMBER ' + QUOTENAME(r.UserName) + ';'
	FROM @roles r
	WHERE r.UserName IS NOT NULL;


	/********************************************
			GENERATE PERMISSION SCRIPTS
	********************************************/
	-- Grant permissions to users and roles
	UPDATE @permissions
	SET GrantScript =
		PermissionState + ' ' + PermissionName +
		CASE
			WHEN ClassDesc = 'DATABASE' THEN ''
			WHEN ClassDesc = 'OBJECT_OR_COLUMN' THEN ' ON OBJECT::' + QUOTENAME(COALESCE(SchemaName, 'dbo')) + '.' + QUOTENAME(MajorName) +
				CASE 
					WHEN MinorName IS NOT NULL THEN ' (' + QUOTENAME(MinorName) + ')'
					ELSE ''
				END
			WHEN ClassDesc = 'SCHEMA' THEN ' ON SCHEMA::' + QUOTENAME(MajorName)
			WHEN ClassDesc = 'DATABASE_PRINCIPAL' THEN ' ON ' +
				CASE 
					WHEN PrincipalType IN ('S','U','G') THEN 'USER::' + QUOTENAME(MajorName)
					WHEN PrincipalType = 'A' THEN 'APPLICATION ROLE::' + QUOTENAME(MajorName)
					WHEN PrincipalType = 'R' THEN 'ROLE::' + QUOTENAME(MajorName)
				END
			WHEN ClassDesc = 'ASSEMBLY' THEN ' ON ASSEMBLY::' + QUOTENAME(MajorName)
			WHEN ClassDesc = 'TYPE' THEN ' ON TYPE::' + QUOTENAME(MajorName)
			WHEN ClassDesc = 'XML_SCHEMA_COLLECTION' THEN ' ON XML SCHEMA COLLECTION::' + QUOTENAME(COALESCE(SchemaName, 'dbo')) + '.' + QUOTENAME(MajorName)
		END
		+ ' TO ' + QUOTENAME(UserName) + ';'
	FROM @permissions;


	/*********************************************************
					GENERATE OUTPUT SCRIPT
	*********************************************************/
	-- Filter rows that didn't create a valid script
	SET @vchScript = 'USE ' + QUOTENAME(@database_name) + ';';

	SELECT @vchScript = @vchScript + @crlf + CreateScript
	FROM @users;

	WITH CreateRoles AS (
		SELECT DISTINCT CreateScript
		FROM @roles
		WHERE CreateScript IS NOT NULL
		)
	SELECT @vchScript = @vchScript + @crlf + CreateScript
	FROM CreateRoles;

	WITH AddRoleMembers AS (
		SELECT AddScript
		FROM @roles
		WHERE AddScript IS NOT NULL
		)
	SELECT @vchScript = @vchScript + @crlf + AddScript
	FROM AddRoleMembers;

	WITH GrantPerms AS (
		SELECT GrantScript
		FROM @permissions
		WHERE GrantScript IS NOT NULL
		)
	SELECT @vchScript = @vchScript + @crlf + GrantScript
	FROM GrantPerms;

	-- Output the final script
	SELECT @vchScript;

	RETURN 0;
END;