﻿/*************************************************
AUTHOR : AJAY H.
CREATE DATE : 11/30/2017
DESCRIPTION: Stored procedure is written to scramble PII columns in NON PROD environment.

EXEC dbo.cfn_ScrambleDatabaseInDEVandQA @source_DBName = NULL,           -- varchar(100)
                                        @target_DBName = NULL ,           -- varchar(100)
										@TableNameToScramble =NULL,		-- VARCHAR(200)
										@ColumnNameToScramble = NULL ,				-- VARCHAR(200) 
                                        @MetaServer = 'DC01DB-P015SQL' ,				-- varchar(100)
                                        @Debug = 1									-- bit
EXECUTE AS LOGIN='SA'
EXEC dbo.cfn_ScrambleDatabaseInDEVandQA @source_DBName = 'CFNAccountDB' ,           -- varchar(100)
                                        @target_DBName = 'CFNAccountDB' ,           -- varchar(100)
										@TableNameToScramble ='CFNAccount' ,		-- VARCHAR(200)
										@ColumnNameToScramble = NULL ,				-- VARCHAR(200) 
                                        @MetaServer = 'DC01DB-P015SQL' ,				-- varchar(100)
                                        @Debug = 1									-- bit


**HISTORY**
-----------------------------------------------------------------------------------
Update Date    Updated By          Notes
-----------------------------------------------------------------------------------		
****************************************************/
--DO NOT RUN IT IF YOU ARE SURE ABOUT WHAT YOU ARE DOING.
CREATE PROC [dbo].[cfn_ScrambleDatabaseInDEVandQA]
@source_DBName VARCHAR(100)=NULL
,@target_DBName VARCHAR(100)=NULL
,@TableNameToScramble VARCHAR(200)=NULL
,@ColumnNameToScramble VARCHAR(200)=NULL
,@MetaServer VARCHAR(100)='DC01DB-P015SQL'
,@Debug BIT=1
AS
SET NOCOUNT ON;

IF @MetaServer <>'DC01DB-P015SQL'
	SET @MetaServer='DC01DB-P015SQL';
/*********************
declare variables
************************/
DECLARE @sqlVersion INT;
DECLARE @Algorithm VARCHAR(10);
DECLARE @queryTxt NVARCHAR(MAX);
DECLARE @SaltValue NVARCHAR(20);
/*******************
decalre table variables
********************/
DECLARE @tempTable TABLE (Salt VARCHAR(20));
DECLARE @tempDatabaseScrambleStatus TABLE (DbScrambleStatus SQL_VARIANT);
DECLARE @ColumnsToScrambleInTestEnvironment TABLE (ServerName VARCHAR(128) NOT NULL,DatabaseName VARCHAR(128) NOT NULL
	,TableName VARCHAR(128) NOT NULL,SchemaName VARCHAR(128) NOT NULL,ColumnName VARCHAR(128) NOT NULL,ClassificationID TINYINT NOT NULL
	,System_Data_Type VARCHAR(50) NOT NULL,Max_Length INT,Is_Nullable BIT,TruncateInTestEnvironment BIT NOT NULL, Failed BIT NOT NULL);
/*********************
declare temp tables
************************/
IF OBJECT_ID('tempdb.dbo.#ColumnToHash') IS NOT NULL
	DROP TABLE #ColumnToHash;

CREATE TABLE #ColumnToHash (DatabaseID INT NOT NULL,ColumnID INT NOT NULL ,ServerName VARCHAR(128) NOT NULL,DatabaseName VARCHAR(128) NOT NULL
	,TableName VARCHAR(128) NOT NULL,SchemaName VARCHAR(128) NOT NULL,ColumnName VARCHAR(128) NOT NULL,ClassificationID TINYINT NOT NULL
	,System_Data_Type VARCHAR(50) NOT NULL,Max_Length INT,Is_Nullable BIT,TruncateInTestEnvironment BIT NOT NULL,Failed BIT NOT NULL
	,PRIMARY KEY CLUSTERED (DatabaseID,ColumnID));
/***************************
set variable values
*****************************/
-- Get the SQL Server major version number	
SET @sqlVersion=(SELECT CONVERT(INT, LEFT(CONVERT(NVARCHAR(128),SERVERPROPERTY('ProductVersion')),CHARINDEX('.', CONVERT(NVARCHAR(128),SERVERPROPERTY('ProductVersion')))-1)));
SET @Algorithm=CASE WHEN @sqlVersion >= 11 THEN 'SHA2_512' ELSE 'SHA1' END;
/*************************************
Check environment. We DON'T want to run it production environment.
**************************************/
IF (SELECT dbo.cfn_ExtendedPropertyGet('SecurityEnvironment')) IN ('Development','QA')
BEGIN
	SELECT 'This is a production or Production like server. PII scramble process can only be run on Dev or QA servers';
	RETURN;
END;
/*************************************
Check extended propery of database to see if scrambled process already ran for the database or not. Scrambled=1 means database is already scramled.
**************************************/
SET @queryTxt='SELECT TOP 1 Value FROM ['+@target_DBName+'].sys.extended_properties AS ep WHERE class = 0 AND ep.NAME=''Scrambled'';';

INSERT INTO @tempDatabaseScrambleStatus ( DbScrambleStatus )
EXEC sys.sp_executesql @stmt=@queryTxt;

IF EXISTS(SELECT t.DbScrambleStatus FROM @tempDatabaseScrambleStatus t) AND @Debug=0
BEGIN
	PRINT 'Database "'+@target_DBName+'" is already scrambled. Nothing to do!';
	RETURN;
END;

/*
/***************************************
This is temporary to handle Clarify db. Once Clarify db migrate to run higher compatibility, we can remove this code.
temporary changing compatibility_level to 100 from 80 for clarify database. Using dynamic sql to supress error if it's running from higher sql server version.
***************************************/
SET @queryTxt='IF EXISTS (SELECT * FROM sys.databases d WHERE  d.name = ''Clarify'' AND d.compatibility_level=80 ) 
    BEGIN
        ALTER DATABASE [Clarify] SET COMPATIBILITY_LEVEL = 100;
    END;';

IF @sqlVersion < 11
	EXEC sys.sp_executesql @stmt=@queryTxt;
*/
/********************************
Get salt from remote server. We need to protect the salt from everyone except DBAs.
***************************************/
SELECT @queryTxt= 'SELECT Salt FROM OPENROWSET(''SQLNCLI'', ''Server='+@MetaServer+';Trusted_Connection=yes;'',''SELECT s.Salt FROM SchemaManager.dbo.HashSaltToUseForScrambleProcess s'')';

INSERT INTO @tempTable ( Salt )
EXEC sys.sp_executesql @queryTxt;

SELECT @SaltValue=tt.Salt FROM @tempTable tt;

/*****************************************************************************************************
Get List of table we need scramble.
*******************************************************************************************************/
DECLARE @CurrentServer VARCHAR(100)=@@SERVERNAME;
SELECT @queryTxt='SELECT v.ServerName,v.DatabaseName,v.SchemaName,v.Table_Name,v.ColumnName,v.ClassificationID,v.DataType,v.DataTypeLength,v.Is_Nullable,v.TruncateInTestEnvironment,v.Failed
FROM OPENROWSET(''SQLNCLI'', ''Server='+@MetaServer+';Trusted_Connection=yes;'',''
SELECT v.ServerName,v.DatabaseName,v.SchemaName,v.Table_Name,v.ColumnName,ClassificationID,DataType,DataTypeLength,Is_Nullable,TruncateInTestEnvironment,ISNULL(m.Failed,0) AS Failed
FROM DatabaseManager.dbo.vColumnsToScrambleInTestEnvironment v
LEFT JOIN DatabaseManager.dbo.Monitor_ColumnScrambleLog m ON m.DatabaseName = v.DatabaseName
	AND m.SchemaName = v.SchemaName
	AND m.TableName=v.Table_Name
	AND m.ColumnName=v.ColumnName
	AND m.ServerName='''''+@CurrentServer+''''' 
	AND m.Message IS NOT NULL ' +CASE WHEN @source_DBName IS NULL OR @source_DBName='' THEN '' ELSE ' WHERE v.DatabaseName='''''+@source_DBName+'''''' END+';'') v';

INSERT INTO @ColumnsToScrambleInTestEnvironment
( ServerName ,DatabaseName ,SchemaName ,TableName ,ColumnName,ClassificationID ,System_Data_Type ,Max_Length ,Is_Nullable, TruncateInTestEnvironment,Failed)
EXEC sys.sp_executesql @stmt=@queryTxt;

INSERT INTO #ColumnToHash ( DatabaseID ,ColumnID ,ServerName ,DatabaseName ,SchemaName ,TableName ,ColumnName,ClassificationID ,System_Data_Type ,Max_Length ,Is_Nullable, TruncateInTestEnvironment,Failed)
SELECT   DENSE_RANK()OVER (ORDER BY cth.DatabaseName) AS DatabaseID
		,ROW_NUMBER()OVER (PARTITION BY cth.DatabaseName ORDER BY cth.TableName,cth.ColumnName) AS ColumnID
		,cth.ServerName ,cth.DatabaseName ,cth.SchemaName ,cth.TableName ,cth.ColumnName,cth.ClassificationID 
		,cth.System_Data_Type ,cth.Max_Length ,Is_Nullable, TruncateInTestEnvironment,cth.Failed
FROM @ColumnsToScrambleInTestEnvironment cth
JOIN sys.databases d ON cth.DatabaseName=d.name
WHERE dbo.dm_hadr_db_role(d.name) IN ('PRIMARY','ONLINE')
	AND cth.DatabaseName=COALESCE(@source_DBName,cth.DatabaseName)
	AND cth.TableName=COALESCE(@TableNameToScramble,cth.TableName)
	AND cth.ColumnName=COALESCE(@ColumnNameToScramble,cth.ColumnName)
ORDER BY cth.DatabaseName,cth.TableName,cth.ColumnName;
/*********************************
Update target database name. This is if we are restoring database temporary as different name, we can scramble it before making it available for users. 
*************************************/
IF @target_DBName IS NOT NULL AND @target_DBName<>''
BEGIN
	UPDATE cth
		SET cth.DatabaseName=@target_DBName
	FROM #ColumnToHash cth
	WHERE cth.DatabaseName=@source_DBName;
END;
/**********************
scrambling start here....
***********************/
DECLARE @UpdateColumnScript NVARCHAR(MAX)
,@RemoveColumnDependecies NVARCHAR(MAX)
,@TotalDatabase INT =(SELECT MAX(C.DatabaseID) FROM #ColumnToHash C)
,@DatabaseID INT=1
,@TotalColumn INT
,@ColumnID INT=1
,@SchemaName VARCHAR(128)
,@TableName VARCHAR(128)
,@ColumnName VARCHAR(128)
,@ClassificationID VARCHAR(20)
,@DataType VARCHAR(50)
,@DataTypeLength VARCHAR(50)
,@TruncateInTestEnvironment BIT
,@IsScrambleScriptFailed BIT =0
,@Failed BIT;

WHILE @DatabaseID <= @TotalDatabase
BEGIN

SET @TotalColumn=(SELECT MAX(C.ColumnID) FROM #ColumnToHash C WHERE C.DatabaseID=@DatabaseID);
SET @ColumnID=1;

WHILE @ColumnID <= @TotalColumn
BEGIN

SELECT @target_DBName=C.DatabaseName
	,@TableName=C.TableName
	,@SchemaName=C.SchemaName
	,@ColumnName=C.ColumnName
	,@ClassificationID=C.ClassificationID
	,@DataType=C.System_Data_Type
	,@DataTypeLength=C.Max_Length
	,@TruncateInTestEnvironment=C.TruncateInTestEnvironment
	,@Failed=C.Failed
FROM #ColumnToHash C
WHERE C.DatabaseID=@DatabaseID 
	AND C.ColumnID=@ColumnID;

IF @TruncateInTestEnvironment=1
BEGIN
SET @UpdateColumnScript='/***Start('+@target_DBName+'.'+@SchemaName+'.'+@TableName+')***/
	USE ['+@target_DBName+'];
	IF NOT EXISTS (SELECT top 1 * FROM sys.extended_properties AS ep WHERE class = 0 AND ep.NAME=''Scrambled'' ) OR 1='+CAST(@Failed AS CHAR(1))+'
	BEGIN
		/*delete record from log table*/DELETE dsl FROM DBA.dbo.DataScrambleLog dsl WHERE dsl.DatabaseName='''+@target_DBName+''' AND dsl.SchemaName='''+@SchemaName+''' AND dsl.TableName='''+@TableName+''' AND dsl.ColumnName='''+@ColumnName+''';
		IF EXISTS(SELECT * FROM sys.triggers T WHERE  T.name=''cfn_TFS_Trigger'' AND t.parent_class = 0 AND t.is_disabled = 0)
				DISABLE TRIGGER cfn_TFS_Trigger ON DATABASE; 
		TRUNCATE TABLE ['+@target_DBName+'].['+@SchemaName+'].['+@TableName+'];
		/*script succeed so log the record*/INSERT INTO DBA.dbo.DataScrambleLog (Failed,DatabaseName, SchemaName,TableName,ColumnName) VALUES ('+CAST(@IsScrambleScriptFailed AS CHAR(1))+','''+@target_DBName+''','''+@SchemaName+''','''+@TableName+''','''+@ColumnName+''');
	END
	IF EXISTS(SELECT * FROM sys.triggers T WHERE  T.name=''cfn_TFS_Trigger''  AND t.parent_class = 0 AND t.is_disabled = 1)
		ENABLE TRIGGER cfn_TFS_Trigger ON DATABASE; 
	/***End***/';	
END;
ELSE
BEGIN
SET @UpdateColumnScript='/***Start('+@target_DBName+'.'+@SchemaName+'.'+@TableName+')***/ 
	USE ['+@target_DBName+'];
	IF NOT EXISTS (SELECT top 1 * FROM sys.extended_properties AS ep WHERE class = 0 AND ep.NAME=''Scrambled'') OR 1='+CAST(@Failed AS CHAR(1))+'
	BEGIN	
		/*delete log record*/DELETE dsl FROM DBA.dbo.DataScrambleLog dsl WHERE dsl.DatabaseName='''+@target_DBName+''' AND dsl.SchemaName='''+@SchemaName+''' AND dsl.TableName='''+@TableName+''' AND dsl.ColumnName='''+@ColumnName+''';
		IF EXISTS(SELECT * FROM sys.triggers T WHERE  T.name=''cfn_TFS_Trigger'' AND t.parent_class = 0 AND t.is_disabled = 0)
				DISABLE TRIGGER cfn_TFS_Trigger ON DATABASE; 
		IF EXISTS(SELECT * FROM sys.columns WHERE Name= N''Hash_'+@ColumnName+'''  AND Object_ID = Object_ID(N'''+@TableName+'''))
				ALTER TABLE ['+@SchemaName+'].['+@TableName+'] DROP COLUMN [Hash_'+@ColumnName+'];
		/*add new column*/ ALTER TABLE ['+@SchemaName+'].['+@TableName+'] ADD [Hash_'+@ColumnName+'] '+ CASE WHEN @DataType IN ( 'datetime','date','smalldatetime' ) THEN @DataType ELSE @DataType +'('+@DataTypeLength+')' END + ';
		/*update new column in batch*/DECLARE @BatchUpdateQuery NVARCHAR(4000);SET @BatchUpdateQuery=''DECLARE @affected INT=1;
			WHILE @affected > 0
			BEGIN 
				UPDATE TOP (250000) RC SET [Hash_'+@ColumnName+']=FH.HashValue FROM ['+@target_DBName+'].['+@SchemaName+'].['+@TableName+'] RC 
				CROSS APPLY DBA.dbo.fn_Hash('''''+@SaltValue+''''',RTRIM(LTRIM(RC.['+@ColumnName+'])),'''''+@ClassificationID+''''','''''+@Algorithm+''''') FH WHERE [Hash_'+@ColumnName+'] IS NULL AND RC.['+@ColumnName+'] IS NOT NULL;
				SELECT @affected=@@ROWCOUNT;
			END''; 
			EXEC sys.sp_executesql @BatchUpdateQuery;
	END;'
	
SET @RemoveColumnDependecies='IF EXISTS(SELECT top 1 * FROM sys.columns WHERE Name= N'''+@ColumnName+'''  AND Object_ID = Object_ID(N'''+@TableName+''')) AND EXISTS(SELECT top 1 * FROM sys.columns WHERE Name= N''Hash_'+@ColumnName+'''  AND Object_ID = Object_ID(N'''+@TableName+'''))
	BEGIN BEGIN TRY BEGIN TRAN ColumnSwap;	DECLARE @DropDependancies NVARCHAR(MAX)='''';
		/*drop index*/ SELECT @DropDependancies=COALESCE(''DROP INDEX '' +i.name +'' ON ''+'''+@SchemaName+'.'+@TableName+';''+@DropDependancies,'';'') FROM  sys.indexes AS i JOIN sys.index_columns AS ic ON i.object_id = ic.object_id AND i.index_id = ic.index_id  
		WHERE ic.object_id = OBJECT_ID('''+@TableName+''') AND COL_NAME(ic.object_id,ic.column_id)='''+@ColumnName+''';
		EXEC sys.sp_executesql @stmt=@DropDependancies;
		/*drop statistics*/ SELECT  @DropDependancies=''DROP STATISTICS ''+'''+@SchemaName+'.'+@TableName+'.['+@ColumnName+']'' 	FROM sys.stats AS s 
		JOIN sys.stats_columns AS sc ON s.object_id = sc.object_id  AND s.stats_id = sc.stats_id JOIN sys.columns AS c ON sc.object_id = c.object_id  AND c.column_id = sc.column_id
		WHERE   s.object_id = OBJECT_ID('''+@TableName+''') AND c.name = '''+@ColumnName+''' AND s.user_created = 1;  
		EXEC sys.sp_executesql @stmt=@DropDependancies;
		IF EXISTS(SELECT * FROM sys.triggers T WHERE  T.name=''cfn_TFS_Trigger'' AND t.parent_class = 0 AND t.is_disabled = 0)
				DISABLE TRIGGER cfn_TFS_Trigger ON DATABASE;
		/*drop old column*/ ALTER TABLE ['+@SchemaName+'].['+@TableName+'] DROP COLUMN ['+@ColumnName+'];
		/*rename new column*/ EXEC sys.sp_rename @objname = N'''+@TableName+'.Hash_'+@ColumnName+''',@newname = '''+@ColumnName+''',@objtype = ''COLUMN'';
		/*script succeed so log the record*/INSERT INTO DBA.dbo.DataScrambleLog (Failed,DatabaseName, SchemaName,TableName,ColumnName) VALUES ('+CAST(@IsScrambleScriptFailed AS CHAR(1))+','''+@target_DBName+''','''+@SchemaName+''','''+@TableName+''','''+@ColumnName+''');
	   COMMIT TRAN ColumnSwap; END TRY BEGIN CATCH IF @@TRANCOUNT > 0 ROLLBACK TRAN ColumnSwap; 		
			DECLARE @ErrorMessage VARCHAR(500) = ERROR_MESSAGE(),@ErrorSeverity VARCHAR(500) = ERROR_SEVERITY(),@ErrorState VARCHAR(500) = ERROR_STATE();
			RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState); END CATCH;
	END; 
	IF EXISTS(SELECT * FROM sys.triggers T WHERE  T.name=''cfn_TFS_Trigger'' AND t.parent_class = 0 AND t.is_disabled = 1)
		ENABLE TRIGGER cfn_TFS_Trigger ON DATABASE; 
	/***End***/';

SET @UpdateColumnScript=@UpdateColumnScript+CHAR(13)+@RemoveColumnDependecies;
END;
BEGIN TRY
		IF NOT EXISTS(SELECT t.DbScrambleStatus FROM @tempDatabaseScrambleStatus t) AND @Debug=0 
			EXEC sys.sp_executesql @stmt=@UpdateColumnScript;
		ELSE
			PRINT @UpdateColumnScript;
END TRY
BEGIN CATCH
	IF ISNULL(@target_DBName,'') <> ''
	BEGIN
		DELETE dsl FROM dbo.DataScrambleLog dsl WHERE dsl.DatabaseName=@target_DBName AND dsl.SchemaName=@SchemaName AND dsl.TableName=@TableName AND dsl.ColumnName=@ColumnName;
		SET @IsScrambleScriptFailed=1; /*At this point, script failed.*/
		INSERT INTO dbo.DataScrambleLog
			( Failed ,DatabaseName,SchemaName,TableName,ColumnName ,DynamicSqlText ,ErrorProcedure ,Line ,Message ,HostName)
		VALUES(@IsScrambleScriptFailed,@target_DBName,@SchemaName,@TableName,@ColumnName,@UpdateColumnScript ,ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE(),HOST_NAME());
	END;
END CATCH;

SET @ColumnID=@ColumnID+1;
END;
/********************
add extended properties for the database aleady scrambled.
**************************************/ 
	IF NOT EXISTS(SELECT t.DbScrambleStatus FROM @tempDatabaseScrambleStatus t) AND @Debug=0 
	BEGIN
		SET @queryTxt='IF NOT EXISTS (SELECT TOP 1 Value FROM ['+@target_DBName+'].sys.extended_properties AS ep WHERE class = 0 AND ep.NAME=''Scrambled'')
		BEGIN
			EXEC ['+@target_DBName+'].sys.sp_addextendedproperty @name=N''Scrambled'', @value=N''1'';
		END';
		EXEC sys.sp_executesql @stmt=@queryTxt;
	END;

	SET @DatabaseID=@DatabaseID+1;
END;

/***************************************
This is temporary to handle Clarify db. Once Clarify db migrate to run higher compatibility, we can remove this code.
switch back to old compatibility for clarify database. Using dynamic sql to supress error if it's running from higher sql server version.
***************************************/
SET @queryTxt='IF EXISTS (SELECT * FROM sys.databases d WHERE  d.name = ''Clarify'' AND d.compatibility_level=100 ) 
    BEGIN
        ALTER DATABASE [Clarify] SET COMPATIBILITY_LEVEL = 80;
    END;';

IF @sqlVersion < 11
	EXEC sys.sp_executesql @stmt=@queryTxt;