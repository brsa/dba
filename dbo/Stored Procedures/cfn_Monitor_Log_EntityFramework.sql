﻿CREATE PROC [dbo].[cfn_Monitor_Log_EntityFramework]
AS
SET NOCOUNT ON;

DECLARE @LogDateTime DATETIME2(0)=SYSDATETIME();

/*******************************
Get file location where xevent is being store
***************************************/
DECLARE @XEventTargetData XML=(SELECT t.target_data FROM sys.dm_xe_session_targets  AS t
JOIN  sys.dm_xe_sessions AS s ON s.address = t.event_session_address
WHERE s.name = 'Perf_EntityFramework');

DECLARE @FilePath NVARCHAR(500)=(SELECT @XEventTargetData.value('(/EventFileTarget/File/@name)[1]', 'NVARCHAR(500)'));

SET @FilePath=(SELECT SUBSTRING(@FilePath,1,CHARINDEX('_0_',@FilePath)));
/********************************
Get last log time
*********************************/
DECLARE @LastMaxEventTime DATETIME=(SELECT ISNULL(MAX(EventTimeUTC),'1/1/2018') FROM dbo.Monitor_Temp_EntityFramework);

/**************************
Get XEvent data from file
****************************/
IF OBJECT_ID('tempdb.dbo.#XEventEF') IS NOT NULL
	DROP TABLE dbo.#XEventEF;

CREATE TABLE dbo.#XEventEF (ID INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,event_data XML NOT NULL);

/****************************************************
Below query would file if file doesn't exist on the server. Cathc block would return and stop executing the sproc
*******************************************************/
BEGIN TRY 
	INSERT INTO dbo.#XEventEF ( event_data )
	SELECT event_data
	FROM sys.fn_xe_file_target_read_file(@FilePath+'*.xel',NULL, NULL, NULL);
END TRY
BEGIN CATCH
	RETURN;
END CATCH;

/*Read XML data and load into SQL table*/
INSERT INTO dbo.Monitor_Temp_EntityFramework
           (LogdateTime,EventName,EventTime,EventTimeUTC,cpu_time,duration,physical_reads,logical_reads,writes,result,object_name,statement
		   ,output_parameters ,username,sql_text,server_principal_name,server_instance_name,nt_username,database_name
		   ,client_hostname,client_app_name,task_time,attach_activity_id,row_count,last_row_count)
SELECT
	 @LogDateTime AS LogDateTime,
	 EventName = event_data.value(N'(event/@name)[1]','varchar(500)'),
	 EventTime = DATEADD(hh, DATEDIFF(hh, GETUTCDATE(), CURRENT_TIMESTAMP), event_data.value(N'(event/@timestamp)[1]', N'datetime')),
	 EventTimeUTC = event_data.value(N'(event/@timestamp)[1]', N'datetime'),
	 cpu_time   = event_data.value(N'(event/data[@name="cpu_time"]/value)[1]', N'int'),
	 duration   = event_data.value(N'(event/data[@name="duration"]/value)[1]', N'int'),
	 physical_reads   = event_data.value(N'(event/data[@name="physical_reads"]/value)[1]', N'int'),
	 logical_reads   = event_data.value(N'(event/data[@name="logical_reads"]/value)[1]', N'int'),
	 writes   = event_data.value(N'(event/data[@name="writes"]/value)[1]', N'int'),
	 result   = event_data.value(N'(event/data[@name="result"]/value)[1]', N'nvarchar(max)'),
	 object_name  = event_data.value(N'(event/data[@name="object_name"]/value)[1]', N'nvarchar(max)'),
	 statement  = event_data.value(N'(event/data[@name="statement"]/value)[1]', N'nvarchar(max)'),
	 output_parameters  = event_data.value(N'(event/action[@name="output_parameters"]/value)[1]', N'nvarchar(max)'),
	 user_name  = event_data.value(N'(event/action[@name="username"]/value)[1]', N'nvarchar(max)'),
	 sql_text  = event_data.value(N'(event/action[@name="sql_text"]/value)[1]', N'nvarchar(max)'),
	 server_principal_name  = event_data.value(N'(event/action[@name="server_principal_name"]/value)[1]', N'nvarchar(max)'),
	 server_instance_name  = event_data.value(N'(event/action[@name="server_instance_name"]/value)[1]', N'nvarchar(max)'),
	 nt_username  = event_data.value(N'(event/action[@name="nt_username"]/value)[1]', N'nvarchar(max)'),
	 database_name  = event_data.value(N'(event/action[@name="database_name"]/value)[1]', N'nvarchar(max)'),
	 client_hostname  = event_data.value(N'(event/action[@name="client_hostname"]/value)[1]', N'nvarchar(max)'),
	 client_app_name  = event_data.value(N'(event/action[@name="client_app_name"]/value)[1]', N'nvarchar(max)'),
	 task_time  = event_data.value(N'(event/action[@name="task_time"]/value)[1]', N'int'),
	 attach_activity_id=event_data.value(N'(event/action[@name="attach_activity_id"]/value)[1]', N'nvarchar(max)'),
	 row_count=event_data.value(N'(event/data[@name="row_count"]/value)[1]', N'int'),
	 last_row_count  = event_data.value(N'(event/data[@name="last_row_count"]/value)[1]', N'int')
FROM dbo.#XEventEF xee
WHERE event_data.value(N'(event/@timestamp)[1]', N'datetime') > @LastMaxEventTime;

--Aggressively clean out temp table of everything except the most recent
WHILE @@ROWCOUNT <> 0
	DELETE TOP (1000) dbo.Monitor_Temp_EntityFramework 
	WHERE EventTimeUTC < DATEADD(hh,-72,@LogDateTime);