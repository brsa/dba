﻿CREATE PROCEDURE dbo.cfn_Monitor_Log_ProcedureStats
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150716
    This procedure logs Procedure stats from the DMVs to cfn_Monitor_ProcedureStats.
	Calculates a delta since the last run, and saves that delta to the table.
    
PARAMETERS
* None
**************************************************************************************************
MODIFICATIONS:
    YYYYMMDDD - Initials - Description of changes
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME(),
	@LastLogDateTime datetime2(0),
	@IntervalMinutes int;

--When did we last check stats?
SELECT @LastLogDateTime = MAX(LogDateTime) FROM Monitor_ProcedureStats;

--If the instance rebooted since then, we should consider that restart time as our last time.
    -- Stats are only cumulative since start time, so @IntervalMinutues should reflect a shorter time.
	-- Also if we just restarted, we oughtn't compare against last collection to compute a delta.
SELECT @LastLogDateTime = CASE WHEN create_date > @LastLogDateTime  OR @LastLogDateTime IS NULL THEN create_date 
								ELSE @LastLogDateTime END 
FROM sys.databases WHERE name = 'tempdb';

--This could easily be in-line below. But I'm going to do it here for clarity
SELECT @IntervalMinutes = COALESCE(DATEDIFF(mi,@LastLogDateTime,@LogDateTime),0);

--We use a physical table for our temp table, because we need to compare between this run & last run
INSERT INTO Monitor_temp_ProcedureStats (LogDateTime, DbName, SchemaName, ObjectName, PlanId, SqlHandle, PlanHandle,
                    CachedTime, ExecutionCount, TotalWorkerTime, TotalLogicalWrites, TotalLogicalReads, TotalElapsedTime)
SELECT @LogDateTime AS LogDateTime,
    DB_NAME(ps.database_id) AS DbName,
    OBJECT_SCHEMA_NAME(ps.object_id, ps.database_id) AS SchemaName,
	OBJECT_NAME(ps.object_id, ps.database_id) AS ObjectName,
    ROW_NUMBER() OVER (PARTITION BY ps.database_id, ps.object_id ORDER BY ps.cached_time) AS PlanId,
    ps.sql_handle,
    ps.plan_handle,
    ps.cached_time,
    ps.execution_count,
    ps.total_worker_time,
    ps.total_logical_writes,
    ps.total_logical_reads,
    ps.total_elapsed_time
FROM sys.dm_exec_procedure_stats ps
--exclude stuff from Resource database. Getting calls from Master will be good 'nuff
WHERE ps.database_id <> 32767;


--Now compare this most recent collection with the previous collection, and compute the delta.
-- We might have multiple plans in cache for the same object
-- Aggregate those plan stats to a single row.
-- NewPlanHandle might indicate that we ADDED a plan, or that one CHANGED.
-- LastCachedTime will reflect the time of that addition or change.
WITH ProcedureStats_Aggregated
AS (
	SELECT LogDateTime, DBName, SchemaName, ObjectName, 
		 COUNT(PlanId) AS PlanCount,
         MAX(CachedTime) AS LastCachedTime,
         SUM(ExecutionCount) AS ExecutionCount,
		 SUM(TotalLogicalReads) AS TotalLogicalReads,
         SUM(TotalLogicalWrites) AS TotalLogicalWrites,
         SUM(TotalWorkerTime) AS TotalWorkerTime,
		 SUM(TotalElapsedTime) AS TotalElapsedTime
    FROM dbo.Monitor_temp_ProcedureStats
    GROUP BY LogDateTime, DBName, SchemaName, ObjectName
)
INSERT INTO Monitor_ProcedureStats (LogDateTime, DbName, SchemaName, ObjectName, IntervalMinutes, NewSqlHandle, NewPlanHandle, 
			PlanCount, LastCachedTime, ExecutionCount, TotalLogicalReads, TotalLogicalWrites, TotalWorkerTime, TotalElapsedTime)
SELECT new.LogDateTime, new.DbName, new.SchemaName, new.ObjectName, 
	@IntervalMinutes AS IntervalMinutes,
    (SELECT TOP 1 1 FROM Monitor_temp_ProcedureStats pn WHERE pn.LogDateTime = new.LogDateTime AND pn.DbName = new.DbName 
            AND pn.SchemaName = new.SchemaName AND pn.ObjectName = new.ObjectName
            AND NOT EXISTS (SELECT 1 FROM Monitor_temp_ProcedureStats po WHERE po.LogDateTime = old.LogDateTime AND po.DbName = old.DbName
                AND po.SchemaName = old.SchemaName AND po.ObjectName = old.ObjectName AND po.SqlHandle = pn.SqlHandle)) AS NewSqlHandle,
    (SELECT TOP 1 1 FROM Monitor_temp_ProcedureStats pn WHERE pn.LogDateTime = new.LogDateTime AND pn.DbName = new.DbName 
            AND pn.SchemaName = new.SchemaName AND pn.ObjectName = new.ObjectName
            AND NOT EXISTS (SELECT 1 FROM Monitor_temp_ProcedureStats po WHERE po.LogDateTime = old.LogDateTime AND po.DbName = old.DbName
                AND po.SchemaName = old.SchemaName AND po.ObjectName = old.ObjectName AND po.PlanHandle = pn.PlanHandle)) AS NewPlanHandle,
    new.PlanCount,
    new.LastCachedTime,
	new.ExecutionCount - COALESCE(old.ExecutionCount,0) AS ExecutionCount,
	new.TotalLogicalReads - COALESCE(old.TotalLogicalReads,0) AS TotalLogicalReads,
	new.TotalLogicalWrites - COALESCE(old.TotalLogicalWrites,0) AS TotalLogicalWrites,
	new.TotalWorkerTime - COALESCE(old.TotalWorkerTime,0) AS TotalWorkerTime,
	new.TotalElapsedTime - COALESCE(old.TotalElapsedTime,0) AS TotalElapsedTime
FROM ProcedureStats_Aggregated new
LEFT JOIN ProcedureStats_Aggregated old
	ON old.DbName = new.DbName AND old.SchemaName = new.SchemaName  
		AND old.ObjectName = new.ObjectName AND old.LogDateTime = @LastLogDateTime
WHERE new.LogDateTime = @LogDateTime
AND new.ExecutionCount - COALESCE(old.ExecutionCount,0) > 0;


--Aggressively clean out temp table of everything except the most recent
WHILE @@ROWCOUNT <> 0
	DELETE TOP (1000) Monitor_temp_ProcedureStats 
	WHERE LogDateTime < DATEADD(hh,-72,@LogDateTime);
