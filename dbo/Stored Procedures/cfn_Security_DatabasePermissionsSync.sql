﻿/*****************************************************************************************
Name: cfn_Security_DatabasePermissionsSync

Desc: Syncs database users and permissions from the standalone databases on the primary 
	Availability Group replica to all secondary replicas.

Parameters:
	@agName - The availability group name to sync. Default value is NULL, which will sync all AGs.
	@databaseName - The database to sync. Default value is NULL, which will sync all standalone databases.

Dependencies: 
	dbo.cfn_Security_DatabasePermissions

Auth: MDAK

Date: 03/31/15

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
05/05/15		MDAK					Renamed sproc	
*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Security_DatabasePermissionsSync @agName = 'CRMDEVDB', @databaseName = 'msdb';
EXEC dbo.cfn_Security_DatabasePermissionsSync @agName = NULL, @databaseName = NULL;
*/

CREATE PROCEDURE [dbo].[cfn_Security_DatabasePermissionsSync] @agName NVARCHAR(255) = NULL,
	@databaseName NVARCHAR(128) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @agPrimary NVARCHAR(255) = NULL,
		@orsServerString NVARCHAR(255) = NULL,
		@orsExecString NVARCHAR(255) = NULL,
		@vchQuery NVARCHAR(MAX) = NULL;
	DECLARE @tmpAGs TABLE (
		agName NVARCHAR(255) NULL,
		agPrimary NVARCHAR(255) NULL
		);
	DECLARE @tmpDatabases TABLE (
		databaseName NVARCHAR(128) NULL
		);
	DECLARE @tmpPermissions TABLE (
		vchQuery NVARCHAR(MAX) NULL
		);

	-- Gather all AGs and primary replicas
	INSERT INTO @tmpAGs (
		agName,
		agPrimary
		)
	SELECT ag.name,
		gs.primary_replica
	FROM sys.availability_groups ag
		JOIN sys.dm_hadr_availability_group_states gs ON ag.group_id = gs.group_id;

	-- If AG is specified, only gather permissions from that AG primary replica
	IF COALESCE(@agName, '') <> ''
	BEGIN
		DELETE @tmpAGs
		WHERE agName <> @agName;
	END;

	-- Loop through all AGs and apply permissions
	WHILE EXISTS (
		SELECT agName
		FROM @tmpAGs
		)
	BEGIN
		SELECT TOP 1 @agName = agName,
			@agPrimary = agPrimary
		FROM @tmpAGs;

		-- Ignore script if current server is the primary replica for the AG
		IF @agPrimary = @@SERVERNAME
		BEGIN
			PRINT 'Current server ' + QUOTENAME(@@SERVERNAME) + ' is the primary replica for ' + @agName + '.';

			DELETE @tmpAGs
			WHERE agName = @agName;

			CONTINUE;
		END;

		-- Gather list of all standalone databases on this server
		INSERT INTO @tmpDatabases (
			databaseName
			)
		SELECT name
		FROM sys.databases
		WHERE DATABASEPROPERTYEX(name, 'status') = 'ONLINE'
			AND DATABASEPROPERTYEX(name, 'updateability') = 'READ_WRITE'
			AND name NOT IN ('model','tempdb','distribution');

		-- If database is specified, only gather permissions from that database
		IF COALESCE(@databaseName, '') <> ''
		BEGIN
			DELETE @tmpDatabases
			WHERE databaseName <> @databaseName;
		END;				

		-- Loop through each database and query the primary AG replica
		WHILE EXISTS (
			SELECT databaseName
			FROM @tmpDatabases
			)
		BEGIN
			SELECT TOP 1 @databaseName = databaseName
			FROM @tmpDatabases;	

			SET @orsServerString = 'Server=' + @agPrimary + ';Trusted_Connection=yes;';

			SET @orsExecString = 'EXEC DBA.dbo.cfn_Security_DatabasePermissions @database_name = ''''' + @databaseName + '''''';
			
			SET @vchQuery = 'SELECT * FROM OPENROWSET(''SQLNCLI'', ''' + @orsServerString + ''', ''' + @orsExecString + ''')';

			INSERT INTO @tmpPermissions (vchQuery)
			EXEC sp_executesql @vchQuery;

			SELECT @vchQuery = vchQuery
			FROM @tmpPermissions;

			-- Run the query to sync database permissions
			EXEC sp_executesql @vchQuery;

			-- Clear out permissions script
			DELETE @tmpPermissions;

			-- Delete database from list and move to next one
			DELETE @tmpDatabases
			WHERE databaseName = @databaseName
		END;

		-- Delete AG from list and move to the next one
		DELETE @tmpAGs
		WHERE agName = @agName;
	END;
END;