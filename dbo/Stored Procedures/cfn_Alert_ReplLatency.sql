﻿CREATE PROCEDURE dbo.cfn_Alert_ReplLatency
	@AlertThreshold int = 5, 
	@ExcludeAgentList varchar(1000) = NULL,
	@IncludeAgentList varchar(1000) = NULL,
	@EmailRecipients varchar(100) = NULL,
    @Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20140409
       This Alert checks replication failures or latency (transaction age) and emails an alert. 
       Latency is based on replication logs & information supplied by get_distribution_status.
	   get_distribution_status is written by Brian Wilson & published on SQL Server Central

	   Monitor Log Reader agents to make sure they are not failed or in retry.

PARAMETERS
* @AlertThreshold - Default 5 - Allowed latency in minutes that will trigger this alert being sent.
* @ExcludeAgentList - Default NULL - A double-pipe (||) delimited blacklist to prevent checking 
						latency on agents with these names
* @IncludeAgentList - Default NULL - A double-pipe (||) delimited whitelist to check ONLY
                        latency on agents with these names
* @EmailRecipients - Default DBA Team - semicolon-separated list of email addresses to send alert
EXAMPLES:
* EMail DBA Team when Unsent log surpasses 5 min
	EXEC cfn_Alert_ReplLatency
**************************************************************************************************
MODIFICATIONS:
    20150929 - AM2 - Add @ExcludedAgentList a double-pipe delimited list. This way we can exclude 
	       subscriptions that only distribute transactions on a schedule (and run a different alert for that)
		   Also add @IncludeAgentList a double-pipe delimited list.
		   This allows us to whitelist ONLY specific agents.
*************************************************************************************************/
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

--If no distribution database, just return
IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name = 'distribution')
	RETURN 0;


DECLARE 
	@EmailFrom varchar(max),
	@EmailBody nvarchar(max),
	@EmailSubject varchar(100),
	@ProductVersion tinyint,
	@sql nvarchar(max);

SET @ProductVersion = LEFT(CAST(SERVERPROPERTY('PRODUCTVERSION') AS varchar(20)),CHARINDEX('.',CAST(SERVERPROPERTY('PRODUCTVERSION') AS varchar(20)))-1)



CREATE TABLE #DistributionStatus (
	Agent sysname,
	RunStatus varchar(16),
	Comments varchar(max),
	XactType varchar(256),
	XactArticle varchar(256),
	XactSeqNo varchar(max),
	XactAge  varchar(32),
	XactAgeMinutes  int,
	DeliveryRate varchar(16),
	DeliveryEst varchar(16)
	);

CREATE TABLE #RedirectedPublishers (
	original_publisher sysname,
	publisher_db sysname,
	redirected_publisher sysname
	);


-- Get distribution Status
INSERT INTO #DistributionStatus (Agent,RunStatus,Comments,XactType, XactArticle, XactSeqNo, XactAge, DeliveryRate, DeliveryEst)
EXEC dbo.get_distribution_status;

--Remove Excluded agents
IF @ExcludeAgentList IS NOT NULL
BEGIN
	DELETE FROM #DistributionStatus
	WHERE Agent IN (SELECT value FROM fn_split(@ExcludeAgentList,'||'));
END;

--Keep only Included Agents, if specified
IF @IncludeAgentList IS NOT NULL
BEGIN
	DELETE FROM #DistributionStatus
	WHERE Agent NOT IN (SELECT value FROM fn_split(@IncludeAgentList,'||'));
END;



IF @ProductVersion >= 11
BEGIN
	INSERT INTO #RedirectedPublishers (original_publisher, publisher_db, redirected_publisher)
	SELECT original_publisher, publisher_db, redirected_publisher FROM dbo.MSredirected_publishers;
END;

--Get some additional details from distribution system tables
	SELECT ds.Agent,  s.name AS Subscriber, 
			da.subscriber_db AS SubscriberDb,
			COALESCE(rp.redirected_publisher,p.name) AS Publisher,
			pub.publisher_db + ': ' + pub.publication AS Publication, 
			ds.RunStatus, ds.XactAge, 
			CAST(SUBSTRING(ds.XactAge,0,2) AS INT)*24*60 --days converted into minutes
				+ CAST(SUBSTRING(ds.XactAge,3,2) AS INT)*60    --hours converted into minutes
				+ CAST(SUBSTRING(ds.XactAge,6,2) AS INT) AS XactAgeMinutes,      --minutes
			ds.DeliveryRate, ds.Comments,
			'EXEC DBA.dbo.get_distribution_status @agent_id = ' + CAST(da.id AS varchar(6)) + ', @get_details =1;' AS SqlCommand
	INTO #DistributionResults
	FROM #DistributionStatus ds
	JOIN dbo.MSdistribution_agents da on da.name = ds.Agent
	JOIN dbo.MSpublications pub ON pub.publication = da.publication 
				AND pub.publisher_db = da.publisher_db 
				AND pub.publisher_id = da.publisher_id
	JOIN sys.servers p ON p.server_id = da.publisher_id
	JOIN sys.servers s ON s.server_id = da.subscriber_id
	LEFT JOIN #RedirectedPublishers rp ON p.name = rp.original_publisher AND da.publisher_db = rp.publisher_db;

--Get Log Reader status
WITH LastHistory AS
(
	SELECT agent_id, max(time) AS time
	FROM dbo.MSlogreader_history
	WHERE time >= DATEADD(hh,-1,getdate())
	GROUP BY agent_id
)
SELECT lrh.agent_id AS AgentID, a.name AS AgentName, a.publisher_db AS PublisherDb, lrh.time AS LastLogTime, 
		DATEDIFF(mi,lrh.time,Getdate()) AS MinSinceLastLog,
		lrh.runstatus AS RunStatusCode,
		CASE lrh.runstatus
			WHEN 1 THEN 'Start'
			WHEN 2 THEN 'Succeed'
			WHEN 3 THEN 'In Progress'
			WHEN 4 THEN 'Idle'
			WHEN 5 THEN 'Retry'
			WHEN 6 THEN 'Fail'
		END AS RunStatus, 
		delivery_rate AS DeliveryRate, delivery_latency AS DeliveryLatency, comments AS Comments
INTO #LogReaderResults
FROM dbo.MSlogreader_agents a
JOIN LastHistory lh ON lh.agent_id = a.id
JOIN dbo.MSlogreader_history lrh ON lrh.agent_id = lh.agent_id AND lrh.time = lh.time;

IF @Debug = 1
BEGIN
    SELECT 'Distribution Status' AS Results;
	SELECT * FROM #DistributionResults ORDER BY XactAgeMinutes DESC, Subscriber, Publisher, Publication;
	SELECT 'Log Reader Status' AS Results;
	SELECT * FROM #LogReaderResults ORDER BY COALESCE(MinSinceLastLog,-1) DESC, AgentName;
END;

--If no agents are failed, and there's low distribution latency, nothing more to do; return
IF NOT EXISTS (SELECT 1 FROM #DistributionResults WHERE XactAgeMinutes >= @AlertThreshold OR RunStatus IN ('Retry','Warning','Fail')
				UNION
				SELECT 1 FROM #LogReaderResults WHERE MinSinceLastLog >= @AlertThreshold OR RunStatus IN ('Retry','Warning','Fail') )
BEGIN
    RETURN (0);
END;

PRINT 'Replication is experiencing latency.';

--OK, now send the email
IF @Debug = 0
BEGIN
	-- Send Email WITH STYLE
	SELECT @EmailBody = dbo.cfn_EmailCSS();

	--Build the body of the email based on the #Results
	--Alert of an Unhealthy DB on a server
	IF EXISTS (SELECT 1 FROM #DistributionResults WHERE XactAgeMinutes >= @AlertThreshold OR RunStatus IN ('Retry','Warning','Fail'))
		SET @EmailBody = @EmailBody +
					 N'<H2>Distribution is behind:</H2>' +
					 N'<table> <tr>' +
					 N'<th> Subscriber </th>' +
					 N'<th> Publisher </th>' +
					 N'<th> Publication </th>' +
					 N'<th> Status </th>' +
					 N'<th> Transaction Age </th>' +
					 N'<th> For more info </th></tr>' +
					 CAST(( SELECT
							td = Subscriber + '.' + SubscriberDb, '',
							td = Publisher, '',
							td = Publication, '',
							td = RunStatus, '',
							td = CAST(XactAgeMinutes AS varchar(10)), '',
							td = SqlCommand, ''
					 FROM #DistributionResults
					 WHERE XactAgeMinutes >= @AlertThreshold 
							OR RunStatus IN ('Retry','Warning','Fail')
					 ORDER BY XactAgeMinutes DESC, Subscriber, Publisher, Publication
					 FOR XML PATH ('tr'), ELEMENTS
					 ) AS nvarchar(max)) +
					N'</table>';

	IF EXISTS (SELECT 1 FROM #LogReaderResults WHERE MinSinceLastLog >= @AlertThreshold OR RunStatus IN ('Retry','Warning','Fail'))
		SET @EmailBody = @EmailBody +
					 N'<H2>Log Reader Agent is Unhealthy:</H2>' +
					 N'<table> <tr>' +
					 N'<th> Agent Name </th>' +
					 N'<th> Database </th>' +
					 N'<th> Status </th>' +
					 N'<th> Last Log Time </th>' +
					 N'<th> Last Log Comments </th></tr>' +
					 CAST(( SELECT
							td = AgentName, '',
							td = PublisherDb, '',
							td = RunStatus, '',
							td = CONVERT(varchar(20),LastLogTime,120), '',
							td = Comments, ''
					 FROM #LogReaderResults
					 WHERE MinSinceLastLog >= @AlertThreshold 
							OR RunStatus IN ('Retry','Warning','Fail')
					 ORDER BY MinSinceLastLog DESC, AgentName
					 FOR XML PATH ('tr'), ELEMENTS
					 ) AS nvarchar(max)) +
					N'</table>';

    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

    SET @EmailSubject = 'ALERT: Replication is behind';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;

/*
DROP TABLE #DistributionStatus;
DROP TABLE #DistributionResults;
DROP TABLE #LogReaderResults;
*/
GO