﻿/*****************************************************************************
AUTHOR : AJAY H.
CREATE DATE : 09/16/2015
DESCRIOTION: alter TFS trigger in all databases on the instance.
SAMPLE CALL: 
EXEC dbo.cfn_Alter_Default_TFS_Trigger @Debug=0,@ExcludeList='ReportServer,ReportServerTempDB,Staging,Clarify,DBA,CFN_LOGS,Clarify_Archive,trading_marts';

**History**
-----------------------------------------------------------------------------------
UpdateDate    UpdatedBy          Notes
-----------------------------------------------------------------------------------		 
										

******************************************************************************/
CREATE PROCEDURE [dbo].[cfn_Alter_Default_TFS_Trigger] 
@ExcludeList NVARCHAR(MAX)=NULL,
@Debug BIT=0
AS
BEGIN
SET NOCOUNT ON;

EXEC dbo.sp_foreachdb @command = N'USE ?;DECLARE @queryStr NVARCHAR(4000);
IF EXISTS (SELECT name FROM sys.triggers WHERE parent_class = 0 AND type = ''TR'' AND name = ''cfn_TFS_Trigger'')
BEGIN
SELECT @queryStr =''ALTER TRIGGER [cfn_TFS_Trigger]
ON DATABASE
FOR ALTER_PROCEDURE, ALTER_FUNCTION, ALTER_TABLE, ALTER_VIEW,
CREATE_PROCEDURE, CREATE_FUNCTION, CREATE_TABLE, CREATE_VIEW, CREATE_SYNONYM,
DROP_PROCEDURE, DROP_FUNCTION, DROP_TABLE, DROP_VIEW, DROP_SYNONYM
AS
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET NOCOUNT ON
							
DECLARE  @eventType VARCHAR(50), @databasename VARCHAR(255), @objName VARCHAR(100);

SELECT @eventType=LOWER(REPLACE(REPLACE(REPLACE(REPLACE(EVENTDATA().value(''''(/EVENT_INSTANCE/EventType)[1]'''',''''nvarchar(50)''''),''''_'''','''' ''''),''''drop '''',''''''''),''''alter '''',''''''''),''''create '''',''''''''))
SELECT @objName = EVENTDATA().value(''''(/EVENT_INSTANCE/ObjectName)[1]'''',''''nvarchar(255)'''')
SELECT @databasename = EVENTDATA().value(''''(/EVENT_INSTANCE/DatabaseName)[1]'''',''''nvarchar(255)'''')
--PRINT @eventType
IF SYSTEM_USER <> ''''SA''''
BEGIN
--PRINT SYSTEM_USER
	IF (program_name() NOT IN ( ''''Microsoft SQL Server Data Tools, T-SQL Editor'''',''''Microsoft SQL Server Data Tools, Schema Compare'''') AND program_name() <> ''''DacFX Deploy'''')
		AND NOT EXISTS (SELECT TOP 1 * FROM DBA.dbo.ObjectExceptions WITH (NOLOCK) WHERE databasename = @databasename AND [ObjectType] = @eventType AND [ObjectName] = @objName)
		AND UPPER(RIGHT(@objName,6)) <> ''''_DEVDB''''
		AND (SELECT DBA.dbo.cfn_ExtendedPropertyGet(''''SecurityEnvironment''''))<>''''Development'''' /*Allow DDL operation in DEV environment*/
		BEGIN
			PRINT ''''CFN: Please use correct deployment tool to change '''' + @eventType + '''' ['''' + @objName + '''']!''''
		ROLLBACK
	END

	IF RIGHT(@objName,6) = ''''_DEVDB''''
	BEGIN
		PRINT ''''ALTER: ['''' + @objName + ''''] Successful''''
		PRINT @eventType + '''' ['''' + @objName + ''''] object is excused from source control but will be automatically dropped in 30 days.''''
	END
END;	''
EXEC sys.sp_executesql @stmt=@queryStr;		
END' ,                   -- nvarchar(max)
@print_dbname = 1 ,             -- bit
@print_command_only = @Debug ,       -- bit
@suppress_quotename = 0 ,       -- bit
@system_only = 0 ,              -- bit
@user_only = 1 ,                -- bit
@exclude_list = @ExcludeList;

END
