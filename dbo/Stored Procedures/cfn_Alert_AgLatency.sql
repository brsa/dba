CREATE PROCEDURE [dbo].[cfn_Alert_AgLatency] 
	@AlertThreshold int = 5000, 
	@EmailRecipients varchar(100) = NULL,
    @Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20140409
       This Alert checks AG latency (unsent logs) and emails an alert. 
       Latency is based on the worst reported condition from both DMVs & perfmon.

PARAMETERS
* @AlertThreshold - Default 5000 - Size in KB of the unsent log that will trigger this alert being sent.
* @EmailRecipients - Default DBA Team - semicolon-separated list of email addresses to send alert
EXAMPLES:
* EMail DBA Team when Unsent log surpasses 5000kb
	EXEC cfn_Alert_HADRLatency
* EMail DBA Team when Unsent log surpasses 1000kb
	EXEC cfn_Alert_HADRLatency @AlertThreshold =1000
**************************************************************************************************
MODIFICATIONS:
    20140804 - Start tracking Hardened LSN from DMV each time sproc runs. 
    20150107 - In email, specify from address, and include standard server info in footer
	         - Add calculation for "minutes behind" to show how far behind primary a DB is
*************************************************************************************************/
SET NOCOUNT ON;

DECLARE 
	@EmailFrom varchar(max),
	@EmailBody nvarchar(max),
	@EmailSubject varchar(100),
	@ProductVersion tinyint;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;
SET @ProductVersion = LEFT(CAST(SERVERPROPERTY('PRODUCTVERSION') AS varchar(20)),CHARINDEX('.',CAST(SERVERPROPERTY('PRODUCTVERSION') AS varchar(20)))-1)

IF @ProductVersion < 11
BEGIN
	SELECT 'SQL Server version does not support AGs';
	RETURN;
END;
    
CREATE TABLE #AgStatus ( --drop table #AgStatus
	RunDate smalldatetime NOT NULL ,
	ServerName sysname NOT NULL ,
	AgName sysname NOT NULL,
    DbName sysname NOT NULL ,
	AgRole nvarchar(60) NULL,
	SynchState nvarchar(60) NULL,
    AgHealth nvarchar(60) NULL,
    SuspendReason nvarchar(60) NULL,
	SynchHardenedLSN numeric(25,0) NULL,
    LastHardenedTime datetime2(3) NULL,
	LastRedoneTime datetime2(3) NULL,
	RedoEstSecCompletion bigint NULL,
	LastCommitTime datetime2(3) NULL,
	PRIMARY KEY  CLUSTERED (RunDate, ServerName, DBName)
    );

CREATE TABLE #SendStatus (
    ServerName sysname,
    DbName sysname,
    UnsentLogKb bigint
    );

CREATE TABLE #Results (
    ServerName sysname,
    AgName sysname,
    DbName sysname,
    UnsentLogKb bigint,
    SynchState nvarchar(60),
    AgHealth nvarchar(60),
    SuspendReason nvarchar(60),
	LastHardenedTime datetime2(3),
	LastRedoneTime datetime2(3),
	RedoEstSecCompletion bigint,
	LastCommitTime datetime2(3),
	MinutesBehind int,
    SortOrder int
    );


-- Grab the current status from DMVs for mirroring & AGs
INSERT INTO #AgStatus (RunDate, ServerName, AgName, DbName, AgRole, SynchState, AgHealth, SuspendReason, 
                      SynchHardenedLSN, LastHardenedTime, LastRedoneTime, RedoEstSecCompletion,LastCommitTime)
    SELECT GETDATE() AS RunDate, 
           rcs.replica_server_name AS ServerName, 
           ag.Name                        COLLATE SQL_Latin1_General_CP1_CI_AS AS AgName,
           db_name(ds.database_id) AS DbName, 
           rs.role_desc                   COLLATE SQL_Latin1_General_CP1_CI_AS AS AgRole,
           ds.synchronization_state_desc  COLLATE SQL_Latin1_General_CP1_CI_AS AS SynchState, 
           ds.synchronization_health_desc COLLATE SQL_Latin1_General_CP1_CI_AS AS AgHealth, 
           ds. suspend_reason_desc        COLLATE SQL_Latin1_General_CP1_CI_AS AS SuspendReason, 
           ds.last_hardened_lsn  AS SynchHardenedLSN,
		   ds.last_hardened_time AS LastHardenedTime,
		   ds.last_redone_time AS LastRedoneTime,
		   CASE WHEN redo_rate = 0 THEN 0 ELSE ds.redo_queue_size/ds.redo_rate END AS RedoEstCompletion,
		   ds.last_commit_time AS LastCommitTime
    FROM sys.dm_hadr_database_replica_states ds
    JOIN sys.availability_groups_cluster ag ON ag.group_id = ds.group_id
    JOIN sys.dm_hadr_availability_replica_cluster_states rcs ON rcs.replica_id = ds.replica_id
    JOIN sys.dm_hadr_availability_replica_states rs ON rs.replica_id = ds.replica_id;
	
-- Grab a snapshot of the mirroring_failover_lsn & store it in Monitor_MirroringLSN
--Update the existing one if we already have a row, Insert it if it's missing
UPDATE mon
SET LogDateTime = sts.RunDate,
    AgRole = sts.AgRole,
    SynchState = sts.SynchState,
    SynchHardenedLSN = sts.SynchHardenedLSN
FROM Monitor_AgStatus mon
JOIN #AgStatus sts ON mon.ServerName = sts.ServerName AND mon.DbName = sts.DBName
WHERE mon.SynchHardenedLSN <> sts.SynchHardenedLSN;

INSERT INTO Monitor_AgStatus (LogDateTime, ServerName, DbName, AgRole, SynchState, SynchHardenedLSN)
SELECT RunDate, ServerName, DbName, AgRole, SynchState, SynchHardenedLSN
FROM #AgStatus s
WHERE NOT EXISTS (SELECT 1 FROM Monitor_AgStatus hadr WHERE hadr.ServerName = s.ServerName AND hadr.DbName = s.DbName);


--Lets make this easy, and create a temp table with the current status
-- UNION perfmon counters with dbm_monitor_data. They should be the same, but we don't trust them, so we check both.
INSERT INTO #SendStatus (ServerName, DbName, UnsentLogKb)
SELECT rcs.replica_server_name AS ServerName,
        db_name( drs.database_id) AS DbName, 
        COALESCE(drs.log_send_queue_size,99999) AS UnsentLogKb
FROM sys.dm_hadr_database_replica_states drs
JOIN sys.dm_hadr_availability_replica_cluster_states rcs ON rcs.replica_id = drs.replica_id
WHERE drs.last_sent_time IS NOT NULL
UNION
SELECT @@SERVERNAME,
        LTRIM(RTRIM(instance_name)) as DbName, 
        cntr_value AS UnsentLogKb
FROM sys.[dm_os_performance_counters]  
WHERE object_name = 'SQLServer:Database Replica'
AND counter_name  = 'Log Send Queue';

INSERT INTO #Results (ServerName, AgName, DbName, UnsentLogKb, SynchState, AgHealth, SuspendReason, 
                      LastHardenedTime, LastRedoneTime, RedoEstSecCompletion, LastCommitTime, SortOrder)
SELECT COALESCE(ag.ServerName,'') AS ServerName,
       COALESCE(ag.AgName,'') AS AgName,
       RTRIM(sync.DbName) AS DbName,
       MAX(sync.UnsentLogKb) AS UnsentLogKb,
       COALESCE(ag.SynchState,'') AS SynchState,
	   COALESCE(ag.AgHealth,'') AS AgHealth,
	   COALESCE(ag.SuspendReason,'') AS SuspendReason,
	   MAX(LastHardenedTime),
	   MAX(LastRedoneTime),
	   MAX(RedoEstSecCompletion),
	   MAX(LastCommitTime),
       CASE
			WHEN RTRIM(sync.DbName) = '_Total' THEN 0
			WHEN MAX(sync.UnsentLogKb) > '1000' THEN 2
			WHEN COALESCE(ag.AgHealth,'') <> 'HEALTHY' THEN 3
            WHEN COALESCE(ag.SynchState,'') NOT IN ('SYNCHRONIZING','SYNCHRONIZED') THEN 4
			ELSE 5
		END AS SortOrder
FROM #SendStatus AS sync
LEFT JOIN #AgStatus AS ag ON sync.ServerName = ag.ServerName AND sync.DbName = ag.DbName 
GROUP BY COALESCE(ag.ServerName,''),  COALESCE(ag.AgName,''), RTRIM(sync.DbName),COALESCE(ag.SynchState,''), COALESCE(ag.AgHealth,''), 
          COALESCE(ag.SuspendReason,'') ;

UPDATE r
SET MinutesBehind = DATEDIFF(mi,r2.LastCommitTime,r.LastCommitTime)
FROM #Results r
JOIN #Results r2 ON r2.AgName = r.AgName AND r2.DbName = r.DbName AND r2.LastHardenedTime IS NULL; --Primary

IF @Debug = 1
BEGIN
    IF NOT EXISTS (SELECT 1 FROM #Results)
    	SELECT 'No AGs Exist' AS AgStatus;
    ELSE
    	SELECT * FROM #Results ORDER BY SortOrder, UnsentLogKb DESC, ServerName, DbName;
END;

-- RETURN if AG is current & Healthy
IF NOT EXISTS (SELECT * FROM #Results WHERE UnsentLogKb >= @AlertThreshold OR (AgHealth <> 'HEALTHY' AND DbName <> '_Total') )
    RETURN (0);

PRINT 'AG Data Movement is behind.';

--OK, now send the email
IF @Debug = 0
BEGIN
	-- Send Email WITH STYLE
	SELECT @EmailBody = dbo.cfn_EmailCSS();

	--Build the body of the email based on the #Results
	--Alert of an Unhealthy DB on a server
	IF EXISTS (SELECT 1 FROM #Results WHERE AGHealth <> 'HEALTHY' AND DbName <> '_Total')
	SET @EmailBody = @EmailBody +
				 N'<H2>Availability Group is Unhealthy:</H2>' +
				 N'<table> <tr>' +
				 N'<th> Server Name </th>' +
				 N'<th> AG Name </th>' +
				 N'<th> Database Name </th>' +
				 N'<th> Health </th></tr>' +
				 CAST(( SELECT
                        td = ServerName, '',
                        td = AgName, '',
                        td = DbName, '',
                        td = AGHealth, ''
				 FROM #Results
				 WHERE AGHealth <> 'HEALTHY'
				   AND DbName <> '_Total'
				 ORDER BY ServerName, DbName
				 FOR XML PATH ('tr'), ELEMENTS
				 ) AS nvarchar(max)) +
				N'</table>';


	SET @EmailBody  = @EmailBody +
				 N'<H2>Availability Group data movement is behind:</H2>' +
				 N'<table> <tr>' +
				 N'<th> Server Name </th>' +
				 N'<th> AG Name </th>' +
				 N'<th> Database Name </th>' +
				 N'<th> Minutes Behind </th>' + 
				 N'<th> Unsent Log (kb) </th>' + 
				 N'<th> Status </th>' +
				 N'<th> Suspend Reason </th></tr>'+
				 CAST(( SELECT
                        td = ServerName, '',
                        td = AgName, '',
                        td = DbName, '',
                        td = CAST(COALESCE(MinutesBehind,'-') AS nvarchar(10)), '',
                        td = CAST(UnsentLogKb AS nvarchar(10)), '',
                        td = SynchState, '',
                        td = SuspendReason, ''
				 FROM #Results
				 ORDER BY SortOrder, UnsentLogKb DESC, ServerName, DbName
				 FOR XML PATH ('tr'), ELEMENTS
				 ) AS nvarchar(max)) +
				N'</table>';



	--highlight the total row 
		SET @EmailBody = REPLACE(@EmailBody,'<td>_Total','<td class="alertbg">_Total')
	--highlight the status if it's not "Synchronized" or "SYNCHRONIZING"
	IF @EmailBody LIKE '%NOT SYNCHRONIZING%' 
		SET @EmailBody = REPLACE(@EmailBody,'<td>NOT SYNCHRONIZING','<td class="alertbg">NOT SYNCHRONIZING');
	IF @EmailBody LIKE '%REVERTING%' 
		SET @EmailBody = REPLACE(@EmailBody,'<td>REVERTING','<td class="alertbg">REVERTING');
	IF @EmailBody LIKE '%INITIALIZING%' 
		SET @EmailBody = REPLACE(@EmailBody,'<td>INITIALIZING','<td class="alertbg">INITIALIZING');
	IF @EmailBody LIKE '%NOT_HEALTHY%' 
		SET @EmailBody = REPLACE(@EmailBody,'<td>NOT_HEALTHY','<td class="alertbg">NOT_HEALTHY');
	IF @EmailBody LIKE '%PARTIALLY_HEALTHY%' 
		SET @EmailBody = REPLACE(@EmailBody,'<td>PARTIALLY_HEALTHY','<td class="alertbg">PARTIALLY_HEALTHY');


    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

    SET @EmailSubject = 'ALERT: Availability Group Unhealthy';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;

DROP TABLE #AgStatus;
DROP TABLE #SendStatus;
DROP TABLE #Results;