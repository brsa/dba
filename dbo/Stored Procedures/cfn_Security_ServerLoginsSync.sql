﻿/*****************************************************************************************
Name: cfn_Security_ServerLoginsSync

Desc: Syncs server level logins and objects from the primary Availability Group replica to all secondary replicas.

Parameters:
	@agName - The availability group name to sync. Default value is NULL, which will sync all AGs.
	@login - The login name to sync. Default value is NULL, which will sync all logins.

Dependencies: 
	dbo.cfn_Security_ServerLogins

Auth: MDAK

Date: 10/24/14

**************************************************
				Change History
**************************************************

Date:			Author:					Description:
--------		--------				--------------------------------------------------
05/05/15		MDAK					Renamed sproc		
*****************************************************************************************/

/***	Sample Execution	***
EXEC dbo.cfn_Security_ServerLoginsSync @agName = 'CRMDEVDB', @login = 'C360User';
EXEC dbo.cfn_Security_ServerLoginsSync @agName = NULL, @login = NULL;
*/

CREATE PROCEDURE [dbo].[cfn_Security_ServerLoginsSync] @agName NVARCHAR(255) = NULL,
	@login NVARCHAR(128) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @agPrimary NVARCHAR(255) = NULL,
		@orsServerString NVARCHAR(255) = NULL,
		@orsExecString NVARCHAR(255) = NULL,
		@vchQuery NVARCHAR(MAX) = NULL;
	DECLARE @tmpAGs TABLE (
		agName NVARCHAR(255) NULL,
		agPrimary NVARCHAR(255) NULL
		);
	DECLARE @tmpLogins TABLE (
		vchQuery NVARCHAR(MAX) NULL
		);

	-- Gather all AGs and primary replicas
	INSERT INTO @tmpAGs (
		agName,
		agPrimary
		)
	SELECT ag.NAME,
		gs.primary_replica
	FROM sys.availability_groups ag
	INNER JOIN sys.dm_hadr_availability_group_states gs ON ag.group_id = gs.group_id
    WHERE gs.primary_replica <> @@SERVERNAME --If this server is primary, exclude it
      -- If AG is specified, only gather permissions from that AG primary replica
      AND ag.NAME = COALESCE(@agName,ag.NAME);

	-- Loop through all SERVERS and apply permissions
	WHILE EXISTS (
		SELECT agPrimary
		FROM @tmpAGs
		)
	BEGIN
		SELECT TOP 1 @agName = agName,
			@agPrimary = agPrimary
		FROM @tmpAGs;

		SET @orsServerString = 'Server=' + @agPrimary + ';Trusted_Connection=yes;';

		-- If a login is not specified, run script for all logins
		IF COALESCE(@login, '') = ''
		BEGIN
			SET @orsExecString = 'EXEC DBA.dbo.cfn_Security_ServerLogins @login_name = NULL';
		END;
		ELSE -- Else run script for specific login
		BEGIN
			SET @orsExecString = 'EXEC DBA.dbo.cfn_Security_ServerLogins @login_name = ''''' + @login + '''''';
		END;

		SET @vchQuery = 'SELECT * FROM OPENROWSET(''SQLNCLI'', ''' + @orsServerString + ''', ''' + @orsExecString + ''')';

		INSERT INTO @tmpLogins (vchQuery)
		EXEC sp_executesql @stmt = @vchQuery; --IGNORE:CFN0136(LINE) sp_executesql is SPecial!

		SELECT @vchQuery = vchQuery
		FROM @tmpLogins;

		-- Run the query to apply server-level permissions
		EXEC sp_executesql @stmt = @vchQuery;  --IGNORE:CFN0136(LINE) sp_executesql is SPecial!

		DELETE @tmpAGs
		WHERE agPrimary = @agPrimary;
	END;
END;