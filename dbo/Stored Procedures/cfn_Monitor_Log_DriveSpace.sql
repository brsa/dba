﻿CREATE PROCEDURE [dbo].[cfn_Monitor_Log_DriveSpace]
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20150128
    This procedure logs drive space for all drives that contain data files.
    Free & used space is logged to a table for further analysis.

PARAMETERS
* NONE
**************************************************************************************************
MODIFICATIONS:
    20150101 - 
*************************************************************************************************/
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @LogDateTime datetime2(0) = SYSDATETIME(),
        @database_id int,
        @file_id int,
        @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255);

CREATE TABLE #FileStats (
	database_id int,
	file_id int,
	volume_mount_point nvarchar(512),
	file_system_type nvarchar(512),
	total_bytes bigint,
	available_bytes bigint
	);

CREATE TABLE #Drives (
	volume_mount_point nvarchar(512),
	file_system_type nvarchar(512),
	total_GB numeric(10,3),
	available_GB numeric(10,3),
    available_percent numeric(5,3)
	);

--Cursors
DECLARE filestats_cur CURSOR FOR
    SELECT database_id, file_id
    FROM sys.master_files;


--Get filestats drive info for every datafile to get every drive
--just in case there's a mountpoint that isn't a drive
	
OPEN filestats_cur;
FETCH NEXT FROM filestats_cur INTO @database_id, @file_id;

WHILE @@FETCH_STATUS = 0
BEGIN
    INSERT INTO #FileStats (database_id, file_id, volume_mount_point, file_system_type, total_bytes, available_bytes)
    SELECT database_id, file_id, volume_mount_point, file_system_type, total_bytes, available_bytes
    FROM sys.dm_os_volume_stats (@database_id,@file_id);

    FETCH NEXT FROM filestats_cur INTO @database_id, @file_id;
END;
CLOSE filestats_cur;
DEALLOCATE filestats_cur;

--dedupe info to get drive info
INSERT INTO #Drives (volume_mount_point, file_system_type, total_GB, available_GB)
SELECT volume_mount_point, file_system_type, min(total_bytes)/1024/1024/1024., min(available_bytes)/1024/1024/1024.
FROM #FileStats
GROUP BY volume_mount_point, file_system_type;

UPDATE #Drives 
SET available_percent = available_GB/total_GB*100;
 

INSERT INTO Monitor_Drives (LogDateTime, DriveLabel, FileSystemType, TotalGB, AvailableGB, AvailablePercent)
SELECT @LogDateTime, volume_mount_point, file_system_type, total_GB, available_GB, available_percent
FROM #Drives


DROP TABLE #Drives;
DROP TABLE #FileStats;