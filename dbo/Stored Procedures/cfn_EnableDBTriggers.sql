﻿/********************************************************************************
AUTHOR:	AJ
CREATE DATE: 02/12/2015
DESCRIPTION: Enable TFS enforce Database trigger
SAMPLE CALL : 
EXEC dbo.cfn_EnableDBTriggers @ExcludeList='ReportServer,ReportServerTempDB' ;
*********************************************************************************/
CREATE PROC [dbo].[cfn_EnableDBTriggers]
@ExcludeList NVARCHAR(MAX)=NULL
AS
    BEGIN
        SET NOCOUNT ON;
			EXEC dbo.sp_foreachdb @command = N'USE ?; IF EXISTS(SELECT * FROM sys.triggers T WHERE  T.name=''cfn_TFS_Trigger'' AND T.parent_class = 0 AND T.is_disabled = 1)
												ENABLE TRIGGER cfn_TFS_Trigger ON DATABASE; ' ,-- nvarchar(max)
								  @print_dbname = 0 ,       -- bit
								  @print_command_only = 0 , -- bit
								  @suppress_quotename = 0 , -- bit
								  @system_only = 0 ,        -- bit
								  @user_only = 1 ,
								  @exclude_list =@ExcludeList;
    END;
