create procedure get_distribution_status	
	(
		@agent_id			int				= -1
	,	@publisher_db		sysname			= null
	,	@publication		sysname			= null
	,	@get_details		bit				= 0 
	,	@get_cmds			bit				= 0
	,	@sort_order			varchar(max)	= 'publisher_db, xact_seqno desc'
	,	@format				bit				= 1
	,	@debug				int				= 0
	,	@help				bit				= 0
	)
as

/********************************************************************************************************************
          Name: Get Distribution Status
       Version:	3.2
		Author:	Brian Wilson
	   Created:	20121126
	  Modified: 20141201
          Desc:	Returns distribution agent and transaction status details
     Reference: sp_MSdistribution_delete
				sp_MSmaximum_cleanup_seqno
				sp_MSget_repl_commands 
**************************************************************************************************
CFN MODIFICATIONS:
    20150330 - AM2 - This sproc originally from Brian Wilson, published on SQL Server Central.
                   - Any new versions from Brian will need to be MERGED into this code.
				   - references to distribution removed & replaced with synonyms
				   - Some referenced are referenced as [$(distribution)] to allow index hints to work. 
********************************************************************************************************************/

begin
	set nocount on;
	set ansi_padding on;
	set transaction isolation level read uncommitted;

	--=@help=--
	if @help = 1
		begin 
			select [parameter] = '@agent_id'			, [type] = 'int'			, [description] = 'filters result set by agent'			, [options] = 'null	= returns oldest undistributed tran/agent | -1 = returns oldest undistributed tran for all agent(s) | 0 = returns oldest undistributed tran for all active agent(s) | n = returns specified agent', [default] = '-1', [example] = 'get_distribution_status @agent_id = 0'
			union all
			select [parameter] = '@publisher_db'		, [type] = 'sysname'		, [description] = 'filters result set by publisher_db'	, [options] = 'n/a'																	, [default] = 'null'							, [example] = 'get_distribution_status @publisher_db = ''AdventureWorks'''
			union all
			select [parameter] = '@publication'			, [type] = 'sysname'		, [description] = 'filters result set by publication'	, [options] = 'n/a'																	, [default] = 'null'							, [example] = 'get_distribution_status @publication = ''shopper_other_03'''
			union all
			select [parameter] = '@get_details'			, [type] = 'bit'			, [description] = 'returns detailed agent results'		, [options] = 'n/a'																	, [default] = '0'								, [example] = 'get_distribution_status @get_details = 1'
			union all
			select [parameter] = '@get_cmds'			, [type] = 'bit'			, [description] = 'returns cmd detail columns'			, [options] = 'n/a'																	, [default] = '0'								, [example] = 'get_distribution_status @get_cmds = 1'
			union all
			select [parameter] = '@sort_order'			, [type] = 'varchar(max)'	, [description] = 'returns sorted result set'			, [options] = 'n/a'																	, [default] = 'publisher_db, xact_seqno desc'	, [example] = 'get_distribution_status @sort_order = ''xact_article'''
			union all
			select [parameter] = '@format'				, [type] = 'bit'			, [description] = 'returns formatted cmd detail columns', [options] = '0 = unformatted | 1 = formatted'										, [default] = '0'								, [example] = 'get_distribution_status @format = 1'
			union all
			select [parameter] = '@debug'				, [type] = 'bit'			, [description] = 'returns debug data'					, [options] = '0 = no debug | 1 = debug agent cursor | 2 = debug publisher cursor'	, [default] = '0'								, [example] = 'get_distribution_status @debug = 1'
			return
		end

	--=declare variables=--
	declare	@active							int				= 2
		,	@agent_id_local					int
		,	@article_id						int
		,	@initiated						int				= 3
		,	@min_agent_sub_xact_seqno		varbinary(16)
		,	@min_cutoff_time				datetime		= (select dateadd(hour, -0, getdate())) --set to default based on @min_distretention = 0 (sp_MSdistribution_cleanup)
		,	@min_xact_seqno					varbinary(16)	= null
		,	@max_agent_hist_xact_seqno		varbinary(16)
		,	@max_agent_hist_xact_seqno_1	varbinary(16)
		,	@max_agent_hist_xact_seqno_2	varbinary(16)
		,	@max_cleanup_xact_seqno			varbinary(16)
		,	@publisher_database_id			int
		,	@sql							nvarchar(max)
		,	@xact_seqno						varbinary(16)
		
	--=create temp tables=--
	create table #max_agent_hist_xact_seqno
		(
			[agent_id]						int	
		,	[xact_seqno]					varbinary(16)
		) 

	create table #transaction 
		(
			[publisher_database_id]			int				null
		,	[agent_id]						int				null
		,	[xact_seqno]					varbinary(16)	null
		,	[type]							int				null
		,	[article_id]					int				null
		)

	create table #ditribution_status 
		(
		--MSdistribution_agents
			[publisher_database_id]			int				null
		,	[publisher_db]					sysname			null
		,	[publication]					sysname			null
		,	[subscriber_db]					sysname			null
		,	[agent]							sysname			null
		--MSdistribution_history
		,	[agent_id]						int				null
		,	[runstatus]						int				null
		,	[start_time]					datetime		null
		,	[time]							datetime		null
		,	[duration]						int				null
		,	[comments]						varchar(max)	null
		,	[current_delivery_rate]			float			null
		,	[current_delivery_latency]		int				null
		,	[delivered_transactions]		int				null
		,	[delivered_commands]			int				null
		,	[average_commands]				int				null
		,	[delivery_rate]					float			null
		,	[delivery_latency]				int				null
		,	[total_delivered_commands]		int				null
		,	[error_id]						int				null
		,	[updateable_row]				bit				null
		,	[timestamp]						varbinary(16)	null
		--MSarticles, MSrepl_transactions, MSrepl_commands
		,	[xact_seqno]					varbinary(16)	null
		,	[xact_type]						varchar(256)	null
		,	[xact_article]					sysname			null
		,	[xact_entry_time]				datetime		null
		,	[xact_cmd]						int				null	default 0
		,	[cmd_pending]					int				null	default 0
		,	[cmd_tbd]						int				null	default 0
		,	[cmd_blk]						int				null	default 0
		)
		
	--=open cursor (1): publisher=--
    declare publisher_cursor cursor local fast_forward 
		for 
		select	distinct 
				publisher_database_id
		  from	dbo.msrepl_transactions
		 where	publisher_database_id in	(	
											select	publisher_database_id
											  from	dbo.MSsubscriptions
											 where	publisher_db =	case
																	  --filter publisher by @agent_id
																		when @agent_id > 0				then	(	
																												select	distinct
																														publisher_db
																												  from	dbo.MSsubscriptions
																												 where	agent_id = @agent_id
																												)
																											  
																	  --filter publisher by @publisher_db
																		when @publisher_db is not null	then	@publisher_db
																		
																	  --all publishers
																		else									publisher_db
																	 end
											) 

	open publisher_cursor
	
	fetch next from publisher_cursor 
	into @publisher_database_id
	 
	while (@@fetch_status <> -1)
		begin
			--truncate temp tables
			truncate table #max_agent_hist_xact_seqno
			truncate table #transaction
			
			--=open cursor (2): agent minimum subscription sequence=--
			declare agent_min_sub_seqno_cursor cursor local forward_only 
				for
				select 	a.id
					,	min(s2.subscription_seqno) 
				  from	dbo.MSsubscriptions		s2 
			inner join  dbo.MSdistribution_agents	a on (a.id = s2.agent_id) 
				 where	s2.status in( @active, @initiated ) 
				   and	not exists	(	
									select	* 
									  from	dbo.MSpublications p 
									 where	s2.publication_id	= p.publication_id 
									   and	p.immediate_sync	= 1
									) 
				   and	a.publisher_database_id = @publisher_database_id
			  group by	a.id
									
			open agent_min_sub_seqno_cursor 
		    
		    fetch next from	agent_min_sub_seqno_cursor 
		    into	@agent_id_local
				,	@min_agent_sub_xact_seqno 
			
			--if not exists: set @min_xact_seqno to min_autonosync_lsn
			if (@@fetch_status = -1)
				begin
				   set @min_xact_seqno = null
				
					if not exists	(	
									select	* 
									  from	dbo.MSpublications			msp
								inner join	dbo.MSpublisher_databases	mspd on mspd.publisher_id = msp.publisher_id and mspd.publisher_db = msp.publisher_db
									 where	mspd.id = @publisher_database_id 
									   and	msp.immediate_sync = 1
									)
						begin
							select	top(1) 
									@min_xact_seqno = msp.min_autonosync_lsn 
							  from	dbo.MSpublications			msp
						inner join	dbo.MSpublisher_databases	mspd on mspd.publisher_id = msp.publisher_id and mspd.publisher_db = msp.publisher_db
							 where	mspd.id								= @publisher_database_id 
							   and	msp.allow_initialize_from_backup	<> 0
							   and	msp.min_autonosync_lsn				is not null
							   and	msp.immediate_sync					= 0
						  order by	msp.min_autonosync_lsn asc
						end
				end
		    
			--if exists: set the @min_xact_seqno to min snapshot subscription_seqno or min of max distributed xact_seqno's
			while (@@fetch_status <> -1)
				begin
					   set	@min_xact_seqno	= null
					   set	@max_agent_hist_xact_seqno = null

					select	top 1 
							@max_agent_hist_xact_seqno = xact_seqno 
					  from	dbo.MSdistribution_history 
					 where	agent_id = @agent_id_local 
				  order by	timestamp desc

					if isnull(@max_agent_hist_xact_seqno, @min_agent_sub_xact_seqno) <= @min_agent_sub_xact_seqno 
						begin
							 set @max_agent_hist_xact_seqno = @min_agent_sub_xact_seqno
						end
					
					--log max distributed xact_seqno
					insert	#max_agent_hist_xact_seqno 
						(
							[agent_id],[xact_seqno]
						)
					select	@agent_id_local
						,	@max_agent_hist_xact_seqno

					if ((@min_xact_seqno is null) or (@min_xact_seqno > @max_agent_hist_xact_seqno))
						begin 
							set @min_xact_seqno = @max_agent_hist_xact_seqno
						end
						
					fetch next from agent_min_sub_seqno_cursor 
					into	@agent_id_local
						,	@min_agent_sub_xact_seqno 
				end
				
			close agent_min_sub_seqno_cursor
			deallocate agent_min_sub_seqno_cursor

			 --set @max_cleanup_xact_seqno
			   set @max_cleanup_xact_seqno	= 0x00

			select top 1 
				   @max_cleanup_xact_seqno	= xact_seqno
			  from dbo.MSrepl_transactions with (nolock)
			 where publisher_database_id	= @publisher_database_id 
			   and (
				    xact_seqno				< @min_xact_seqno
				or  @min_xact_seqno			is null
				   ) 
			   and entry_time				<= @min_cutoff_time
		  order by xact_seqno desc
			
			 --set @max_agent_hist_xact_seqno_n
			   set @max_agent_hist_xact_seqno_1	= (	
													select xact_seqno 
													  from (
															select	xact_seqno
																,	row_number() over (order by xact_seqno) as row 
															  from	#max_agent_hist_xact_seqno
														  group by	xact_seqno
															) r
													  where row = 1
													)
			   set @max_agent_hist_xact_seqno_2	= (	
													select xact_seqno 
													  from (
															select	xact_seqno
																,	row_number() over (order by xact_seqno) as row 
															  from	#max_agent_hist_xact_seqno
														  group by	xact_seqno
															) r
													  where row = 2
													)
				
			--stage transactions
			insert	#transaction 
				(
					[publisher_database_id],[agent_id],[xact_seqno],[type],[article_id]
				)
			select	distinct
					s.publisher_database_id
				,	s.agent_id
				,	rc.xact_seqno
				,	rc.type
				,	rc.article_id
			  from	[$(distribution)].dbo.MSrepl_commands rc with (index(ucMSrepl_commands))
		inner join	[$(distribution)].dbo.MSsubscriptions s  with (index(ucMSsubscriptions)) on (rc.article_id = s.article_id) and rc.publisher_database_id = s.publisher_database_id
			 where	s.publisher_database_id	=	@publisher_database_id
			   and	s.agent_id				=	case
													when @agent_id > 0 then @agent_id
													else s.agent_id
												end

			create	clustered index IX_xact_seqno on #transaction (xact_seqno)

			--=open cursor (3): agent minimum transaction sequence=--
			declare agent_min_xact_seqno_cursor cursor local forward_only
				for
				select	agent_id 
					,	xact_seqno
				  from	#max_agent_hist_xact_seqno
				 where	agent_id =	case
										when @agent_id > 0 then @agent_id
										else agent_id
									end
					
			open agent_min_xact_seqno_cursor 
		    
		    fetch next from agent_min_xact_seqno_cursor 
		    into	@agent_id_local
				,	@max_agent_hist_xact_seqno
			
			while (@@fetch_status <> -1)
				begin
					--set @xact_seqno
					select	@xact_seqno	=	(	
											--oldest undistributed
											select	min(xact_seqno)
											  from	#transaction
											 where	publisher_database_id	=	@publisher_database_id
											   and	agent_id				=	@agent_id_local
											   and	xact_seqno				>	@max_agent_hist_xact_seqno
											)

					--set article_id
					select	@article_id	=	(	
											select	top 1
													article_id 
											  from	#transaction
											 where	publisher_database_id	= @publisher_database_id
											   and	agent_id				= @agent_id_local
											   and	xact_seqno				= @xact_seqno
											)
						
					--log agent & xact_seqno details
					insert	#ditribution_status 
						(
						--MSdistribution_agents
							[publisher_database_id],[publisher_db],[publication],[subscriber_db],[agent]
						--MSdistribution_history
						,	[agent_id],[runstatus],[start_time],[time],[duration],[comments],[current_delivery_rate],[current_delivery_latency],[delivered_transactions],[delivered_commands],[average_commands],[delivery_rate],[delivery_latency],[total_delivered_commands],[error_id],[updateable_row],[timestamp]
						--MSarticles, MSrepl_transactions, MSrepl_commands
						,	[xact_seqno],[xact_type],[xact_article],[xact_entry_time]
						)
					select	top 1					
						--MSdistribution_agents
							[publisher_database_id]		=	da.publisher_database_id 
						,	[publisher_db]				=	da.publisher_db
						,	[publication]				=	da.publication
						,	[subscriber_db]				=	da.subscriber_db
						,	[agent]						=	da.name
						--MSdistribution_history
						,	[agent_id]					=	dh.agent_id
						,	[runstatus]					=	dh.runstatus
						,	[start_time]				=	dh.start_time
						,	[time]						=	dh.time
						,	[duration]					=	nullif(dh.duration,0)
						,	[comments]					=	dh.comments
						,	[current_delivery_rate]		=	nullif(dh.current_delivery_rate,0)
						,	[current_delivery_latency]	=	dh.current_delivery_latency
						,	[delivered_transactions]	=	dh.delivered_transactions
						,	[delivered_commands]		=	dh.delivered_commands
						,	[average_commands]			=	dh.average_commands
						,	[delivery_rate]				=	nullif(dh.delivery_rate,0)
						,	[delivery_latency]			=	dh.delivery_latency
						,	[total_delivered_commands]	=	dh.total_delivered_commands
						,	[error_id]					=	dh.error_id
						,	[updateable_row]			=	dh.updateable_row
						,	[timestamp]					=	dh.timestamp
						--MSarticles, MSrepl_transactions, MSrepl_commands
						,	[xact_seqno]				=	@xact_seqno 
						,	[xact_type]					=	(	
															select	case	
																		when exists (	
																					select	type
																					  from	#transaction
																					 where	publisher_database_id	= @publisher_database_id
																					   and	agent_id				= @agent_id_local
																					   and	xact_seqno				= @xact_seqno
																					   and	type					= -2147483611
																					)
																		then 'snapshot'
																		else 'transaction'
																	end
															)
						,	[xact_article]				=	(	
															select	article
															  from	dbo.MSarticles 
															 where	publisher_db	=	(
																						select	top 1
																								publisher_db
																						  from	dbo.MSsubscriptions
																						 where	publisher_database_id = @publisher_database_id
																						)
															   and	article_id		=	@article_id
															)

						,	[xact_entry_time]			=	(	
															select	entry_time 
															  from	dbo.MSrepl_transactions 
															 where	publisher_database_id	= @publisher_database_id
															   and	xact_seqno				= @xact_seqno
															 )
				  from	dbo.MSdistribution_agents	da
			inner join	dbo.MSdistribution_history	dh on da.id = dh.agent_id
				 where	publisher_database_id	=	@publisher_database_id
				   and	id						=	@agent_id_local
			  order by	dh.timestamp desc
				 
					--update command count
					if @get_cmds = 1 and @xact_seqno is not null
						begin
							--update xact_cmd 
							update	#ditribution_status
							   set	xact_cmd				=	(	
																select	count(*)
																  from	[$(distribution)].dbo.MSrepl_commands	rc with (index(ucmsrepl_commands))
																 where	rc.publisher_database_id	= @publisher_database_id
																   and	rc.xact_seqno				= @xact_seqno
																)
							 where	publisher_database_id	=	@publisher_database_id
							   and	xact_seqno				=	@xact_seqno
							   and	xact_cmd				=	0 
						end

					--debug (agent cursor)
					if @debug = 1
						begin
							select	'=======Debug Agent Cursor======='
							  
							select	param_agent_id					= @agent_id
								,	param_publisher_database_id		= @publisher_database_id
								,	param_get_details				= @get_details
								,	param_get_cmds					= @get_cmds
								,	param_debug						= @debug
								,	agent_id						= @agent_id_local
								,	min_xact_seqno					= @min_xact_seqno
								,	max_cleanup_xact_seqno			= @max_cleanup_xact_seqno
								,	max_agent_hist_xact_seqno_1		= @max_agent_hist_xact_seqno_1
								,	max_agent_hist_xact_seqno_2		= @max_agent_hist_xact_seqno_2
								,	xact_seqno						= @xact_seqno
								,	article_id						= @article_id

							select	*
							  from	#ditribution_status

							select	agent_id
								,	xact_seqno
								,	row_number() over (order by xact_seqno) as row 
							  from	#max_agent_hist_xact_seqno
						end 
						
				fetch next from agent_min_xact_seqno_cursor 
				into	@agent_id_local
					,	@max_agent_hist_xact_seqno
					
				end
				
			close agent_min_xact_seqno_cursor
			deallocate agent_min_xact_seqno_cursor

			--update command count
			if @get_cmds = 1
				begin
					--set/check variables					--set to oldest undistributed tran
					select	@xact_seqno						= xact_seqno
															--set to agent_id for oldest undistributed tran
						,	@agent_id_local					= agent_id
															--set to last undistributed tran when only 1 distrubtion agent
						,	@max_agent_hist_xact_seqno_2	= coalesce	(
																			@max_agent_hist_xact_seqno_2
																		,	(
																			select	top 1 
																					xact_seqno
																			  from	dbo.MSrepl_transactions
																			 where	publisher_database_id = @publisher_database_id
																		  order by	xact_seqno desc
																			)
																		)
					  from	#ditribution_status 
					 where	xact_seqno	=	(
											select min(xact_seqno) 
											  from #ditribution_status ds
											 where publisher_database_id = @publisher_database_id
											)

					--update cmd_pending
					update	ad
					   set	cmd_pending	=	(	
											select count(*)
											  from [$(distribution)].dbo.MSrepl_commands	rc with (index(ucmsrepl_commands))
										inner join [$(distribution)].dbo.MSsubscriptions	s  with (index(ucmssubscriptions)) on (rc.article_id = s.article_id)
											 where s.publisher_database_id	= @publisher_database_id
											   and s.agent_id				= @agent_id_local
											   and rc.xact_seqno		   >= @xact_seqno
											   and rc.xact_seqno		   <  @max_agent_hist_xact_seqno_2
											)
					  from	#ditribution_status ad
					 where	publisher_database_id	= @publisher_database_id
					   and	xact_seqno				= @xact_seqno

					--update cmd_tbd
					update	ad
					   set	cmd_tbd		=	(	
											select count(*)
											  from [$(distribution)].dbo.MSrepl_commands	rc with (index(ucmsrepl_commands))
											 where rc.publisher_database_id	= @publisher_database_id
											   and rc.xact_seqno		   <= @max_cleanup_xact_seqno
											)
					  from	#ditribution_status ad
					 where	publisher_database_id	= @publisher_database_id

					--update cmd_blk
					update	ad
					   set	cmd_blk		=	(	
											select count(*)
											  from [$(distribution)].dbo.MSrepl_commands	rc with (index(ucmsrepl_commands))
											 where rc.publisher_database_id	= @publisher_database_id
											   and rc.xact_seqno		   >= @xact_seqno
											   and rc.xact_seqno		   <= @max_agent_hist_xact_seqno_2
											)
					  from	#ditribution_status ad
					 where	publisher_database_id	= @publisher_database_id
					   and	xact_seqno				= @xact_seqno
				end			

			--drop clustered index
			drop index IX_xact_seqno on #transaction

			--debug (publisher cursor)
			if @debug = 2
				begin
					select	'=======Debug Publisher Cursor======='

					select	param_agent_id					= @agent_id
						,	param_publisher_database_id		= @publisher_database_id
						,	param_get_details				= @get_details
						,	param_get_cmds					= @get_cmds
						,	param_debug						= @debug
						,	agent_id						= @agent_id_local
						,	min_xact_seqno					= @min_xact_seqno
						,	max_cleanup_xact_seqno			= @max_cleanup_xact_seqno
						,	max_agent_hist_xact_seqno_1		= @max_agent_hist_xact_seqno_1
						,	max_agent_hist_xact_seqno_2		= @max_agent_hist_xact_seqno_2
						,	xact_seqno						= @xact_seqno
						,	article_id						= @article_id

					select	*
					  from	#ditribution_status

					select	agent_id
						,	xact_seqno
						,	row_number() over (order by xact_seqno) as row 
					  from	#max_agent_hist_xact_seqno
				end 

		fetch next from publisher_cursor 
		into @publisher_database_id 
		
		end
		
	close publisher_cursor
	deallocate publisher_cursor

	--=format output=--
	if @debug = 0 
		begin
			set @sql =											'select	agent				=	agent
																	,	runstatus			=	case 
																									when runstatus = 1	then ''Start''
																									when runstatus = 2	then ''Succeed''
																									when runstatus = 3	then ''In Progress''
																									when runstatus = 4	then ''Idle''
																									when runstatus = 5	then ''Retry''
																									when comments like		 ''The replication agent has not logged a progress message in 10 minutes.%''
																														then ''Warning''
																									when runstatus = 6	then ''Fail''
																								end 
																'
						+
						case
							when @get_details = 1	then		'	,	start_time			=	start_time
																	,	time				=	time
																	,	[duration]			=	convert (varchar(10),(duration)/86400) 
																							+	''-'' 
																							+	substring(convert(varchar(25),(select dateadd(second,duration,''2012-01-01 00:00:00.000'')),126),12,12)'
							else								'	'
						end 
						+
																'	,	comments
																	,	xact_type
																	,	xact_article
																	,	xact_seqno			=	(	
																								substring(     (master.dbo.fn_varbintohexstr(xact_seqno)), 1, 2) 
																								+
																								substring(upper(master.dbo.fn_varbintohexstr(xact_seqno)), 3, 8000)
																								)
																	,	xact_age			=	convert (varchar(10),(datediff(second,(xact_entry_time),getdate()))/86400) + ''-'' +
																								substring(convert(varchar(25),(select dateadd(second,datediff(second,(xact_entry_time),getdate()),''2012-01-01 00:00:00.000'')),126),12,12)' 
						+
						case 
							when @get_cmds = 1	
								then
									case
										when @format = 1 then	'	,	xact_cmd			=	convert(varchar,left(convert(char(22),convert(money,xact_cmd),1),19)) '
										else					'	,	xact_cmd '
									end
							else								' ' 
						end 
						+
																'	,	delivery_rate		=	convert (varchar(max),convert(numeric(38,2),delivery_rate)) + ''/sec''
																	,	delivery_est		=	convert (varchar(10),(datediff(second,(dateadd(second,-(xact_cmd/convert(numeric(38,2),delivery_rate)),getdate())),getdate()))/86400) + ''-'' +
																								substring(convert(varchar(25),(select dateadd(second,datediff(second,(dateadd(second,-(xact_cmd/convert(numeric(38,2),delivery_rate)),getdate())),getdate()),''2012-01-01 00:00:00.000'')),126),12,12)
																' 
						+
						case 
							when @get_cmds = 1	
								then
									case
										when @format = 1 then	'	,	cmd_pending			=	convert(varchar,left(convert(char(22),convert(money,cmd_pending),1),19))
																	,	cmd_tbd				=	convert(varchar,left(convert(char(22),convert(money,cmd_tbd),1),19)) 
																	,	cmd_blk				=	convert(varchar,left(convert(char(22),convert(money,cmd_blk),1),19)) '
										else					'	,	cmd_pending			=	cmd_pending
																	,	cmd_tbd				=	cmd_tbd
																	,	cmd_blk				=	cmd_blk'
									end
							 else								' ' 
						end 
						+					
																' from	#ditribution_status ds
																 where	1=1' 
						+
						case 
							when @agent_id is null		then	'  and	xact_seqno = (select min(xact_seqno) from #ditribution_status ad2 where ad.publisher_db = ad2.publisher_db) '
							when @agent_id = 0			then	'  and	xact_seqno is not null '
							else ' '
						end 
						+
						case 
							when @publication is null	then	' '
							else								'  and	publication = ''' + @publication + ''''
						end 
						+
															' order by ' + @sort_order

			exec sp_executesql @sql
		end

end
go