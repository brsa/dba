﻿/********************************************************************************
 AUTHOR:	AJ
 CREATE DATE: 04/13/2015
 DESCRIPTION: Collect stored procedure which using dynamic sql for dependencies search.
 SAMPLE CALL : 
 EXEC dbo.cfn_Collect_StoredProcedureWithDynamicSQL;

*********************************************************************************/ 
CREATE PROC [dbo].[cfn_Collect_ProcWithDynamicSQL]
AS
BEGIN
SET NOCOUNT ON;


	DECLARE @Databases TABLE
	(
	iID INT IDENTITY(1,1)
	,DatabaseName VARCHAR(200)
	)

IF ((SELECT LEFT(@@VERSION,25))<>'Microsoft SQL Server 2008')
	BEGIN
	    INSERT INTO @Databases (  DatabaseName )
		SELECT name
		FROM sys.databases 
		WHERE dbo.dm_hadr_db_role(name)  IN ('PRIMARY' ,'ONLINE')
		AND name NOT IN ('master','msdb','model','tempdb')
	END
ELSE
	BEGIN
		INSERT INTO @Databases (  DatabaseName )
		SELECT D.name AS DatabaseName
		FROM sys.databases D
		WHERE D.state_desc='ONLINE' 
		AND name NOT IN ('master','msdb','model','tempdb')
	END

	DECLARE @StoredProcedureName TABLE
	(
	ServerName NVARCHAR(200)
	,DatabaseName sysname
	,ProcName sysname NULL
	,Text NVARCHAR(4000)
	)

	DECLARE @iID INT=1
		,@DatabaseName VARCHAR(100)
		,@SQL VARCHAR(MAX)

	WHILE @iID <= (SELECT COUNT(*) FROM @Databases)
	BEGIN

		SELECT @DatabaseName=DatabaseName
		FROM @Databases D 
		WHERE D.iID=@iID

		SET @SQL='USE ['+@DatabaseName+'] 

					SELECT 
					@@SERVERNAME AS ServerName
					,DB_NAME() AS DatabaseName
					,CAST( SL.Value AS VARCHAR(30))+''.''+DB_NAME()+''.dbo.''+so.name AS ProcName
					,sc.text
					FROM sysobjects so
					JOIN syscomments sc ON sc.id = so.id
					LEFT JOIN master.sys.extended_properties SL WITH(NOLOCK) ON SL.name=''ServerAlias''
					WHERE (sc.text LIKE ''%EXEC (%''
						OR sc.text LIKE ''%EXECUTE (%''
						OR sc.text LIKE ''%OPENQUERY%''
						OR sc.text LIKE ''%OPENROWSET%''
						OR sc.text LIKE ''%SP_EXECUTESQL%''	)
					AND xtype = ''P''/*For Procedure*/
					AND so.name NOT LIKE ''%_DEVDB''; '

		INSERT INTO @StoredProcedureName
		EXEC (@SQL)

		SET @iID=@iID+1

	END

	SELECT *
	FROM @StoredProcedureName SPN

END
GO