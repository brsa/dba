﻿CREATE PROCEDURE [dbo].[cfn_Alert_FullLog]
	@EmailRecipients varchar(max) = NULL,
    @Debug bit = 0
AS
/*************************************************************************************************
AUTHOR: Andy Mallon
CREATED: 20141001
    This procedure checks all transaction logs to determine if they are becoming "too full."
    When transaction logs become "too full" an email alert is sent to @EmailRecipients with
    a summary of all logs that are becoming full.
    "Too full" is defined as 80% for TempDb, and 40% on all user DBs.
    Transaction Logs smaller than 100MB are ignored.

PARAMETERS
* @EmailRecipients - delimited list of email addresses. Used for sending email alert.
* @Debug           - Outputs result set, rather than sending email.
**************************************************************************************************
MODIFICATIONS:
    20150107 - In email, specify from address, and include standard server info in footer
*************************************************************************************************/
SET NOCOUNT ON;
SET @EmailRecipients=CASE WHEN @EmailRecipients IS NULL THEN (SELECT API.NotificationListGet('Notify_DBOperationsTeam')) ELSE @EmailRecipients END;

DECLARE @EmailFrom varchar(max),
        @EmailBody nvarchar(max),
        @EmailSubject nvarchar(255)

CREATE TABLE #DBFileSizeInfo (
  DatabaseName varchar(50), 
       DataFileSizeKB bigint, 
       LogFileSizeKB bigint , 
       LogFileUsedKB bigint, 
       PercentLogUsed smallint );
  
--get info for how much log we're using
INSERT INTO #DBFileSizeInfo (DatabaseName, DataFileSizeKB, LogFileSizeKB, LogFileUsedKB, PercentLogUsed)
SELECT instance_name AS DatabaseName, 
       [Data File(s) Size (KB)] AS DataFileSizeKB, 
       [Log File(s) Size (KB)] AS LogFileSizeKB, 
       [Log File(s) Used Size (KB)] AS LogFileUsedKB, 
       [Percent Log Used] AS PercentLogUsed 
FROM 
  (SELECT   * 
   FROM sys.dm_os_performance_counters
   WHERE counter_name IN 
   (   'Data File(s) Size (KB)', 
       'Log File(s) Size (KB)', 
       'Log File(s) Used Size (KB)', 
       'Percent Log Used' ) 
     AND instance_name != '_Total'
     ) AS Src 
PIVOT 
( 
   MAX(cntr_value) 
   FOR counter_name IN 
   (   [Data File(s) Size (KB)], 
       [Log File(s) Size (KB)], 
       [Log File(s) Used Size (KB)], 
       [Percent Log Used] ) 
) AS pvt;


--Filter Results.
----Exclude "really small" log files (Less than 100 MB)
----Alert for TempDB when using >80% log
----Alert for everything else at 40%
SELECT *
INTO #Results
FROM #DBFileSizeInfo 
WHERE 1=1
--Exclusions
AND NOT LogFileUsedKB/1024 < 100
--Inclusions
AND dbo.dm_hadr_db_role(DatabaseName) IN ('PRIMARY','ONLINE')
AND (  (DatabaseName = 'tempdb' AND PercentLogUsed > 80)
    OR (DatabaseName <> 'Tempdb' AND PercentLogUsed > 70)
    );

IF @Debug = 1
BEGIN
    SELECT * FROM #DBFileSizeInfo WHERE DatabaseName <> 'mssqlsystemresource' ORDER BY PercentLogUsed DESC;
END

-- If no rows returned, nothing to do... just return
IF NOT EXISTS (SELECT 1 FROM #Results)
    RETURN (0);

IF @Debug = 0
BEGIN
    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

    --Build the body of the email based on the #Results
    SET @EmailBody  = @EmailBody +
             N'<H2>Transaction Log is getting too full for these databases:</H2>' +
             N'<table> <tr>' +
             N'<th> Database Name </th>' +
             N'<th> Data Files(s) Size (MB) </th>' + 
             N'<th> Log File(s) Size in (MB) </th>' +
             N'<th> Log File(s) Used Size (MB) </th>' +
             N'<th> Percent Log Used </th></tr>'+
	     CAST(( SELECT
		     td = DatabaseName, '',
		     td = CAST(DataFileSizeKB/1024 AS nvarchar(10)), '',
		     td = CAST(LogFileSizeKB/1024 AS nvarchar(10)), '',
		     td = CAST(LogFileUsedKB/1024 AS nvarchar(10)), '',
		     td = CAST(PercentLogUsed AS nvarchar(10)), ''
		     FROM #Results
		     ORDER BY DatabaseName
		     FOR XML PATH ('tr'), ELEMENTS
		     ) AS nvarchar(max)) +
            N'</table>';
 
	SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();

    SET @EmailSubject = 'ALERT: Transaction Log is getting too full';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_') + '@commonwealth.com>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @EmailRecipients,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;
