﻿CREATE VIEW [dbo].[Inventory_ServerRoleMembers]
AS
SELECT DISTINCT rm.ServerName,
	r.name AS [role_principal_name],
	m.name AS [member_principal_name],
	rm.DateAdded,
	rm.DateRemoved,
	rm.Active,
	'USE [master] ALTER SERVER ROLE ' + QUOTENAME(r.name) + ' ADD MEMBER ' + QUOTENAME(m.name) AS [Script],
	'USE [master] EXEC sp_addsrvrolemember ''' + m.name + ''', ''' + r.name + '''' AS [Script2008]
FROM dbo.Inventory_sys_server_role_members rm
	JOIN dbo.Inventory_sys_server_principals r ON rm.role_principal_id = r.principal_id
		AND r.type_desc = 'SERVER_ROLE'
		AND rm.DateAdded >= r.DateAdded
	JOIN dbo.Inventory_sys_server_principals m ON rm.member_principal_id = m.principal_id
		AND rm.DateAdded >= m.DateAdded