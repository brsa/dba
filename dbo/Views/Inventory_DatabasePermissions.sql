﻿CREATE VIEW [dbo].[Inventory_DatabasePermissions]
AS
SELECT DISTINCT perm.ServerName,
	perm.AGName,
	perm.DatabaseName,
	perm.class_desc,
	perm.schema_name,
	perm.major_name,
	perm.minor_name,
	gee.name AS [grantee_principal_name],
	gor.name AS [grantor_principal_name],
	perm.permission_name,
	perm.state_desc,
	perm.DateAdded,
	perm.DateRemoved,
	perm.Active,
	'USE ' + QUOTENAME(perm.DatabaseName) + ' '
	+ CASE perm.state_desc
		WHEN 'GRANT_WITH_GRANT_OPTION' THEN 'GRANT'
		ELSE perm.state_desc
		END
	+ ' ' + perm.permission_name
	+ CASE perm.class_desc
		WHEN 'DATABASE' THEN ''
		WHEN 'OBJECT_OR_COLUMN' THEN ' ON OBJECT::' + QUOTENAME(COALESCE(perm.schema_name,'dbo')) + '.' + QUOTENAME(perm.major_name) +
			CASE WHEN perm.minor_name IS NOT NULL THEN '(' + QUOTENAME(perm.minor_name) + ')'
				ELSE ''
			END
		WHEN 'SCHEMA' THEN ' ON SCHEMA::' + QUOTENAME(perm.major_name)
		WHEN 'DATABASE_PRINCIPAL' THEN ' ON ' +
			CASE WHEN prin.type_desc IN ('SQL_USER','WINDOWS_USER','WINDOWS_GROUP','CERTIFICATE_MAPPED_USER','ASYMMETRIC_KEY_MAPPED_USER')
					THEN 'USER'
				WHEN prin.type_desc = 'DATABASE_ROLE' THEN 'ROLE'
				WHEN prin.type_desc = 'APPLICATION_ROLE' THEN 'APPLICATION ROLE'
			END + '::' + QUOTENAME(perm.major_name)
		WHEN 'ASSEMBLY' THEN ' ON ASSEMBLY::' + QUOTENAME(perm.major_name)
		WHEN 'TYPE' THEN ' ON TYPE::' + QUOTENAME(COALESCE(perm.schema_name,'dbo')) + '.' + QUOTENAME(perm.major_name)
		WHEN 'XML_SCHEMA_COLLECTION' THEN ' ON XML SCHEMA COLLECTION::' + QUOTENAME(COALESCE(perm.schema_name,'dbo')) + '.' + QUOTENAME(perm.major_name)
		WHEN 'MESSAGE_TYPE' THEN ' ON MESSAGE TYPE::' + QUOTENAME(perm.major_name)
		WHEN 'SERVICE_CONTRACT' THEN ' ON CONTRACT::' + QUOTENAME(perm.major_name)
		WHEN 'SERVICE' THEN ' ON SERVICE::' + QUOTENAME(perm.major_name)
		WHEN 'REMOTE_SERVICE_BINDING' THEN ' ON REMOTE SERVICE BINDING::' + QUOTENAME(perm.major_name)
		WHEN 'ROUTE' THEN ' ON ROUTE::' + QUOTENAME(perm.major_name)
		WHEN 'FULLTEXT_CATALOG' THEN ' ON FULLTEXT CATALOG::' + QUOTENAME(perm.major_name)
		WHEN 'SYMMETRIC_KEY' THEN ' ON SYMMETRIC KEY::' + QUOTENAME(perm.major_name)
		WHEN 'CERTIFICATE' THEN ' ON CERTIFICATE::' + QUOTENAME(perm.major_name)
		WHEN 'ASYMMETRIC_KEY' THEN ' ON ASYMMETRIC KEY::' + QUOTENAME(perm.major_name)
		WHEN 'FULLTEXT_STOPLIST' THEN ' ON FULLTEXT STOPLIST::' + QUOTENAME(perm.major_name)
		WHEN 'SEARCH_PROPERTY_LIST' THEN ' ON SEARCH PROPERTY LIST::' + QUOTENAME(perm.major_name)
		END
	+ ' TO ' + QUOTENAME(gee.name)
	+ CASE perm.state_desc WHEN 'GRANT_WITH_GRANT_OPTION' THEN ' WITH GRANT OPTION'
		ELSE ''
		END
	+ ' AS ' + QUOTENAME(gor.name)
	 AS [Script]
FROM dbo.Inventory_sys_database_permissions perm
	JOIN dbo.Inventory_sys_database_principals gee ON perm.grantee_principal_id = gee.principal_id
		AND COALESCE(perm.AGName,'') = COALESCE(gee.AGName,'')
		AND perm.DatabaseName = gee.DatabaseName
		AND perm.DateAdded >= gee.DateAdded
	JOIN dbo.Inventory_sys_database_principals gor ON perm.grantor_principal_id = gor.principal_id
		AND COALESCE(perm.AGName,'') = COALESCE(gor.AGName,'')
		AND perm.DatabaseName = gor.DatabaseName
		AND perm.DateAdded >= gor.DateAdded
	LEFT JOIN dbo.Inventory_sys_database_principals prin ON perm.major_name = prin.name
		AND COALESCE(perm.AGName,'') = COALESCE(prin.AGName,'')
		AND perm.DatabaseName = prin.DatabaseName
		AND perm.DateAdded >= prin.DateAdded
WHERE perm.major_name IS NOT NULL