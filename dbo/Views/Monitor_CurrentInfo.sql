CREATE VIEW dbo.Monitor_CurrentInfo
AS
WITH CurrenInfo AS
(
	SELECT ServerName, FacetType, FacetName, MAX(LogDateTime) AS LogDateTime
	FROM Monitor_Info
	GROUP BY ServerName, FacetType, FacetName
)
SELECT i.ServerName, i.LogDateTime, i.FacetType, i.FacetName, i.Value, i.ValueVarchar
FROM Monitor_Info i
JOIN CurrenInfo ci ON ci.LogDateTime = i.LogDateTime 
		AND ci.ServerName = i.ServerName
		AND ci.FacetName = i.FacetName
		AND ci.FacetType = i.FacetType
GO

