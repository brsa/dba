﻿CREATE VIEW [dbo].[Inventory_DatabaseRoleMembers]
AS
SELECT DISTINCT rm.ServerName,
	rm.AGName,
	rm.DatabaseName,
	r.name AS [role_principal_name],
	m.name AS [member_principal_name],
	rm.DateAdded,
	rm.DateRemoved,
	rm.Active,
	'USE ' + QUOTENAME(rm.DatabaseName) + ' ALTER ROLE ' + QUOTENAME(r.name) + ' ADD MEMBER ' + QUOTENAME(m.name) AS [Script],
	'USE ' + QUOTENAME(rm.DatabaseName) + ' EXEC sp_addrolemember ''' + r.name + ''', ''' + m.name + '''' AS [Script2008]
FROM dbo.Inventory_sys_database_role_members rm
	JOIN dbo.Inventory_sys_database_principals r ON rm.role_principal_id = r.principal_id
		AND r.type_desc = 'DATABASE_ROLE'
		AND COALESCE(rm.AGName,'') = COALESCE(r.AGName,'')
		AND rm.DatabaseName = r.DatabaseName
		AND rm.DateAdded >= r.DateAdded
	JOIN dbo.Inventory_sys_database_principals m ON rm.member_principal_id = m.principal_id
		AND COALESCE(rm.AGName,'') = COALESCE(m.AGName,'')
		AND rm.DatabaseName = m.DatabaseName
		AND rm.DateAdded >= m.DateAdded