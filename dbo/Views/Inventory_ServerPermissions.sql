﻿CREATE VIEW [dbo].[Inventory_ServerPermissions]
AS
SELECT DISTINCT perm.ServerName,
	perm.class_desc,
	perm.major_name,
	perm.minor_name,
	gee.name AS [grantee_principal_name],
	gor.name AS [grantor_principal_name],
	perm.permission_name,
	perm.state_desc,
	perm.DateAdded,
	perm.DateRemoved,
	perm.Active,
	'USE [master] '
	+ CASE perm.state_desc
		WHEN 'GRANT_WITH_GRANT_OPTION' THEN 'GRANT'
		ELSE perm.state_desc
		END
	+ ' ' + perm.permission_name
	+ CASE perm.class_desc
		WHEN 'SERVER' THEN ''
		WHEN 'SERVER_PRINCIPAL' THEN ' ON '
			+ CASE
				WHEN prin.type_desc IN ('SQL_LOGIN','WINDOWS_LOGIN','WINDOWS_GROUP',
					'CERTIFICATE_MAPPED_LOGIN','ASYMMETRIC_KEY_MAPPED_LOGIN') THEN 'LOGIN::' + QUOTENAME(perm.major_name)
				WHEN prin.type_desc = 'SERVER_ROLE' THEN 'SERVER ROLE::' + QUOTENAME(perm.major_name)
				END
		WHEN 'ENDPOINT' THEN ' ON ENDPOINT::' + QUOTENAME(perm.major_name)
		WHEN 'AVAILABILITY GROUP' THEN ' ON AVAILABILITY GROUP::' + QUOTENAME(perm.major_name)
		END
	+ ' TO ' + QUOTENAME(gee.name)
	+ CASE perm.state_desc WHEN 'GRANT_WITH_GRANT_OPTION' THEN ' WITH GRANT OPTION'
		ELSE ''
		END
	+ ' AS ' + QUOTENAME(gor.name)
	 AS [Script]
FROM dbo.Inventory_sys_server_permissions perm
	JOIN dbo.Inventory_sys_server_principals gee ON perm.grantee_principal_id = gee.principal_id
		AND perm.DateAdded >= gee.DateAdded
	JOIN dbo.Inventory_sys_server_principals gor ON perm.grantor_principal_id = gor.principal_id
		AND perm.DateAdded >= gor.DateAdded
	LEFT JOIN dbo.Inventory_sys_server_principals prin ON perm.major_name = prin.name
		AND perm.DateAdded >= prin.DateAdded