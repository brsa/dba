﻿CREATE CONTRACT [//Transmission/Contract]
    AUTHORIZATION [dbo]
    ([//Backup/Message/Full] SENT BY INITIATOR, [//Backup/Message/Log] SENT BY INITIATOR);

