﻿CREATE SERVICE [//Transmission/Receive]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[ReceiveQueue]
    ([//Transmission/Contract]);
GO

GRANT SEND
    ON SERVICE::[//Transmission/Receive] TO PUBLIC;
GO
