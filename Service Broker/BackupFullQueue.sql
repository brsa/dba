﻿CREATE QUEUE [dbo].[BackupFullQueue]
    WITH ACTIVATION (STATUS = ON, PROCEDURE_NAME = [dbo].[cfn_SBBackupFull], MAX_QUEUE_READERS = 3, EXECUTE AS N'dbo');

