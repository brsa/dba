﻿CREATE QUEUE [dbo].[BackupLogQueue]
    WITH ACTIVATION (STATUS = ON, PROCEDURE_NAME = [dbo].[cfn_SBBackupLog], MAX_QUEUE_READERS = 3, EXECUTE AS N'dbo');

