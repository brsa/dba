﻿
DECLARE @Tenant varchar(200)
DECLARE @TenantEmail varchar(200) 

SET @Tenant = CAST((SELECT dbo.cfn_ExtendedPropertyGet('Tenant')) AS VARCHAR(200))

SET @TenantEmail = (SELECT 
						CASE @Tenant 
							WHEN 'CFN'
								THEN '@Commonwealth.com'
							WHEN 'MML' 
								THEN '@Advisor360.com'
							ELSE
								''
						END
					)


MERGE [DBO].[NotificationList] TARGET
Using ( 
select 'Notify_UI' as NotificationListName, 'alerts_ui@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_DevSupport' as NotificationListName, 'alerts_developmentsupport@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_DevTeam' as NotificationListName, 'alerts_development@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_TradingTeam' as NotificationListName, 'alerts_team_trading@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_AccountOpsTeam' as NotificationListName, 'alerts_team_accountops@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_AccountSvcsTeam' as NotificationListName, 'alerts_team_accountservices@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_WMTeam' as NotificationListName, 'alerts_team_wealthmanagement@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_ComplianceTeam' as NotificationListName, 'alerts_team_compliance@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_QAOpsTeam' as NotificationListName, 'alerts_qaops@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_MobileTeam' as NotificationListName, 'alerts_team_mobile@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_360Team' as NotificationListName, 'alerts_team_360@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_DBOperationsTeam' as NotificationListName, 'alerts_databaseoperations@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_DBSupportTeam' as NotificationListName, 'alerts_databasesupport@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_ReportsTeam' as NotificationListName, 'alerts_team_reports@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_PPSTeam' as NotificationListName, 'alerts_team_ppsops@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_ProdMasterTeam' as NotificationListName, 
							CASE @Tenant 
								WHEN 'CFN' THEN 'alerts_productmaster@advisor360.com'
								WHEN 'MML' THEN 'alerts_productmasterdl@advisor360.com'
								ELSE 'alerts_productmaster@advisor360.com'
							END as EmailList
UNION ALL SELECT 'Notify_ProdMasterTeamMML' as NotificationListName, 'alerts_productmasterdl@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_AppEventErrors' as NotificationListName, 'alerts_development@advisor360.com;AppEventNotifications' + @TenantEmail  as EmailList
UNION ALL SELECT 'Notify_AppEventDBErrors' as NotificationListName,'alerts_development@advisor360.com;alerts_databasesupport@advisor360.com;alerts_databaseoperations@advisor360.com;AppEventNotifications' + @TenantEmail  as EmailList
UNION ALL SELECT 'Notify_DataMigrationTeam' as NotificationListName,'alerts_dataintegritydatamigration@advisor360.com' as EmailList
UNION ALL SELECT 'Notify_AccountReconciliationTeam' as NotificationListName,'Alerts_DataIntegrityAccountReconciliation@advisor360.com' as EmailList 
UNION ALL SELECT 'Notify_DataIntegrityTeam' as NotificationListName,'Alerts_DataIntegrity@advisor360.com' as EmailList 
UNION ALL SELECT 'Notify_PPSOpsProductionSupportTeam' as NotificationListName,'Alerts_PPSOpsProductionSupport@advisor360.com' as EmailList 
UNION ALL SELECT 'Notify_PPSQATeam' as NotificationListName,'Alerts_PPSQA@advisor360.com' as EmailList 
UNION ALL SELECT 'Notify_PPSReconDelayedTeam' as NotificationListName,'Alerts_PPSReconDelayed@advisor360.com' as EmailList  
UNION ALL SELECT 'Notify_AlertsApp' as NotificationListName,'alerts_team_accountservices@advisor360.com; alerts_team_trading@advisor360.com; alerts_databasesupport@advisor360.com' as EmailList  
UNION ALL SELECT 'Notify_TradeCentralTaskForce' as NotificationListName,'alerts_tradecentraltaskforce@advisor360.com' as EmailList  
UNION ALL SELECT 'Notify_ACDAReports' as NotificationListName,'ACDAReports' + @TenantEmail  as EmailList  
UNION ALL SELECT 'Notify_Licensing' as NotificationListName,'licensing' + @TenantEmail as EmailList  
UNION ALL SELECT 'Notify_ComplianceTR' as NotificationListName,'ComplianceTR' + @TenantEmail  as EmailList  
UNION ALL SELECT 'Notify_SurveillanceDailyEmail' as NotificationListName,'SurveillanceDailyEmail' + @TenantEmail  as EmailList  

) AS SOURCE
ON TARGET.NotificationListName = SOURCE.NotificationListName
WHEN MATCHED
THEN UPDATE
SET TARGET.EmailList = SOURCE.EmailList
WHEN NOT MATCHED BY TARGET
THEN INSERT
(NotificationListName, EmailList) VALUES (SOURCE.NotificationListName, SOURCE.EmailList);