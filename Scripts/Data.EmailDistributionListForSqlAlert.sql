﻿MERGE dbo.EmailDistributionListForSqlAlert TARGET
USING	(          SELECT 'Notify_DBOperationsTeam' AS DistributionCode ,'ahirapara@advisor360.com;bdeshpande@advisor360.com;dtam@advisor360.com;mmedina@advisor360.com; wyang@advisor360.com;' AS EmailAddress ,'DBA Team' AS Note
		 UNION ALL SELECT 'DBA' AS DistributionCode ,'ahirapara@advisor360.com;bdeshpande@advisor360.com;dtam@advisor360.com;mmedina@advisor360.com; wyang@advisor360.com;' AS EmailAddress ,'Job Monitoring Team' AS Note
		 UNION ALL SELECT 'DbProcessing' AS DistributionCode ,'DatabaseProcessing@COMMONWEALTH.COM' AS EmailAddress ,'Job Monitoring Team' AS Note
		 UNION ALL SELECT 'DbSupport' AS DistributionCode ,'aparker@advisor360.com;mdaiute@advisor360.com;npimentel@advisor360.com;rlynch@advisor360.com;' AS EmailAddress ,'Database Support Team' AS Note
		 UNION ALL SELECT 'DbDev' AS DistributionCode ,'DatabaseSystemsDev@COMMONWEALTH.COM' AS EmailAddress ,'Database Support Team' AS Note
		 UNION ALL SELECT 'UndocumentedObject' AS DistributionCode ,'ahirapara@advisor360.com;bdeshpande@advisor360.com;dtam@advisor360.com;mmedina@advisor360.com; wyang@advisor360.com;' AS EmailAddress ,'Undocumented Object monitoring Team' AS Note
		 UNION ALL SELECT 'DbDeploymentAdmin' AS DistributionCode ,'ahirapara@advisor360.com;bdeshpande@advisor360.com;dtam@advisor360.com;mmedina@advisor360.com; wyang@advisor360.com;' AS EmailAddress ,'Database Deployment monitoring Team' AS Note
		) AS SOURCE
ON	TARGET.DistributionCode = SOURCE.DistributionCode 
WHEN MATCHED AND EXISTS
	(	SELECT TARGET.DistributionCode,TARGET.EmailAddress,TARGET.Note
		EXCEPT
		SELECT Source.DistributionCode,Source.EmailAddress,Source.Note
	)
	THEN UPDATE
	SET	
		TARGET.DistributionCode = Source.DistributionCode
	   ,TARGET.EmailAddress = Source.EmailAddress
	   ,TARGET.Note = Source.Note
WHEN NOT MATCHED BY TARGET
THEN INSERT
	(DistributionCode,EmailAddress,Note)
	VALUES
	(Source.DistributionCode,Source.EmailAddress,Source.Note)
WHEN NOT MATCHED BY Source
THEN DELETE;
GO