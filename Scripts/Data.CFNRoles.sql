﻿/*
Environment: Accepted values are any combination of Production,QA,Development,Processing,Migration
	'ALL' keyword includes all, less 'Management'.
	'Management' will be ignored.
	Comma separated values are inclusive. Uses fn_Split to parse.
DatabaseName: Use syntax from Ola Hallengren's dbo.DatabaseSelect table-valued function
	https://ola.hallengren.com/sql-server-backup.html
	Ex. 'USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset' will include all user databases, exclude DBA and PPS and PPS_MASTER
ObjectType is used to determine role scope (server / database) or permission scope (server / database / object)
When PermissionState=ADDROLEMEMBER, PermissionType is the role member (which can be a role or user)
	Can be a string of users to add, comma separated list, uses fn_Split to parse.
*/
IF OBJECT_ID('dbo.CFNRoles') IS NOT NULL
BEGIN
	TRUNCATE TABLE dbo.CFNRoles;
-- test for build
	-- public
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('ALL','master','public','GRANT','EXECUTE','OBJECT','dbo','sp_WhoIsActive',NULL)

	-- sysadmin
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('ALL',NULL,'sysadmin','ADDROLEMEMBER','WALTHAM\Database DBA','SERVER',NULL,NULL,NULL),
	('ALL',NULL,'sysadmin','ADDROLEMEMBER','WALTHAM\Database Operations','SERVER',NULL,NULL,NULL),
	('Production,QA,Development',NULL,'sysadmin','ADDROLEMEMBER','WALTHAM\ProdSQLAdmin','SERVER',NULL,NULL,NULL),
	('Processing',NULL,'sysadmin','ADDROLEMEMBER','WALTHAM\Database Support','SERVER',NULL,NULL,NULL)
	
	-- CFNAlterTrace
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('ALL',NULL,'CFNAlterTrace','GRANT','ALTER TRACE','SERVER',NULL,NULL,NULL),
	('Migration',NULL,'CFNAlterTrace','ADDROLEMEMBER','WALTHAM\DeploymentAdmins,WALTHAM\Support Engineers','SERVER',NULL,NULL,NULL),
	('QA,Development',NULL,'CFNAlterTrace','ADDROLEMEMBER','WALTHAM\Development','SERVER',NULL,NULL,NULL),
	('Processing',NULL,'CFNAlterTrace','ADDROLEMEMBER','WALTHAM\Database Support','SERVER',NULL,NULL,NULL)

	-- CFNViewServerState
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('ALL',NULL,'CFNViewServerState','GRANT','VIEW SERVER STATE','SERVER',NULL,NULL,NULL),
	('Production,QA,Development',NULL,'CFNViewServerState','ADDROLEMEMBER','WALTHAM\Development,WALTHAM\EnterpriseSharePointAdmins','SERVER',NULL,NULL,NULL),
	('Migration',NULL,'CFNViewServerState','ADDROLEMEMBER','WALTHAM\Development,WALTHAM\Converters','SERVER',NULL,NULL,NULL)

	-- CFNCreateDatabase
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('ALL',NULL,'CFNCreateDatabase','GRANT','CREATE ANY DATABASE','SERVER',NULL,NULL,NULL),
	('Migration',NULL,'CFNCreateDatabase','ADDROLEMEMBER','WALTHAM\Converters','SERVER',NULL,NULL,NULL)
	
	-- CFNDevelopers
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNDevelopers','GRANT','VIEW DEFINITION','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset,-PPS,-PPS_MASTER_MASTER','CFNDevelopers','GRANT','SHOWPLAN','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNDevelopers','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datareader','ADDROLEMEMBER','CFNDevelopers','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datawriter','ADDROLEMEMBER','CFNDevelopers','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNDevelopers','ADDROLEMEMBER','WALTHAM\Development','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES','CFNDevelopers','GRANT','SELECT','OBJECT','sys','sql_expression_dependencies',NULL)

	-- CFNSupport
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Processing','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNSupport','GRANT','VIEW DEFINITION','DATABASE',NULL,NULL,NULL),
	('Processing','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNSupport','GRANT','SHOWPLAN','DATABASE',NULL,NULL,NULL),
	('Processing','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNSupport','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Processing','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datareader','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Processing','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datawriter','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Processing','msdb','SQLAgentOperatorRole','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Processing','msdb','DatabaseMailUserRole','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Processing','SSISDB','ssis_admin','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Processing','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset,msdb','CFNSupport','ADDROLEMEMBER','WALTHAM\Support Engineers','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNSupport','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datawriter','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','msdb','SQLAgentOperatorRole','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','msdb','DatabaseMailUserRole','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','SSISDB','ssis_admin','ADDROLEMEMBER','CFNSupport','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset,msdb','CFNSupport','ADDROLEMEMBER','WALTHAM\Database Support,WALTHAM\Support Engineers','DATABASE',NULL,NULL,NULL)

	-- CFNDeploymentAdmins
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datawriter','ADDROLEMEMBER','CFNDeploymentAdmins','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_ddladmin','ADDROLEMEMBER','CFNDeploymentAdmins','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNDeploymentAdmins','ADDROLEMEMBER','WALTHAM\DeploymentAdmins','DATABASE',NULL,NULL,NULL),
	('Processing','SSISDB','ssis_admin','ADDROLEMEMBER','CFNDeploymentAdmins','DATABASE',NULL,NULL,NULL),
	('Processing','SSISDB','CFNDeploymentAdmins','ADDROLEMEMBER','WALTHAM\DeploymentAdmins','DATABASE',NULL,NULL,NULL)

	-- CFNReaders
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNReaders','GRANT','VIEW DEFINITION','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNReaders','GRANT','SHOWPLAN','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datareader','ADDROLEMEMBER','CFNReaders','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNReaders','ADDROLEMEMBER','WALTHAM\QA Team,WALTHAM\SystemsAnalysis,WALTHAM\Business Analysis,WALTHAM\SoftwareArchitects','DATABASE',NULL,NULL,NULL),
	('QA,Development','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','CFNReaders','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('QA,Development','USER_DATABASES,-DBA,-PPS,-PPS_MASTER,-DataIntegrationStage,-OutsideAsset','db_datawriter','ADDROLEMEMBER','CFNReaders','DATABASE',NULL,NULL,NULL)

	-- CFNSharePoint
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development','code,product,rep,CFN_BackOffice,Financials,Trades','CFNSharePoint','GRANT','VIEW DEFINITION','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','code,product,rep,CFN_BackOffice,Financials,Trades','CFNSharePoint','GRANT','SHOWPLAN','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','code,product,rep,CFN_BackOffice,Financials,Trades','db_datareader','ADDROLEMEMBER','CFNSharePoint','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CFN_BackOffice','db_datawriter','ADDROLEMEMBER','CFNSharePoint','DATABASE',NULL,NULL,NULL),
	('Development','rep,CFN_BackOffice,CFN_Security,Financials','CFNSharePoint','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','code,product,rep,CFN_BackOffice,Financials,Trades','CFNSharePoint','ADDROLEMEMBER','WALTHAM\EnterpriseSharePointAdmins,WALTHAM\SharePoint Contractors,SharePointService','DATABASE',NULL,NULL,NULL)

	-- CFNConverters
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES 
	('Migration','CRM,CRM_Conversion_Stage,CRM_FileStream','db_owner','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Migration','CRM,CRM_Conversion_Stage,CRM_FileStream','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CESS','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CESS','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Conversion_Stage','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Conversion_Stage','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_FileStream','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_FileStream','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Stage','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Stage','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CFN_MSCRM','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CFN_MSCRM','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','ESS_SyncService','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','ESS_SyncService','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','Staging','db_datareader','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','Staging','db_datawriter','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','Staging','db_ddladmin','ADDROLEMEMBER','CFNConverters','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','Staging','CFNConverters','ADDROLEMEMBER','WALTHAM\Converters','DATABASE',NULL,NULL,NULL)

	-- CFNCRMAdmin
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development','CESS','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CESS','db_datawriter','ADDROLEMEMBER','CFNCRMAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CFN_MSCRM','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CFN_MSCRM','db_datawriter','ADDROLEMEMBER','CFNCRMAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM','CFNCRMAdmin','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM','db_datawriter','ADDROLEMEMBER','CFNCRMAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Conversion_Stage','CFNCRMAdmin','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Conversion_Stage','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Conversion_Stage','db_datawriter','ADDROLEMEMBER','CFNCRMAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_FileStream','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_FileStream','db_datawriter','ADDROLEMEMBER','CFNCRMAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Stage','CFNCRMAdmin','GRANT','EXECUTE','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Stage','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CRM_Stage','db_datawriter','ADDROLEMEMBER','CFNCRMAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','Accounts','CFNCRMAdmin','GRANT','SELECT','OBJECT','dbo','Household',NULL),
	('Production,QA,Development,Migration','Accounts','CFNCRMAdmin','GRANT','INSERT','OBJECT','dbo','Household',NULL),
	('Production,QA,Development,Migration','Accounts','CFNCRMAdmin','GRANT','UPDATE','OBJECT','dbo','Household',NULL),
	('Production,QA,Development,Migration','Accounts','CFNCRMAdmin','GRANT','DELETE','OBJECT','dbo','Household',NULL),
	('Production,QA,Development,Migration','Accounts','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','EXECUTE','OBJECT','dbo','cfn_CRMIntegrationLogInsert',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','EXECUTE','OBJECT','dbo','cfn_OfficeValueInsert',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','SELECT','OBJECT','dbo','CRMIntegrationLog',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','INSERT','OBJECT','dbo','CRMIntegrationLog',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','UPDATE','OBJECT','dbo','CRMIntegrationLog',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','DELETE','OBJECT','dbo','CRMIntegrationLog',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','SELECT','OBJECT','dbo','CRMOfficeValues',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','INSERT','OBJECT','dbo','CRMOfficeValues',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','UPDATE','OBJECT','dbo','CRMOfficeValues',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','GRANT','DELETE','OBJECT','dbo','CRMOfficeValues',NULL),
	('Production,QA,Development,Migration','CFN_Security','CFNCRMAdmin','ADDROLEMEMBER','WALTHAM\CRMDBRequestAdmin','DATABASE',NULL,NULL,NULL)

	-- CFNEasySiteContractors
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development','CommonSite','CFNEasySiteContractors','GRANT','VIEW DEFINITION','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CommonSite','db_datareader','ADDROLEMEMBER','CFNEasySiteContractors','DATABASE',NULL,NULL,NULL),
	('Production,QA,Development','CommonSite','CFNEasySiteContractors','ADDROLEMEMBER','WALTHAM\EasySiteCIT','DATABASE',NULL,NULL,NULL)

	/*
	-- CFNAccessDMV
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development,Migration','USER_DATABASES','CFNAccessDMV','GRANT','SELECT','OBJECT','sys','sql_expression_dependencies',NULL),
	('Production,QA,Development,Migration','USER_DATABASES','CFNAccessDMV','ADDROLEMEMBER','WALTHAM\Development','DATABASE',NULL,NULL,NULL)
	*/

	/* MORE EXAMPLES
	INSERT INTO dbo.CFNRoles (Environment, DatabaseName, RoleName, PermissionState, PermissionType, ObjectType, SchemaName, ObjectName, ColumnName)
	VALUES ('Production,QA,Development','CFN_BackOffice','CFNSharePoint','GRANT','SELECT','OBJECT','dbo','Departments',NULL),
	('Production,QA,Development','CFN_BackOffice','CFNSharePoint','GRANT','SELECT','OBJECT','dbo','Staff_Bios','first_name'),
	('Production,QA,Development','CFN_BackOffice','CFNSharePoint','GRANT','SELECT','OBJECT','dbo','Staff_Bios','last_name'),
	('Production,QA,Development','CFN_BackOffice','CFNSharePoint','GRANT','SELECT','SCHEMA','etl',NULL,NULL)
	*/
END