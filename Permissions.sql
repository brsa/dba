﻿/*IF YOU ARE RENAMING ANY ROLE, APPLY THAT UPDATE TO ROLEMEMBERSHIPS.SQL FILE AS WELL.*/
GRANT CONNECT TO guest;
GO
----------------------------
CREATE ROLE [db_AlterUser] AUTHORIZATION [dbo];
GO
GRANT CONNECT TO [db_AlterUser];
GO 
GRANT ALTER ANY USER TO [db_AlterUser]
GO
--------+++++++
CREATE ROLE [db_Execute] AUTHORIZATION [dbo];
GO
GRANT CONNECT TO [db_Execute];
GO 
GRANT EXECUTE TO [db_Execute];
GO
----------------------------
CREATE ROLE [db_Alter] AUTHORIZATION [dbo];
GO
GRANT CONNECT TO [db_Alter];
GO 
GRANT ALTER ANY SCHEMA TO [db_Alter];
GO
---------------------------
CREATE ROLE [db_create] AUTHORIZATION [dbo];
GO
GRANT CONNECT TO [db_create];
GO
GRANT ALTER ANY SCHEMA TO [db_create];
GO
GRANT CREATE FUNCTION TO [db_create];
GO
GRANT CREATE PROCEDURE TO [db_create];
GO
GRANT CREATE SCHEMA TO [db_create];
GO
GRANT CREATE SYNONYM TO [db_create];
GO
GRANT CREATE TABLE TO [db_create];
GO
GRANT CREATE TYPE TO [db_create];
GO
GRANT CREATE VIEW TO [db_create];
GO
GRANT REFERENCES TO [db_create];
GO
------------------------
/*Below listed objects are shared objects and used by other teams. There is no risk in allowing PUBLIC access to these function.
Adding these objects to PUBLIC role helps managing access to these objects. No explicit user permission is required for these objects.*/
GRANT SELECT,EXECUTE ON SCHEMA::[API] TO PUBLIC;
GO 
GRANT SELECT ON OBJECT::[dbo].[fn_split] TO PUBLIC;
GO 
GRANT SELECT ON OBJECT::[dbo].[cfn_AgentJobStatus] TO PUBLIC;
GO 
GRANT EXECUTE ON OBJECT::[dbo].[dm_hadr_db_role] TO PUBLIC;
GO 
GRANT SELECT ON OBJECT::[dbo].[ObjectExceptions] TO PUBLIC;
GO 
GRANT EXECUTE ON OBJECT::[dbo].[cfn_ExtendedPropertyGet] TO PUBLIC;
GO 
GRANT EXECUTE ON OBJECT::[dbo].[cfn_FindTextAllDB] TO PUBLIC;
GO 
GRANT EXECUTE ON OBJECT::[dbo].[cfn_EmailCss] TO PUBLIC;
GO
GRANT SELECT ON OBJECT::[dbo].[NotificationList] TO PUBLIC;
GO
------------------------
CREATE ROLE [Access Development] AUTHORIZATION [dbo];
GO
GRANT CONNECT TO [Access Development];
go
GRANT EXECUTE ON OBJECT::[dbo].[cfn_Check_Column_Dependency] TO [Access Development];
GO 
GRANT EXECUTE ON OBJECT::[dbo].[sp_BlitzIndex] TO [Access Development];
GO 
GRANT EXECUTE ON OBJECT::[dbo].[sp_BlitzCache] TO [Access Development];
GO 
GRANT EXECUTE ON OBJECT::[dbo].[sp_WhoIsActive] TO [Access Development];
GO
GRANT EXECUTE ON OBJECT::[dbo].[sp_BlitzLock] TO [Access Development];
GO
GRANT SELECT ON OBJECT::[dbo].[Monitor_ProcedureStats] TO [Access Development];
GO
----------------
