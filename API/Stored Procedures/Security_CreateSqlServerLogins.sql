﻿/**************************************************
-- DO NOT CHNAGE ANYTHING WITHOUT THOROUGH TESTING
This is shared stored procedure that being use by all databases from all environments for all tenants. 
Make sure to do thorough testing in sandbox environment for any changes to this stored procedure.
Service account that being use to deploy database would need execute permission for this stored procedure and account should also be security admin at database or server level.
************************************************/
CREATE PROC [API].[Security_CreateSqlServerLogins]
 @Print_Command_Only BIT=0
,@DropLoginNotInSource BIT =0
,@Debug BIT = 0
,@SrvAcctReviewEmailTo VARCHAR(1000)=NULL
,@IgnoreFailure BIT = 1
AS
SET NOCOUNT ON;

/***********************************************************************
Check for table #RoleMemebrship as that is being use by stored procedure
***********************************************************************/
IF OBJECT_ID('tempdb.dbo.#LoginsList') IS NULL
BEGIN
	THROW 51000, 'Temp table #LoginsList is missing. Create and populate temp table first as that is being use in stored procedure. 
	Table definition: 
	CREATE TABLE #LoginsList (Id INT IDENTITY(1, 1),name VARCHAR(128) NOT NULL,type CHAR(1) NOT NULL ,type_desc VARCHAR(20) NOT NULL
			,SvcAcctName NVARCHAR(200) DEFAULT (''''),Script NVARCHAR(MAX),PRIMARY KEY CLUSTERED (name,type),CHECK  (type IN (''U'',''S'',''G'')));', 1; 
END
/**********************************
Adding below code just to resolve TFS project error.
**********************************/
IF 1=0
BEGIN
	CREATE TABLE #LoginsList (Id INT IDENTITY(1, 1),name VARCHAR(128) NOT NULL,type CHAR(1) NOT NULL ,type_desc VARCHAR(20) NOT NULL
			,SvcAcctName NVARCHAR(200) DEFAULT (''),Script NVARCHAR(MAX),PRIMARY KEY CLUSTERED (name,type),CHECK  (type IN ('U','S','G')));
END;

/***************************************************/
-- DO NOT CHNAGE ANYTHING BELOW THIS LINE.
/****************************************************/

/*------------------------------------------------------
Prepare Service account name. Add domain and environment prefix if needed as per stardard.
---------------------------------------------------------*/
/*Retrieve domain of the target server. This would be used to for service account domain and environment prefix in the service account name.*/
DECLARE @DefaultDomain NVARCHAR(50)=DEFAULT_DOMAIN();
DECLARE @CFNEnvironment NVARCHAR(50)=(SELECT CAST(dbo.cfn_ExtendedPropertyGet('SecurityEnvironment') AS VARCHAR(50)));
DECLARE @EnvironmentPrefix CHAR(2)=CASE WHEN @DefaultDomain='WALTHAM' THEN CASE WHEN @CFNEnvironment='DEVELOPMENT' THEN 'DV' ELSE LEFT(@CFNEnvironment,2) END
	ELSE CASE WHEN RIGHT(@DefaultDomain,3)='DEV' THEN 'DV' ELSE LEFT(RIGHT(@DefaultDomain,3),2) END END;

UPDATE l
	SET SvcAcctName=CASE WHEN type = 'S' THEN name 
						 WHEN type  IN ('U','G') AND l.name NOT LIKE 'advisor360corp\pr%' THEN @DefaultDomain+'\'+ CASE WHEN name NOT LIKE '%gmsa$' AND type='U' THEN  @EnvironmentPrefix ELSE '' END +name  
						 ELSE name
						END
FROM #LoginsList l;
/************************************************/
-- PREPARE RUNNABLE SCRIPT. Script would check first if login exist on the server or not. If login doesn't exist, script will create the login and print the service account name for log\review purpose.
/***************************************************/
DECLARE @FakePassword NVARCHAR(30)='@kddd#454nADNN74';
--SELECT *
UPDATE l
	SET l.Script=  CASE  WHEN type = 's' 
			THEN 'IF NOT EXISTS (SELECT * FROM sys.server_principals sp WHERE sp.name ='''+SvcAcctName+''')'
			+CHAR(13)+CHAR(10)+'BEGIN'
			+CHAR(13)+CHAR(10)+ CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'CREATE LOGIN [' + SvcAcctName + '] WITH PASSWORD=N'''+@FakePassword+''', DEFAULT_DATABASE=[master];'
			+CHAR(13)+CHAR(10)+'PRINT ''Login created for service account: '+SvcAcctName+''' '
			+CHAR(13)+CHAR(10)+'END;'
			+CHAR(13)+CHAR(10)+'/**********************/'
		WHEN type IN ( 'U' ,'G')
			THEN 'IF NOT EXISTS (SELECT * FROM sys.server_principals sp WHERE sp.name ='''+SvcAcctName+''')'
			+CHAR(13)+CHAR(10)+'BEGIN'
			+CHAR(13)+CHAR(10)+CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'CREATE LOGIN [' + SvcAcctName + '] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]'
			+CHAR(13)+CHAR(10)+'PRINT ''Login created for service account: '+SvcAcctName+''' '	
			+CHAR(13)+CHAR(10)+'END;'
			+CHAR(13)+CHAR(10)+'/**********************/'
		END
FROM #LoginsList l;

--SELECT * FROM #LoginsList l;
--RETURN

/********************/
--SELECT Script+CHAR(12)+CHAR(10)+'GO' FROM #LoginsList l;
/**************************************************
Below code will run the script prepared in previous statment.
*************************************************/

DECLARE @ID INT=1
	,@total INT =(SELECT MAX(l.Id) FROM #LoginsList l)
	,@Sql NVARCHAR(MAX)='';

WHILE @ID <=@total
BEGIN
	SET @Sql=(SELECT l.Script FROM #LoginsList l WHERE l.Id=@ID);

	BEGIN TRY
		IF @Print_Command_Only = 1
			PRINT @Sql;			
		ELSE 
			EXEC sys.sp_executesql @stmt=@Sql;			
	END TRY
	BEGIN CATCH
		PRINT '--Script Failed:';
		PRINT @Sql;
		IF @IgnoreFailure <> 1
		BEGIN
			;THROW;
		END
	END CATCH;

	SET @ID=@ID+1;

END;

/************************************************
-- Send an email if service account is either not as per naming standard or missing in the TFS project. we are sending email not but once we have all accounts
clean up. we would do hard fail in script find any mismatch. This is to make sure process is being followed.
*************************************************/

IF OBJECT_ID('tempdb.dbo.#LoginsOnServerBeforeUpdate') IS NOT NULL
	DROP TABLE #LoginsOnServerBeforeUpdate;

CREATE TABLE #LoginsOnServerBeforeUpdate (ID INT IDENTITY(1,1) NOT NULL,SvcAcctName NVARCHAR(128) NOT NULL);

INSERT INTO #LoginsOnServerBeforeUpdate (SvcAcctName)
SELECT sp.name
FROM sys.server_principals sp
WHERE sp.is_disabled=0
	AND sp.type IN ('S','U','G')
	AND sp.name NOT IN ('SA','NT SERVICE\MSSQLSERVER','NT AUTHORITY\SYSTEM','NT SERVICE\SQLTELEMETRY','NT SERVICE\SQLWriter','NT SERVICE\Winmgmt','NT SERVICE\SQLSERVERAGENT');


/**************************************************
DropLoginNotInSource.
*************************************************/

IF OBJECT_ID('tempdb.dbo.#LoginsToDrop') IS NOT NULL
	DROP TABLE #LoginsToDrop;

CREATE TABLE #LoginsToDrop (ID INT IDENTITY(1,1) NOT NULL,SvcAcctName NVARCHAR(128) NOT NULL, Script NVARCHAR(1000) NOT NULL);

INSERT INTO #LoginsToDrop (SvcAcctName, Script)
SELECT cl.SvcAcctName , CASE WHEN @Debug = 0 THEN '' ELSE '--' END + 'DROP LOGIN ' + QUOTENAME(cl.SvcAcctName) +';'
FROM #LoginsOnServerBeforeUpdate cl
LEFT JOIN #LoginsList ll ON ll.SvcAcctName = cl.SvcAcctName
WHERE ll.SvcAcctName IS NULL;

IF @DropLoginNotInSource =1
BEGIN
	SET @ID =1;
	SET @Sql = '';

	WHILE @ID <= (SELECT MAX(ltd.ID) FROM #LoginsToDrop ltd)
	BEGIN
		SET @Sql = (SELECT ltd.Script FROM #LoginsToDrop ltd WHERE ltd.ID = @ID);

		BEGIN TRY
			IF @Print_Command_Only = 1
				PRINT @Sql;			
			ELSE 
				EXEC sys.sp_executesql @stmt=@Sql;			
		END TRY
		BEGIN CATCH
			PRINT '--Script Failed:';
			PRINT @Sql;
			IF @IgnoreFailure <> 1
			BEGIN
				;THROW;
			END
		END CATCH;

		SET @ID= @ID + 1;
	END;
END;
/*
SELECT cl.SvcAcctName
FROM @CurrentLogins cl
LEFT JOIN #LoginsList l ON l.SvcAcctName = cl.SvcAcctName
WHERE l.SvcAcctName IS NULL
--*/
/************************************************
Send email for all Service Accounts which will be dropped.
************************************************/
IF EXISTS (SELECT * FROM #LoginsToDrop ltd) AND @DropLoginNotInSource = 0 AND @SrvAcctReviewEmailTo IS NOT NULL
BEGIN
	DECLARE @EmailFrom varchar(max),
			@EmailBody nvarchar(max),
			@EmailSubject nvarchar(255);
    -- Send Email WITH STYLE
    SELECT @EmailBody = dbo.cfn_EmailCSS();

    --Build the body of the email based on the #Results
    SET @EmailBody = @EmailBody + N'<h2><font size="1"> Below listed service accounts needs to be reviewed by Db Ops. Source control repository and target server are not in sync. Here are possible reason.
	<br> ->  This could be either login created directly on server or inconstency in service account name.. 
	</h2>' + CHAR(10) +
            N'<table><tr>' +
            N'<th>Service Account Name</th>' +
            CAST(( SELECT
                    td = l.SvcAcctName, ''
                    FROM #LoginsToDrop l
                    FOR XML PATH ('tr'), ELEMENTS
                    ) AS nvarchar(max)) +
            N'</table>';

    SELECT @EmailBody = @EmailBody + N'<hr>' + dbo.cfn_EmailServerInfo();
		
	SET @EmailSubject = 'Review Required: Sql Server logins is added manually..';
	SET @EmailFrom = @@SERVERNAME + ' <' + REPLACE(@@SERVERNAME,'\','_')+'@'+DEFAULT_DOMAIN()+ '.COM>';

    EXEC msdb.dbo.sp_send_dbmail
        @recipients = @SrvAcctReviewEmailTo,
		@from_address = @EmailFrom,
        @subject = @EmailSubject,
        @body = @EmailBody,
        @body_format = 'HTML',
        @importance = 'High';
END;
GO