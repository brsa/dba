﻿/**************************************************
-- DO NOT CHNAGE ANYTHING WITHOUT THOROUGH TESTING
This is shared stored procedure that being use by all databases from all environments for all tenants. 
Make sure to do thorough testing in sandbox environment for any changes to this stored procedure.
Service account that being use to deploy database would need execute permission for this stored procedure and account should also be security admin at database or server level.
************************************************/
CREATE PROC [API].[Security_ApplyRoleMemberships]
@DbName VARCHAR(128)
,@Print_Command_Only BIT=0
,@DropRoleMembershipNotInSource BIT =0
,@DropUserNotInSource BIT=0
,@CreateLoginNotInTarget BIT=1
,@ExcludeADgroupsFromDrop BIT = 1 /*This has not been implemented yet.*/
,@Debug BIT = 0
AS
SET NOCOUNT ON;
/***********************************************
CREATE TABLE 
***********************************************/
/*Retrieve domain of the target server. This would be used to for service account domain and environment prefix in the service account name.*/
/*Retrieve domain of the target server. This would be used to for service account domain and environment prefix in the service account name.*/
DECLARE @DefaultDomain NVARCHAR(50)=DEFAULT_DOMAIN();
DECLARE @CFNEnvironment NVARCHAR(50)=(SELECT CAST(dbo.cfn_ExtendedPropertyGet('SecurityEnvironment') AS VARCHAR(50)));
DECLARE @EnvironmentPrefix CHAR(2)=CASE WHEN @DefaultDomain='WALTHAM' THEN CASE WHEN @CFNEnvironment='DEVELOPMENT' THEN 'DV' ELSE LEFT(@CFNEnvironment,2) END
	ELSE CASE WHEN RIGHT(@DefaultDomain,3)='DEV' THEN 'DV' ELSE LEFT(RIGHT(@DefaultDomain,3),2) END END;

/***********************************************************************
Check for Database existence
***********************************************************************/
IF NOT EXISTS (SELECT * FROM sys.databases d WHERE d.name=@DbName) OR @DbName IS NULL OR @DbName=''
BEGIN
	THROW 51000, 'Database doesn''t exist on the server or NULL value is passed for @DbName.', 1; 
END;
/***********************************************************************
Check for table #RoleMemebrship as that is being use by stored procedure
***********************************************************************/
IF OBJECT_ID('tempdb.dbo.#RoleMemebrship') IS NULL
BEGIN
	THROW 51000, 'Temp table #RoleMemebrship is missing. Create and populate temp table first as that is being use in stored procedure. 
	Table definition: 
	CREATE TABLE #RoleMemebrship (Id INT IDENTITY(1, 1),UserName sysname NOT NULL,type CHAR(1) NOT NULL
	   ,RoleName sysname NOT NULL,CHECK  (type IN (''U'',''S'',''G'')),PRIMARY KEY CLUSTERED (type,UserName,RoleName));', 1; 
END;
/**********************************
Adding below code just to resolve TFS project error.
**********************************/
IF 1=0
BEGIN
	CREATE TABLE #RoleMemebrship(Id INT IDENTITY(1, 1),UserName sysname NOT NULL,type CHAR(1) NOT NULL
		,RoleName sysname NOT NULL,CHECK  (type IN ('U','S','G')),PRIMARY KEY CLUSTERED (type,UserName,RoleName));
END;
/***********************************************************/
IF OBJECT_ID('tempdb.dbo.#FinalRoleMemebrship') IS NOT NULL
	DROP TABLE #FinalRoleMemebrship;

CREATE TABLE #FinalRoleMemebrship
(
    Id INT IDENTITY(1, 1)
   ,UserName sysname NOT NULL
   ,type CHAR(1) NOT NULL
   ,RoleName sysname NOT NULL
   ,UsernameOnTarget VARCHAR(128)
   ,CHECK  (type IN ('U','S','G'))
   ,Script NVARCHAR(MAX)
   ,PRIMARY KEY CLUSTERED (type,UserName,RoleName)
);

INSERT INTO #FinalRoleMemebrship (UserName, type, RoleName, UsernameOnTarget)
SELECT rm.UserName
      ,rm.type
      ,rm.RoleName
      ,CASE WHEN rm.type = 'S' THEN rm.UserName
			WHEN rm.type IN ( 'U', 'G' )
				AND rm.UserName NOT LIKE 'advisor360corp\pr%' THEN
				@DefaultDomain + '\' + CASE WHEN rm.UserName NOT LIKE '%gmsa$' AND rm.type = 'U' THEN @EnvironmentPrefix ELSE '' END + rm.UserName ELSE rm.UserName END
FROM #RoleMemebrship rm;

/***********************************************
-- PREPARE RUNNABLE SCRIPT. (Below script can be added as derived column but just adding as seperate statement for simplicity.) 
-- Script would check first if database user exist on the server or not for the service account. If database user doesn't exist, script will 
   create the database user and print the service account name for log\review purpose.
-- To Add database user to the role, script would check existenace of database user, database role and role membership combination and then would apply the script.
***************************************************/
UPDATE rm
	SET rm.Script='
	/*************************START**************************/
	USE '+QUOTENAME(@DbName)+';
	/*---------------------Check if server login exist-----------------------------*/
	IF NOT EXISTS (SELECT * FROM sys.server_principals sp WHERE sp.name='''+rm.UsernameOnTarget+''') 
		BEGIN
			IF 1 = '+CAST(@CreateLoginNotInTarget AS CHAR(1))+' AND '''+rm.type+''' <> ''s''
				BEGIN
					PRINT ''Creating server login:'+rm.UsernameOnTarget+''';
					'+ CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'CREATE LOGIN [' + rm.UsernameOnTarget + '] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english];
				END
			ELSE
				BEGIN
					PRINT ''Server login missing for the account:'+rm.UsernameOnTarget+''';
				END
		END;
	/*--------------------Check for existance of database role----------------------*/
	IF NOT EXISTS (SELECT * FROM sys.database_principals dp WHERE dp.name='''+rm.RoleName+''') 
		BEGIN
			PRINT ''Database Role is missing:'+rm.RoleName+''';
		END;
	/*-------------------Create database user----------------------------------------*/
	IF EXISTS (SELECT * FROM sys.server_principals sp WHERE sp.name='''+rm.UsernameOnTarget+''') 
		AND NOT EXISTS (SELECT * FROM sys.database_principals sp WHERE sp.name='''+rm.UsernameOnTarget+''')
		BEGIN
			PRINT ''Creating Database user:'+rm.UsernameOnTarget+''';
			' + CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'CREATE USER ['+rm.UsernameOnTarget+'] FOR LOGIN ['+rm.UsernameOnTarget+'];
		END;
	/*------------------Add database user to database role------------------------------*/
	IF EXISTS (SELECT * FROM sys.database_principals dp WHERE dp.name='''+rm.RoleName+''') 
		AND EXISTS (SELECT * FROM sys.database_principals sp WHERE sp.name='''+rm.UsernameOnTarget+''')
		AND NOT EXISTS (SELECT * FROM sys.database_principals users
						JOIN sys.database_role_members link ON link.member_principal_id = users.principal_id
						JOIN sys.database_principals roles ON roles.principal_id = link.role_principal_id
						WHERE users.name='''+rm.UsernameOnTarget+''' AND roles.name='''+rm.RoleName+''')
		BEGIN
			PRINT ''Adding Database user "'+rm.UsernameOnTarget+'" to the database role "'+rm.RoleName+'"'';
			' + CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'ALTER ROLE ['+rm.RoleName+'] ADD MEMBER ['+rm.UsernameOnTarget+'];
		END;
   /***********************END********************/'
FROM #FinalRoleMemebrship rm;

/**************************************************
Apply Rolemembership
*************************************************/

DECLARE @ID INT=1
	,@total INT =(SELECT MAX(l.Id) FROM #FinalRoleMemebrship  l)
	,@Sql NVARCHAR(MAX)='';

WHILE @ID <=@total
BEGIN
	SET @Sql=(SELECT l.Script FROM #FinalRoleMemebrship l WHERE l.Id=@ID);

	BEGIN TRY
		IF @Print_Command_Only=1
			BEGIN
				PRINT @Sql;
			END;
		ELSE 
			BEGIN 
				EXEC sys.sp_executesql @stmt=@Sql;
			END;
	END TRY
	BEGIN CATCH
		PRINT '--Script Failed:';
		PRINT @Sql;
		THROW;
	END CATCH;

	SET @ID=@ID+1;
END;


/**************************************************
Remove RoleMembership if not in source
*************************************************/
--/*
IF OBJECT_ID('tempdb.dbo.#CurrentRoleMemebrship') IS NOT NULL
	DROP TABLE #CurrentRoleMemebrship;

CREATE TABLE #CurrentRoleMemebrship
(
    Id INT IDENTITY(1,1)
   ,UserName VARCHAR(128) NOT NULL
   ,type CHAR(1) NOT NULL
   ,RoleName VARCHAR(128) NOT NULL
);

/*Get the current role member from the database.*/
SET @Sql='USE '+QUOTENAME(@DbName)+';SELECT users.name AS username, users.type, roles.name AS rolename
	FROM sys.database_principals users
		JOIN sys.database_role_members link ON link.member_principal_id = users.principal_id
		JOIN sys.database_principals roles ON roles.principal_id = link.role_principal_id
	WHERE users.name NOT LIKE ''advisor360corp\%''
		  AND users.name <> ''dbo''
		  AND users.name NOT LIKE ''NT %'';';

INSERT INTO #CurrentRoleMemebrship (UserName, type, RoleName)
EXEC sys.sp_executesql @stmt=@Sql;

IF OBJECT_ID('tempdb.dbo.#DropRoleMemebrship') IS NOT NULL
	DROP TABLE #DropRoleMemebrship;

CREATE TABLE #DropRoleMemebrship
(
    Id INT IDENTITY(1, 1)
   ,UserName VARCHAR(128) NOT NULL
   ,type CHAR(1) NOT NULL
   ,RoleName VARCHAR(128) NOT NULL
   ,Script_DropUserFromRole VARCHAR(1000) NULL
   ,Script_DropUser VARCHAR(1000) NULL

);

INSERT INTO #DropRoleMemebrship (UserName, type, RoleName)
SELECT crm.UserName,crm.type,crm.RoleName
FROM #CurrentRoleMemebrship crm 
LEFT JOIN #FinalRoleMemebrship rm ON crm.UserName = rm.UsernameOnTarget
	AND crm.RoleName = rm.RoleName 
	AND crm.type = rm.type
WHERE rm.UserName IS NULL;

--SELECT *
UPDATE drm
	SET drm.Script_DropUserFromRole ='USE '+QUOTENAME(@DbName)+'; IF EXISTS (SELECT * FROM sys.database_principals users
						JOIN sys.database_role_members link ON link.member_principal_id = users.principal_id
						JOIN sys.database_principals roles ON roles.principal_id = link.role_principal_id
						WHERE users.name='''+drm.UserName+''' AND roles.name='''+drm.RoleName+''')
						BEGIN
							PRINT ''Drop user "'+QUOTENAME(UserName)+'" from role "'+QUOTENAME(RoleName)+'"'';
							'+ CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'ALTER ROLE ' +QUOTENAME(drm.RoleName) +' DROP MEMBER ' +QUOTENAME(drm.UserName)+';
						END'
		,drm.Script_DropUser = 'USE '+QUOTENAME(@DbName)+'; IF NOT EXISTS (SELECT * FROM sys.database_principals users
						JOIN sys.database_role_members link ON link.member_principal_id = users.principal_id
						JOIN sys.database_principals roles ON roles.principal_id = link.role_principal_id
						WHERE users.name='''+drm.UserName+''') AND EXISTS(SELECT * FROM sys.database_principals users WHERE users.name='''+drm.UserName+''')
						BEGIN
							PRINT ''Drop user "'+QUOTENAME(drm.UserName)+''';
							'+ CASE WHEN @Debug=0 THEN '' ELSE '--' END + 'DROP USER ' + QUOTENAME(drm.UserName)+';
						END'
FROM #DropRoleMemebrship drm;

/*****************************************************************
Remove user for db role if it is not part of source control table list.
*****************************************************************/
IF @DropRoleMembershipNotInSource = 1
BEGIN
	SET @ID=1;
	SET @total = (SELECT MAX(drm.Id) FROM #DropRoleMemebrship drm);

	WHILE @ID <=@total
	BEGIN
		SET @Sql=(SELECT l.Script_DropUserFromRole FROM #DropRoleMemebrship l WHERE l.Id=@ID);

		BEGIN TRY
			IF @Print_Command_Only=1
				BEGIN
					PRINT @Sql;
				END;
			ELSE 
				BEGIN 
					EXEC sys.sp_executesql @stmt=@Sql;
				END;
		END TRY
		BEGIN CATCH
			PRINT '--Script Failed:';
			PRINT @Sql;
			THROW;
		END CATCH;

		SET @ID=@ID+1;
	END;
END;

/************************************************************
Drop users if user is not a member of any role in a database.
*************************************************************/
IF @DropUserNotInSource = 1
BEGIN

	SET @ID=1;
	SET @total = (SELECT MAX(drm.Id) FROM #DropRoleMemebrship drm);

	WHILE @ID <=@total
	BEGIN
		SET @Sql=(SELECT l.Script_DropUser FROM #DropRoleMemebrship l WHERE l.Id=@ID);

		BEGIN TRY
			IF @Print_Command_Only=1
				BEGIN
					PRINT @Sql;
				END;
			ELSE 
				BEGIN 
					EXEC sys.sp_executesql @stmt=@Sql;
				END;
		END TRY
		BEGIN CATCH
			PRINT '--Script Failed:';
			PRINT @Sql;
			THROW;
		END CATCH;

		SET @ID=@ID+1;
	END;
END;