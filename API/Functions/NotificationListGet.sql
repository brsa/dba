﻿CREATE FUNCTION [API].[NotificationListGet] (@NotificationListName varchar(500))
	RETURNS VARCHAR(8000)
AS
BEGIN
	RETURN(
		SELECT EmailList
		FROM dbo.NotificationList
		WHERE NotificationListName = @NotificationListName
	)
END;