﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
-- Insert data into dbo.CFNRoles
:r .\Scripts\Data.CFNRoles.sql
:r .\Scripts\ObjectExceptions.sql
:r .\Scripts\Data.EmailDistributionListForSqlAlert.sql
:r .\Scripts\Data.NotificationList.sql
:r RoleMemberships.sql

--make sure DBA is owned by sa
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND owner_sid <> 0x01)
       ALTER AUTHORIZATION ON DATABASE::DBA TO sa;

--/*
--Make sure DBA is in SIMPLE mode
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND recovery_model_desc <> 'SIMPLE')
       ALTER DATABASE [DBA] SET RECOVERY SIMPLE;
--*/
--Make sure snapshot isolation is allowed
IF EXISTS (SELECT 1 FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND snapshot_isolation_state = 0)
	ALTER DATABASE [DBA] SET ALLOW_SNAPSHOT_ISOLATION ON;

--Make sure isolation is Read-Committed with row versioning 
IF EXISTS (SELECT 1 FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND is_read_committed_snapshot_on = 0)
	ALTER DATABASE [DBA] SET READ_COMMITTED_SNAPSHOT ON;

--make sure DBA has checksum page verification
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND page_verify_option_desc <> 'CHECKSUM')
       ALTER DATABASE [DBA] SET PAGE_VERIFY CHECKSUM;

--make sure DBA has service broker enabled
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND is_broker_enabled = 0)
       ALTER DATABASE [DBA] SET ENABLE_BROKER;

--make sure DBA is trustworthy
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'DBA' AND state_desc = 'ONLINE' AND is_trustworthy_on = 0)
       ALTER DATABASE [DBA] SET TRUSTWORTHY ON;


IF DBO.cfn_ExtendedPropertyGet('SecurityEnvironment')='Development'
BEGIN
	PRINT N'Grant modify access on table [dbo].[NotificationList] to [Access Development]...';
	GRANT UPDATE, DELETE, INSERT ON [dbo].[NotificationList] TO [Access Development];	
END;
GO
